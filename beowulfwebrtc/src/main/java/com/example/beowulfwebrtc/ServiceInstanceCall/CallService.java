package com.example.beowulfwebrtc.ServiceInstanceCall;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.os.Build;
import android.os.IBinder;

import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;

import com.example.beowulfwebrtc.BuildConfig;
import com.example.beowulfwebrtc.R;

public class CallService extends Service{
    public static String CHANNEL_ID = "";
    public static CallService getInstance() {
        return instance;
    }

    private static CallService instance = null;

    public static boolean isInstance() {
        return null != instance;
    }

    @Override
    public void onCreate() {
        instance = this;

        CHANNEL_ID = "";
        CHANNEL_ID += BuildConfig.APPLICATION_ID + this.getClass().getSimpleName();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            InitForegroundService();
        } else {
            startForeground(1, new Notification());
        }



        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }



    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, startId, startId);
        return START_STICKY;
    }


    public static boolean isReady() {
        return instance != null;
    }



    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    public void InitForegroundService() {

        NotificationCompat.Builder notificationBuilder;
        notificationBuilder = new NotificationCompat.Builder(this, CHANNEL_ID);
        NotificationChannel notificationChannel = new NotificationChannel(CHANNEL_ID, "BEOWULF", NotificationManager.IMPORTANCE_DEFAULT);
        notificationChannel.enableVibration(true);

        ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE)).createNotificationChannel(notificationChannel);
        notificationBuilder
                .setContentText("Call is processing")
                .setAutoCancel(true)
                .setStyle(new NotificationCompat.DecoratedCustomViewStyle())
                .setColor(Color.RED)
                .setSmallIcon(R.drawable.phone_pickup_audio);
        //notificationBuilder.setDefaults(DEFAULT_VIBRATE);
        notificationBuilder.setLights(Color.YELLOW, 1000, 300);
        Notification notification = notificationBuilder.build();
        startForeground(1, notification);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

}