package com.example.beowulfwebrtc.ConnectorInterface;

public class CallTrigger
{
    interface XMPPNotification
    {
        public void onLoginSucess();
        public void onLoginFail();
        public void onDisconnected();
        public void onError(String error);
    }


    public static void notifyXMPPLoginSuccess()
    {

    }

    public static void notifyXMPPLoginFail()
    {

    }

    public static void notifyXMPPDisconnected()
    {

    }

    public static void notifyXMPPError(String error)
    {

    }
}
