package com.example.beowulfwebrtc.CallCenterManager;

import java.util.ArrayList;
import java.util.concurrent.Semaphore;

public class CallCenterInstance {

    CallCenterInstance instance = null;
    private static Object mutex = new Object();

    private static Semaphore outGoingCallSemaphore = new Semaphore(1);

    public CallCenterInstance getInstance() {
        CallCenterInstance result = instance;
        if (null == result) {
            synchronized (mutex) {
                result = instance;
                if (null == result) {
                    instance = new CallCenterInstance();
                    result = instance;
                }
            }
        }
        return result;
    }

    static ArrayList<CallCenter> listeners = new ArrayList();

    public interface CallCenter {

        public void onCallCenterConnected();

        public void onCallCenterDisconnected();

        public void onCallCancel();

        public void onCallIncomming();

        public void onCallError(String error);

        public void onChannelMessage(String message);
    }

    public static void addListener(CallCenter listener) {
        listeners.add(listener);
    }

    public static void removeListener(CallCenter listener) {
        listeners.remove(listener);
    }

    public static void notifyOnLogin() {
        for (CallCenter callcenter : listeners) {
            callcenter.onCallCenterConnected();
        }
    }

    public static void notifyOnLogout() {
        for (CallCenter callcenter : listeners) {
            callcenter.onCallCenterDisconnected();
        }
    }

    public static void nofityOnCallCanceled() {
        for (CallCenter callcenter : listeners) {
            callcenter.onCallCancel();
        }
    }

    public static void nofityOnError(String error) {
        for (CallCenter callcenter : listeners) {
            callcenter.onCallError(error);
        }
    }

    public static void notifyOnMessage(String message)

    {
        for (CallCenter callcenter : listeners) {
            callcenter.onChannelMessage(message);
        }
    }

    public static void nofityOnIncommingCall() {
        if (outGoingCallSemaphore.tryAcquire()) {
            for (CallCenter callcenter : listeners) {
                callcenter.onCallIncomming();
            }
        }
    }

    public static void AutoAcceptIncommingRequest() {

    }

    public static void releaseOutGoingCall() {
        outGoingCallSemaphore.release();
    }

    public static void Accept()
    {

    }

}
