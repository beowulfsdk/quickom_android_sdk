package com.example.beowulfwebrtc.WebSocket;

import android.os.Handler;
import android.util.Log;

import java.net.URI;
import java.net.URISyntaxException;

import com.example.beowulfwebrtc.Interface.ChannelMessage;

import de.tavendo.autobahn.WebSocket;
import de.tavendo.autobahn.WebSocketConnection;
import de.tavendo.autobahn.WebSocketException;
import de.tavendo.autobahn.WebSocketOptions;

public class WebSocketChannel implements ChannelMessage {

    private final String TAG = "WebSocketChannelClient";

    WebSocketConnection webSocketConnection;
    WsObserver wsObserver = null;
    WebSocketOptions webSocketOptions = new WebSocketOptions();

    ChannelMessage.ChannelEvents listent = null;
    long CLOSE_TIMEOUT = 1000;
    Boolean closeEvent = false;
    Handler handler = null;
    ChannelEvents channelEvents = null;

    public ConnectionState getState() {
        return state;
    }

    ConnectionState state = ConnectionState.NEW;

    Object closeEventLock = new Object();

    public enum ConnectionState {
        NEW,
        CONNECTED,
        CLOSED,
        ERROR
    }

    public WebSocketChannel(Handler handler, ChannelEvents channelEvents) {
        this.handler = handler;
        this.channelEvents = channelEvents;
        webSocketOptions.setReconnectInterval(5000);
    }

    public void RemoveChanelEvents()
    {
        this.channelEvents=null;
    }

    @Override
    public void connect(String id, boolean useReconnectOption) {

        if (state != ConnectionState.NEW && state != ConnectionState.CLOSED) {
            Log.e(TAG, "WebSocket is already connected.");
            return;
        }
        closeEvent = false;
        webSocketConnection = new WebSocketConnection();
        wsObserver = new WsObserver();
        try {
            if (true == useReconnectOption)
                webSocketConnection.connect(new URI(id), wsObserver, webSocketOptions);
            else
                webSocketConnection.connect(new URI(id), wsObserver);
        } catch (WebSocketException e) {
            reportError("URI error: " + e.toString());
        } catch (URISyntaxException e) {
            reportError("WebSocket connection error: " + e.toString());
        }
    }

    @Override
    public void disconnect(Boolean waitForComplete) {
       // Log.d(TAG, "Disconnect WebSocket. State: " + state);
        // Close WebSocket in CONNECTED or ERROR states only.
        if (state == ConnectionState.CONNECTED || state == ConnectionState.ERROR) {
            webSocketConnection.disconnect();
            state = ConnectionState.CLOSED;

            // Wait for websocket close event to prevent websocket library from
            // sending any pending messages to deleted looper thread.
            if (waitForComplete) {
                synchronized (closeEventLock) {
                    while (!closeEvent) {
                        try {
                            closeEventLock.wait(CLOSE_TIMEOUT);
                            break;
                        } catch (InterruptedException e) {
                            Log.e(TAG, "Wait error: " + e);
                        }

                    }
                }
            }
        //    Log.d(TAG, "Disonnecting WebSocket done.");
        }
    }

    @Override
    public void sendMessage(String message) {

       // Log.d(TAG, "Send message s: $message, state: $state");


        if (ConnectionState.NEW == state) {

        }

        if (ConnectionState.CONNECTED == state) {
            webSocketConnection.sendTextMessage(message);
        }

        if (ConnectionState.CLOSED == state
                || ConnectionState.ERROR == state) {
            Log.e(TAG, "WebSocket send() in error or closed state : " + message);
        }

    }

    @Override
    public void reportError(String error) {
        Log.e(TAG, error);
        handler.post(new Runnable() {
            @Override
            public void run() {
            //    Log.d(TAG, "WebSocket error" + error + "State" + state);
                if (state != ConnectionState.ERROR) {
                    state = ConnectionState.ERROR;
                    channelEvents.onChannelError(error);
                }
            }
        });
    }

    @Override
    public boolean isConnected() {
        return state == ConnectionState.CONNECTED;
    }

    @Override
    public void setListener(ChannelEvents listener) {
        this.listent = listener;
    }

//    private void checkIfCalledOnValidThread() {
//        if (Thread.currentThread() != handler.getLooper().getThread()) {
//            IllegalStateException e = new IllegalStateException("WebSocket method is not called on valid thread");
//            Log.e(TAG, "WebSocket method is not called on valid thread", e);
//            throw e;
//        }
//    }

    public void disconnect() {
        if (null != webSocketConnection)
            webSocketConnection.disconnect();
    }

    private class WsObserver implements WebSocket.WebSocketConnectionObserver {

        @Override
        public void onOpen() {

            handler.post(new Runnable() {
                @Override
                public void run() {
                 //   Log.d(TAG, "WebSocket connection opened");
                    state = ConnectionState.CONNECTED;
                    channelEvents.onChannelOpen();
                }
            });

        }

        @Override
        public void onClose(WebSocketCloseNotification webSocketCloseNotification, String reason) {
          //  Log.d(TAG, "WebSocket connection closed. Code: " + webSocketCloseNotification + ". Reason: " + reason + ". State: " + state);
            synchronized (closeEventLock) {
                closeEvent = true;
                closeEventLock.notify();
            }
            handler.post(new Runnable() {
                @Override
                public void run() {
                    if (state != ConnectionState.CLOSED) {
                        state = ConnectionState.CLOSED;
                        channelEvents.onChannelClosed("WebSocket connection closed. Code: " + webSocketCloseNotification + ". Reason: " + reason + ". State: " + state);
                    }
                }
            });

        }

        @Override
        public void onTextMessage(String payload) {
          //  Log.d(TAG, "WebSocket receive message: " + payload);
            handler.post(new Runnable() {
                @Override
                public void run() {
                    if (state == ConnectionState.CONNECTED)
                        channelEvents.onChannelMessage(payload);
                }
            });


        }

        @Override
        public void onRawTextMessage(byte[] bytes) {

        }

        @Override
        public void onBinaryMessage(byte[] bytes) {

        }
    }
}
