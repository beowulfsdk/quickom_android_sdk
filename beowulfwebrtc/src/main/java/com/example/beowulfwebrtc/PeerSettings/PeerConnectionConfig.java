package com.example.beowulfwebrtc.PeerSettings;

import com.example.beowulfwebrtc.Signal.CallingSignal;

import org.webrtc.PeerConnection;

import java.util.ArrayList;
import java.util.List;

public class PeerConnectionConfig {

    static boolean isOnProduction = true;
    public static final int videoWidth = 640;
    public static final int videoHeight = 480;
    public static final int videoFps = 30;
    public static final int videoMaxBitRate = 1024;
    public static final int audioMaxBitRate = 64;

    public static final String videoCodec = "VP8";
    public static final String audioCodec = "OPUS";
    public static boolean hwDecodeEnable = true;
    public static boolean tracing = false;

    public final static List<PeerConnection.IceServer> iceServers = new ArrayList<PeerConnection.IceServer>() {{
        add(PeerConnection.IceServer.builder("turn:34.92.91.187:3478")
                .setUsername("username")
                .setPassword("password")
                .createIceServer());
        add(PeerConnection.IceServer.builder("turn:35.234.51.4:3478")
                .setUsername("username")
                .setPassword("password")
                .createIceServer());

        add(PeerConnection.IceServer.builder("turn:35.220.187.252:3478")
                .setUsername("username")
                .setPassword("password")
                .createIceServer());

        add(PeerConnection.IceServer.builder("stun:35.234.51.4")
                .createIceServer());
        add(PeerConnection.IceServer.builder("stun:35.220.187.252")
                .createIceServer());
    }};


    public static final String callingEndPoint = isOnProduction ? "wss://signaler-v2.beowulfchain.com" : "wss://signal-test.dechen.app";
    public static final String waitCallEndPoint = isOnProduction ? "wss://dispatcher-v2.beowulfchain.com" : "wss://call-center.beowulfchain.com";
    public static final String outgoingCallEndPoint =  isOnProduction ? "wss://dispatcher-v2.beowulfchain.com" : "wss://call-center.beowulfchain.com";




//    public static final String callingEndPoint = isOnProduction ? "wss://signaler-v2.beowulfchain.com" : "wss://signal-test.dechen.app";
//
//    public static final String waitCallEndPoint = "ws://dispatcher.drivor.vn";
//
//    public static final String outgoingCallEndPoint = "ws://dispatcher.drivor.vn";
    // testing for drivo

}
