package com.example.beowulfwebrtc.SDKProtocol;

import java.util.ArrayList;

public class BWF_CMM_MessageDef
{
    final static ArrayList<String> messageAliases = new ArrayList<String>() {{
        add("helpRequest");
        add("helpResponse");
        add("helpDrop");
    }};
    final static ArrayList<String> messageTypes = new ArrayList<String>() {{
        add("notification");
        add("drop");
        add("incoming");
    }};

    final static ArrayList<String>dropMessageTypes = new ArrayList<String>() {{
        add("deny");
        add("canceled");
        add("timeout");
    }};


    final static ArrayList<String>pushMessageTypes = new ArrayList<String>() {{
        add("incoming");
        add("canceled");
        add("timeout");
    }};



    public enum enum_message_alias {
        helpRequest(0),
        helpResponse(1),
        helpDrop(2);
        int val = 0;
        enum_message_alias(int val) {
            this.val = val;
        }
        public String getString() {
            return messageAliases.get(val);
        }
    }

    public enum enum_message_type {
        notification(0),
        drop(1),
        incoming(2);
        int val = 0;
        enum_message_type(int val) {
            this.val = val;
        }
        public String getString() {
            return messageTypes.get(val);
        }
    }
    public enum enum_deny_type {
        deny(0),
        canceled(1),
        timeout(2);
        int val = 0;
        enum_deny_type(int val) {
            this.val = val;
        }
        public String getString() {
            return dropMessageTypes.get(val);
        }
    }



    public enum bwf_cmm_push_content_type {
        BWF_CMM_PushContentTypeOutgoingCall(1), //Outgoing call
        BWF_CMM_PushContentTypeCancelOutgoingCallWithoutConnected(2),//Cancel the outgoing call which isn't connected
        BWF_CMM_PushContentTypeTimeoutOutgoingCallWithoutConnected(3);//The outgoing call is timeout before connected

        public int getVal() {
            return val;
        }

        int val = 0;
        bwf_cmm_push_content_type(int val) {
            this.val = val;
        }
    }

    public enum enumbwf_cmm_call_state
    {
        Outgoing(0),
        Connected(1),
        End(2),
        Incoming(3);

        public int getVal() {
        return val;
    }

        int val = 0;
        enumbwf_cmm_call_state(int val) {
        this.val = val;
    }

    }

}
