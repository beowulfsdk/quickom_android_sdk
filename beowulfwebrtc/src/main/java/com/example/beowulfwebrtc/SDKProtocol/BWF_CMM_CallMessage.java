package com.example.beowulfwebrtc.SDKProtocol;

import org.json.JSONObject;

import java.util.ArrayList;

public abstract class BWF_CMM_CallMessage {

    JSONObject data=new JSONObject();
    JSONObject fullMessage=new JSONObject();
    JSONObject pushContent=new JSONObject();

    String __receiverId="";
    String __senderId="";

    String requestId="";
    String msgId="";
    String name="";
    String type="";

    public abstract JSONObject getJSONObjectFullMessage();
    public abstract JSONObject getJSONObjectPushContent(String _displayLabel,String _userRefId );
    public abstract JSONObject getJSONObjectData();


}
