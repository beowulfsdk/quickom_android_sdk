package com.example.beowulfwebrtc.SDKProtocol;

public class BWF_CMM_Error {
    int code;

    public String getDescription() {
        return description;
    }

    public String description = "";
    Object extraObject;

    public BWF_CMM_Error(int code, String description) {
        this.code = code;
        this.description = description;
    }

    BWF_CMM_Error(int code, String description, Object extraObject) {
        this.code = code;
        this.description = description;
        this.extraObject = extraObject;
    }


}
