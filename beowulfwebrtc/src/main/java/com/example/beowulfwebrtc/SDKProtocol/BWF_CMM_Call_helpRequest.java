package com.example.beowulfwebrtc.SDKProtocol;

import android.util.Log;

import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;


import java.util.UUID;

public class BWF_CMM_Call_helpRequest extends BWF_CMM_CallMessage {


    String identifier = "";
    String requestTime="";
    String userChatId="";
    String video = "";
    BWF_CMM_Call_helpResponse helpResponse;
    BWF_CMM_Call_helpDrop helpDrop;
    BWF_CMM_Call_helpRequest_push_content push_content;

    String displayLabel = "";

    public BWF_CMM_Call_helpRequest(String _senderId, String _receiverId) {

        __senderId = _senderId;
        __receiverId = _receiverId;

        // this is me send someone a request
        requestId = UUID.randomUUID().toString();
        requestTime = "" + (System.currentTimeMillis() / 1000);
        userChatId = _senderId;
        video = "0";


        msgId = userChatId + "_" + requestTime;
        name = BWF_CMM_MessageDef.enum_message_alias.helpRequest.getString();
        type = BWF_CMM_MessageDef.enum_message_type.notification.getString();

        this.getJSONObjectData();

    }

    public String getRequestId() {
        return requestId;
    }


    public BWF_CMM_Call_helpRequest(JSONObject object, String _receiverId) {
// this is from someone send me a helpRequest with xmpp;
        try {
            name = object.getString("name");
            type = object.getString("type");
            msgId = object.getString("msgId");
            data = object.getJSONObject("data");
            requestId = data.getString("requestId");
            requestTime = data.getString("requestTime");
            userChatId = data.getString("userChatId");
            video = data.getString("video");


            __senderId = userChatId;
            __receiverId = _receiverId;

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    public String getDisplayLabel() {
        return displayLabel;
    }

    public BWF_CMM_Call_helpRequest(JSONObject object) {
// this is from someone send me a helpRequest with PUSHING notification;
        try {
            type = "incomming";
            displayLabel = object.getString("displayLabel");
            requestId = object.getString("requestId");
            requestTime = object.getString("requestTime");
            identifier = object.getString("userRefId");
            video = object.getString("video");

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    BWF_CMM_Call_helpRequest() {
        //default constractor for pushcontent object
    }

    @Override
    public JSONObject getJSONObjectFullMessage() {

        try {
            fullMessage.put("data", data);
            fullMessage.put("msgId", msgId);
            fullMessage.put("name", name);
            fullMessage.put("type", type);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        return fullMessage;
    }

    @Override
    public JSONObject getJSONObjectPushContent(String _displayLabel, String _userRefId) {
        push_content = new BWF_CMM_Call_helpRequest_push_content(__senderId, __receiverId, _displayLabel, _userRefId,this.requestId);
        return push_content.formPushMessage();
    }

    @Override
    public JSONObject getJSONObjectData() {
        try {
            data.put("requestId", requestId);
            data.put("requestTime", requestTime);
            data.put("userChatId", userChatId);
            data.put("video", video);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return data;
    }


    public String getUserChatId() {

        return userChatId.matches("")?__senderId:userChatId;
    }

    public JSONObject getHelpResponse(String myChatId) {
        helpResponse = new BWF_CMM_Call_helpResponse(myChatId);
        return helpResponse.getJSONObjectFullMessage();

    }

    public JSONObject getHelpDenyIncommingCall(String myChatId) {
        helpDrop = new BWF_CMM_Call_helpDrop(myChatId, requestId, msgId, BWF_CMM_MessageDef.enum_deny_type.deny.getString());
        return helpDrop.getJSONObjectFullMessage();
    }

    public JSONObject getHelpCancelOutGoingCall(String myChatId) {
        helpDrop = new BWF_CMM_Call_helpDrop(myChatId, requestId, msgId, BWF_CMM_MessageDef.enum_deny_type.canceled.getString());


        JSONObject pushDrop = helpDrop.getJSONObjectPushContent("", "");
        Log.d("DROPxxx", pushDrop.toString());

        return helpDrop.getJSONObjectFullMessage();
    }

    public JSONObject getHelpTimeoutOutGoingCall(String myChatId) {
        helpDrop = new BWF_CMM_Call_helpDrop(myChatId, requestId, msgId, BWF_CMM_MessageDef.enum_deny_type.timeout.getString());
        return helpDrop.getJSONObjectFullMessage();
    }


    public String getReciverId() {
        return __receiverId;
    }

    public String getSenderId() {
        return __senderId;
    }


    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getIdentifier() {
        return identifier;
    }


    public JSONObject formPushMessage() {

        // this method will implement on pushcontent object
        return null;
    }

    public JSONObject getPushDropMessage() {
        return helpDrop.getJSONObjectPushContent("", "");
    }

    public class BWF_CMM_Call_helpRequest_push_content extends BWF_CMM_Call_helpRequest {


        String displayLabel;
        String userRefId = "";

        BWF_CMM_Call_helpRequest_push_content(String _senderId, String _receiverId, String _displayLabel, String _userRefId,String _requestId) {
            super(_senderId, _receiverId);
            this.requestId=_requestId;
            this.displayLabel = _displayLabel;
            this.userRefId = _userRefId;
            this.requestTime = "" + (System.currentTimeMillis() / 1000);
            this.type = BWF_CMM_MessageDef.enum_message_type.incoming.getString();

        }

        @Override
        public JSONObject formPushMessage() {
            try {
                this.pushContent.put("displayLabel", this.displayLabel);
                this.pushContent.put("requestId", this.requestId);
                this.pushContent.put("requestTime", this.requestTime);
                this.pushContent.put("type", this.type);
                this.pushContent.put("userRefId", this.userRefId);
                this.pushContent.put("video", this.video);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return this.pushContent;
        }

        BWF_CMM_Call_helpRequest_push_content(JSONObject object) throws JSONException {
            super();
            if (!object.isNull("displayLabel"))
                this.displayLabel = object.getString("displayLabel");
            if (!object.isNull("requestId"))
                this.requestId = object.getString("requestId");
            if (!object.isNull("requestTime"))
                this.requestTime = object.getString("requestTime");
            if (!object.isNull("type"))
                this.type = object.getString("type");
            if (!object.isNull("userRefId"))
                this.userRefId = object.getString("userRefId");
            if (!object.isNull("video"))
                this.video = object.getString("video");
        }


    }

}
