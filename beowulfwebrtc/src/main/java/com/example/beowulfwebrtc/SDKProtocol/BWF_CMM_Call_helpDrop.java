package com.example.beowulfwebrtc.SDKProtocol;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.UUID;

public class BWF_CMM_Call_helpDrop extends BWF_CMM_CallMessage {
    String dropRequestId;

    public String getReason() {
        return reason;
    }

    String reason = "deny";
    String requestTime;
    String userChatId;

    BWF_CMM_Call_helpDrop_push_content push_content;

    BWF_CMM_Call_helpDrop(String _userChatId, String _requestId, String _msgId, String _reason) {
        reason = _reason;
        requestId = _requestId;
        msgId = _msgId;
        dropRequestId = UUID.randomUUID().toString();
        requestTime = "" + (System.currentTimeMillis() / 1000);
        userChatId = _userChatId;
        name = BWF_CMM_MessageDef.enum_message_alias.helpDrop.getString();
        type = BWF_CMM_MessageDef.enum_message_type.notification.getString();
        this.getJSONObjectData();
    }


    BWF_CMM_Call_helpDrop(String _requestId) {
        requestId = _requestId;
        dropRequestId = UUID.randomUUID().toString();
        type = "drop";
    }

    BWF_CMM_Call_helpDrop() {

    }

    public String getRequestId()
    {
        return requestId;
    }

    public BWF_CMM_Call_helpDrop(JSONObject object) {
        try {
        if (!object.isNull("data")) {

            data = object.getJSONObject("data");
            if (!data.isNull("dropRequestId"))
                dropRequestId = data.getString("dropRequestId");
            if (!data.isNull("reason"))
                reason = data.getString("reason");

            if (!data.isNull("requestId"))
                requestId = data.getString("requestId");

            if (!data.isNull("requestTime"))
                requestTime = data.getString("requestTime");

            if (!data.isNull("userChatId"))
                userChatId = data.getString("userChatId");
        }

            if(!object.isNull("msgId"))
                msgId=object.getString("msgId");
            if(!object.isNull("name"))
                name=object.getString("name");

            if(!object.isNull("type"))
                type=object.getString("type");
        }
        catch (JSONException e) {
            e.printStackTrace();
        }


    }

    @Override
    public JSONObject getJSONObjectFullMessage() {
        try {
            fullMessage.put("data", data);
            fullMessage.put("msgId", msgId);
            fullMessage.put("name", name);
            fullMessage.put("type", type);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return fullMessage;
    }

    @Override
    public JSONObject getJSONObjectPushContent(String _displayLabel, String _userRefId) {
        push_content = new BWF_CMM_Call_helpDrop_push_content(requestId);
        return push_content.formPushMessage();
    }


    @Override
    public JSONObject getJSONObjectData() {
        try {
            data.put("dropRequestId", dropRequestId);
            data.put("reason", reason);
            data.put("requestId", requestId);
            data.put("requestTime", requestTime);
            data.put("userChatId", userChatId);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return data;
    }

    public JSONObject formPushMessage() {
        return null;
    }

    public class BWF_CMM_Call_helpDrop_push_content extends BWF_CMM_Call_helpDrop {

        BWF_CMM_Call_helpDrop_push_content(String _requestId) {
            super(_requestId);
        }

        @Override
        public JSONObject formPushMessage() {
            try {
                this.pushContent.put("dropRequestId", this.dropRequestId);
                this.pushContent.put("requestId", this.requestId);
                this.pushContent.put("type", this.type);

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return this.pushContent;
        }

        BWF_CMM_Call_helpDrop_push_content(JSONObject object) throws JSONException {
            super();

            if (!object.isNull("dropRequestId"))
                this.dropRequestId = object.getString("dropRequestId");
            if (!object.isNull("requestId"))
                this.requestId = object.getString("requestId");
            if (!object.isNull("type"))
                this.type = object.getString("type");


        }


    }
}
