package com.example.beowulfwebrtc.SDKProtocol;

public class BWF_CMM_AccountData {
    public static String referenceId = "";
    public static String partnerReferenceId = "";
    static Object mutext = new Object();

    public static void setHelpRequest(BWF_CMM_Call_helpRequest helpRequest) {

        synchronized (mutext)
        {
            BWF_CMM_AccountData.helpRequest = helpRequest;
        }

    }

    private static BWF_CMM_Call_helpRequest helpRequest = null;

    public static BWF_CMM_Call_helpRequest getHelpRequest() {
        return helpRequest;
    }

}
