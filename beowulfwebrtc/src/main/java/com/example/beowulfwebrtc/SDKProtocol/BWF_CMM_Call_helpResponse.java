package com.example.beowulfwebrtc.SDKProtocol;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.UUID;

public class BWF_CMM_Call_helpResponse extends BWF_CMM_CallMessage
{
    String responseTime;
    String helperChatId;

    BWF_CMM_Call_helpResponse(String userChatId)
    {
        helperChatId=userChatId;
        requestId = UUID.randomUUID().toString();
        responseTime = "" + (System.currentTimeMillis() / 1000);


        msgId = helperChatId + "_" + responseTime;
        name = BWF_CMM_MessageDef.enum_message_alias.helpResponse.getString();
        type = BWF_CMM_MessageDef.enum_message_type.notification.getString();

        this.getJSONObjectData();

    }
    @Override
    public JSONObject getJSONObjectFullMessage() {
        try {
            fullMessage.put("data", data);
            fullMessage.put("msgId", msgId);
            fullMessage.put("name", name);
            fullMessage.put("type", type);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        return fullMessage;
    }

    @Override
    public JSONObject getJSONObjectPushContent(String _displayLabel, String _userRefId) {
        return null;
    }



    @Override
    public JSONObject getJSONObjectData() {
        try {
            data.put("helperChatId", helperChatId);
            data.put("requestId", requestId);
            data.put("responseTime", responseTime);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return data;

    }
}
