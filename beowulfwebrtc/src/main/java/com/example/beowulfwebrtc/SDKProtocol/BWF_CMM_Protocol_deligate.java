package com.example.beowulfwebrtc.SDKProtocol;

import android.app.NotificationManager;
import android.content.Context;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.HandlerThread;
import android.util.Base64;
import android.util.Log;

import com.example.beowulfwebrtc.API.GetGeneralInfo;
import com.example.beowulfwebrtc.AppData.WebRTCCallInfo;
import com.example.beowulfwebrtc.MessageTriggling.XMPP.BeowulfXMPPConfig;
import com.example.beowulfwebrtc.MessageTriggling.XMPP.NewChatConnection;
import com.example.beowulfwebrtc.SDKApplication;
import com.example.beowulfwebrtc.util.Const;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import javax.net.ssl.HttpsURLConnection;

public class BWF_CMM_Protocol_deligate {

    public static final String TAG = "MAILINH";
    static ArrayList<BWF_CMM_Protocol> listener = new ArrayList<>();

    public static void AddListener(BWF_CMM_Protocol protocol) {
        if (!listener.contains(protocol))
            listener.add(protocol);
    }

    public static void RemoveListener(BWF_CMM_Protocol protocol) {
        if (listener.contains(protocol))
            listener.remove(protocol);
    }

    public static void notifiOnError(BWF_CMM_Error error) {
        for (BWF_CMM_Protocol it : listener)
            it.bwf_cmm_failedWithError(error);
    }

    public static void notifiOnStartSucess(String identifier) {
        for (BWF_CMM_Protocol it : listener)
            it.bwf_cmm_didStartWithIdentifier(identifier);

    }


    public static void notifyOnCalling_CallDidChangeToStateOutgoing(boolean isVideo) {
        for (BWF_CMM_Protocol it : listener)
            it.bwf_cmm_Calling_CallDidChangeToStateOutgoing(isVideo);
    }


    public static void notifyOnCallDenied(String fromIdentifier, String reason) {
        for (BWF_CMM_Protocol it : listener)
            it.bwf_cmm_Call_Denied(fromIdentifier, reason);

        BWF_CMM_CallManager.getInstance().EndCallAndNotification();

    }

    public static void notifyOnGeneratePushContent(String data, String identifier, BWF_CMM_MessageDef.bwf_cmm_push_content_type type) {
        byte[] base64byte = String.valueOf(data).getBytes();
        String base64Str = Base64.encodeToString(base64byte, Base64.DEFAULT);

        for (BWF_CMM_Protocol it : listener)
            it.bwf_cmm_generatePushContent(base64Str, identifier, type);
    }

    public static void nofityOnReceivePushContent(String base64data) {

        NewChatConnection.getInstance().InitChatConenction();

        byte[] base64bytes = Base64.decode(base64data, Base64.DEFAULT);
        String base64DecodedStr = new String(base64bytes);

        Log.d("FIREBASE_BODY_STRING", base64DecodedStr);

        try {
            JSONObject jsonObject = new JSONObject(base64DecodedStr);
            if (!jsonObject.isNull("type")) {

                String type = jsonObject.getString("type");

                if (type.matches("incoming")) {

                    String referalId = jsonObject.getString("userRefId");
                    String requestId = jsonObject.getString("requestId");

                    GetGeneralInfo generalInfo = new GetGeneralInfo(new GetGeneralInfo.onResultListener() {
                        @Override
                        public void onResult(int code, String result) {

                            Log.d("GetGeneralInfo","______"+code);
                            if (code == HttpsURLConnection.HTTP_OK) {
                                Log.d("GetGeneralInfo","2000");
                                if (BWF_CMM_CallManager.shoultAcceptMsgRequestOrBlock()) {
                                    BWF_CMM_AccountData.setHelpRequest(new BWF_CMM_Call_helpRequest(jsonObject));


                                    Log.d("GetGeneralInfo",result);

                                    try {
                                        JSONObject object = new JSONObject(result);

                                        if (!object.isNull("chat_id")) {
                                            String chat_id = object.getString("chat_id");


                                            BWF_CMM_AccountData.getHelpRequest().__senderId = chat_id;

                                            WebRTCCallInfo.setMyPartnerCallId(chat_id);
                                            WebRTCCallInfo.setQrAlias(BWF_CMM_AccountData.getHelpRequest().getDisplayLabel());
                                            BWF_CMM_Protocol_deligate.notifyOnIncommingCall("", false);

                                        } else {
                                            BWF_CMM_Protocol_deligate.notifiOnError(new BWF_CMM_Error(11, "can't start framework _ get chat id fail"));

                                        }

                                    } catch (JSONException e) {
                                        BWF_CMM_Protocol_deligate.notifiOnError(new BWF_CMM_Error(11, "can't start framework _ get chat id fail"));
                                        e.printStackTrace();
                                    }

                                }


                            } else
                                BWF_CMM_Protocol_deligate.notifiOnError(new BWF_CMM_Error(11, "can't start framework _ get chat id fail"));


                        }
                    });

                    generalInfo.Execute(referalId, "", "", "");

                }
                if (type.matches("drop")) {
                    if (!jsonObject.isNull("requestId")) {
                        String requestId = jsonObject.getString("requestId");
                        if (null != BWF_CMM_AccountData.getHelpRequest()) {
                            String currentRequestId = BWF_CMM_AccountData.getHelpRequest().getRequestId();
                            if (requestId.matches(currentRequestId))
                                BWF_CMM_Protocol_deligate.notifyOnEndedCall(0, new BWF_CMM_Error(0, "cancel current outgoing call"));
                        }
                    }


                }
                if (type.matches("notification")) {
                    try {

                        if (!jsonObject.isNull("name")) {
                            String name = jsonObject.getString("name");
                            do {

                                String messTo = BeowulfXMPPConfig.getUsername().split("@")[0];
                                if (name.matches(BWF_CMM_MessageDef.enum_message_alias.helpRequest.getString())) {

                                    if (BWF_CMM_CallManager.shoultAcceptMsgRequestOrBlock()) {
                                        BWF_CMM_AccountData.setHelpRequest( new BWF_CMM_Call_helpRequest(jsonObject, messTo));
                                        BWF_CMM_Protocol_deligate.notifyOnIncommingCall("", false);

                                    }
                                    break;
                                }

                                if (name.matches(BWF_CMM_MessageDef.enum_message_alias.helpResponse.getString())) {

                                    if (BWF_CMM_CallManager.shoultAcceptMsgResponeOrBlock()) {
                                        BWF_CMM_Protocol_deligate.notifyOnCalling_CallDidChangeToStateOutgoing(false);
                                        BWF_CMM_CallManager.StopTimeOutOutGoingTimer();
                                    }

                                    break;
                                }

                                if (name.matches(BWF_CMM_MessageDef.enum_message_alias.helpDrop.getString())) {
                                    if (!jsonObject.isNull("data")) {

                                        JSONObject dataObject = jsonObject.getJSONObject("data");
                                        if (!dataObject.isNull("reason")) {
                                            String messFrom = "";

                                            if (dataObject.isNull("userChatId"))
                                                messFrom = dataObject.getString("userChatId");
                                            String reason = dataObject.getString("reason");
                                            BWF_CMM_Protocol_deligate.notifyOnCallDenied(messFrom, reason);

                                            NotificationManager notificationManager = (NotificationManager) SDKApplication.getAppContext().getSystemService(Context.NOTIFICATION_SERVICE);
                                            notificationManager.cancel(Const.INCOMMING_CALL_NOTIFICATION);
                                        }

                                        BWF_CMM_CallManager.StopTimeOutOutGoingTimer();
                                    }
                                    break;
                                }
                            }
                            while (false);


                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }

            for (BWF_CMM_Protocol it : listener)
                it.bwf_cmm_receivePushContent(base64DecodedStr);


        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    public static void notifyOnOutGoingCall(String fromIdentifier, boolean isVideo) {
        for (BWF_CMM_Protocol it : listener)
            it.bwf_cmm_Calling_CallDidChangeToStateOutgoing(fromIdentifier, isVideo);
    }

    public static void notifyOnConnectedCall(String fromIdentifier, boolean isVideo) {
        for (BWF_CMM_Protocol it : listener)
            it.bwf_cmm_Calling_CallDidChangeToStateConnected(fromIdentifier, isVideo);
    }




    public static void notifyOnIncommingCall(String fromIdentifier, boolean isVideo) {

        Log.d("INCOMMING__", "notify in comming calll");
        HandlerThread handlerThread = new HandlerThread(TAG);
        handlerThread.start();
        Handler handler = new Handler(handlerThread.getLooper());

        handler.post(new Runnable() {
            @Override
            public void run() {
                new CountDownTimer(2000, 1000) {
                    public void onTick(long millisUntilFinished) {
                    }

                    public void onFinish() {

                        if (0 == listener.size()) {
                            BWF_CMM_CallManager.LauchIncommingCall();
                        } else {
                            if (null != BWF_CMM_AccountData.getHelpRequest()) {
                                for (BWF_CMM_Protocol it : listener)
                                    it.bwf_cmm_Calling_CallDidChangeToStateReceivedIncomingCallFrom(fromIdentifier, isVideo);
                            }
                        }
                        BWF_CMM_CallManager.StartIncommingTimeOutTimer();
                    }

                }.start();

            }
        });
    }


    public static void notifyOnEndedCall(long totalDuration, BWF_CMM_Error error) {

        BWF_CMM_CallManager.CancelIncommingNotification();
        BWF_CMM_CallManager.AllowToReceiveRequest();

        for (BWF_CMM_Protocol it : listener)
            it.bwf_cmm_Calling_CallDidChangeToStateEnd(totalDuration, error);
    }

    public static void notifyOnPushGenerate(String base64EncodedStr, String _request_id) {
        for (BWF_CMM_Protocol it : listener)
            it.bwf_cmm_generatePushContent(base64EncodedStr, _request_id);
    }


}
