package com.example.beowulfwebrtc.SDKProtocol;

public interface BWF_CMM_Protocol {

    public void bwf_cmm_didStartWithIdentifier(String identifier);

    public void bwf_cmm_failedWithError(BWF_CMM_Error error);

    public void bwf_cmm_Calling_CallDidChangeToStateOutgoing(boolean isVideo);

    public void bwf_cmm_Call_Denied(String fromIdentifier, String reason);

    public void bwf_cmm_generatePushContent(String data, String identifier, BWF_CMM_MessageDef.bwf_cmm_push_content_type type);

    public void bwf_cmm_receivePushContent(String data, String identifier, BWF_CMM_MessageDef.bwf_cmm_push_content_type type);

    public void bwf_cmm_Calling_CallDidChangeToStateReceivedIncomingCallFrom(String fromIdentifier, boolean isVideo);

    public void bwf_cmm_Calling_CallDidChangeToStateOutgoing(String fromIdentifier, boolean isVideo);

    public void bwf_cmm_Calling_CallDidChangeToStateConnected(String fromIdentifier, boolean isVideo);

    public void bwf_cmm_Calling_CallDidChangeToStateEnd(long totalDuration, BWF_CMM_Error error);

    public void bwf_cmm_receivePushContent(String base64DecodedStr);

    public void bwf_cmm_generatePushContent(String base64EncodedStr,String _request_id);


   // public void bwf_cmm_Call_Waiting_For_Respone(String base64EncodedStr,String _request_id);

}
