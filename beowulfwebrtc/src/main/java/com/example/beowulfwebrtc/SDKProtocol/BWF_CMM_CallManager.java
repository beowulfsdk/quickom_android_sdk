package com.example.beowulfwebrtc.SDKProtocol;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.KeyguardManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.PowerManager;
import android.util.Log;
import android.widget.RemoteViews;

import androidx.core.app.NotificationCompat;

import com.example.beowulfwebrtc.API.GetGeneralInfo;
import com.example.beowulfwebrtc.API.TestPush;
import com.example.beowulfwebrtc.AppData.ChatAccountData;
import com.example.beowulfwebrtc.AppData.WebRTCCallInfo;
import com.example.beowulfwebrtc.BWBroadcastReciever.NotificationReceiver;
import com.example.beowulfwebrtc.MessageTriggling.XMPP.BeowulfXMPPConfig;
import com.example.beowulfwebrtc.MessageTriggling.XMPP.NewChatConnection;
import com.example.beowulfwebrtc.R;
import com.example.beowulfwebrtc.SDKApplication;
import com.example.beowulfwebrtc.Signal.CallingSignal;
import com.example.beowulfwebrtc.View.WebRTCIncomming;
import com.example.beowulfwebrtc.View.WebRTCOutGoing;
import com.example.beowulfwebrtc.util.Const;
import com.example.beowulfwebrtc.util.ForegroundCheckTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Semaphore;

import javax.net.ssl.HttpsURLConnection;

import static android.content.Context.NOTIFICATION_SERVICE;
import static android.content.Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS;
import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class BWF_CMM_CallManager {

    static Ringtone ringtone = null;

    private static final String CHANNEL_ID = "BEOWULF_NOTIFICATION_CHANNEL";
    private static final String TAG = "BWF_CMM_CallManager";

    private static BWF_CMM_CallManager instance = null;
    private static Object mutex = new Object();
    static long timeStamp = 0;

    static Semaphore semaphore_receive_help_request = new Semaphore(1);
    static Semaphore semaphore_receive_help_respone = new Semaphore(1);

    static Semaphore semaphore_lauch_incomming_call_onece_a_time = new Semaphore(1);

    static Handler handler = null;
    static Object closeEventLock = new Object();

    public static long timeoutInSecond = 60000;

    static CallingSignal callingSignal = null;


    static Timer outGoingTimer = null;
    static Timer inCommingTimer = null;

    public static int outgoingMediaPlayerResource = 0;
    public static int incommingMediaPlayerResource = 0;
    public static boolean shouldPlay_for_out = true;
    public static boolean shouldPlay_for_in = true;

    public void setCallingSignal(CallingSignal callingSignal) {
        this.callingSignal = callingSignal;
    }

    public void ClearCallingSignal() {
        if (null != this.callingSignal) {
            callingSignal.disconnect();
            this.callingSignal = null;
        }
    }

    public static void HandleLauchIntent(Activity activity) {
        if (!activity.isTaskRoot()
                && activity.getIntent().hasCategory(Intent.CATEGORY_LAUNCHER)
                && activity.getIntent().getAction() != null
                && activity.getIntent().getAction().equals(Intent.ACTION_MAIN)) {

            activity.finish();
            return;
        }
    }

    BWF_CMM_CallManager() {
        HandlerThread handlerThread = new HandlerThread(TAG);
        handlerThread.start();
        handler = new Handler(handlerThread.getLooper());

    }


    public static BWF_CMM_CallManager getInstance() {
        BWF_CMM_CallManager result = instance;
        if (null == result) {
            synchronized (mutex) {
                if (null == result) {
                    result = instance = new BWF_CMM_CallManager();
                }
            }
        }
        return result;
    }

    public static void voiceCallTo(String _identifier) {

        BWF_CMM_CallManager.shoultAcceptMsgRequestOrBlock();

        BWF_CMM_AccountData.partnerReferenceId = _identifier;

        Log.d(BWF_CMM_Protocol_deligate.TAG, getInstance().getClass().getSimpleName());
        GetGeneralInfo generalInfo = new GetGeneralInfo(new GetGeneralInfo.onResultListener() {
            @Override
            public void onResult(int code, String result) {

                if (code == HttpsURLConnection.HTTP_OK) {
                    String chat_id = "";
                    try {
                        JSONObject object = new JSONObject(result);

                        if (!object.isNull("chat_id")) {
                            chat_id = object.getString("chat_id");

                            if (chat_id.matches(WebRTCCallInfo.getMyCallId())) {
                                BWF_CMM_Protocol_deligate.notifiOnError(new BWF_CMM_Error(12, "can not call to yourshelf"));
                                return;
                            }


                            BWF_CMM_Call_helpRequest helpRequest = new BWF_CMM_Call_helpRequest(BeowulfXMPPConfig.getUsername().split("@")[0], chat_id);
                            helpRequest.setIdentifier(_identifier);
                            BWF_CMM_AccountData.setHelpRequest(helpRequest);
// ---------------------------------using push mechanism--------------------------------------

                            JSONObject pushIncoming = BWF_CMM_AccountData.getHelpRequest().getJSONObjectPushContent("dinh's calling", BWF_CMM_AccountData.referenceId);
                            pushMessageToIdentifier(pushIncoming.toString(), _identifier, BWF_CMM_MessageDef.bwf_cmm_push_content_type.BWF_CMM_PushContentTypeOutgoingCall);

// ---------------------------------using push mechanism--------------------------------------

                            NewChatConnection.getInstance().SendMessageTo(BWF_CMM_AccountData.getHelpRequest().getJSONObjectFullMessage().toString(), chat_id, pushIncoming.toString());

                            StartOutgoingTimeOutTimer();


                            WebRTCCallInfo.setMyPartnerCallId(chat_id);
                            WebRTCCallInfo.setQrAlias("");

                            BWF_CMM_Protocol_deligate.notifyOnOutGoingCall(_identifier, false);

                        } else {
                            BWF_CMM_Protocol_deligate.notifiOnError(new BWF_CMM_Error(11, "can't start framework _ get chat id fail"));

                        }

                    } catch (JSONException e) {
                        BWF_CMM_Protocol_deligate.notifiOnError(new BWF_CMM_Error(11, "can't start framework _ get chat id fail"));
                        e.printStackTrace();
                    }

                } else
                    BWF_CMM_Protocol_deligate.notifiOnError(new BWF_CMM_Error(11, "can't start framework _ get chat id fail"));

            }
        });


        generalInfo.Execute(_identifier, "", "", "");

    }


    public static void SetMyCallId(String myID) {
        WebRTCCallInfo.setMyCallId(myID);
    }


    public static void CancelIncommingNotification() {
        NotificationManager notificationManager = (android.app.NotificationManager) SDKApplication.getAppContext().getSystemService(NOTIFICATION_SERVICE);
        notificationManager.cancel(Const.INCOMMING_CALL_NOTIFICATION);

        if (null != notificationManager) {
            notificationManager.cancel(Const.INCOMMING_CALL_NOTIFICATION);
            if (null != ringtone)
                ringtone.stop();
        }
    }

    public static void StartOutgoingTimeOutTimer() {
        if (null != handler)
            handler.post(new Runnable() {
                @Override
                public void run() {
                    synchronized (closeEventLock) {
                        outGoingTimer = new Timer(true);
                        outGoingTimer.schedule(new TimerTask() {
                            @Override
                            public void run() {
                                timeoutCurrentOutgoingCall();
                                CancelIncommingNotification();
                            }
                        }, timeoutInSecond);
                    }

                }
            });


    }

    public static void StopTimeOutOutGoingTimer() {
        if (null != handler)
            handler.post(new Runnable() {
                @Override
                public void run() {
                    synchronized (closeEventLock) {
                        if (null != outGoingTimer) {
                            outGoingTimer.cancel();
                            outGoingTimer = null;
                        }
                    }

                }
            });

    }


    public static void StartIncommingTimeOutTimer() {
        if (null != handler)
            handler.post(new Runnable() {
                @Override
                public void run() {
                    synchronized (closeEventLock) {
                        inCommingTimer = new Timer(true);
                        inCommingTimer.schedule(new TimerTask() {
                            @Override
                            public void run() {
                                timeoutCurrentIncommingCall();
                            }
                        }, timeoutInSecond);
                    }

                }
            });


    }

    public static void StopTimeOutIncommingTimer() {
        if (null != handler)
            handler.post(new Runnable() {
                @Override
                public void run() {
                    synchronized (closeEventLock) {
                        if (null != inCommingTimer) {
                            inCommingTimer.cancel();
                            inCommingTimer = null;
                        }
                    }

                }
            });

    }


    public static void acceptIncomingCall() {
        String username = WebRTCCallInfo.getMyCallId();
        BWF_CMM_Call_helpRequest helpRequest = BWF_CMM_AccountData.getHelpRequest();
        JSONObject response = helpRequest.getHelpResponse(username);

        String messFrom = helpRequest.getUserChatId();

        NewChatConnection.SendMessageTo(response.toString(), messFrom, response.toString());

        WebRTCCallInfo.setMyPartnerCallId(helpRequest.getUserChatId());
        WebRTCCallInfo.setQrAlias("");


    }

    public static void denyIncomingCall() {

        BWF_CMM_CallManager.StopTimeOutOutGoingTimer();

        String username = ChatAccountData.GetChatIdWithoutDomain();
        BWF_CMM_Call_helpRequest helpRequest = BWF_CMM_AccountData.getHelpRequest();

        if (null == BWF_CMM_AccountData.getHelpRequest())
            return;

        JSONObject deny = helpRequest.getHelpDenyIncommingCall(username);

        String messFrom = helpRequest.getUserChatId();

        NewChatConnection.SendMessageTo(deny.toString(), messFrom, deny.toString());




        BWF_CMM_Protocol_deligate.notifyOnEndedCall(BWF_CMM_CallManager.getDurationInSecond(), new BWF_CMM_Error(0, "deny incomming call"));
        BWF_CMM_CallManager.resetDurationCall();
    }

    public static void cancelCurrentCall() {

        BWF_CMM_CallManager.StopTimeOutOutGoingTimer();

        String username = BeowulfXMPPConfig.getUsername().split("@")[0];

        if (null == BWF_CMM_AccountData.getHelpRequest())
            return;

        BWF_CMM_Call_helpRequest helpRequest = BWF_CMM_AccountData.getHelpRequest();
        JSONObject cancel = helpRequest.getHelpCancelOutGoingCall(username);
        String receiverId = helpRequest.getReciverId();
        NewChatConnection.SendMessageTo(cancel.toString(), receiverId, cancel.toString());


// ---------------------------------using push mechanism--------------------------------------
        String _identifier = helpRequest.getIdentifier();
        JSONObject pushedCancel = helpRequest.getPushDropMessage();
        pushMessageToIdentifier(pushedCancel.toString(), _identifier, BWF_CMM_MessageDef.bwf_cmm_push_content_type.BWF_CMM_PushContentTypeCancelOutgoingCallWithoutConnected);
// ---------------------------------using push mechanism--------------------------------------


        BWF_CMM_CallManager.resetDurationCall();
        BWF_CMM_Protocol_deligate.notifyOnEndedCall(BWF_CMM_CallManager.getDurationInSecond(), new BWF_CMM_Error(0, "cancel current outgoing call"));
    }

    public static void timeoutCurrentOutgoingCall() {

        BWF_CMM_CallManager.StopTimeOutOutGoingTimer();

        String username = BeowulfXMPPConfig.getUsername().split("@")[0];
        BWF_CMM_Call_helpRequest helpRequest = BWF_CMM_AccountData.getHelpRequest();
        JSONObject timeOut = helpRequest.getHelpTimeoutOutGoingCall(username);
        String receiverId = helpRequest.getReciverId();
        NewChatConnection.SendMessageTo(timeOut.toString(), receiverId, timeOut.toString());

// ---------------------------------using push mechanism--------------------------------------
        String _identifier = helpRequest.getIdentifier();
        JSONObject pushedTimeout = helpRequest.getPushDropMessage();
        pushMessageToIdentifier(pushedTimeout.toString(), _identifier, BWF_CMM_MessageDef.bwf_cmm_push_content_type.BWF_CMM_PushContentTypeTimeoutOutgoingCallWithoutConnected);

// ---------------------------------using push mechanism--------------------------------------
        BWF_CMM_Protocol_deligate.notifyOnEndedCall(BWF_CMM_CallManager.getDurationInSecond(), new BWF_CMM_Error(0, "call is timeout "));
        BWF_CMM_CallManager.resetDurationCall();

    }


    public static void timeoutCurrentIncommingCall() {

        BWF_CMM_CallManager.AllowToReceiveRequest();

        BWF_CMM_CallManager.StopTimeOutOutGoingTimer();


        EndCallAndNotification();

        BWF_CMM_Protocol_deligate.notifyOnEndedCall(BWF_CMM_CallManager.getDurationInSecond(), new BWF_CMM_Error(0, "call is timeout "));
        BWF_CMM_CallManager.resetDurationCall();

    }


    public static void endCurrentCall() {
        callingSignal.sendClosedcall();
        callingSignal.disconnect();
        callingSignal.release();
    }

    public static void switchPreviewCamera() {
        callingSignal.SwitchCamera();

    }

    public static void startDurationCall() {
        timeStamp = System.currentTimeMillis() / 1000;//time stamp in second
    }

    public static long getDurationInSecond() {
        return timeStamp == 0 ? 0 : System.currentTimeMillis() / 1000 - timeStamp;
    }

    public static void resetDurationCall() {
        timeStamp = 0;
    }


    public static void pushMessageToIdentifier(String data, String identifier, BWF_CMM_MessageDef.bwf_cmm_push_content_type type) {

        BWF_CMM_Protocol_deligate.notifyOnGeneratePushContent(data, identifier, type);

    }

    public static void LauchIncommingCall() {

        if (semaphore_lauch_incomming_call_onece_a_time.tryAcquire()) {
            if (WebRTCIncomming.isInstance()) {
                Log.d("INCOMMING___", "LAUNCHED");
                return;
            }
            Log.d("INCOMMING___", "Start to LAUNCH");

            new ForegroundCheckTask() {

                @Override
                protected void onPostExecute(Boolean aBoolean) {
                    super.onPostExecute(aBoolean);

                    if (true == isOnForeGround()) {

                        Intent t = new Intent(SDKApplication.getAppContext(), WebRTCIncomming.class);
                        t.addFlags(FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS | FLAG_ACTIVITY_NEW_TASK);

                        (SDKApplication.getAppContext()).startActivity(t);

                    } else {

                        createNotificationChannel();
                        showNotification();

                    }
                    semaphore_lauch_incomming_call_onece_a_time.release();
                }
            }.execute(SDKApplication.getAppContext());


        }


    }

    private static void showNotification() {

        Uri soundUri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + SDKApplication.getAppContext().getPackageName() + "/" + BWF_CMM_CallManager.outgoingMediaPlayerResource);
        ringtone = RingtoneManager.getRingtone(SDKApplication.getAppContext(), soundUri);
        ringtone.play();


        RemoteViews notificationLayout =
                new RemoteViews(SDKApplication.getAppContext().getPackageName(), R.layout.notification_custom);
        String call_type_video_audio = BWF_CMM_AccountData.getHelpRequest().type;
        if (call_type_video_audio.contains("video")) {
            notificationLayout.setTextViewText(R.id.message, BWF_CMM_AccountData.getHelpRequest().displayLabel + "(video)");
            notificationLayout.setImageViewResource(R.id.btnAccept, R.drawable.phone_pickup_video);
        } else
            notificationLayout.setTextViewText(R.id.message, BWF_CMM_AccountData.getHelpRequest().displayLabel);

        Intent intent = new Intent(SDKApplication.getAppContext(), WebRTCIncomming.class);
        intent.setFlags(FLAG_ACTIVITY_NEW_TASK);

        PendingIntent pendingIntent = PendingIntent.getActivity(SDKApplication.getAppContext(), 0,
                intent, PendingIntent.FLAG_UPDATE_CURRENT);

        notificationLayout.setOnClickPendingIntent(R.id.btnAccept, onAcceptNotificationClick());
        notificationLayout.setOnClickPendingIntent(R.id.btnDenied, onDismissNotificationClick());


        NotificationCompat.Builder builder = new NotificationCompat.Builder(SDKApplication.getAppContext(), CHANNEL_ID);

        builder.setVisibility(NotificationCompat.VISIBILITY_PUBLIC);
        builder.setSmallIcon(R.drawable.beowulficon);
        builder.setCustomBigContentView(notificationLayout);
        builder.setCustomContentView(notificationLayout);
        builder.setCustomHeadsUpContentView(notificationLayout);
        builder.setDeleteIntent(onDismissNotificationClick());

        builder.setFullScreenIntent(pendingIntent, true);

        Notification notification = builder.build();

        NotificationManager notificationManager = (android.app.NotificationManager) SDKApplication.getAppContext().getSystemService(NOTIFICATION_SERVICE);
        notificationManager.notify(Const.INCOMMING_CALL_NOTIFICATION, notification);

        PowerManager pm = (PowerManager) SDKApplication.getAppContext().getSystemService(Context.POWER_SERVICE);
        boolean isScreenOn = pm.isScreenOn();
        if (isScreenOn == false) {
            @SuppressLint("InvalidWakeLockTag") PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP | PowerManager.ON_AFTER_RELEASE, "MyLock");
            wl.acquire(10000);
            @SuppressLint("InvalidWakeLockTag") PowerManager.WakeLock wl_cpu = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "MyCpuLock");

            wl_cpu.acquire(10000);
        }
    }

    private static void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "xxxx";
            String description = "xxxxxxx";
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = (android.app.NotificationManager) SDKApplication.getAppContext().getSystemService(NOTIFICATION_SERVICE);
            notificationManager.createNotificationChannel(channel);
        }
    }


    public static void EndCallAndNotification() {
        Intent intent = new Intent(SDKApplication.getAppContext(), NotificationReceiver.class);
        intent.putExtra(NotificationReceiver.REQUEST_CODE_EXTRA, NotificationReceiver.DENY_INCOMMING_NOTI);
        SDKApplication.getAppContext().sendBroadcast(intent);
    }

    private static PendingIntent onDismissNotificationClick() {

        Intent intent = new Intent(SDKApplication.getAppContext(), NotificationReceiver.class);
        intent.putExtra(NotificationReceiver.REQUEST_CODE_EXTRA, NotificationReceiver.DENY_INCOMMING_NOTI);
        return PendingIntent.getBroadcast(SDKApplication.getAppContext(), (int) (System.currentTimeMillis() % 1000), intent, 0);
    }



    private static PendingIntent onAcceptNotificationClick() {
        Intent intent = new Intent(SDKApplication.getAppContext(), NotificationReceiver.class);
        intent.putExtra(NotificationReceiver.REQUEST_CODE_EXTRA, NotificationReceiver.ACCEPT_INCOMMING_NOTI);
        return PendingIntent.getBroadcast(SDKApplication.getAppContext(), (int) (System.currentTimeMillis() % 1000), intent, 0);

    }



    public static void LauchOutgoingCall(boolean isVideoCall) {
        Intent t = new Intent(SDKApplication.getAppContext(), WebRTCOutGoing.class);
        t.setFlags(FLAG_ACTIVITY_NEW_TASK);
        t.putExtra(Const.IS_VIDEO_CALL, isVideoCall);
        SDKApplication.getAppContext().startActivity(t);
    }


    public static void AllowToReceiveRequest() {

        int permisson = semaphore_receive_help_request.availablePermits();
        if (permisson <= 0) {
            semaphore_receive_help_request.release(1);
        }
    }

    public static boolean shoultAcceptMsgRequestOrBlock() {
        return semaphore_receive_help_request.tryAcquire();
    }


    public static void AllowToReceiveRespone() {

        int permisson = semaphore_receive_help_respone.availablePermits();
        if (permisson <= 0) {
            semaphore_receive_help_respone.release(1);
        }


    }

    public static boolean shoultAcceptMsgResponeOrBlock() {
        return semaphore_receive_help_respone.tryAcquire();
    }


    public static void setCustomCallingSoundPathForOutgoingCall(int media_file_id) {
        outgoingMediaPlayerResource = media_file_id;
    }

    public static void setCustomCallingSoundPathForIncomingCall(int media_file_id) {
        incommingMediaPlayerResource = media_file_id;
    }

    public static void shouldPlaySoundForOutgoingCall(boolean _shouldPlay) {
        shouldPlay_for_out = _shouldPlay;
    }

    public static void shouldPlaySoundForIncomingCall(boolean _shouldPlay) {
        shouldPlay_for_in = _shouldPlay;
    }


    public static void configureWithApiKey(String apikey) {
        SDKApplication.setApi_key(apikey);
    }

    public static void PushMessage(String base64Str, String request_id) {
//
        BWF_CMM_Protocol_deligate.notifyOnPushGenerate(base64Str, request_id);


//        TestPush note8 =
//                new TestPush(
//                        "eMgr90-Y1ac:APA91bHePkki3Qy1Rzuk12SvwG0bMYjPFQklDZtQuNaOWCuppyqCdw4QZA0A2Vl_FKDZS2wKrvozthMquIiPu5luWaKnd7A1FfZ5rdcrjCFog--JPSoDC3_4X9PhlGIZTdW6sWE45dt0"
//                        , base64Str
//                        , request_id);// to make a call to sam sung note 8
//
//        note8.Execute();
//
//        TestPush pc =
//                new TestPush(
//                        "fg-plvfIAHI:APA91bG4l_V6xMKiRsqKR_FLxXvG4NQHmlVNjS1kL-2q5ORyLeKZHEG6pNNIL_miDjfS6v2R2RBbT44HKo-ND5osD9hBOUm4CakUghlH5OShx6LPmIdRsMwDF2N9J2Q6epwIuCwXuNbw"
//                        , base64Str



//                        , request_id);
//        //    pc.Execute();
//
//
//        TestPush samsungcong =
//                new TestPush(
//                        "eR951ZRZ0YY:APA91bGXNYdCAc6qs-kqM76vQFlO6cz16UrAqe9J0ktEAHCn_YOoQ31RLEEHVFx0uZPC-1ly-7bExeX-thu0rwEHOxfiuTs1UQ-GwU_dQBk8bYIidvlnTIn0QpgyNMIkvUsvuTDVLku-"
//                        , base64Str FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
//            @Override
//            public void onSuccess(InstanceIdResult instanceIdResult) {
//                String fireBaseToken = instanceIdResult.getToken();
//                Log.d("FIREBASETOKEN", fireBaseToken);
//            }
//        });
//                        , request_id);
//        //    samsungcong.Execute();
//
//
//
//        TestPush hieuphone =
//                new TestPush(
//                        "dqRVHkeMugc:APA91bEOnLBmuc1spej8o0aB9j4xCT3jfrwp3tnYZ198eDb2mYbqt_AJVg9rRTCxqpkLQG6qNjy5SzVBVEb2P8Ia6usrF8-8bWHUh8DKJsh5J_BaRj4f2uNof8XUkNUS8w-9c13AXI7n"
//                        , base64Str
//                        , request_id);
//            hieuphone.Execute();


        // to make a call to semulator on pc

        //new TestPush("cvJxnJKvbg4:APA91bGhoZppPjNFLUGXL8RGGoiXiQRwSjSobyNBHTgythBHsM1mM3cj0Ogt3UA8EszH7kg1NQSUEZH-0FEooiKfNsCwXiU_sLlT7bTXiC5G_aUoL_t6Egkf0O1VvwrQbADPgyiWTu59",base64Str,request_id).Execute();// sam sung khac
    }

}
