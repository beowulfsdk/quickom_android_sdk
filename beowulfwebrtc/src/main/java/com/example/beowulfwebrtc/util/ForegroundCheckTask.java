package com.example.beowulfwebrtc.util;

import android.app.ActivityManager;
import android.content.Context;
import android.os.AsyncTask;

import java.util.List;

public class ForegroundCheckTask extends AsyncTask<Context, Void, Boolean> {


    public boolean isOnForeGround() {
        return isOnForeGround;
    }

    boolean isOnForeGround = true;

    @Override
    protected Boolean doInBackground(Context... params) {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        final Context context = params[0].getApplicationContext();
        isOnForeGround= isAppOnForeground(context);
        return isOnForeGround;
    }

    public boolean isAppOnForeground(Context context) {
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> appProcesses = activityManager.getRunningAppProcesses();
        if (appProcesses == null) {
            return false;
        }
        final String packageName = context.getPackageName();
        for (ActivityManager.RunningAppProcessInfo appProcess : appProcesses) {
            if (appProcess.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND && appProcess.processName.equals(packageName)) {
                return true;
            }
        }
        return false;
    }


}

