package com.example.beowulfwebrtc.API;

import android.util.Log;


import com.example.beowulfwebrtc.LogUtil;
import com.example.beowulfwebrtc.SDKProtocol.BWF_CMM_AccountData;
import com.example.beowulfwebrtc.SDKProtocol.BWF_CMM_Error;
import com.example.beowulfwebrtc.SDKProtocol.BWF_CMM_Protocol_deligate;

import org.json.JSONException;
import org.json.JSONObject;

import javax.net.ssl.HttpsURLConnection;

public class startWithIdentifier {


    private static startWithIdentifier instance = null;

    public static boolean isInstance() {
        return null != instance;
    }


    public startWithIdentifier(String referenceId) {

        BWF_CMM_AccountData.referenceId = referenceId;
        LogUtil.displayLog("set identifier:" + referenceId);
    }


    public void Execute() {

        GetGeneralInfo generalInfo = new GetGeneralInfo();

      //  Log.d(BWF_CMM_Protocol_deligate.TAG, this.getClass().getSimpleName());

        generalInfo.SetOnResultListener(new GetGeneralInfo.onResultListener() {
            @Override
            public void onResult(int code, String result) {
                if (code == HttpsURLConnection.HTTP_OK) {


                    String account_id = "";

                    try {
                        JSONObject object = new JSONObject(result);


                        if (!object.isNull("account_id")) {
                            account_id = object.getString("account_id");
                            LogUtil.displayLog("get chat info :"+"success account id "+account_id);
                            RegisterToChat(account_id);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    LogUtil.displayLog("get chat info :"+"account not found -> create new account ");
                    CreateNewUserForCall newUserForCall = new CreateNewUserForCall();
                    newUserForCall.SetOnCallbackListener(new CreateNewUserForCall.CreateUserCallBack() {
                        @Override
                        public void onResult(int code, String str) {

                        //    Log.d(BWF_CMM_Protocol_deligate.TAG, this.getClass().getSimpleName() + " - " + str);

                            if (HttpsURLConnection.HTTP_OK == code) {

                                LogUtil.displayLog("create account done :" + BWF_CMM_AccountData.referenceId);
                                GetAccountid(BWF_CMM_AccountData.referenceId);
                            }
                            else {
                                BWF_CMM_Protocol_deligate.notifiOnError(new BWF_CMM_Error(11, "can't start framework _ create account fail"));
                                LogUtil.displayLog("create account fail : can't start framework _ create account fail");
                            }
                        }
                    });

                    newUserForCall.Execute(BWF_CMM_AccountData.referenceId, "123456", BWF_CMM_AccountData.referenceId);
                }
            }
        });
        generalInfo.Execute(BWF_CMM_AccountData.referenceId, "", "", "");
    }


    public void RegisterToChat(String account_id) {
        new GetChatInfoMaiLinh().Execute(account_id);
    }

    public void GetAccountid(String referenceId) {

        LogUtil.displayLog("get account id  :");

        GetGeneralInfo generalInfo = new GetGeneralInfo();
        generalInfo.SetOnResultListener(new GetGeneralInfo.onResultListener() {
            @Override
            public void onResult(int code, String result) {
              //  Log.d(BWF_CMM_Protocol_deligate.TAG, this.getClass().getSimpleName() + " - " + result);
                if (code == HttpsURLConnection.HTTP_OK) {
                    String account_id = "";
                    try {
                        JSONObject object = new JSONObject(result);
                        if (!object.isNull("account_id")) {
                            account_id = object.getString("account_id");

                            LogUtil.displayLog("get account id  done: "+account_id);

                            RegisterToChat(account_id);

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    LogUtil.displayLog("get account id  fail: "+"can't start framework _ get account id fail");
                    BWF_CMM_Protocol_deligate.notifiOnError(new BWF_CMM_Error(11, "can't start framework _ get account id fail"));
                }

            }
        });
        generalInfo.Execute(referenceId, "", "", "");
    }

}



