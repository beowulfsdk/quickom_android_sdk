package com.example.beowulfwebrtc.API;

import com.example.beowulfwebrtc.AppData.AppUri;
import com.example.beowulfwebrtc.AppData.BeowulInfo;
import com.example.beowulfwebrtc.network.JsonAPI;



import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import javax.net.ssl.HttpsURLConnection;

public class GetEnterpriseAccountDetail {

    private static GetEnterpriseAccountDetail instance = null;

    public static boolean isInstance() {
        return null != instance;
    }

    public static GetEnterpriseAccountDetail getInstance() {
        if (null == instance) {
            instance = new GetEnterpriseAccountDetail();
        }
        return instance;
    }

    private GetEnterpriseAccountDetail() {

    }

    HashMap<String, String> requestHeader = new HashMap<String, String>();
    JsonAPI.JsonCallback jsonCallback = (statusCode, str) -> {
        if (statusCode == HttpsURLConnection.HTTP_OK) {
//            try {
//
//                JSONObject object = new JSONObject(str);
//                       BeowulInfo.getInstance().Parse(object);
//                       new GetChatInfo().Execute();
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }

        }
    };

    JsonAPI.JsonRequestListener jsonRequestListener = new JsonAPI.JsonRequestListener() {
        @Override
        public void onBeginRequest() {
        }

        @Override
        public void onEndRequest() {

        }
    };

    public void Execute() {

        requestHeader.put("Content-Type", "application/json");
        requestHeader.put("Authorization", BeowulInfo.getInstance().getToken());
        JsonAPI.getInstance().get(requestHeader, AppUri.ENTERPRISE_ACCDETAIL_GET, jsonCallback, jsonRequestListener);


    }
}
