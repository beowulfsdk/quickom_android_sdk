package com.example.beowulfwebrtc.API;


import com.example.beowulfwebrtc.AppData.AppUri;
import com.example.beowulfwebrtc.network.JsonAPI;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class AcceptCallRequest {

    String callerId = "";
    String calleeId = "";
    String alias = "";
    String calleeChatId = "";
    String calleeName = "";
    String requestId = "";
    onResultListender onResultListender = null;

    public AcceptCallRequest(String callerId, String calleeId, String alias, String calleeChatId,String calleeName, String requestId) {
        this.callerId = callerId;
        this.calleeId = calleeId;
        this.alias = alias;
        this.calleeChatId = calleeChatId;
    }


    JsonAPI.JsonCallback jsonCallback = (statusCode, str) -> {

        if (null != onResultListender) {
            onResultListender.onResult(statusCode, str);
        }

    };

    JsonAPI.JsonRequestListener jsonRequestListener = new JsonAPI.JsonRequestListener() {
        @Override
        public void onBeginRequest() {
        }

        @Override
        public void onEndRequest() {
        }
    };


    public void Execute(onResultListender onResultListender) {

        this.onResultListender = onResultListender;

        JSONObject json = new JSONObject();
        try {
            json.put("callerId", this.callerId);
            json.put("calleeId", this.calleeId);
            json.put("alias", this.alias);
            json.put("calleeChatId", this.calleeChatId);
            json.put("calleeName", this.calleeName);
            json.put("requestId", this.requestId);
        } catch (
                JSONException e) {
            e.printStackTrace();
        }

        String jsonStr = json.toString();
        HashMap<String, String> requestHeader = new HashMap<String, String>();
        requestHeader.put("Content-Type", "application/json");
        JsonAPI.getInstance().post(requestHeader, AppUri.WEBRTC_ACCEPT_CALL, jsonStr, jsonCallback, jsonRequestListener);

    }

    public interface onResultListender {
        public void onResult(int code, String str);
    }
}
