package com.example.beowulfwebrtc.API;

import android.util.Log;

import com.example.beowulfwebrtc.AppData.AppUri;
import com.example.beowulfwebrtc.LogUtil;
import com.example.beowulfwebrtc.SDKApplication;
import com.example.beowulfwebrtc.network.JsonAPI;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class GetGeneralInfo {

    public GetGeneralInfo() {

    }

    public GetGeneralInfo(onResultListener _resultListener) {
        resultListener = _resultListener;
    }

    onResultListener resultListener;


    public void SetOnResultListener(onResultListener onresult) {
        resultListener = onresult;
    }

    JsonAPI.JsonCallback jsonCallback = (statusCode, str) -> {

        if (null != resultListener) {
            resultListener.onResult(statusCode, str);
        }


    };

    JsonAPI.JsonRequestListener jsonRequestListener = new JsonAPI.JsonRequestListener() {
        @Override
        public void onBeginRequest() {
        }

        @Override
        public void onEndRequest() {
        }
    };


    public void Execute(String reference_id, String account_id, String sip_id, String chat_id) {

        LogUtil.displayLog("get chat info :"+" reference_id: "+reference_id+" account_id : "+account_id+" sip_id: "+sip_id+" chat_id: "+chat_id);

        JSONObject json = new JSONObject();
        try {
            json.put("reference_id", reference_id);
            json.put("account_id", account_id);
            json.put("sip_id", sip_id);
            json.put("chat_id", chat_id);
        } catch (
                JSONException e) {
            e.printStackTrace();
        }

        String jsonStr = json.toString();
        HashMap<String, String> requestHeader = new HashMap<String, String>();
        requestHeader.put("Content-Type", "application/json");
        requestHeader.put("Authorization",SDKApplication.getAPIKey());

        JsonAPI.getInstance().post(requestHeader, AppUri.GET_GENERAL_INFO_POST, jsonStr, jsonCallback, jsonRequestListener);


    }

    public interface onResultListener {
        public void onResult(int code, String result);


    }

}
