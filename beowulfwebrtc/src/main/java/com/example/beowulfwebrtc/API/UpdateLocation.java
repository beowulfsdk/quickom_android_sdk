package com.example.beowulfwebrtc.API;

import com.example.beowulfwebrtc.network.JsonAPI;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class UpdateLocation {

    double longitude = 0;
    double latitude = 0;


    onResultListender onResultListender = null;

    public UpdateLocation(double longitude, double latitude) {
        this.longitude = longitude;
        this.latitude = latitude;
    }


    JsonAPI.JsonCallback jsonCallback = (statusCode, str) -> {

        if (null != onResultListender) {
            onResultListender.onResult(statusCode, str);
        }

    };

    JsonAPI.JsonRequestListener jsonRequestListener = new JsonAPI.JsonRequestListener() {
        @Override
        public void onBeginRequest() {
        }

        @Override
        public void onEndRequest() {
        }
    };

    public void Execute(String authorization) {

        JSONObject json = new JSONObject();
        try {
            json.put("x", latitude);
            json.put("y", longitude);
        } catch (
                JSONException e) {
            e.printStackTrace();
        }

        String jsonStr = json.toString();
        HashMap<String, String> requestHeader = new HashMap<String, String>();
        requestHeader.put("Content-Type", "application/json");
        requestHeader.put("Authorization","Basic YTBkOTI1YTYtMGY1Yy00NGNlLTljM2ItZDhmMzNhMTEyZGJhOjAyY2MyZWExLTU1NmQtNGNiOC04ZDY4LWFlYmQwODgzZTgyMg==" );
        JsonAPI.getInstance().post(requestHeader, "http://api.drivor.vn/api/v1/account/location/update", jsonStr, jsonCallback, jsonRequestListener);
    }


    public interface onResultListender {
        public void onResult(int code, String str);
    }
}
