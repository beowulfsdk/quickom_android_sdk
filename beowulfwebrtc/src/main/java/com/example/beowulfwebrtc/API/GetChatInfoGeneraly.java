package com.example.beowulfwebrtc.API;


import android.util.Log;

import com.example.beowulfwebrtc.AppData.AppUri;
import com.example.beowulfwebrtc.AppData.ChatAccountData;
import com.example.beowulfwebrtc.LogUtil;
import com.example.beowulfwebrtc.MessageTriggling.XMPP.BeowulfXMPPConfig;
import com.example.beowulfwebrtc.MessageTriggling.XMPP.NewChatConnection;
import com.example.beowulfwebrtc.SDKApplication;
import com.example.beowulfwebrtc.SDKProtocol.BWF_CMM_Error;
import com.example.beowulfwebrtc.SDKProtocol.BWF_CMM_Protocol_deligate;
import com.example.beowulfwebrtc.network.JsonAPI;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import javax.net.ssl.HttpsURLConnection;

public class GetChatInfoGeneraly {

    final String TAG = "GETCHATINFO";


    GetChatInfoGeneraly() {
    }

    JsonAPI.JsonCallback jsonCallback = (statusCode, str) -> {

        Log.d(BWF_CMM_Protocol_deligate.TAG, this.getClass().getSimpleName() + " - " + str);

        if (statusCode == HttpsURLConnection.HTTP_OK) {

            try {
                JSONObject object = new JSONObject(str);
                ChatAccountData.getInstance().ParseData(object);
                String chatId = ChatAccountData.getInstance().GetChatIdWithoutDomain();
                String domain = ChatAccountData.getInstance().getChat_domain();
                String host = ChatAccountData.getInstance().getChat_host();
                int port = Integer.parseInt(ChatAccountData.getInstance().getChat_port());
                String password = ChatAccountData.getInstance().getChat_password();
                String username = ChatAccountData.getInstance().getChat_username();

                LogUtil.displayLog("get chat info :" + "success with info " + " chat id: " + chatId + " domain: " + domain + " host: " + host + " port: " + port + " username: " + username + " password: " + password);


                BeowulfXMPPConfig.setDomain(domain);
                BeowulfXMPPConfig.setHost(host);
                BeowulfXMPPConfig.setPort(port);
                BeowulfXMPPConfig.setUsername(username);
                BeowulfXMPPConfig.setPassword(password);
            } catch (Exception e) {
                Log.d(TAG, e.toString());
            }

            LogUtil.displayLog("start to login to chat server");

            NewChatConnection.getInstance().InitChatConenction();


        } else {
            LogUtil.displayLog("get chat info :" + "fail can't start framework _ get chat id fail");
            BWF_CMM_Protocol_deligate.notifiOnError(new BWF_CMM_Error(11, "can't start framework _ get chat id fail"));
        }
    };

    JsonAPI.JsonRequestListener jsonRequestListener = new JsonAPI.JsonRequestListener() {
        @Override
        public void onBeginRequest() {
        }

        @Override
        public void onEndRequest() {
        }
    };

    public void Execute(String accountId) {
        JSONObject json = new JSONObject();
        try {
            json.put("account_id", accountId);
        } catch (
                JSONException e) {
            e.printStackTrace();
        }
        String jsonStr = json.toString();
        HashMap<String, String> requestHeader = new HashMap<String, String>();
        requestHeader.put("Content-Type", "application/json");
        requestHeader.put("Authorization", SDKApplication.getAPIKey());
        JsonAPI.getInstance().post(requestHeader, AppUri.CHAT_ACCOUNT_INFO, jsonStr, jsonCallback, jsonRequestListener);

    }
}
