package com.example.beowulfwebrtc.API;
import com.example.beowulfwebrtc.AppData.AppUri;
import com.example.beowulfwebrtc.SDKApplication;
import com.example.beowulfwebrtc.network.JsonAPI;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class CreateNewUserForCall {


    CreateUserCallBack onResult=null;

    public CreateNewUserForCall() {

    }

    public void SetOnCallbackListener(  CreateUserCallBack onResult)
    {
        this.onResult = onResult;
    }

    JsonAPI.JsonCallback jsonCallback = (statusCode, str) -> {

        onResult.onResult(statusCode,str);


    };

    JsonAPI.JsonRequestListener jsonRequestListener = new JsonAPI.JsonRequestListener() {
        @Override
        public void onBeginRequest() {
        }

        @Override
        public void onEndRequest() {
        }
    };


    public void Execute(final String username, final String password, final String name) {


        JSONObject json = new JSONObject();
        try {
            json.put("username", username);
            json.put("password", password);
            json.put("group_name", "");
            json.put("name", name);
        } catch (
                JSONException e) {
            e.printStackTrace();
        }

        String jsonStr = json.toString();
        HashMap<String, String> requestHeader = new HashMap<String, String>();
        requestHeader.put("Content-Type", "application/json");
        requestHeader.put("Authorization", SDKApplication.getAPIKey());
        JsonAPI.getInstance().post(requestHeader, AppUri.CREATE_SUB_USER_POST, jsonStr, jsonCallback, jsonRequestListener);

    }

    public interface CreateUserCallBack {
        public void onResult(int code, String strResult);
    }

}
