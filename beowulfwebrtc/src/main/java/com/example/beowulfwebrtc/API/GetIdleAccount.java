package com.example.beowulfwebrtc.API;

import android.util.Log;


import com.example.beowulfwebrtc.AppData.AppUri;
import com.example.beowulfwebrtc.AppData.IdleAccountInfo;
import com.example.beowulfwebrtc.MessageTriggling.XMPP.BeowulfXMPPConfig;
import com.example.beowulfwebrtc.SDKApplication;
import com.example.beowulfwebrtc.SDKProtocol.BWF_CMM_CallManager;
import com.example.beowulfwebrtc.network.JsonAPI;
import com.example.beowulfwebrtc.util.GsonSingleton;
import com.google.gson.reflect.TypeToken;

import java.util.HashMap;

import javax.net.ssl.HttpsURLConnection;


public class GetIdleAccount {


    private static OnResult onResult = null;

    public static void Execute() {

        JsonAPI.JsonCallback jsonCallback = (statusCode, str) -> {
            if (statusCode == HttpsURLConnection.HTTP_OK) {
                Log.d("BEOLOGIN2", str);
                IdleAccountInfo idleAccountInfo = GsonSingleton.getInstance().fromJson(str, new TypeToken<IdleAccountInfo>() {
                }.getType());
                new GetChatInfoGeneraly().Execute(idleAccountInfo.getAccount_id());

                BWF_CMM_CallManager.SetMyCallId(idleAccountInfo.getChat_id());

                onResult.onResultListener(idleAccountInfo);
            }
        };

        JsonAPI.JsonRequestListener jsonRequestListener = new JsonAPI.JsonRequestListener() {
            @Override
            public void onBeginRequest() {
            }

            @Override
            public void onEndRequest() {

            }
        };
        HashMap<String, String> requestHeader = new HashMap<String, String>();
        requestHeader.put("Authorization", SDKApplication.getAPIKey());
        JsonAPI.getInstance().get(requestHeader, AppUri.ENTERPRISE_ACCOUNT_IDLE_GET, jsonCallback, jsonRequestListener);
    }

    public static void addListener(OnResult _onResult) {
        onResult = _onResult;
    }

    public static void removeListener() {
        onResult = null;
    }

    public interface OnResult {
        public void onResultListener(IdleAccountInfo idleAccountInfo);
    }
}
