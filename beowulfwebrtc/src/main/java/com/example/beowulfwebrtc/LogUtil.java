package com.example.beowulfwebrtc;

import java.util.ArrayList;

public class LogUtil {


    public interface LoggingDisplay {
        public void DisplayString(String log);
    }


    static  ArrayList<LoggingDisplay> list = new ArrayList<>();

    public static void addListener(LoggingDisplay listener) {
        if (!list.contains(listener))
            list.add(listener);
    }

    public static  void removeListener(LoggingDisplay listener) {
        if (list.contains(listener))
            list.remove(listener);
    }

    public static  void displayLog(String logcat) {
        for (LoggingDisplay display : list) {
            display.DisplayString(logcat);
        }
    }

}
