package com.example.beowulfwebrtc.AppData;

import org.json.JSONException;
import org.json.JSONObject;

public class BeowulInfo {
    public boolean get_info = false;

    public boolean isGet_info() {
        return get_info;
    }

    public void setGet_info(boolean get_info) {
        this.get_info = get_info;
    }

    public void setAccount_status(String account_status) {
        this.account_status = account_status;
    }

    public String getDomain() {
        return domain;
    }

    public void setAccount_type(String account_type) {
        this.account_type = account_type;
    }

    public String getProxyconfig() {
        return proxyconfig;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public void setAccount_setting(String account_setting) {
        this.account_setting = account_setting;
    }

    public String getAccountId() {
        return accountId;
    }

    private String domain = "beowulfchain.com";
    private String proxyconfig = "<sip:prosip.beowulfchain.com:443;transport=tls>";
    private String login_username = "";
    private String login_password = "";
    private String token = "";
    private String accountId = "";
    private String sip_id = "";
    private String chat_id = "";
    private boolean secure_pwd = false;

    public boolean isSecure_pwd() {
        return secure_pwd;
    }

    public void setSecure_pwd(boolean secure_pwd) {
        this.secure_pwd = secure_pwd;
    }



    public String getEmail() {
        return email;
    }

    private String email = "";

    public String getUsername() {
        return username;
    }

    private String username = "";
    private String company_name = "";
    private long created_at = 000;
    private String account_status = "";
    private String account_type = "";
    private boolean enable_2fa = false;
    private String account_setting = "";
    private String displayname = "";

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }


    public String getReferral_id() {
        return referral_id;
    }

    private String referral_id = "";
    private String online_status = "online";
    private JSONObject object = null;

    static BeowulInfo instance = null;


    ///////------- idle acc----------
    public String super_account_id = "";
    public String account_id = "";
    public String reference_id = "";
    public String status = "";
    public String name = "";
    public String group = "";

    public static BeowulInfo getInstance() {
        if (null == instance)
            instance = new BeowulInfo();
        return instance;
    }

    public static boolean isInstance() {
        return null != instance;
    }

    public void Parse(JSONObject object) throws JSONException {
        this.object = object;

        if (!object.isNull("account_id"))
            BeowulInfo.getInstance().accountId = object.getString("account_id");

        if (!object.isNull("sip_id"))
            BeowulInfo.getInstance().sip_id = object.getString("sip_id");

        if (!object.isNull("chat_id"))
            BeowulInfo.getInstance().chat_id = object.getString("chat_id");

        if (!object.isNull("email"))
            BeowulInfo.getInstance().email = object.getString("email");

        if (!object.isNull("username"))
            BeowulInfo.getInstance().username = object.getString("username");

        if (!object.isNull("company_name"))
            BeowulInfo.getInstance().company_name = object.getString("company_name");


        if (!object.isNull("created_at"))
            BeowulInfo.getInstance().created_at = object.getLong("created_at");


        if (!object.isNull("account_status"))
            BeowulInfo.getInstance().account_status = object.getString("account_status");

        if (!object.isNull("account_type"))
            BeowulInfo.getInstance().account_type = object.getString("account_type");


        if (!object.isNull("enable_2fa"))
            BeowulInfo.getInstance().enable_2fa = object.getBoolean("enable_2fa");


        if (!object.isNull("account_setting"))
            BeowulInfo.getInstance().account_setting = object.getString("account_setting");

        if (!object.isNull("referral_id"))
            BeowulInfo.getInstance().referral_id = object.getString("referral_id");

        if (!object.isNull("online_status"))
            BeowulInfo.getInstance().online_status = object.getString("online_status");

        if (!object.isNull("secure_pwd"))
            BeowulInfo.getInstance().secure_pwd = object.getBoolean("secure_pwd");

        this.get_info = true;
    }


    public void ParseIdleAcc(JSONObject object) throws JSONException {
        this.object = object;

        super_account_id = "";
        account_id = "";
        reference_id = "";
        sip_id = "";
        created_at = 000;
        account_type = "";
        chat_id = "";

        if (!object.isNull("super_account_id"))
            super_account_id = object.getString("super_account_id");

        if (!object.isNull("account_id"))
            account_id = object.getString("account_id");

        if (!object.isNull("reference_id"))
            reference_id = object.getString("reference_id");

        if (!object.isNull("sip_id"))
            sip_id = object.getString("sip_id");

        if (!object.isNull("created_at"))
            created_at = object.getLong("created_at");

        if (!object.isNull("account_type"))
            account_type = object.getString("account_type");

        if (!object.isNull("status"))
            status = object.getString("status");

        if (!object.isNull("name"))
            name = object.getString("name");

        if (!object.isNull("group"))
            group = object.getString("group");

        if (!object.isNull("chat_id")) {
            chat_id = object.getString("chat_id");


        }

    }

}
