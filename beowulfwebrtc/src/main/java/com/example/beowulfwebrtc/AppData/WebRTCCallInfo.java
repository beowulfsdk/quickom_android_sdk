package com.example.beowulfwebrtc.AppData;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.beowulfwebrtc.SDKApplication;

public class WebRTCCallInfo {

    public final static String APPNAME_REF = "BEOWULFSDK";
    static String ACCOUNTID = "ACCOUNTID";

    static String qrAlias = "";

    static String requestId = "";

    static String myCallId = "";

    static String myChatId = "";

    static String myPartnerCallId = "";


    static String myPartnerChatId = "";

    //----------------------------------//


    public static String getQrAlias() {
        return qrAlias;
    }

    public static String getRequestId() {
        return requestId;
    }

    public static String getMyCallId() {
        myCallId = getAccountIdPref(SDKApplication.getAppContext());
        return myCallId;
    }

    public static String getMyChatId() {
        return myChatId;
    }

    public static String getMyPartnerCallId() {
        return myPartnerCallId;
    }

    public static String getMyPartnerChatId() {
        return myPartnerChatId;
    }

//----------------------------------//

    public static void setMyPartnerChatId(String myPartnerChatId) {
        WebRTCCallInfo.myPartnerChatId = myPartnerChatId;
    }


    public static void setQrAlias(String qrAlias) {
        WebRTCCallInfo.qrAlias = qrAlias;
    }

    public static void setRequestId(String requestId) {
        WebRTCCallInfo.requestId = requestId;
    }

    public static void setMyCallId(String myCallId) {
        setAccountIdPref(SDKApplication.getAppContext(), myCallId);
        WebRTCCallInfo.myCallId = myCallId;
    }

    public static void setMyChatId(String myChatId) {
        WebRTCCallInfo.myChatId = myChatId;
    }

    public static void setMyPartnerCallId(String myPartnerCallId) {
        WebRTCCallInfo.myPartnerCallId = myPartnerCallId;
    }


    public static void setAccountIdPref(Context context, String accountId) {
        SharedPreferences myPrefs = context.getSharedPreferences(APPNAME_REF, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = myPrefs.edit();
        prefsEditor.putString(ACCOUNTID, accountId);
        prefsEditor.commit();
    }

    public static String getAccountIdPref(Context context) {
        String ret = "";
        SharedPreferences myPrefs = context.getSharedPreferences(APPNAME_REF, Context.MODE_PRIVATE);
        ret = myPrefs.getString(ACCOUNTID, ret);
        return ret;
    }


}
