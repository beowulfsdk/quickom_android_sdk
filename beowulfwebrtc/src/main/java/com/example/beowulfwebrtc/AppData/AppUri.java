package com.example.beowulfwebrtc.AppData;

public class AppUri {
    public static boolean bAutoAccept = true;
    public static boolean bProduction =true;
    static String URL_TEST_SERVER = "https://testnet-tel.beowulfchain.com";
    static String URL_REAL_SERVER = "https://tel.beowulfchain.com";
    static String URL_SERVER = bProduction ? URL_REAL_SERVER : URL_TEST_SERVER;

    public static String CALL_CENTER_DOMAIN = bProduction ? "dispatcher-v2.beowulfchain.com" : "call-center.beowulfchain.com";
   // public static String CALL_CENTER_DOMAIN ="dispatcher.drivor.vn";
    public static String CALL_SIGNAL_DOMAIN = bProduction ? "signaler-v2.beowulfchain.com" : "call-signal.beowulfchain.com";
    public static String ENTERPRISE_ACCDETAIL_GET = URL_SERVER + "/apiv1/enterprise/details";
    public static String ENTERPRISE_ACCOUNT_IDLE_GET = URL_SERVER + "/apiv1/account/users/available";
    public static String CHAT_ACCOUNT_INFO = URL_SERVER + "/apiv1/account/chat/info";
    public static String CREATE_CHAT_CONVERSATION = URL_SERVER + "/apiv1/chat/create";
    public static String WEBRTC_ACCEPT_CALL = "https://" + CALL_CENTER_DOMAIN + "/callee/take-call";
    public static String WEBRTC_DENY_CALL = "https://" + CALL_CENTER_DOMAIN + "/callee/cancel-call";
    public static String GET_GENERAL_INFO_POST = URL_SERVER + "/apiv1/account/general-info";
    public static String CREATE_SUB_USER_POST = URL_SERVER + "/apiv1/enterprise/users/create";
    public static String TESTING_PUSH="http://35.229.222.105/kpush/api/v1/push/demo";

}
