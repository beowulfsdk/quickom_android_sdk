package com.example.beowulfwebrtc.AppData;

public class IdleAccountInfo {
    private String super_account_id;
    private String account_id;
    private String reference_id;
    private String sip_id;
    private Long created_at;
    private String account_type;
    private String status;
    private String name;
    private String group;

    public String getChat_id() {
        return chat_id;
    }

    private String chat_id;
    private Chat_account chat_account;

    private class Chat_account {
        public String getChat_username() {
            return chat_username;
        }

        private String chat_username;
        private String chat_password;
        private String chat_host;
        private String chat_port;
        private String chat_domain;
    }

    public String getAccount_id() {
        return account_id;
    }
}
