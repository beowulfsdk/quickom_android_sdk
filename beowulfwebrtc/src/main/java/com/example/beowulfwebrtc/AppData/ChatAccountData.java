package com.example.beowulfwebrtc.AppData;

import android.content.Context;
import android.content.SharedPreferences;


import com.example.beowulfwebrtc.SDKApplication;

import org.json.JSONException;
import org.json.JSONObject;

public class ChatAccountData {

    public static ChatAccountData getInstance() {
        return instance;
    }

    private static ChatAccountData instance = null;

    ChatAccountData() {
        instance = this;
    }

    static String CHAT_USER_NAME = "CHAT_USER_NAME";
    static String CHAT_PASSWORD = "CHAT_PASSWORD";
    static String CHAT_HOST = "CHAT_HOST";
    static String CHAT_PORT = "CHAT_PORT";
    static String CHAT_DOMAIN = "CHAT_DOMAIN";


    public final static String APPNAME_REF = "QUICKOM";

    private static String chat_username = "";
    private static String chat_password = "";
    private static String chat_host = "";
    private static String chat_port = "";
    private static String chat_domain = "";

    public static void ParseData(JSONObject object) {

        try {
            if (!object.isNull("chat_username")) {
                String chat_username = object.getString("chat_username");
                setChat_username(chat_username);
            }

            if (!object.isNull("chat_password")) {
                String chat_password = object.getString("chat_password");
                setChat_password(chat_password);
            }

            if (!object.isNull("chat_host")) {
                String chat_host = object.getString("chat_host");
                setChat_host(chat_host);
            }

            if (!object.isNull("chat_port")) {
                String chat_port = object.getString("chat_port");
                setChat_port(chat_port);
            }


            if (!object.isNull("chat_domain")) {
                String chat_domain = object.getString("chat_domain");
                setChat_domain(chat_domain);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public static void setChat_username(String _chat_username) {
        chat_username = _chat_username;

        SharedPreferences myPrefs = SDKApplication.getAppContext().getSharedPreferences(APPNAME_REF, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = myPrefs.edit();
        prefsEditor.putString(CHAT_USER_NAME, chat_username);
        prefsEditor.commit();
    }


    public static void setChat_password(String _chat_password) {
        chat_password = _chat_password;
        SharedPreferences myPrefs = SDKApplication.getAppContext().getSharedPreferences(APPNAME_REF, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = myPrefs.edit();
        prefsEditor.putString(CHAT_PASSWORD, chat_password);
        prefsEditor.commit();
    }


    public static void setChat_host(String _chat_host) {
        chat_host = _chat_host;
        SharedPreferences myPrefs = SDKApplication.getAppContext().getSharedPreferences(APPNAME_REF, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = myPrefs.edit();
        prefsEditor.putString(CHAT_HOST, chat_host);
        prefsEditor.commit();
    }


    public static void setChat_port(String _chat_port) {
        chat_port = _chat_port;
        SharedPreferences myPrefs = SDKApplication.getAppContext().getSharedPreferences(APPNAME_REF, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = myPrefs.edit();
        prefsEditor.putString(CHAT_PORT, chat_port);
        prefsEditor.commit();
    }


    public static void setChat_domain(String _chat_domain) {
        chat_domain = _chat_domain;
        SharedPreferences myPrefs = SDKApplication.getAppContext().getSharedPreferences(APPNAME_REF, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = myPrefs.edit();
        prefsEditor.putString(CHAT_DOMAIN, chat_domain);
        prefsEditor.commit();
    }


    public static String getChat_username() {
        SharedPreferences myPrefs = SDKApplication.getAppContext().getSharedPreferences(APPNAME_REF, Context.MODE_PRIVATE);
        chat_username = myPrefs.getString(CHAT_USER_NAME, chat_username);
        return chat_username;
    }

    public static String getChat_password() {

        SharedPreferences myPrefs = SDKApplication.getAppContext().getSharedPreferences(APPNAME_REF, Context.MODE_PRIVATE);
        chat_password = myPrefs.getString(CHAT_PASSWORD, chat_password);

        return chat_password;
    }

    public static String getChat_host() {
        SharedPreferences myPrefs = SDKApplication.getAppContext().getSharedPreferences(APPNAME_REF, Context.MODE_PRIVATE);
        chat_host = myPrefs.getString(CHAT_HOST, chat_host);

        return chat_host;
    }

    public static String getChat_port() {
        SharedPreferences myPrefs = SDKApplication.getAppContext().getSharedPreferences(APPNAME_REF, Context.MODE_PRIVATE);
        chat_port = myPrefs.getString(CHAT_PORT, chat_port);
        return chat_port;
    }

    public static String getChat_domain() {
        SharedPreferences myPrefs = SDKApplication.getAppContext().getSharedPreferences(APPNAME_REF, Context.MODE_PRIVATE);
        chat_domain = myPrefs.getString(CHAT_DOMAIN, chat_domain);
        return chat_domain;
    }

    public static String GetChatIdWithoutDomain() {
        String ret = "";
        if (chat_username.split("@").length > 1)
            ret = chat_username.split("@")[0];
        return ret;
    }


}
