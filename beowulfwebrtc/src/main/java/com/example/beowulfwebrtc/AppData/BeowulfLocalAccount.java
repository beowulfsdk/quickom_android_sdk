package com.example.beowulfwebrtc.AppData;

import android.content.Context;
import android.content.SharedPreferences;

public class BeowulfLocalAccount {

    public static BeowulfLocalAccount instance = null;

    public static BeowulfLocalAccount getInstance() {
        if (null == instance)
            instance = new BeowulfLocalAccount();
        return instance;
    }

    public final static String APPNAME_REF = "QUICKOM";

    static String USERNAME_SETTING = "USERNAME";
    static String PASSWORD_SETTING = "PASSWORD";
    static String FIRST_TIME_SETTING = "FIRSTTIME";
    static String ACCESS_TOKEN = "ACCESSTOKEN";
    static String IS_TABLET = "TABLET";

    public void setIsTabletPref(Context context, boolean isTablet) {
        SharedPreferences myPrefs = context.getSharedPreferences(APPNAME_REF, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = myPrefs.edit();
        prefsEditor.putBoolean(IS_TABLET, isTablet);
        prefsEditor.commit();
    }

    public void setUserNamePref(Context context, String username) {

        SharedPreferences myPrefs = context.getSharedPreferences(APPNAME_REF, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = myPrefs.edit();
        prefsEditor.putString(USERNAME_SETTING, username);
        prefsEditor.commit();
    }

    public void setPasswordPref(Context context, String password) {

        SharedPreferences myPrefs = context.getSharedPreferences(APPNAME_REF, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = myPrefs.edit();
        prefsEditor.putString(PASSWORD_SETTING, password);
        prefsEditor.commit();
    }


    public String getUserNamePref(Context context) {
        SharedPreferences myPrefs = context.getSharedPreferences(APPNAME_REF, Context.MODE_PRIVATE);
        String username = myPrefs.getString(USERNAME_SETTING, "");
        return username;
    }

    public String getPasswordSetting(Context context) {
        SharedPreferences myPrefs = context.getSharedPreferences(APPNAME_REF, Context.MODE_PRIVATE);
        String password = myPrefs.getString(PASSWORD_SETTING, "");
        return password;
    }

    public boolean getIsTabletPref(Context context) {
        SharedPreferences myPrefs = context.getSharedPreferences(APPNAME_REF, Context.MODE_PRIVATE);
        boolean isTablet = myPrefs.getBoolean(IS_TABLET, false);
        return isTablet;
    }

    public void setFirstTimePref(Context context, boolean isFirstTime) {
        SharedPreferences myPrefs = context.getSharedPreferences(APPNAME_REF, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = myPrefs.edit();
        prefsEditor.putBoolean(FIRST_TIME_SETTING,isFirstTime);
        prefsEditor.commit();
    }

    public static boolean getFirstTimePref(Context context) {
        SharedPreferences myPrefs = context.getSharedPreferences(APPNAME_REF, Context.MODE_PRIVATE);
        boolean isFirstTime = myPrefs.getBoolean(FIRST_TIME_SETTING, true);
        return isFirstTime;
    }

    public void setAccessTokenPref(Context context,final  String  token) {
        SharedPreferences myPrefs = context.getSharedPreferences(APPNAME_REF, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = myPrefs.edit();
        prefsEditor.putString(ACCESS_TOKEN, token);
        prefsEditor.commit();
    }

    public String getAccessTokenPref(Context context) {
        String ret= "";
        SharedPreferences myPrefs = context.getSharedPreferences(APPNAME_REF, Context.MODE_PRIVATE);
        ret = myPrefs.getString(ACCESS_TOKEN, ret);
        return ret;
    }





}
