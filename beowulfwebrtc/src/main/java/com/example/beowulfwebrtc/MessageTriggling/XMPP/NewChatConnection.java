package com.example.beowulfwebrtc.MessageTriggling.XMPP;

import android.os.Looper;
import android.util.Base64;
import android.util.Log;


import com.example.beowulfwebrtc.AppData.ChatAccountData;
import com.example.beowulfwebrtc.LogUtil;
import com.example.beowulfwebrtc.SDKProtocol.BWF_CMM_AccountData;
import com.example.beowulfwebrtc.SDKProtocol.BWF_CMM_CallManager;
import com.example.beowulfwebrtc.SDKProtocol.BWF_CMM_Call_helpDrop;
import com.example.beowulfwebrtc.SDKProtocol.BWF_CMM_Call_helpRequest;
import com.example.beowulfwebrtc.SDKProtocol.BWF_CMM_MessageDef;
import com.example.beowulfwebrtc.SDKProtocol.BWF_CMM_Protocol_deligate;

import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.ConnectionListener;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.chat2.Chat;
import org.jivesoftware.smack.chat2.ChatManager;
import org.jivesoftware.smack.chat2.IncomingChatMessageListener;
import org.jivesoftware.smack.chat2.OutgoingChatMessageListener;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration;
import org.jivesoftware.smackx.debugger.android.AndroidDebugger;
import org.json.JSONException;
import org.json.JSONObject;
import org.jxmpp.jid.EntityBareJid;
import org.jxmpp.jid.impl.JidCreate;
import org.jxmpp.stringprep.XmppStringprepException;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class NewChatConnection {


    private static NewChatConnection instance = null;
    private static XMPPTCPConnection mConnection = null;
    private static ConnectionListener mConnectionListener = null;
    private static IncomingChatMessageListener mIncommingMessageListener = null;
    private static OutgoingChatMessageListener mOutgoingMessageListener = null;


    public boolean isAuthenticated() {
        return authenticated;
    }

    private static boolean authenticated = false;

    public void Disconnect() {
        if (null != mConnection)
            mConnection.disconnect();
    }

    public static boolean isInstance() {
        return null != instance;
    }


    public String getCurrentConversation_at_toReciver() {
        return currentConversation_at_toReciver;
    }

    private String currentConversation_at_toReciver = "";


    public static NewChatConnection getInstance() {
        if (null == instance)
            instance = new NewChatConnection();
        return instance;
    }

    private NewChatConnection() {

        mConnectionListener = new ConnectionListener() {
            @Override
            public void connected(XMPPConnection connection) {
                ReconnectionChat.getInstance().Init();
            }

            @Override
            public void authenticated(XMPPConnection connection, boolean resumed) {
                authenticated = true;

                BWF_CMM_CallManager.SetMyCallId(BeowulfXMPPConfig.getUsername().split("@")[0]);

                if (false == resumed) {
                    LogUtil.displayLog("XMPPCONT" + " authenticated first time");
                    Log.d("XMPPCONT", "authenticated first time");
                    BWF_CMM_Protocol_deligate.notifiOnStartSucess(BWF_CMM_AccountData.referenceId);

                } else {
                    Log.d("XMPPCONT", "authenticated resume");
                }

            }

            @Override
            public void connectionClosed() {
                Log.d("XMPPCONT", "connection close");
                LogUtil.displayLog("XMPPCONT" + "connection close");
                authenticated = false;
            }

            @Override
            public void connectionClosedOnError(Exception e) {

                LogUtil.displayLog("XMPPCONT" + "connection close + error:" + e.toString());

                Log.d("XMPPCONT", "connection close + error:" + e.toString());
                authenticated = false;

            }
        };


        mOutgoingMessageListener = (to, message, chat) -> {

            String messTo = message.getTo().toString();
            String messToWithDomain = message.getTo().toString();

            messTo = messTo.split("@")[0];
            try {
                JSONObject commonMessObj = new JSONObject(message.getBody());
                Log.d("MESS_OUT_", commonMessObj.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }

        };

        mIncommingMessageListener = (from, message, chat) -> {
            String messFrom = message.getFrom().toString().split("@")[0];
            String messTo = message.getTo().toString().split("@")[0];

            try {

                JSONObject commonMessObj = new JSONObject(message.getBody());
                Log.d("MESS_IN_", commonMessObj.toString());

                if (!commonMessObj.isNull("name")) {
                    String name = commonMessObj.getString("name");
                    do {
                        if (name.matches(BWF_CMM_MessageDef.enum_message_alias.helpRequest.getString())) {

                            if (BWF_CMM_CallManager.shoultAcceptMsgRequestOrBlock()) {
                                Log.d("MESS_IN_", "Access try");
                                BWF_CMM_AccountData.setHelpRequest( new BWF_CMM_Call_helpRequest(commonMessObj, messTo));
                                BWF_CMM_Protocol_deligate.notifyOnIncommingCall("", false);
                            }

                            break;
                        }

                        if (name.matches(BWF_CMM_MessageDef.enum_message_alias.helpDrop.getString())) {

                            Log.d("MESS_IN_", "Access dropppppppppppp");
                            BWF_CMM_Call_helpDrop drop = new BWF_CMM_Call_helpDrop(commonMessObj);

                            String reason = drop.getReason();

                            BWF_CMM_CallManager.StopTimeOutOutGoingTimer();

                            String requestid = drop.getRequestId();
                            if (null != BWF_CMM_AccountData.getHelpRequest()) {


                                if (BWF_CMM_AccountData.getHelpRequest().getRequestId().matches(requestid)) {

                                    BWF_CMM_CallManager.AllowToReceiveRequest();

                                    BWF_CMM_Protocol_deligate.notifyOnCallDenied(messFrom, reason);
                                    BWF_CMM_AccountData.setHelpRequest( null);

                                }

                            }



                            break;
                        }
                        if (name.matches(BWF_CMM_MessageDef.enum_message_alias.helpResponse.getString())) {


                            if(BWF_CMM_CallManager.shoultAcceptMsgResponeOrBlock())
                            {
                                BWF_CMM_Protocol_deligate.notifyOnCalling_CallDidChangeToStateOutgoing(false);
                                BWF_CMM_CallManager.StopTimeOutOutGoingTimer();
                            }
                            BWF_CMM_CallManager.StopTimeOutIncommingTimer();

                            break;
                        }

                    }
                    while (false);


                }


            } catch (JSONException e) {
                e.printStackTrace();
            }

        };


    }


    public void InitChatConenction() {
        new Thread(() -> {
            Looper.prepare();
            NewChatConnection.getInstance().Disconnect();
            NewChatConnection.getInstance().ConnectServer();
            Looper.loop();
        }).start();
        //THE CODE HERE RUNS IN A BACKGROUND THREAD.


    }


    private static void Login(String username, String password) {

        try {


            mConnection.login(username, password);


//            AccountManager accountManager = AccountManager.getInstance(mConnection);
//            accountManager.sensitiveOperationOverInsecureConnection(true);
//            accountManager.createAccount(Localpart.from(username), password);

        } catch (XMPPException e) {
            e.printStackTrace();
        } catch (SmackException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    public void ConnectServer() {
        String domain = ChatAccountData.getChat_domain();
        String chatHost = ChatAccountData.getChat_host();
        String chatPort = ChatAccountData.getChat_port();
        String password = ChatAccountData.getChat_password();
        String username = ChatAccountData.getChat_username().split("@")[0];
        EntityBareJid jid;
        try {
            android.util.Log.d("XMPPConnectServer", "" + domain);
            android.util.Log.d("XMPPConnectServer", "" + chatHost);
            android.util.Log.d("XMPPConnectServer", "" + chatPort);
            android.util.Log.d("XMPPConnectServer", "" + password);
            android.util.Log.d("XMPPConnectServer", "" + username);

            jid = JidCreate.entityBareFrom(ChatAccountData.getChat_username() + "/android");

            XMPPTCPConnectionConfiguration conf = XMPPTCPConnectionConfiguration.builder()
                    .setXmppDomain(domain)
                    .setHost(chatHost)
                    .setPort(Integer.parseInt(chatPort))
                    .setAuthzid(jid)
                    .setDebuggerFactory(AndroidDebugger::new)
                    .allowEmptyOrNullUsernames()
                    //Was facing this issue
                    //https://discourse.igniterealtime.org/t/connection-with-ssl-fails-with-java-security-keystoreexception-jks-not-found/62566
                    .setKeystoreType(null) //This line seems to get rid of the problem
                    .setSecurityMode(ConnectionConfiguration.SecurityMode.required)
                    .setCompressionEnabled(true).build();


            mConnection = new XMPPTCPConnection(conf);
            mConnection.removeConnectionListener(mConnectionListener);
            mConnection.addConnectionListener(mConnectionListener);
            ChatManager.getInstanceFor(mConnection).setXhmtlImEnabled(true);
//            ChatManager.getInstanceFor(mConnection).removeIncomingListener(mIncommingMessageListener);
            ChatManager.getInstanceFor(mConnection).addIncomingListener(mIncommingMessageListener);

//            ChatManager.getInstanceFor(mConnection).removeOutgoingListener(mOutgoingMessageListener);
            ChatManager.getInstanceFor(mConnection).addOutgoingListener(mOutgoingMessageListener);
            mConnection.connect();
            Login(username, password);
          //  SendOnline();

        } catch (IOException | InterruptedException | SmackException | XMPPException | IllegalArgumentException e) {
            e.printStackTrace();
        }

    }

    public static boolean SendMessageTo(String payload, String sendToChatId) {

        boolean ret = false;
        String domain = BeowulfXMPPConfig.getDomain();

        EntityBareJid jid = null;
        ChatManager chatManager = ChatManager.getInstanceFor(mConnection);
        try {
            jid = JidCreate.entityBareFrom(sendToChatId + "@" + domain);

            Chat chat = chatManager.chatWith(jid);
            Message message = new Message(jid, Message.Type.chat);
            message.setBody(payload);
            chat.send(message);

            ret = true;

        } catch (XmppStringprepException e) {
            e.printStackTrace();
        } catch (SmackException.NotConnectedException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        return ret;
    }

    public static boolean SendMessageTo(String payload, String sendToChatId, String pushContent) {

        boolean ret = false;

        if(null!=mConnection)
        {
            String domain = ChatAccountData.getChat_domain();

            EntityBareJid jid = null;
            ChatManager chatManager = ChatManager.getInstanceFor(mConnection);
            try {
                jid = JidCreate.entityBareFrom(sendToChatId + "@" + domain);

                Chat chat = chatManager.chatWith(jid);
                Message message = new Message(jid, Message.Type.chat);
                message.setBody(payload);
                chat.send(message);

                ret = true;

            } catch (XmppStringprepException e) {
                e.printStackTrace();
            }
            catch (SmackException.NotConnectedException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        if (!pushContent.matches("")) {
            byte[] base64byte = String.valueOf(pushContent).getBytes();
            String base64Str = Base64.encodeToString(base64byte, Base64.DEFAULT);
          BWF_CMM_CallManager.PushMessage(base64Str, "");
        }

        return ret;
    }


    public void disconnectConnection() {
        if (null != mConnection) {
            mConnection.disconnect();
        }
    }


    private static class ReconnectionChat {
        static ReconnectionChat instance = null;
        static org.jivesoftware.smack.ReconnectionManager reconnectionManager = null;

        public static ReconnectionChat getInstance() {
            if (null == instance) {
                instance = new ReconnectionChat();
            }
            return instance;
        }

        ReconnectionChat() {
            reconnectionManager = org.jivesoftware.smack.ReconnectionManager.getInstanceFor(mConnection);

        }

        public static void Init() {
            reconnectionManager.setEnabledPerDefault(true);
            reconnectionManager.enableAutomaticReconnection();
            reconnectionManager.setFixedDelay(5);
        }
    }


    public static String recurseKeys(JSONObject jObj, String findKey) throws JSONException {
        String finalValue = "";
        if (jObj == null) {
            return "";
        }

        Iterator<String> keyItr = jObj.keys();
        Map<String, String> map = new HashMap<>();

        while (keyItr.hasNext()) {
            String key = keyItr.next();
            map.put(key, jObj.getString(key));
        }

        for (Map.Entry<String, String> e : (map).entrySet()) {
            String key = e.getKey();
            if (key.equalsIgnoreCase(findKey)) {
                return jObj.getString(key);
            }

            // read value
            Object value = jObj.get(key);

            if (value instanceof JSONObject) {
                finalValue = recurseKeys((JSONObject) value, findKey);
            }
        }

        // key is not found
        return finalValue;
    }

    public static class MyChat {
        private Chat chat;

        public MyChat(Chat chat) {
            this.chat = chat;
        }

        public void send(Message message) {
            switch (message.getType()) {
                case normal:
                case chat:
                    break;
                default:
                    throw new IllegalArgumentException("Message must be of type 'normal' or 'chat'");
            }

            message.setTo(this.chat.getXmppAddressOfChatPartner());

            try {
                mConnection.sendStanza(message);
            } catch (SmackException.NotConnectedException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
