package com.example.beowulfwebrtc.MessageTriggling.XMPP;

public class BeowulfXMPPConfig {
    public static String getDomain() {
        return domain;
    }

    public static String getHost() {
        return host;
    }



    public static String getUsername() {
        return username;
    }

    public static String getPassword() {
        return password;
    }

    public static void setDomain(String domain) {
        BeowulfXMPPConfig.domain = domain;
    }

    public static void setHost(String host) {
        BeowulfXMPPConfig.host = host;
    }


    public static void setUsername(String username) {
        BeowulfXMPPConfig.username = username;
    }

    public static void setPassword(String password) {
        BeowulfXMPPConfig.password = password;
    }
    public static int getPort() {
        return port;
    }

    public static void setPort(int port) {
        BeowulfXMPPConfig.port = port;
    }

    private static String domain="";
    private static String host="";
    private static int port=1234;
    private static String username="";
    private static String password="";
}
