package com.example.beowulfwebrtc.MessageTriggling;

public interface ConnectionStatus {
    void onConnected();

    void onDisconnected();

    void onAuthenticated();

    void onConnectionClosedOnError();
}
