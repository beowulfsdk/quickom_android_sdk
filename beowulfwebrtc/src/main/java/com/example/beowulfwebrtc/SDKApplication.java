package com.example.beowulfwebrtc;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;




public class SDKApplication extends ContentProvider {

    private static Context context;

    public final static String APPNAME_REF = "BEOWULFSDK";

    static String API_KEY_SETTING = "APIKEY";

    public static void setApi_key(String api_key) {

        setAPIKey(context,api_key);
        SDKApplication.api_key = api_key;
    }

    private static String api_key = "";

    public static Context getAppContext() {
        return context;
    }


    public static void setAPIKey(Context context, String username) {
        SharedPreferences myPrefs = context.getSharedPreferences(APPNAME_REF, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = myPrefs.edit();
        prefsEditor.putString(API_KEY_SETTING, username);
        prefsEditor.commit();
    }

    public static String getAPIKey() {
        SharedPreferences myPrefs = context.getSharedPreferences(APPNAME_REF, Context.MODE_PRIVATE);
        String username = myPrefs.getString(API_KEY_SETTING, "");
        return username;
    }

    @Override
    public boolean onCreate() {
        context = getContext();

//        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
//            @Override
//            public void onSuccess(InstanceIdResult instanceIdResult) {
//                String fireBaseToken = instanceIdResult.getToken();
//                Log.d("FIREBASETOKEN", fireBaseToken);
//            }
//        });

        return false;
    }

//    public static String getCurrentIdentifier() {
//        return currentIdentifier;
//    }
//
//    public static void setCurrentIdentifier(String newIdentifier) {
//        currentIdentifier = newIdentifier;
//    }


    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        return null;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        return null;
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        return 0;
    }


}
