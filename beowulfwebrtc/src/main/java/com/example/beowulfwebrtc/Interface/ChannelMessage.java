package com.example.beowulfwebrtc.Interface;

public interface ChannelMessage {

    public void connect(String id,boolean useReconnectOption);

    public void disconnect(Boolean waitForComplete);

    public void sendMessage(String message);

    public void reportError(String error);

    public boolean isConnected();

    public void setListener(ChannelEvents listener);

    public interface ChannelEvents {

        public void onChannelOpen();

        public void onChannelError(String error);

        public void onChannelClosed(String reseason);

        public void onChannelMessage(String message);
    }
}


