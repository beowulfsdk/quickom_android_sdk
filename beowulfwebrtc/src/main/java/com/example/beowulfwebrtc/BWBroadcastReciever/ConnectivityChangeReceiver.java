package com.example.beowulfwebrtc.BWBroadcastReciever;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import static android.content.Context.CONNECTIVITY_SERVICE;

public class ConnectivityChangeReceiver extends BroadcastReceiver {
    private static ConnectivityChangeReceiver instance;

    public static ConnectivityChangeReceiver getInstance() {
        if (instance == null) {
            instance = new ConnectivityChangeReceiver();
            return instance;
        }
        return instance;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (isConnectedToInternet(context)) {
            android.util.Log.d("NetworkHere", "AAAAAAA");
        }
        else {
            android.util.Log.d("NetworkHere", "BBBBBBB");
        }

    }

    public boolean isConnectedToInternet(Context context) {
        try {
            if (context != null) {
                ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(CONNECTIVITY_SERVICE);
                NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
                return networkInfo != null && networkInfo.isConnected();
            }
            return false;
        } catch (Exception e) {
            Log.e(ConnectivityChangeReceiver.class.getName(), e.getMessage());
            return false;
        }
    }

}