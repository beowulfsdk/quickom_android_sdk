package com.example.beowulfwebrtc.BWBroadcastReciever;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;


import com.example.beowulfwebrtc.R;
import com.example.beowulfwebrtc.SDKApplication;
import com.example.beowulfwebrtc.SDKProtocol.BWF_CMM_CallManager;
import com.example.beowulfwebrtc.View.WebRTCIncomming;
import com.example.beowulfwebrtc.util.Const;

import org.json.JSONException;
import org.json.JSONObject;

import static android.content.Context.NOTIFICATION_SERVICE;
import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class NotificationReceiver extends BroadcastReceiver {

    public final static int DENY_INCOMMING_NOTI = 12345;
    public final static int ACCEPT_INCOMMING_NOTI = 54321;
    public static final String REQUEST_CODE_EXTRA = "REQUEST_CODE_EXTRA";

    @Override
    public void onReceive(Context context, Intent intent) {
        int requestCode = intent.getIntExtra(REQUEST_CODE_EXTRA, -1);

        switch (requestCode) {
            case ACCEPT_INCOMMING_NOTI:



                Intent intentAccept = new Intent(SDKApplication.getAppContext(), WebRTCIncomming.class);
                intentAccept.setFlags(FLAG_ACTIVITY_NEW_TASK);
                intentAccept.putExtra(Const.AUTO_ACCEPT_INCOMMING_CALL, true);
                SDKApplication.getAppContext().startActivity(intentAccept);

                break;

            case DENY_INCOMMING_NOTI:

                BWF_CMM_CallManager.denyIncomingCall();
                BWF_CMM_CallManager.AllowToReceiveRequest();
                BWF_CMM_CallManager.AllowToReceiveRespone();
                BWF_CMM_CallManager.CancelIncommingNotification();
                break;


            default:
                break;
        }
    }


}