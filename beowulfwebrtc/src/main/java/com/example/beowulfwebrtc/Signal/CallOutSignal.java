package com.example.beowulfwebrtc.Signal;

import android.os.Handler;
import android.os.HandlerThread;
import android.util.Log;

import com.example.beowulfwebrtc.AppData.WebRTCCallInfo;
import com.example.beowulfwebrtc.Interface.ChannelMessage;
import com.example.beowulfwebrtc.PeerSettings.PeerConnectionConfig;
import com.example.beowulfwebrtc.SDKProtocol.BWF_CMM_CallManager;
import com.example.beowulfwebrtc.SDKProtocol.BWF_CMM_Error;
import com.example.beowulfwebrtc.SDKProtocol.BWF_CMM_Protocol_deligate;
import com.example.beowulfwebrtc.WebSocket.WebSocketChannel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class CallOutSignal implements ChannelMessage.ChannelEvents {

    String TAG = "CallOutSignal";

    public interface CallCenterDelegate {
        void onCallcenterConnected();

        void onCallcenterDisconnected();

        void onCallcenterCallBusy();

        void onCallcenterOutingCallInitiated();

        void onCallCenterOnChatInitiated();

        void onCallCenterError(String error);

    }

    WebSocketChannel socket = null;
    Handler handler = null;
    String endPoint = "";
    String type_QR = "";
    String loginId = "";
    ArrayList<CallCenterDelegate> listeners = new ArrayList();


    @Override
    public void onChannelOpen() {
        //WebRTCCallInfo.setMyCallId(loginId);
        notifyOnLogin();
    }

    @Override
    public void onChannelError(String error) {
        notifyOnError(error);
    }

    @Override
    public void onChannelClosed(String reason) {
      //  Log.d("CALLOUTSIGNAL",reason);
        notifyOnLogout();
    }


    public CallOutSignal() {

        HandlerThread handlerThread = new HandlerThread(TAG);
        handlerThread.start();
        handler = new Handler(handlerThread.getLooper());
        socket = new WebSocketChannel(handler, this);
        endPoint = PeerConnectionConfig.outgoingCallEndPoint;

    }

    @Override
    public void onChannelMessage(String message) {
        try {
          //  Log.d("WebSocketChannelClient",message);
            JSONArray dataJson = new JSONArray(message);
            if (dataJson.length() >= 2) {
                String call_type = dataJson.getString(0);
                JSONObject request_respone = dataJson.getJSONObject(1);
                if (call_type.equals("call-wait")) {

                }

                if (call_type.equals("call-busy")) {
                    notifyOnCallBusy();
                }

                if (call_type.equals("call-accepted")) {

                    if (!request_respone.isNull("request_id")) {
                        String requestId = request_respone.getString("request_id");
                        WebRTCCallInfo.setRequestId(requestId);
                    }


                    if (!request_respone.isNull("support_chat_id")) {
                        String myPartnerChatId = request_respone.getString("support_chat_id");
                        WebRTCCallInfo.setMyPartnerChatId(myPartnerChatId);
                    }


                    if (!request_respone.isNull("calleeId")) {

                        String myPartnerCallId = request_respone.getString("calleeId");
                        WebRTCCallInfo.setMyPartnerCallId(myPartnerCallId);

                        if (type_QR.matches("chat"))
                            notifyOnChatInitiated();
                        else {
                            //notifyOnOutgoingCallInitiated();
                            BWF_CMM_Protocol_deligate.notifyOnCalling_CallDidChangeToStateOutgoing(true);
                            BWF_CMM_CallManager.StopTimeOutOutGoingTimer();
                        }

                    }



                }

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public void connectToWsAndCall(String callerId, String qrAlias, String displayName, String requestType, String callerChatId) {

        loginId = callerId;

        handler.post(new Runnable() {
            @Override
            public void run() {
                String loginUri = generateLoginURI(callerId, qrAlias, displayName, requestType, callerChatId).toString();
                socket.connect(loginUri,false);
            }
        });

    }

    public void connectToWsAndCallDrivo(String callerId, String displayName, String requestType, String callerChatId,double longitude,double latitude,String customData) {

        loginId = callerId;

        handler.post(new Runnable() {
            @Override
            public void run() {
                String loginUri = generateLoginURIDrivo(callerId, displayName, requestType, callerChatId,longitude,latitude,customData).toString();
                socket.connect(loginUri,false);
            }
        });

    }



    private void notifyOnLogin() {
        for (CallCenterDelegate callCenterDelegate : listeners) {
            callCenterDelegate.onCallcenterConnected();
        }
    }

    private void notifyOnError(String error) {
        for (CallCenterDelegate callCenterDelegate : listeners) {
            callCenterDelegate.onCallCenterError(error);
        }
    }


    private void notifyOnLogout() {
        for (CallCenterDelegate callCenterDelegate : listeners) {
            callCenterDelegate.onCallcenterDisconnected();
        }
    }

    private void notifyOnChatInitiated() {
        for (CallCenterDelegate callCenterDelegate : listeners) {
            callCenterDelegate.onCallCenterOnChatInitiated();
        }
    }

    private void notifyOnOutgoingCallInitiated() {
        for (CallCenterDelegate callCenterDelegate : listeners) {
            callCenterDelegate.onCallcenterOutingCallInitiated();
        }
    }

    private void notifyOnCallBusy() {
        for (CallCenterDelegate callCenterDelegate : listeners) {
            callCenterDelegate.onCallcenterCallBusy();
        }
        BWF_CMM_CallManager.resetDurationCall();
        BWF_CMM_Protocol_deligate.notifyOnEndedCall(BWF_CMM_CallManager.getDurationInSecond(), new BWF_CMM_Error(0, "cancel current outgoing call"));
    }

    private String generateLoginURI(String callerId, String qrAlias, String displayName, String requestType, String callerChatId) {
        return endPoint + "/call?" + "callerId=" + callerId + "&alias=" + qrAlias + "&displayName=" + displayName + "&requestType=" + requestType + "&callerChatId=" + callerChatId;
    }


    private String generateLoginURIDrivo(String callerId, String displayName, String requestType, String callerChatId,double longitude,double latitude,String customData) {
        return endPoint + "/call?" + "callerId=" + callerId
                + "&longitude="+longitude
                +"&latitude="+latitude
                +"&customData=" +customData
                + "&displayName=" + displayName
                + "&requestType=" + requestType
                + "&callerChatId=" + callerChatId;
    }

    public void addOutGoingCallListener(CallCenterDelegate listener) {
        listeners.add(listener);
    }

    public void removeOutGoingCallListener(CallCenterDelegate listener) {
        listeners.remove(listener);
    }

}
