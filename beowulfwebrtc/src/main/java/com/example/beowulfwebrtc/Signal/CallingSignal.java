package com.example.beowulfwebrtc.Signal;

import android.content.Context;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.HandlerThread;
import android.util.Log;

import com.example.beowulfwebrtc.BWBroadcastReciever.ConnectivityChangeReceiver;
import com.example.beowulfwebrtc.Interface.ChannelMessage;
import com.example.beowulfwebrtc.PeerConnectionClient;
import com.example.beowulfwebrtc.PeerSettings.PeerConnectionConfig;
import com.example.beowulfwebrtc.SDKApplication;
import com.example.beowulfwebrtc.SDKProtocol.BWF_CMM_CallManager;
import com.example.beowulfwebrtc.SDKProtocol.BWF_CMM_Protocol_deligate;
import com.example.beowulfwebrtc.SignalConnectionParameter;
import com.example.beowulfwebrtc.WebSocket.WebSocketChannel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.webrtc.Camera2Enumerator;
import org.webrtc.CameraEnumerator;
import org.webrtc.EglBase;
import org.webrtc.IceCandidate;
import org.webrtc.Logging;
import org.webrtc.PeerConnectionFactory;
import org.webrtc.RendererCommon;
import org.webrtc.SessionDescription;
import org.webrtc.StatsReport;
import org.webrtc.SurfaceViewRenderer;
import org.webrtc.VideoCapturer;
import org.webrtc.VideoFrame;
import org.webrtc.VideoSink;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;

public class CallingSignal implements ChannelMessage.ChannelEvents, PeerConnectionClient.PeerConnectionEvents {

    // signaling between 2 peers
    private static final String TAG = "CallingSignal";
    final String RESPONE = "response";
    final String ACCEPT = "accept";
    final String REJECT = "reject";
    final String CLOSED = "closed";

    final String CANDIDATE = "candidate";
    final String SDP = "sdp";

    final String STATUS = "status";

    final String READY = "ready";
    final String DISCONNECT = "disconnect";
    final String RESTART = "restart";


    Semaphore semaphreReconnection = new Semaphore(1);

    final int NUMBER_OF_READY_NEED_TO_SEND = 1;
    int numberOfReadyRemind = 2;


    private boolean micEnabled = true;
    long callDuration = 0;

    boolean connectedToNetwork = false;

    boolean reconnect = false;


    public void setNeedToRestartCall(boolean needToRestartCall) {
        this.needToRestartCall = needToRestartCall;
    }

    boolean needToRestartCall = true;


    public void setStreaming(boolean streaming) {
        isStreaming = streaming;
    }


    boolean isStreaming = false;

    @Override
    public void onLocalDescription(SessionDescription sdp) {
        if (!isInitiator)
            sendAnswer(sdp);
        else
            sendOffer(sdp);

        if (peerConnectionParameters.videoMaxBitrate > 0)
            peerConnectionClient.setVideoMaxBitrate(peerConnectionParameters.videoMaxBitrate);

    }

    @Override
    public void onIceCandidate(IceCandidate candidate) {
        sendIceCandidate(candidate);
    }

    @Override
    public void onIceCandidatesRemoved(IceCandidate[] candidates) {
        Log.d(TAG, "onIceCandidatesRemoved");
    }

    @Override
    public void onIceConnected() {

    }

    @Override
    public void onIceDisconnected() {
        Log.d(TAG, "onIceDisconnected");
    }

    @Override
    public void onConnected() {
        Log.d(TAG, "onConnected");
    }

    @Override
    public void onDisconnected() {
        Log.d(TAG, "onDisconnected");
    }

    @Override
    public void onPeerConnectionClosed() {
        Log.d(TAG, "onPeerConnectionClosed");
    }

    @Override
    public void onPeerConnectionStatsReady(StatsReport[] reports) {
        Log.d(TAG, "onPeerConnectionStatsReady");
    }

    @Override
    public void onPeerConnectionError(String description) {
        Log.d(TAG, description);
    }

    @Override
    public void onCallcenterDisconnected() {
        Log.d(TAG, "onCallcenterDisconnected");
    }


    public interface CallingStatus {
        void onReadyToCall();
    }

    public interface CallingSignalInterface {
        void onConnected();

        void onDisconnected();

        void onOfferReceived(String from, JSONObject data);

        void onAnswerReceived(String from, JSONObject data);

        void onIceCandidateReceived(String from, JSONObject data);

        void onCallRejected(String from);

        void onCallAccepted(String from);

        void onCallClosed(String from);


    }


    WebSocketChannel socket = null;
    String partner = "";
    public boolean isInitiator = false;
    String loginId = "";
    Handler handler = null;
    String endPoint = "";
    private Context context;
    String uri = "";

    CallingStatus currentCall = null;

    ArrayList<CallingSignalInterface> listeners = new ArrayList();


    private SurfaceViewRenderer pipRenderer;

    private SurfaceViewRenderer fullscreenRenderer;

    private static PeerConnectionClient peerConnectionClient = null;
    private PeerConnectionClient.PeerConnectionParameters peerConnectionParameters = null;
    private SignalConnectionParameter signalConnectionParameter;

    Semaphore semaphore = new Semaphore(NUMBER_OF_READY_NEED_TO_SEND);

    private final ProxyVideoSink remoteProxyRenderer = new ProxyVideoSink();
    private final ProxyVideoSink localProxyVideoSink = new ProxyVideoSink();
    private final List<VideoSink> remoteSinks = new ArrayList<>();
    VideoCapturer videoCapturer = null;
    final PeerConnectionFactory.Options options = new PeerConnectionFactory.Options();
    boolean loopback = true;
    EglBase eglBase;
    boolean videoCallEnabled = false;


    public void StartToCallWithPartnerId(String partnerId, boolean isInitiator) {
        if (loopback) {
            options.networkIgnoreMask = 0;
        }
        this.partner = partnerId;

        this.isInitiator = isInitiator;
    }


    public CallingSignal(Context context, SurfaceViewRenderer pipRenderer, SurfaceViewRenderer fullscreenRenderer, boolean videoCallEnabled) {

        needToRestartCall = true;

        this.callDuration = 0;
        this.context = context;
        this.eglBase = EglBase.create();

        this.videoCallEnabled = videoCallEnabled;

        if (true == this.videoCallEnabled) {
            videoCapturer = createVideoCapturer();

            this.pipRenderer = pipRenderer;
            this.fullscreenRenderer = fullscreenRenderer;

            this.pipRenderer.init(eglBase.getEglBaseContext(), null);
            this.pipRenderer.setScalingType(RendererCommon.ScalingType.SCALE_ASPECT_FIT);
            this.fullscreenRenderer.init(eglBase.getEglBaseContext(), null);
            this.fullscreenRenderer.setScalingType(RendererCommon.ScalingType.SCALE_ASPECT_FILL);

            this.pipRenderer.setZOrderMediaOverlay(true);
            this.pipRenderer.setEnableHardwareScaler(true /* enabled */);
            this.fullscreenRenderer.setEnableHardwareScaler(false /* enabled */);
            remoteSinks.add(remoteProxyRenderer);
        }


        HandlerThread handlerThread = new HandlerThread(TAG);
        handlerThread.start();
        handler = new Handler(handlerThread.getLooper());
        socket = new WebSocketChannel(handler, this);
        endPoint = PeerConnectionConfig.callingEndPoint;

        peerConnectionParameters =
                new PeerConnectionClient.PeerConnectionParameters(videoCallEnabled, loopback,
                        PeerConnectionConfig.tracing, PeerConnectionConfig.videoWidth, PeerConnectionConfig.videoHeight, PeerConnectionConfig.videoFps,
                        PeerConnectionConfig.videoMaxBitRate,
                        PeerConnectionConfig.videoCodec,
                        PeerConnectionConfig.hwDecodeEnable,
                        true,
                        PeerConnectionConfig.audioMaxBitRate,
                        PeerConnectionConfig.audioCodec,
                        false,
                        false,
                        false,
                        false,
                        false,
                        false,
                        false,
                        false,
                        false,
                        null);

        peerConnectionClient = new PeerConnectionClient(SDKApplication.getAppContext(), eglBase, peerConnectionParameters, this);
        if (loopback)
            options.networkIgnoreMask = 0;

        peerConnectionClient.createPeerConnectionFactory(options);


        this.localProxyVideoSink.setTarget(pipRenderer);
        this.remoteProxyRenderer.setTarget(fullscreenRenderer);

    }


    public void StopCapture() {
        if (null != peerConnectionClient)
            peerConnectionClient.stopVideoSource();
    }

    public void ResumeCapture() {
        if (null != peerConnectionClient)
            peerConnectionClient.startVideoSource();
    }

    VideoCapturer createCameraCapturer(CameraEnumerator enumerator) {
        final String[] deviceNames = enumerator.getDeviceNames();

        // First, try to find front facing camera
        Logging.d(TAG, "Looking for front facing cameras.");
        for (String deviceName : deviceNames) {
            if (enumerator.isFrontFacing(deviceName)) {
                Logging.d(TAG, "Creating front facing camera capturer.");
                VideoCapturer videoCapturer = enumerator.createCapturer(deviceName, null);

                if (videoCapturer != null) {
                    return videoCapturer;
                }
            }
        }

        // Front facing camera not found, try something else
        Logging.d(TAG, "Looking for other cameras.");
        for (String deviceName : deviceNames) {
            if (!enumerator.isFrontFacing(deviceName)) {
                Logging.d(TAG, "Creating other camera capturer.");
                VideoCapturer videoCapturer = enumerator.createCapturer(deviceName, null);

                if (videoCapturer != null) {
                    return videoCapturer;
                }
            }
        }

        return null;
    }

    VideoCapturer createVideoCapturer() {

        videoCapturer = createCameraCapturer(new Camera2Enumerator(this.context));
        //    videoCapturer = createCameraCapturer(new Camera1Enumerator(captureToTexture()));


//        String videoFileAsCamera = getIntent().getStringExtra(EXTRA_VIDEO_FILE_AS_CAMERA);
//        if (videoFileAsCamera != null) {
//            try {
//                videoCapturer = new FileVideoCapturer(videoFileAsCamera);
//            } catch (IOException e) {
//                reportError("Failed to open video file for emulated camera");
//                return null;
//            }
//        } else if (screencaptureEnabled) {
//            return createScreenCapturer();
//        } else if (useCamera2()) {
//            if (!captureToTexture()) {
//                reportError(getString(R.string.camera2_texture_only_error));
//                return null;
//            }
//
//            Logging.d(TAG, "Creating capturer using camera2 API.");
//            videoCapturer = createCameraCapturer(new Camera2Enumerator(this));
//        } else {
//            Logging.d(TAG, "Creating capturer using camera1 API.");
//            videoCapturer = createCameraCapturer(new Camera1Enumerator(captureToTexture()));
//        }
//        if (videoCapturer == null) {
//            reportError("Failed to open camera");
//            return null;
//        }
        return videoCapturer;
    }


    public void connect(final String userId) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                loginId = userId;
                uri = generateLoginURI(userId).toString();
                socket.connect(uri, false);
            }
        });
    }

    public void SwitchCamera() {
        handler.post(new Runnable() {
            @Override
            public void run() {
                if (peerConnectionClient != null) {
                    peerConnectionClient.switchCamera();
                }
            }
        });

    }

    public void disconnect() {
        handler.post(new Runnable() {
            @Override
            public void run() {
                socket.disconnect();
            }
        });


    }

    private String generateLoginURI(String userId) {
        String ret = "";
        try {
            ret = new URI(endPoint + "/?id=" + userId).toString();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return ret;
    }


    @Override
    public void onChannelOpen() {
        if (false == reconnect) {
            nofifyOnConnected();
            SendReady();
        } else {

            if (isInitiator) {

                if (null != peerConnectionClient)
                    peerConnectionClient.restartOffer();

            } else {
                SendRestart();
            }

            reconnect = false;
        }

    }

    @Override
    public void onChannelError(String error) {
        Log.d(TAG, "onChannelError: " + error);
    }


    public void ReleaseReconnectSemaphore() {
        int permisson = semaphreReconnection.availablePermits();
        if (permisson <= 0) {
            semaphreReconnection.release(1);
        }
    }

    @Override
    public void onChannelClosed(String reason) {
        Log.d(TAG, "onChannelClosed:" + reason);

        if (false == needToRestartCall)
            return;


        socket.disconnect();
        connectedToNetwork = false;

        if (semaphreReconnection.tryAcquire()) {

            new CountDownTimer(30000, 2000) {
                @Override
                public void onTick(long millisUntilFinished) {
                    if (ConnectivityChangeReceiver.getInstance().isConnectedToInternet(SDKApplication.getAppContext())) {
                        if (!connectedToNetwork) {
                            connectedToNetwork = true;

                            new Handler().post(new Runnable() {
                                @Override
                                public void run() {
                                    if (!uri.equals("")) {
                                        reconnect = true;
                                        socket.connect(uri, false);
                                        cancel();
                                        ReleaseReconnectSemaphore();

                                    }
                                }
                            });


                        }
                    } else {
                        connectedToNetwork = false;

                    }
                }

                @Override
                public void onFinish() {

                    if (false == connectedToNetwork) {

                    }
                    cancel();
                    ReleaseReconnectSemaphore();
                }
            }.start();
        }


    }

    public void release() {
        try {
            remoteProxyRenderer.setTarget(null);
            localProxyVideoSink.setTarget(null);
            if (pipRenderer != null) {
                pipRenderer.release();
                pipRenderer = null;
            }

            if (fullscreenRenderer != null) {
                fullscreenRenderer.release();
                fullscreenRenderer = null;
            }
            if (peerConnectionClient != null) {
                peerConnectionClient.close();
                peerConnectionClient = null;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onChannelMessage(String message) {

        try {

            JSONArray messageObject = new JSONArray(message);
            if (messageObject.length() >= 2) {
                partner = messageObject.getString(0).toString();
                JSONObject jsonObject = messageObject.getJSONObject(1);
                Log.d(TAG + "MESS", "Received Message" + message);

                do {
                    if (!jsonObject.isNull(SDP)) {
                        SessionDescription sdp = new SessionDescription(SessionDescription.Type.fromCanonicalForm(jsonObject.getString("type")), jsonObject.getString("sdp"));

                        if (SessionDescription.Type.OFFER == sdp.type) {
                            onReceivedOffer(partner, jsonObject);
                        } else
                            onReceivedAnswer(partner, jsonObject);

                        break;
                    }

                    if (!jsonObject.isNull(CANDIDATE)) {
                        onReceivedIceCandidate(partner, jsonObject.getJSONObject(CANDIDATE));

                        break;
                    }

                    if (!jsonObject.isNull(RESPONE)) {
                        String action = jsonObject.getString(RESPONE);

                        if (ACCEPT.matches(action))
                            notifyOnCallAccepted(partner);

                        if (REJECT.matches(action))
                            notifyOnCallRejected(partner);

                        if (CLOSED.matches(action))
                            notifyOnCallClosed(partner);

                        break;
                    }

                    if (!jsonObject.isNull(STATUS)) {

                        String status = jsonObject.getString(STATUS);

                        if (CLOSED.matches(status))
                            notifyOnCallClosed(partner);

                        if (READY.matches(status))
                            onReceiveReady();

                        if (DISCONNECT.matches(status))
                            notifyOnDisconnect();

                        if (RESTART.matches(status))
                            onReceiveRestart();


                        break;
                    }


                }
                while (false);

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void addListener(CallingSignalInterface listener) {
        listeners.add(listener);
    }

    public void removeListener(CallingSignalInterface listener) {
        listeners.remove(listener);
    }

    public void nofifyOnConnected() {
        for (CallingSignalInterface callingSignalInterface : listeners) {
            callingSignalInterface.onConnected();
        }
    }


    public void onReceivedIceCandidate(String from, JSONObject jsonObject) {

        BWF_CMM_CallManager.startDurationCall();

        if (null != peerConnectionClient)
            try {
                peerConnectionClient.addRemoteIceCandidate(new IceCandidate(jsonObject.getString("sdpMid"), jsonObject.getInt("sdpMLineIndex"), jsonObject.getString("candidate")));
            } catch (JSONException e) {
                e.printStackTrace();
            }

    }


    public void onReceivedOffer(String from, JSONObject jsonObject) {
        sendAcceptcall();
        if (false == isStreaming) {
            if (null != peerConnectionClient) {
                signalConnectionParameter = new SignalConnectionParameter(PeerConnectionConfig.iceServers, false, loginId, "", "", null, new ArrayList<>());
                try {
                    peerConnectionClient.setRemoteDescription(new SessionDescription(SessionDescription.Type.OFFER, jsonObject.getString("sdp")));
                    peerConnectionClient.createAnswer();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }


    }


    public void onReceivedAnswer(String from, JSONObject jsonObject) {
        if (null != peerConnectionClient) {
            try {
                if (!jsonObject.isNull("sdp")) {
                    String sdp = jsonObject.getString("sdp");
                    String type = jsonObject.getString("type");
                    peerConnectionClient.setRemoteDescription(new SessionDescription(SessionDescription.Type.fromCanonicalForm(type.toLowerCase()), sdp));
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


    }

    public void notifyOnCallAccepted(String from) {
        for (CallingSignalInterface callingSignalInterface : listeners) {
            callingSignalInterface.onCallAccepted(from);
        }
    }

    public void notifyOnCallRejected(String from) {
        for (CallingSignalInterface callingSignalInterface : listeners) {
            callingSignalInterface.onCallRejected(from);
        }
    }

    public void notifyOnReadyToCall() {
//        for (CallingSignalInterface callingSignalInterface : listeners) {
//            callingSignalInterface.onReadyToCall();
//        }
    }


    public boolean onToggleMic() {
        if (null != peerConnectionClient) {
            micEnabled = !micEnabled;
            peerConnectionClient.setAudioEnabled(micEnabled);
        }
        return micEnabled;
    }


    public void onReceiveRestart() {
        if (isInitiator)
            if (null != peerConnectionClient)
                peerConnectionClient.restartOffer();

    }

    public void onReceiveReady() {
        if (null != peerConnectionClient) {
            if (semaphore.tryAcquire()) {
                SendReady();
                signalConnectionParameter = new SignalConnectionParameter(PeerConnectionConfig.iceServers, true, loginId, "", "", null, new ArrayList<>());

                peerConnectionClient.createPeerConnection2(localProxyVideoSink, remoteSinks, videoCapturer, signalConnectionParameter);

                if (isInitiator)
                    peerConnectionClient.createOffer();
            }
        }
    }


    private void notifyOnCallClosed(String from) {

        BWF_CMM_Protocol_deligate.notifyOnEndedCall(BWF_CMM_CallManager.getDurationInSecond(), null);

        for (CallingSignalInterface callingSignalInterface : listeners) {
            callingSignalInterface.onCallClosed(from);
        }
    }

    private void notifyOnDisconnect() {

        disconnect();

        BWF_CMM_Protocol_deligate.notifyOnEndedCall(BWF_CMM_CallManager.getDurationInSecond(), null);

        for (CallingSignalInterface callingSignalInterface : listeners) {
            callingSignalInterface.onDisconnected();
        }
    }


    public void SendReady() {


        handler.post(new Runnable() {
            @Override
            public void run() {
                JSONArray sendJsonArray = new JSONArray();
                sendJsonArray.put(partner);
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("status", "ready");
                    sendJsonArray.put(jsonObject);
                    Log.d(TAG, "Send Ready Call" + sendJsonArray);
                    if (numberOfReadyRemind > 0) {
                        socket.sendMessage(sendJsonArray.toString());
                        numberOfReadyRemind -= 1;
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    public void SendRestart() {

        handler.post(new Runnable() {
            @Override
            public void run() {
                JSONArray sendJsonArray = new JSONArray();
                sendJsonArray.put(partner);
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("status", "restart");
                    sendJsonArray.put(jsonObject);
                    Log.d(TAG, "Send Ready Call" + sendJsonArray);

                    socket.sendMessage(sendJsonArray.toString());

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }


    public void sendClosedcall() {
        handler.post(new Runnable() {
            @Override
            public void run() {
                JSONArray sendJsonArray = new JSONArray();
                sendJsonArray.put(partner);
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("status", "closed");
                    sendJsonArray.put(jsonObject);
                    Log.d(TAG, "Send Close Call" + sendJsonArray);
                    socket.sendMessage(sendJsonArray.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    void sendRejectcall() {
        handler.post(new Runnable() {
            @Override
            public void run() {
                JSONArray sendJsonArray = new JSONArray();
                sendJsonArray.put(partner);
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("response", "reject");
                    sendJsonArray.put(jsonObject);
                    Log.d(TAG, "Send Rejected Call" + sendJsonArray);
                    socket.sendMessage(sendJsonArray.toString());

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void sendAcceptcall() {
        handler.post(new Runnable() {
            @Override
            public void run() {
                JSONArray sendJsonArray = new JSONArray();
                sendJsonArray.put(partner);
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("response", "accept");
                    sendJsonArray.put(jsonObject);

                    Log.d(TAG, "Send Accepted Call" + sendJsonArray);
                    socket.sendMessage(sendJsonArray.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
    }

    void setEndPoint(String endPoint) {
        this.endPoint = endPoint;
    }

    public void sendOffer(SessionDescription sessionDescription) {

        handler.post(new Runnable() {
            @Override
            public void run() {
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("type", sessionDescription.type.canonicalForm());
                    jsonObject.put("sdp", sessionDescription.description);

                    Log.d(TAG, "send offer" + jsonObject);
                    JSONArray sendJsonArray = new JSONArray();
                    sendJsonArray.put(partner);
                    sendJsonArray.put(jsonObject);
                    socket.sendMessage(sendJsonArray.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
    }

    public void sendIceCandidate(IceCandidate iceCandidate) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("sdpMLineIndex", iceCandidate.sdpMLineIndex);
                    jsonObject.put("sdpMid", iceCandidate.sdpMid);
                    jsonObject.put("candidate", iceCandidate.sdp);

                    JSONObject jsonSend = new JSONObject();
                    jsonSend.put("candidate", jsonObject);

                    JSONArray sendJsonArray = new JSONArray();
                    sendJsonArray.put(partner);
                    sendJsonArray.put(jsonSend);

                    Log.d(TAG, "send candidate" + jsonSend);
                    socket.sendMessage(sendJsonArray.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void sendAnswer(SessionDescription sessionDescription) {
        handler.post(new Runnable() {
            @Override
            public void run() {

                try {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("type", sessionDescription.type.canonicalForm());
                    jsonObject.put("sdp", sessionDescription.description);

                    Log.d(TAG, "send offer::$jsonObject");
                    JSONArray sendJsonArray = new JSONArray();
                    sendJsonArray.put(partner);
                    sendJsonArray.put(jsonObject);

                    socket.sendMessage(sendJsonArray.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });


    }

    private static class ProxyVideoSink implements VideoSink {
        private VideoSink target;

        @Override
        synchronized public void onFrame(VideoFrame frame) {
            if (target == null) {
                Logging.d(TAG, "Dropping frame in proxy because target is null.");
                return;
            }

            target.onFrame(frame);
        }

        synchronized public void setTarget(VideoSink target) {
            this.target = target;
        }
    }

    public void CloseStream() {
        if (null != peerConnectionClient)
            peerConnectionClient.close();
    }
}
