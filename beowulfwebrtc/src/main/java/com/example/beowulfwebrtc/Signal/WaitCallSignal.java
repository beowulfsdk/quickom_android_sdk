package com.example.beowulfwebrtc.Signal;

import android.os.Handler;
import android.os.HandlerThread;
import android.util.Log;

import com.example.beowulfwebrtc.AppData.WebRTCCallInfo;
import com.example.beowulfwebrtc.CallCenterManager.CallCenterInstance;
import com.example.beowulfwebrtc.Interface.ChannelMessage;
import com.example.beowulfwebrtc.PeerSettings.PeerConnectionConfig;
import com.example.beowulfwebrtc.WebRTCRequestMessage;
import com.example.beowulfwebrtc.WebSocket.WebSocketChannel;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;

import javax.net.ssl.HttpsURLConnection;

public class WaitCallSignal implements ChannelMessage.ChannelEvents {
    // wait for incomming call request

    enum ConnectionState {
        NEW,
        CONNECTED,
        CLOSED,
        ERROR
    }


    String TAG = "WaitingCallSignal";
    WebSocketChannel socket = null;

    Handler handler = null;

    String endPoint = "";

    String loginId = "";

    ConnectionState state = ConnectionState.NEW;


    private static WaitCallSignal instance;

    WaitCallSignal() {
        HandlerThread handlerThread = new HandlerThread(TAG);
        handlerThread.start();

        handler = new Handler(handlerThread.getLooper());

        socket = new WebSocketChannel(handler, this);

        endPoint = PeerConnectionConfig.waitCallEndPoint;
    }

    public static WaitCallSignal getInstance() {
        if (null == instance) {
            synchronized (WaitCallSignal.class) {
                if (null == instance) {
                    instance = new WaitCallSignal();
                }
            }
        }
        return instance;
    }

    public void connect(String userId) {

        loginId = userId;
        handler.post(new Runnable() {
            @Override
            public void run() {
                socket.connect(generateLoginURI(userId).toString(),true);
            }
        });
    }

    private String generateLoginURI(String userId) {
        return endPoint + "/callee?calleeId=" + userId;
    }

    @Override
    public void onChannelOpen() {
        state=ConnectionState.CONNECTED;

        CallCenterInstance.notifyOnLogin();
    }

    @Override
    public void onChannelError(String error) {
        state=ConnectionState.ERROR;
        CallCenterInstance.nofityOnError(error);
    }

    @Override
    public void onChannelClosed(String reason) {
       // Log.d("WAITINGCALL",reason);
        state=ConnectionState.CLOSED;
        CallCenterInstance.notifyOnLogout();
    }

    @Override
    public void onChannelMessage(String strMessage) {

        CallCenterInstance.notifyOnMessage(strMessage);

        WebRTCRequestMessage message = new WebRTCRequestMessage(strMessage);
        String ation = message.getAction();
        String requestType = message.getRequest_type();

        WebRTCCallInfo.setMyPartnerCallId(message.getCallerId());
        WebRTCCallInfo.setQrAlias(message.getAlias());
       // WebRTCCallInfo.setMyCallId(message.getRequestId());

        do {
            if (ation.equals("cancel-call")) {
                CallCenterInstance.nofityOnCallCanceled();
                break;
            }
            if (ation.equals("call")) {
                if (!requestType.equals("chat"))
                    CallCenterInstance.nofityOnIncommingCall();
                else
                    CallCenterInstance.AutoAcceptIncommingRequest();
            }

        }
        while (false);


    }

}
