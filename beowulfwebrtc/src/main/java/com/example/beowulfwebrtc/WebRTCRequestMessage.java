package com.example.beowulfwebrtc;


import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class WebRTCRequestMessage implements Parcelable {
    public String getCallerId() {
        return callerId;
    }
    public String getAction() {
        return action;
    }
    public String getAlias() {
        return alias;
    }
    public String getRequest_type() {
        return request_type;
    }
    public String getCall_type() {
        return call_type;
    }


    String action = "";
    String callerId = "";

    public String getRequestId() {
        return requestId;
    }

    String requestId = "";

    String alias = "";
    String label = "";
    String group_name = "";
    String type = "";//personal or ...
    String call_type = "";//video audio...
    String request_type = "";

    public String getDisplayName() {
        return displayName;
    }

    String displayName = "";

    boolean allow_chat = true;
    boolean allow_call = true;

    public String getLabel() {
        return label;
    }

    public WebRTCRequestMessage(String payload) {
        try {
            JSONArray array = new JSONArray(payload);

            if (array.length() > 0) {
                JSONObject jsonObject = array.getJSONObject(0);

                if (!jsonObject.isNull("callerId"))
                    callerId = jsonObject.getString("callerId");
                if (!jsonObject.isNull("requestId"))
                    requestId = jsonObject.getString("requestId");

                if (!jsonObject.isNull("action"))
                    action = jsonObject.getString("action");

                if (!jsonObject.isNull("requestType"))
                    request_type = jsonObject.getString("requestType");

                if (!jsonObject.isNull("displayName"))
                    displayName = jsonObject.getString("displayName");


                if (!jsonObject.isNull("qrData")) {
                    JSONObject jsonQrData = jsonObject.getJSONObject("qrData");
                    if (!jsonQrData.isNull("alias"))
                        alias = jsonQrData.getString("alias");

                    if (!jsonQrData.isNull("label"))
                        label = jsonQrData.getString("label");

                    if (!jsonQrData.isNull("group_name"))
                        group_name = jsonQrData.getString("group_name");

                    if (!jsonQrData.isNull("type"))
                        type = jsonQrData.getString("type");

                    if (!jsonQrData.isNull("call_type"))
                        call_type = jsonQrData.getString("call_type");

                    if (!jsonQrData.isNull("allow_chat"))
                        allow_chat = jsonQrData.getBoolean("allow_chat");

                    if (!jsonQrData.isNull("allow_call"))
                        allow_call = jsonQrData.getBoolean("allow_call");

                }

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public WebRTCRequestMessage(String t, String body) {
        try {

            JSONObject jsonObject = new JSONObject(body);

            if (!jsonObject.isNull("callerId"))
                callerId = jsonObject.getString("callerId");
            if (!jsonObject.isNull("requestId"))
                requestId = jsonObject.getString("requestId");

            if (!jsonObject.isNull("action"))
                action = jsonObject.getString("action");

            if (!jsonObject.isNull("requestType"))
                request_type = jsonObject.getString("requestType");

            if (!jsonObject.isNull("displayName"))
                displayName = jsonObject.getString("displayName");


            if (!jsonObject.isNull("qrData")) {
                JSONObject jsonQrData = jsonObject.getJSONObject("qrData");
                if (!jsonQrData.isNull("alias"))
                    alias = jsonQrData.getString("alias");

                if (!jsonQrData.isNull("label"))
                    label = jsonQrData.getString("label");

                if (!jsonQrData.isNull("group_name"))
                    group_name = jsonQrData.getString("group_name");

                if (!jsonQrData.isNull("type"))
                    type = jsonQrData.getString("type");

                if (!jsonQrData.isNull("call_type"))
                    call_type = jsonQrData.getString("call_type");

                if (!jsonQrData.isNull("allow_chat"))
                    allow_chat = jsonQrData.getBoolean("allow_chat");

                if (!jsonQrData.isNull("allow_call"))
                    allow_call = jsonQrData.getBoolean("allow_call");

            }


        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    protected WebRTCRequestMessage(Parcel in) {
        callerId = in.readString();
        requestId = in.readString();
        action = in.readString();
        alias = in.readString();
        label = in.readString();
        group_name = in.readString();
        type = in.readString();
        call_type = in.readString();
        displayName = in.readString();
        allow_chat = in.readByte() != 0;
        allow_call = in.readByte() != 0;
    }

    public static final Creator<WebRTCRequestMessage> CREATOR = new Creator<WebRTCRequestMessage>() {
        @Override
        public WebRTCRequestMessage createFromParcel(Parcel in) {
            return new WebRTCRequestMessage(in);
        }

        @Override
        public WebRTCRequestMessage[] newArray(int size) {
            return new WebRTCRequestMessage[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(callerId);
        parcel.writeString(requestId);
        parcel.writeString(action);
        parcel.writeString(alias);
        parcel.writeString(label);
        parcel.writeString(group_name);
        parcel.writeString(type);
        parcel.writeString(call_type);
        parcel.writeString(displayName);
        parcel.writeByte((byte) (allow_chat ? 1 : 0));
        parcel.writeByte((byte) (allow_call ? 1 : 0));
    }
}


