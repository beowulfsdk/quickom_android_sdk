package com.example.beowulfwebrtc.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.beowulfwebrtc.AppData.WebRTCCallInfo;
import com.example.beowulfwebrtc.AppRTCAudioManager;
import com.example.beowulfwebrtc.R;
import com.example.beowulfwebrtc.SDKProtocol.BWF_CMM_CallManager;
import com.example.beowulfwebrtc.SDKProtocol.BWF_CMM_Error;
import com.example.beowulfwebrtc.SDKProtocol.BWF_CMM_MessageDef;
import com.example.beowulfwebrtc.SDKProtocol.BWF_CMM_Protocol;
import com.example.beowulfwebrtc.SDKProtocol.BWF_CMM_Protocol_deligate;
import com.example.beowulfwebrtc.ServiceInstanceCall.CallService;
import com.example.beowulfwebrtc.Signal.CallingSignal;
import com.example.beowulfwebrtc.util.Const;

import org.webrtc.SurfaceViewRenderer;

import java.util.ArrayList;
import java.util.Set;

public class WebRTCOutGoing extends AppCompatActivity implements View.OnClickListener, View.OnTouchListener, BWF_CMM_Protocol {

    Button bt_cancel_end_call;
    CallingSignal callingSignal;
    TextView tv_calling_alias;
    boolean isAcceptCall = false;
    Chronometer current_call_timer;
    private long lastClickTime = 0;

    ConstraintLayout cl_speaker_mic;
    ImageButton ib_mic;
    ImageButton ib_speaker;
    private AppRTCAudioManager audioManager = null;
    boolean isSpeakerEnable = true;

    MediaPlayer player = null;
    int media_player_id = BWF_CMM_CallManager.outgoingMediaPlayerResource;

    boolean isVideoCall = false;

    @Nullable
    private SurfaceViewRenderer pipRenderer;
    @Nullable
    private SurfaceViewRenderer fullscreenRenderer;
    private View _root;
    LinearLayout pre_view_cam;
    private int _xDelta;
    private int _yDelta;
    final DisplayMetrics metrics = new DisplayMetrics();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_rtcout_going);

        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        pipRenderer = findViewById(R.id.pip_video_view);
        fullscreenRenderer = findViewById(R.id.fullscreen_video_view);
        pre_view_cam = findViewById(R.id.pre_view_cam);
        _root = findViewById(R.id.video_frame);
        pre_view_cam.setOnTouchListener(this);


        if (0 != media_player_id) {
            player = MediaPlayer.create(this, media_player_id);
            player.setLooping(true);
        }

        cl_speaker_mic = findViewById(R.id.cl_speaker_mic);

        ib_mic = findViewById(R.id.ib_mic);
        ib_speaker = findViewById(R.id.ib_speaker);

        ib_mic.setOnClickListener(this);
        ib_speaker.setOnClickListener(this);

        checkAndRequestCallPermissions();

        BWF_CMM_Protocol_deligate.AddListener(this);
        bt_cancel_end_call = findViewById(R.id.bt_cancel_end_call);
        bt_cancel_end_call.setOnClickListener(this);
        current_call_timer = findViewById(R.id.current_call_timer);
        tv_calling_alias = findViewById(R.id.tv_calling_alias);

        isVideoCall = getIntent().getBooleanExtra(Const.IS_VIDEO_CALL, true);

        //callingSignal = new CallingSignal(this, pipRenderer, fullscreenRenderer, true);
        callingSignal = new CallingSignal(this, null, null, false);

        BWF_CMM_CallManager.getInstance().setCallingSignal(callingSignal);


        audioManager = AppRTCAudioManager.create(getApplicationContext());
        // Store existing audio settings and change audio mode to
        // MODE_IN_COMMUNICATION for best possible VoIP performance.

        audioManager.start(new AppRTCAudioManager.AudioManagerEvents() {
            // This method will be called each time the number of available audio
            // devices has changed.
            @Override
            public void onAudioDeviceChanged(
                    AppRTCAudioManager.AudioDevice audioDevice, Set<AppRTCAudioManager.AudioDevice> availableAudioDevices) {
                onAudioManagerDevicesChanged(audioDevice, availableAudioDevices);
            }
        });

        audioManager.setDefaultAudioDevice(isSpeakerEnable ? AppRTCAudioManager.AudioDevice.SPEAKER_PHONE : AppRTCAudioManager.AudioDevice.EARPIECE);

        startService(new Intent(WebRTCOutGoing.this, CallService.class));
        Stream();

    }

    private void onAudioManagerDevicesChanged(
            final AppRTCAudioManager.AudioDevice device, final Set<AppRTCAudioManager.AudioDevice> availableDevices) {

    }


    public void connectWebRTC(String id) {
        callingSignal.connect(id);
    }

    @Override
    public void onClick(View v) {


        if (R.id.bt_cancel_end_call == v.getId()) {
            if (!isAcceptCall) {
                BWF_CMM_CallManager.cancelCurrentCall();
            } else {
                BWF_CMM_CallManager.endCurrentCall();
            }
            v.setEnabled(false);
            finish();
        }

        if (R.id.ib_mic == v.getId()) {

            if (callingSignal.onToggleMic()) {
                ib_mic.setImageResource(R.drawable.ic_mic_white);
            } else {
                ib_mic.setImageResource(R.drawable.ic_microphone_off);
            }

        }
        if (R.id.ib_speaker == v.getId()) {
            if (onToggleSpeaker()) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ib_speaker.setImageResource(R.drawable.ic_speaker_white);
                    }
                });

            } else
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ib_speaker.setImageResource(R.drawable.ic_speaker_off);
                    }
                });

//            }
        }


        v.startAnimation(AnimationUtils.loadAnimation(WebRTCOutGoing.this, R.anim.image_click));
        if (SystemClock.elapsedRealtime() - lastClickTime < 1000) {
            return;
        }
    }


    public boolean onToggleSpeaker() {
        if (audioManager != null) {
            isSpeakerEnable = !isSpeakerEnable;
            audioManager.setDefaultAudioDevice(isSpeakerEnable ? AppRTCAudioManager.AudioDevice.SPEAKER_PHONE : AppRTCAudioManager.AudioDevice.EARPIECE);
        }
        return isSpeakerEnable;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        callingSignal.setNeedToRestartCall(false);
        callingSignal.setStreaming(false);

        if (audioManager != null) {
            audioManager.stop();
            audioManager = null;
        }

        BWF_CMM_CallManager.getInstance().ClearCallingSignal();
        BWF_CMM_Protocol_deligate.RemoveListener(this);
        BWF_CMM_CallManager.AllowToReceiveRequest();
        BWF_CMM_CallManager.AllowToReceiveRespone();
        BWF_CMM_CallManager.resetDurationCall();
        StopSound();

        if (CallService.isInstance())
            CallService.getInstance().stopSelf();
        callingSignal.CloseStream();
        callingSignal = null;
    }

    @Override
    public void bwf_cmm_didStartWithIdentifier(String identifier) {

    }

    @Override
    public void onBackPressed() {

    }

    @Override
    public void bwf_cmm_failedWithError(BWF_CMM_Error error) {

    }

    @Override
    public void bwf_cmm_Calling_CallDidChangeToStateOutgoing(boolean isVideo) {


    }

    public void Stream() {
        String partnerId = WebRTCCallInfo.getMyPartnerCallId();
        String myCallId = WebRTCCallInfo.getMyCallId();

        callingSignal.StartToCallWithPartnerId(partnerId, true);
        connectWebRTC(myCallId);

    }

    @Override
    public void bwf_cmm_Call_Denied(String fromIdentifier, String reason) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
             //   Toast.makeText(WebRTCOutGoing.this, "Callee " + reason, Toast.LENGTH_SHORT).show();

            }
        });
        finish();
    }

    @Override
    public void bwf_cmm_generatePushContent(String data, String identifier, BWF_CMM_MessageDef.bwf_cmm_push_content_type type) {

    }

    @Override
    public void bwf_cmm_receivePushContent(String data, String identifier, BWF_CMM_MessageDef.bwf_cmm_push_content_type type) {

    }

    @Override
    public void bwf_cmm_Calling_CallDidChangeToStateReceivedIncomingCallFrom(String fromIdentifier, boolean isVideo) {

    }

    @Override
    public void bwf_cmm_Calling_CallDidChangeToStateOutgoing(String fromIdentifier, boolean isVideo) {

    }

    @Override
    public void bwf_cmm_Calling_CallDidChangeToStateConnected(String fromIdentifier, boolean isVideo) {

        StopSound();
        isAcceptCall = true;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                cl_speaker_mic.setVisibility(View.VISIBLE);
                current_call_timer.setVisibility(View.VISIBLE);
                tv_calling_alias.setVisibility(View.GONE);
                bt_cancel_end_call.setText("End call");
                current_call_timer.setBase(SystemClock.elapsedRealtime());
                current_call_timer.start();

            }
        });

    }

    @Override
    public void bwf_cmm_Calling_CallDidChangeToStateEnd(long totalDuration, BWF_CMM_Error error) {

        if (null != error) {
            String reason = error.description;
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                   // Toast.makeText(WebRTCOutGoing.this, reason + " _ duration:" + totalDuration, Toast.LENGTH_SHORT).show();

                }
            });
        } else {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    //Toast.makeText(WebRTCOutGoing.this, " _ duration:" + totalDuration, Toast.LENGTH_SHORT).show();

                }
            });
        }

        finish();
    }

    @Override
    public void bwf_cmm_receivePushContent(String base64DecodedStr) {

    }

    @Override
    public void bwf_cmm_generatePushContent(String base64EncodedStr, String _request_id) {

    }

    public void PlaySound() {
        if (null != player && BWF_CMM_CallManager.shouldPlay_for_out)
            player.start();
    }

    public void StopSound() {
        if (null != player)
            player.stop();
    }

    private void checkAndRequestCallPermissions() {
        ArrayList<String> permissionsList = new ArrayList<>();

        int recordAudio = getPackageManager().checkPermission(Manifest.permission.RECORD_AUDIO, getPackageName());
        permissionsList.add(Manifest.permission.RECORD_AUDIO);
        int camera = getPackageManager().checkPermission(Manifest.permission.CAMERA, getPackageName());
        permissionsList.add(Manifest.permission.CAMERA);
        int phone = getPackageManager().checkPermission(Manifest.permission.CALL_PHONE, getPackageName());
        permissionsList.add(Manifest.permission.CALL_PHONE);
        int data_read = getPackageManager().checkPermission(Manifest.permission.READ_EXTERNAL_STORAGE, getPackageName());
        permissionsList.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        int data_write = getPackageManager().checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, getPackageName());
        permissionsList.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);


        if (recordAudio != PackageManager.PERMISSION_GRANTED) {
            permissionsList.add(Manifest.permission.RECORD_AUDIO);
        }
        if (camera != PackageManager.PERMISSION_GRANTED) {
            permissionsList.add(Manifest.permission.CAMERA);
        }

        if (data_read != PackageManager.PERMISSION_GRANTED) {
            permissionsList.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }

        if (data_write != PackageManager.PERMISSION_GRANTED) {
            permissionsList.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }


        if (permissionsList.size() > 0) {
            String[] permissions = new String[permissionsList.size()];
            permissions = permissionsList.toArray(permissions);
            ActivityCompat.requestPermissions(this, permissions, 0);
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        PlaySound();
        callingSignal.ResumeCapture();
    }

    @Override
    public void onStart() {
        super.onStart();
        if (null != callingSignal)
            callingSignal.ResumeCapture();

    }

    //
    @Override
    public void onStop() {
        super.onStop();
        if (null != callingSignal)
            callingSignal.StopCapture();
    }

    @Override
    public boolean onTouch(View view, MotionEvent event) {
        pre_view_cam.getHeight();

        final int X = (int) event.getRawX();
        final int Y = (int) event.getRawY();
        switch (event.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN:
                ConstraintLayout.LayoutParams lParams = (ConstraintLayout.LayoutParams) view.getLayoutParams();
                _xDelta = X - lParams.leftMargin;
                _yDelta = Y - lParams.topMargin;
                break;
            case MotionEvent.ACTION_UP:
                break;
            case MotionEvent.ACTION_POINTER_DOWN:
                break;
            case MotionEvent.ACTION_POINTER_UP:
                break;
            case MotionEvent.ACTION_MOVE:
                android.util.Log.d("MARGIN__bview ", view.getX() + "" + view.getY());

                ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams) view.getLayoutParams();

                layoutParams.leftMargin = X - _xDelta;
                layoutParams.topMargin = Y - _yDelta;
                layoutParams.rightMargin = -250;
                layoutParams.bottomMargin = -250;

                android.util.Log.d("MARGIN__l", "" + layoutParams.leftMargin);
                android.util.Log.d("MARGIN__t", "" + layoutParams.topMargin);
                android.util.Log.d("MARGIN__r", "" + layoutParams.rightMargin);
                android.util.Log.d("MARGIN__b", "" + layoutParams.bottomMargin);
                android.util.Log.d("MARGIN__b", "" + (X + pre_view_cam.getWidth()));

                if (view.getX() < 0)
                    view.setX(0);
                if (view.getY() < 0)
                    view.setY(0);

                if ((view.getX() + pre_view_cam.getWidth()) > metrics.widthPixels)
                    view.setX(metrics.widthPixels - pre_view_cam.getWidth());

                if ((view.getY() + pre_view_cam.getHeight() > metrics.heightPixels))
                    view.setY(metrics.heightPixels - pre_view_cam.getHeight());


                view.setLayoutParams(layoutParams);
                break;
        }
        _root.invalidate();

        return true;
    }
}
