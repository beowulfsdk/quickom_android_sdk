package com.example.beowulfwebrtc.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.PowerManager;
import android.os.SystemClock;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.beowulfwebrtc.AppData.WebRTCCallInfo;
import com.example.beowulfwebrtc.AppRTCAudioManager;
import com.example.beowulfwebrtc.BWBroadcastReciever.ConnectivityChangeReceiver;
import com.example.beowulfwebrtc.R;
import com.example.beowulfwebrtc.SDKApplication;
import com.example.beowulfwebrtc.SDKProtocol.BWF_CMM_CallManager;
import com.example.beowulfwebrtc.SDKProtocol.BWF_CMM_Error;
import com.example.beowulfwebrtc.SDKProtocol.BWF_CMM_MessageDef;
import com.example.beowulfwebrtc.SDKProtocol.BWF_CMM_Protocol;
import com.example.beowulfwebrtc.SDKProtocol.BWF_CMM_Protocol_deligate;
import com.example.beowulfwebrtc.ServiceInstanceCall.CallService;
import com.example.beowulfwebrtc.Signal.CallingSignal;
import com.example.beowulfwebrtc.util.Const;

import org.json.JSONObject;
import org.webrtc.SurfaceViewRenderer;

import java.util.ArrayList;
import java.util.Set;

public class WebRTCIncomming extends AppCompatActivity implements View.OnClickListener, View.OnTouchListener, BWF_CMM_Protocol {

    Button bt_cancel_end_call;
    Button bt_answer_call;
    View v_margin;
    CallingSignal callingSignal;
    TextView tv_calling_alias;
    TextView caller_identify;
    boolean isAcceptCall = false;
    Chronometer current_call_timer;
    private long lastClickTime = 0;
    ConstraintLayout cl_speaker_mic;
    ImageButton ib_mic;
    ImageButton ib_speaker;

    private AppRTCAudioManager audioManager = null;
    boolean isSpeakerEnable = true;

    boolean connectedToNetwork = false;
    MediaPlayer player = null;
    int media_player_id = BWF_CMM_CallManager.incommingMediaPlayerResource;


    static WebRTCIncomming instance = null;

    @Nullable
    private SurfaceViewRenderer pipRenderer;
    @Nullable
    private SurfaceViewRenderer fullscreenRenderer;
    private View _root;
    LinearLayout pre_view_cam;
    private int _xDelta;
    private int _yDelta;
    final DisplayMetrics metrics = new DisplayMetrics();


    public boolean auto_accept_call = false;


    public static boolean isInstance() {
        return null != instance;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        BWF_CMM_Protocol_deligate.AddListener(this);
        BWF_CMM_CallManager.StopTimeOutIncommingTimer();
        BWF_CMM_CallManager.CancelIncommingNotification();

        instance = this;

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON |
                WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON |
                WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS |
                WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
        setContentView(R.layout.activity_web_rtcincomming);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(Const.INCOMMING_CALL_NOTIFICATION);

        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        pipRenderer = findViewById(R.id.pip_video_view);
        fullscreenRenderer = findViewById(R.id.fullscreen_video_view);
        pre_view_cam = findViewById(R.id.pre_view_cam);
        _root = findViewById(R.id.video_frame);
        pre_view_cam.setOnTouchListener(this);


        if (getIntent().hasExtra(Const.AUTO_ACCEPT_INCOMMING_CALL))
        {
            auto_accept_call=getIntent().getBooleanExtra(Const.AUTO_ACCEPT_INCOMMING_CALL,false);

        }



        WakeScreenUp();

        if (0 != media_player_id) {
            player = MediaPlayer.create(this, media_player_id);
            player.setLooping(true);
        }




        cl_speaker_mic = findViewById(R.id.cl_speaker_mic);

        ib_mic = findViewById(R.id.ib_mic);
        ib_speaker = findViewById(R.id.ib_speaker);

        ib_mic.setOnClickListener(this);
        ib_speaker.setOnClickListener(this);

        checkAndRequestCallPermissions();

        bt_cancel_end_call = findViewById(R.id.bt_cancel_end_call);
        bt_answer_call = findViewById(R.id.bt_answer_call);
        bt_cancel_end_call.setOnClickListener(this);
        bt_answer_call.setOnClickListener(this);
        v_margin = findViewById(R.id.v_margin);

        current_call_timer = findViewById(R.id.current_call_timer);
        tv_calling_alias = findViewById(R.id.tv_calling_alias);
        caller_identify = findViewById(R.id.caller_identify);

        String displayLable = WebRTCCallInfo.getQrAlias();
        if (!"".matches(displayLable))
            caller_identify.setText(displayLable);


        //callingSignal = new CallingSignal(this, pipRenderer, fullscreenRenderer, true);
        callingSignal = new CallingSignal(this, null, null, false);


        BWF_CMM_CallManager.getInstance().setCallingSignal(callingSignal);
        // Create and audio manager that will take care of audio routing,
        // audio modes, audio device enumeration etc.
        audioManager = AppRTCAudioManager.create(getApplicationContext());
        // Store existing audio settings and change audio mode to
        // MODE_IN_COMMUNICATION for best possible VoIP performance.

        audioManager.start(new AppRTCAudioManager.AudioManagerEvents() {
            // This method will be called each time the number of available audio
            // devices has changed.
            @Override
            public void onAudioDeviceChanged(
                    AppRTCAudioManager.AudioDevice audioDevice, Set<AppRTCAudioManager.AudioDevice> availableAudioDevices) {
                onAudioManagerDevicesChanged(audioDevice, availableAudioDevices);
            }
        });

        audioManager.setDefaultAudioDevice(isSpeakerEnable ? AppRTCAudioManager.AudioDevice.SPEAKER_PHONE : AppRTCAudioManager.AudioDevice.EARPIECE);

        if (true == auto_accept_call)
            PickTheCall();
    }


    public void WakeScreenUp() {
        PowerManager pm = (PowerManager) this.getSystemService(Context.POWER_SERVICE);
        boolean isScreenOn = pm.isScreenOn();
//        Log.e("screen on.................................", "" + isScreenOn);
        if (!isScreenOn) {
            PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP | PowerManager.ON_AFTER_RELEASE, "android:wake");
            wl.acquire(10000);
            PowerManager.WakeLock wl_cpu = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "android:cpu");

            wl_cpu.acquire(10000);

            getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON | WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
        // sendNotification();
    }

    private void onAudioManagerDevicesChanged(
            final AppRTCAudioManager.AudioDevice device, final Set<AppRTCAudioManager.AudioDevice> availableDevices) {
    }

    public void connectWebRTC(String id) {
        callingSignal.connect(id);
    }


    @Override
    protected void onDestroy() {

        callingSignal.setNeedToRestartCall(false);
        callingSignal.setStreaming(false);


        if (audioManager != null) {
            audioManager.stop();
            audioManager = null;
        }
        super.onDestroy();

        BWF_CMM_CallManager.getInstance().ClearCallingSignal();
        BWF_CMM_Protocol_deligate.RemoveListener(this);
        BWF_CMM_CallManager.AllowToReceiveRequest();
        BWF_CMM_CallManager.AllowToReceiveRespone();
        BWF_CMM_CallManager.resetDurationCall();
        callingSignal.CloseStream();
        callingSignal = null;
        StopSound();
        instance = null;

        if(CallService.isInstance())
            CallService.getInstance().stopSelf();

    }




    @Override
    public void onBackPressed() {

    }

    public void PickTheCall() {
        BWF_CMM_CallManager.acceptIncomingCall();

        String partnerId = WebRTCCallInfo.getMyPartnerCallId();
        String myCallId = WebRTCCallInfo.getMyCallId();
        callingSignal.StartToCallWithPartnerId(partnerId, false);

       // android.util.Log.d("PICKCALLXXX", partnerId + "   __  " + myCallId);
        bt_cancel_end_call.setText("End call");
        connectWebRTC(myCallId);

    }

    @Override
    public void onClick(View v) {


        if (R.id.bt_cancel_end_call == v.getId()) {
            if (!isAcceptCall)
                BWF_CMM_CallManager.denyIncomingCall();
            else
                BWF_CMM_CallManager.endCurrentCall();

            v.setEnabled(false);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                finishAndRemoveTask();
            }
            else
                finish();
        }

        if (R.id.bt_answer_call == v.getId()) {
            PickTheCall();
            v.setEnabled(false);
        }

        if (R.id.ib_mic == v.getId()) {
            if (callingSignal.onToggleMic()) {
                ib_mic.setImageResource(R.drawable.ic_mic_white);
            } else {
                ib_mic.setImageResource(R.drawable.ic_microphone_off);
            }

        }
        if (R.id.ib_speaker == v.getId()) {
            if (onToggleSpeaker()) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ib_speaker.setImageResource(R.drawable.ic_speaker_white);
                    }
                });

            } else
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ib_speaker.setImageResource(R.drawable.ic_speaker_off);
                    }
                });

//            }
        }


        v.startAnimation(AnimationUtils.loadAnimation(WebRTCIncomming.this, R.anim.image_click));
        if (SystemClock.elapsedRealtime() - lastClickTime < 1000) {
            return;
        }
    }


    public boolean onToggleSpeaker() {
        if (audioManager != null) {
            isSpeakerEnable = !isSpeakerEnable;
            audioManager.setDefaultAudioDevice(isSpeakerEnable ? AppRTCAudioManager.AudioDevice.SPEAKER_PHONE : AppRTCAudioManager.AudioDevice.EARPIECE);
        }
        return isSpeakerEnable;
    }


    @Override
    public void bwf_cmm_didStartWithIdentifier(String identifier) {
        String androidxxx = "ddd";

    }

    @Override
    public void bwf_cmm_failedWithError(BWF_CMM_Error error) {
        String androidxxx = "ddd";

    }

    @Override
    public void bwf_cmm_Calling_CallDidChangeToStateOutgoing(boolean isVideo) {
        String androidxxx = "ddd";

    }


    @Override
    public void bwf_cmm_Call_Denied(String fromIdentifier, String reason) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //Toast.makeText(WebRTCIncomming.this, "Callee  denied current incomming call", Toast.LENGTH_SHORT).show();

            }
        });
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            finishAndRemoveTask();
        }
        else
            finish();
    }

    @Override
    public void bwf_cmm_generatePushContent(String data, String identifier, BWF_CMM_MessageDef.bwf_cmm_push_content_type type) {
        String androidxxx = "ddd";

    }

    @Override
    public void bwf_cmm_receivePushContent(String data, String identifier, BWF_CMM_MessageDef.bwf_cmm_push_content_type type) {
        String androidxxx = "ddd";

    }

    @Override
    public void bwf_cmm_Calling_CallDidChangeToStateReceivedIncomingCallFrom(String fromIdentifier, boolean isVideo) {
        String androidxxx = "ddd";

    }

    @Override
    public void bwf_cmm_Calling_CallDidChangeToStateOutgoing(String fromIdentifier, boolean isVideo) {
        String androidxxx = "ddd";

    }

    @Override
    public void bwf_cmm_Calling_CallDidChangeToStateConnected(String fromIdentifier, boolean isVideo) {
        isAcceptCall = true;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                StopSound();

                cl_speaker_mic.setVisibility(View.VISIBLE);

                current_call_timer.setVisibility(View.VISIBLE);
                tv_calling_alias.setVisibility(View.GONE);




                bt_answer_call.setVisibility(View.GONE);
                v_margin.setVisibility(View.GONE);
                current_call_timer.setBase(SystemClock.elapsedRealtime());
                current_call_timer.start();

            }
        });


        startService(new Intent(WebRTCIncomming.this, CallService.class));

    }

    @Override
    public void bwf_cmm_Calling_CallDidChangeToStateEnd(long totalDuration, BWF_CMM_Error error) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //Toast.makeText(WebRTCIncomming.this, "Current call is over( duration:" + totalDuration + ")", Toast.LENGTH_SHORT).show();

            }
        });
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            finishAndRemoveTask();
        }
        else
            finish();

    }

    @Override
    public void bwf_cmm_receivePushContent(String base64DecodedStr) {

    }

    @Override
    public void bwf_cmm_generatePushContent(String base64EncodedStr, String _request_id) {

    }

    public void PlaySound() {
        if (null != player && BWF_CMM_CallManager.shouldPlay_for_in)
            player.start();
    }

    public void StopSound() {
        if (null != player)
            player.stop();
    }

    private void checkAndRequestCallPermissions() {
        ArrayList<String> permissionsList = new ArrayList<>();

        int recordAudio = getPackageManager().checkPermission(Manifest.permission.RECORD_AUDIO, getPackageName());
        permissionsList.add(Manifest.permission.RECORD_AUDIO);
        int camera = getPackageManager().checkPermission(Manifest.permission.CAMERA, getPackageName());
        permissionsList.add(Manifest.permission.CAMERA);
        int phone = getPackageManager().checkPermission(Manifest.permission.CALL_PHONE, getPackageName());
        permissionsList.add(Manifest.permission.CALL_PHONE);
        int data_read = getPackageManager().checkPermission(Manifest.permission.READ_EXTERNAL_STORAGE, getPackageName());
        permissionsList.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        int data_write = getPackageManager().checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, getPackageName());
        permissionsList.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);


        if (recordAudio != PackageManager.PERMISSION_GRANTED) {
            permissionsList.add(Manifest.permission.RECORD_AUDIO);
        }
        if (camera != PackageManager.PERMISSION_GRANTED) {
            permissionsList.add(Manifest.permission.CAMERA);
        }

        if (data_read != PackageManager.PERMISSION_GRANTED) {
            permissionsList.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }

        if (data_write != PackageManager.PERMISSION_GRANTED) {
            permissionsList.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }


        if (permissionsList.size() > 0) {
            String[] permissions = new String[permissionsList.size()];
            permissions = permissionsList.toArray(permissions);
            ActivityCompat.requestPermissions(this, permissions, 0);
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        PlaySound();
        callingSignal.ResumeCapture();
    }

    @Override
    public void onStart() {
        super.onStart();
        if (null != callingSignal)
            callingSignal.ResumeCapture();

    }

    //
    @Override
    public void onStop() {
        super.onStop();
        if (null != callingSignal)
            callingSignal.StopCapture();
    }


    @Override
    public boolean onTouch(View view, MotionEvent event) {
        pre_view_cam.getHeight();

        final int X = (int) event.getRawX();
        final int Y = (int) event.getRawY();
        switch (event.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN:
                ConstraintLayout.LayoutParams lParams = (ConstraintLayout.LayoutParams) view.getLayoutParams();
                _xDelta = X - lParams.leftMargin;
                _yDelta = Y - lParams.topMargin;
                break;
            case MotionEvent.ACTION_UP:
                break;
            case MotionEvent.ACTION_POINTER_DOWN:
                break;
            case MotionEvent.ACTION_POINTER_UP:
                break;
            case MotionEvent.ACTION_MOVE:
              //  android.util.Log.d("MARGIN__bview ", view.getX() + "" + view.getY());

                ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams) view.getLayoutParams();

                layoutParams.leftMargin = X - _xDelta;
                layoutParams.topMargin = Y - _yDelta;
                layoutParams.rightMargin = -250;
                layoutParams.bottomMargin = -250;

               // android.util.Log.d("MARGIN__l", "" + layoutParams.leftMargin);
               // android.util.Log.d("MARGIN__t", "" + layoutParams.topMargin);
                //android.util.Log.d("MARGIN__r", "" + layoutParams.rightMargin);
               // android.util.Log.d("MARGIN__b", "" + layoutParams.bottomMargin);
                //android.util.Log.d("MARGIN__b", "" + (X + pre_view_cam.getWidth()));

                if (view.getX() < 0)
                    view.setX(0);
                if (view.getY() < 0)
                    view.setY(0);

                if ((view.getX() + pre_view_cam.getWidth()) > metrics.widthPixels)
                    view.setX(metrics.widthPixels - pre_view_cam.getWidth());

                if ((view.getY() + pre_view_cam.getHeight() > metrics.heightPixels))
                    view.setY(metrics.heightPixels - pre_view_cam.getHeight());


                view.setLayoutParams(layoutParams);
                break;
        }
        _root.invalidate();

        return true;
    }

}
