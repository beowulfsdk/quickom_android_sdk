package com.beowulfchain.beowulfchainsdk.AppData.AppData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class QrData {
    public String getAlias() {
        return alias;
    }

    String alias = "";

    public String getLabel() {
        return label;
    }

    String label = "";

    public String getGroup_name() {
        return group_name;
    }

    String group_name = "";

    public String getType() {
        return type;
    }

    String type = "";

    public String getCall_type() {
        return call_type;
    }

    String call_type = "";
    boolean allow_chat = false;
    boolean allow_call = false;
    boolean me = false;

    public boolean get_me() {
        return me;
    }

    public String GetSipIdCallDirect() {
        String ret = "";
        if (true == me
                && users.size() > 0) {
            ret = users.get(0).sip_id;
        }
        return ret;
    }

    public boolean isCalLDirect()
    {
        return me;
    }


    public ArrayList<datausers> getUsers() {
        return users;
    }

    ArrayList<datausers> users = new ArrayList<>();

    public String getUrl() {
        return url;
    }

    String url;

    public class datausers {

        String name = "";
        String sip_id = "";
        String chat_id = "";
        String reference_id = "";

        public String getName() {
            return name;
        }

        public String getReference_id() {
            return reference_id;
        }

        public String getSip_id() {
            return sip_id;
        }



        public String getChat_id() {
            return chat_id;
        }



        datausers(String name, String sip_id, String chat_id, String reference_id) {
            this.name = name;
            this.sip_id = sip_id;
            this.chat_id = chat_id;
            this.reference_id = reference_id;
        }
    }

    public QrData(String jsonData) {
        alias = "";
        label = "";
        group_name = "";
        type = "";
        call_type = "";
        allow_chat = false;
        allow_call = false;
        me = false;
        url = "";

        try {
            JSONObject object = new JSONObject(jsonData);
            if (!object.isNull("alias"))
                alias = object.getString("alias");

            if (!object.isNull("label"))
                label = object.getString("label");

            if (!object.isNull("group_name"))
                group_name = object.getString("group_name");



            if (!object.isNull("type"))
                type = object.getString("type");


            if (!object.isNull("call_type"))
                call_type = object.getString("call_type");

            if (!object.isNull("allow_chat"))
                allow_chat = object.getBoolean("allow_chat");

            if (!object.isNull("allow_call"))
                allow_call = object.getBoolean("allow_call");

            if (!object.isNull("me"))
                me = object.getBoolean("me");


            if(allow_call==false
            &&allow_chat==true)
            {
                call_type="chat";
            }


            if (!object.isNull("users")) {
                JSONArray userArray = object.getJSONArray("users");
                for (int i = 0; i < userArray.length(); i++) {
                    String name = "";
                    String sip_id = "";
                    String chat_id = "";
                    String reference_id = "";
                    JSONObject user = userArray.getJSONObject(i);
                    if (!user.isNull("name"))
                        name = user.getString("name");

                    if (!user.isNull("sip_id"))
                        sip_id = user.getString("sip_id");

                    if (!user.isNull("chat_id"))
                        chat_id = user.getString("chat_id");

                    if (!user.isNull("reference_id"))
                        reference_id = user.getString("reference_id");

                    users.add(new datausers(name, sip_id, chat_id, reference_id));


                    if (true == me)
                        break;

                }
            }

            if (!object.isNull("url"))
                url = object.getString("url");

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
