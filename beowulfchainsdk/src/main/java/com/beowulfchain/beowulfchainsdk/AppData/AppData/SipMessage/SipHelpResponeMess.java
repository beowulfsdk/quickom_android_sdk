package com.beowulfchain.beowulfchainsdk.AppData.AppData.SipMessage;

import org.json.JSONException;
import org.json.JSONObject;

public class SipHelpResponeMess extends SipHelpMess{

    JSONObject sipMessobject = null;

    JSONObject data = new JSONObject();
    JSONObject extraInfo = new JSONObject();
    JSONObject qrcode = new JSONObject();


    String time = "";
    String helperChatId = "";

    String responseTime = "";

    String request_type = "";
    String apiURL = "";
    String au = "";
    String label = "";

    public String getAlias() {
        return alias;
    }

    String alias = "";
    String group_name = "";
    String type_qr = "";

    public  SipHelpResponeMess(JSONObject object) {
        sipMessobject = object;

        try {
            if (!object.isNull("msgId"))
                this.msgId = object.getString("msgId");

            if (!object.isNull("type"))
                type = object.getString("type");

            if (!object.isNull("name"))
                name = object.getString("name");

            if (!object.isNull("time"))
                time = object.getString("time");

            if (!object.isNull("data")) {
                data = object.getJSONObject("data");

                if (!data.isNull("helperChatId"))
                    helperChatId = data.getString("helperChatId");

                if (!data.isNull("userChatId"))
                    user_chat_id = data.getString("userChatId");

                if (!data.isNull("requestTime"))
                    requestTime = data.getString("requestTime");

                if (!data.isNull("responseTime"))
                    responseTime = data.getString("responseTime");

                if (!data.isNull("requestId"))
                    requestId = data.getString("requestId");

                if (!data.isNull("extraInfo")) {
                    extraInfo = data.getJSONObject("extraInfo");

                    if (!extraInfo.isNull("request_type"))
                        request_type = extraInfo.getString("request_type");

                    if (!extraInfo.isNull("apiURL"))
                        apiURL = extraInfo.getString("apiURL");

                    if (!extraInfo.isNull("qrcode")) {
                        qrcode = extraInfo.getJSONObject("qrcode");

                        if (!qrcode.isNull("label"))
                            label = qrcode.getString("label");

                        if (!qrcode.isNull("alias"))
                            alias = qrcode.getString("alias");

                        if (!qrcode.isNull("group_name"))
                            group_name = qrcode.getString("group_name");

                        if (!qrcode.isNull("type"))
                            type_qr = qrcode.getString("type");

                    }

                    if (!extraInfo.isNull("au"))
                        au = extraInfo.getString("au");
                }

            }


        } catch (JSONException e) {
            e.printStackTrace();
        }


    }


    public SipHelpResponeMess(String msgId, JSONObject object)
    {
        sipMessobject=new JSONObject();
        this.data=object;
        this.msgId=msgId;
        this.type="notification";
        this.name="helpResponse";
        try {
            sipMessobject.put("data", this.data);
            sipMessobject.put("msgId", this.msgId);
            sipMessobject.put("type", this.type);
            sipMessobject.put("name", this.name);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    public  SipHelpResponeMess(JSONObject object,String msgFrom,String msgTo) {
        sipMessobject = object;
        this.messageFrom=msgFrom;
        this.messageTo=msgTo;

        try {
            if (!object.isNull("msgId"))
                this.msgId = object.getString("msgId");

            if (!object.isNull("type"))
                type = object.getString("type");

            if (!object.isNull("name"))
                name = object.getString("name");

            if (!object.isNull("time"))
                time = object.getString("time");

            if (!object.isNull("data")) {
                data = object.getJSONObject("data");

                if (!data.isNull("helperChatId"))
                    helperChatId = data.getString("helperChatId");

                if (!data.isNull("userChatId"))
                    user_chat_id = data.getString("userChatId");

                if (!data.isNull("requestTime"))
                    requestTime = data.getString("requestTime");

                if (!data.isNull("responseTime"))
                    responseTime = data.getString("responseTime");

                if (!data.isNull("requestId"))
                    requestId = data.getString("requestId");

                if (!data.isNull("extraInfo")) {
                    extraInfo = data.getJSONObject("extraInfo");

                    if (!extraInfo.isNull("request_type"))
                        request_type = extraInfo.getString("request_type");

                    if (!extraInfo.isNull("apiURL"))
                        apiURL = extraInfo.getString("apiURL");

                    if (!extraInfo.isNull("qrcode")) {
                        qrcode = extraInfo.getJSONObject("qrcode");

                        if (!qrcode.isNull("label"))
                            label = qrcode.getString("label");

                        if (!qrcode.isNull("alias"))
                            alias = qrcode.getString("alias");

                        if (!qrcode.isNull("group_name"))
                            group_name = qrcode.getString("group_name");

                        if (!qrcode.isNull("type"))
                            type_qr = qrcode.getString("type");

                    }

                    if (!extraInfo.isNull("au"))
                        au = extraInfo.getString("au");
                }

            }


        } catch (JSONException e) {
            e.printStackTrace();
        }


    }


    public String GetJsonString()
    {
        return sipMessobject.toString();
    }




}
