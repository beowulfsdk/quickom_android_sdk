package com.beowulfchain.beowulfchainsdk.AppData.AppData.ChatMessage;

import java.util.ArrayList;

public class MsgDefinition
{
    public static ArrayList<String> commonMessageType = new ArrayList<String>() {{
        add("text");
        add("notification");
        add("image");
    }};
    public enum enum_common_message_type {
        text(0),
        notification(1),
        image(2);
        private int val;
        enum_common_message_type(int val) {
            this.val = val;
        }

        public int getVal() {
            return this.val;
        }
        public String getStringVal()
        {
            return commonMessageType.get(this.val);
        }
    }





    public static ArrayList<String> messageSendStatus = new ArrayList<String>() {{
        add("send");
        add("delivered");
        add("seen");
    }};

    public enum enum_message_send_status {
        send(0),
        delivered(1) ,
        seen(2);
        private int val;
        enum_message_send_status(int val) {
            this.val = val;
        }
        public int getVal() {
            return this.val;
        }
        public String getStringVal()
        {
            return messageSendStatus.get(this.val);
        }
    }



}
