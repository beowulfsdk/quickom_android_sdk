package com.beowulfchain.beowulfchainsdk.AppData.AppData.SipMessage;

import org.json.JSONException;
import org.json.JSONObject;

public class SipHelpDropMess extends SipHelpMess {

    JSONObject sipMessobject = null;

    JSONObject data = new JSONObject();

    String userChatId = "";
    String requestTime = "";
    //String requestId = "";

    String msgId = "";
    String type = "";
    String name = "";

    String helperChatId = "";
    String responseTime = "";


    public SipHelpDropMess(JSONObject object) {
        sipMessobject = object;

        try {

            if (!object.isNull("data")) {
                data = object.getJSONObject("data");

                if (!data.isNull("userChatId"))
                    helperChatId = data.getString("userChatId");

                if (!data.isNull("requestTime"))
                    responseTime = data.getString("requestTime");

                if (!data.isNull("requestId"))
                    requestId = data.getString("requestId");
            }


            if (!object.isNull("msgId"))
                msgId = object.getString("msgId");

            if (!object.isNull("type"))
                type = object.getString("type");
            if (!object.isNull("name"))
                name = object.getString("name");

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    public String GetJsonString() {
        return sipMessobject.toString();
    }

    public SipHelpDropMess(String _userChatId, String _requestTime, String _requestId, String _msgId) {
        try {
            userChatId = _userChatId;
            userChatId = _userChatId;
            requestId = _requestId;
            data = new JSONObject();
            data.put("userChatId", userChatId);
            data.put("requestTime", _requestTime);
            //  data.put("userChatId", userChatId);
            data.put("requestId", requestId);
            sipMessobject = new JSONObject();
            sipMessobject.put("data", data);
            sipMessobject.put("msgId", msgId);
            sipMessobject.put("type", "notification");
            sipMessobject.put("name", "helpDrop");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


}
