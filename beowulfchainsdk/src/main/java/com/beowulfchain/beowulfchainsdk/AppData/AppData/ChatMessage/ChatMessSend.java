package com.beowulfchain.beowulfchainsdk.AppData.AppData.ChatMessage;

import android.graphics.Bitmap;
import android.util.Base64;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class ChatMessSend extends ChatMess {
    public JSONObject getChatMessSendObject() {
        chatMessSendObject = new JSONObject();
        try {
            chatMessSendObject.put("data", this.data);
            chatMessSendObject.put("conversationId", this.conversationId);
            chatMessSendObject.put("msgId", this.msgId);
            chatMessSendObject.put("type", this.type);
            chatMessSendObject.put("pushContent", this.pushContent);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return chatMessSendObject;
    }

    JSONObject chatMessSendObject;

    public ChatMessSend(String content, String messageTo, String messageFrom, String conversationId)//text
    {
        try {
            this.messageFrom = messageFrom;
            this.messageTo = messageTo;
            this.msgId = System.currentTimeMillis() / 1000 + 1000 + "" + (Math.random() % 1000);
            this.data = content;
            this.conversationId = conversationId;
            this.type = "text";
            this.pushContent = content;
            chatMessSendObject = new JSONObject();
            chatMessSendObject.put("data", this.data);
            chatMessSendObject.put("conversationId", this.conversationId);
            chatMessSendObject.put("msgId", this.msgId);
            chatMessSendObject.put("type", this.type);
            chatMessSendObject.put("pushContent", this.pushContent);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        time_stamp = getCurrentTimeInString();
        this.messageFrom = messageFrom;
        this.messageTo = messageTo;

    }

    public ChatMessSend(String contentOrUrl, String messageTo, String messageFrom, String conversationId, boolean isImage)//text
    {
        try {
            this.messageFrom = messageFrom;
            this.messageTo = messageTo;
            this.msgId = System.currentTimeMillis() / 1000 + 1000 + "" + (Math.random() % 1000);
            this.data = contentOrUrl;
            this.conversationId = conversationId;
            this.type = "text";
            this.pushContent = contentOrUrl;
            if(true==isImage)
            {
                this.type = "image";
                pushContent="Image Message";
            }


            chatMessSendObject = new JSONObject();
            chatMessSendObject.put("data", this.data);
            chatMessSendObject.put("conversationId", this.conversationId);
            chatMessSendObject.put("msgId", this.msgId);
            chatMessSendObject.put("type", this.type);
            chatMessSendObject.put("pushContent", this.pushContent);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        time_stamp = getCurrentTimeInString();
        this.messageFrom = messageFrom;
        this.messageTo = messageTo;

    }


    public static String ConvertBitmapToString(Bitmap bitmap){
        String encodedImage = "";

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        try {
            encodedImage= URLEncoder.encode(Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return encodedImage;
    }


    public ChatMessSend(Bitmap bitmap, String messageTo, String messageFrom) {


        String encoded = ConvertBitmapToString(bitmap);

        try {
            this.messageFrom = messageFrom;
            this.messageTo = messageTo;
            this.msgId = System.currentTimeMillis() / 1000 + 1000 + "" + (Math.random() % 1000);
            this.data = encoded;
            this.type = "image";
            this.pushContent = "Image Message";
            chatMessSendObject = new JSONObject();
            chatMessSendObject.put("data", this.data);
            chatMessSendObject.put("msgId", this.msgId);
            chatMessSendObject.put("type", this.type);
            chatMessSendObject.put("pushContent", this.pushContent);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        time_stamp = getCurrentTimeInString();
        this.messageFrom = messageFrom;
        this.messageTo = messageTo;


    }

    public String getCurrentTimeInString() {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat mdformat = new SimpleDateFormat("HH:mm:ss");
        return mdformat.format(calendar.getTime());
    }

    public long getCurrentTimeInLong() {
        return System.currentTimeMillis() / 1000;
    }

    public String GetString() {
        return chatMessSendObject.toString();
    }

}
