package com.beowulfchain.beowulfchainsdk.AppData.AppData;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import org.greenrobot.greendao.annotation.Generated;

@Entity(nameInDb = "activity")
public class ActivityLog {

    @Id(autoincrement = true)
    private Long id;

    @Property(nameInDb = "lable")
    String lable = "";

    @Property(nameInDb = "activity")
    int activity = 2;

    @Property(nameInDb = "chatIdFrom")
    String chatIdFrom;

    @Property(nameInDb = "chatIdTo")
    String chatIdTo;

    @Property(nameInDb = "outGoing")
    boolean outGoing = true;//0->out 1->in


    public String getCurrentTime() {
        String formatedTime=currentTime;
        return formatedTime;

    }

    @Property(nameInDb = "currentTime")
    String currentTime = "";

    public String getCurrentDateWithoutYear() {
        String formatedData=currentDate.substring(0,currentDate.length()-1-4);
        return formatedData;
    }

    public String getCurrentDateWithYear() {

        return currentDate;
    }





    @Property(nameInDb = "currentDate")
    String currentDate = "";

    @Property(nameInDb = "duration")
    long duration = 0;

    public String getPreviousChat() {
        return previousChat;
    }

    @Property(nameInDb = "previousChat")
    String previousChat = "";


    @Property(nameInDb = "conversationId")
    String conversationId = "";


    public ActivityLog(String lable, int activity, String chatIdFrom, String chatIdTo, long duration, boolean outGoing, String conversationId) {
        this.lable = lable;
        this.activity = activity;
        this.chatIdFrom = chatIdFrom;
        this.chatIdTo = chatIdTo;
        this.outGoing = outGoing;
        this.duration = duration;
        this.currentTime = getCurrentTimeData();
        this.currentDate = getCurrentDateData();
        this.conversationId=conversationId;
    }


    public ActivityLog(String lable, int activity, String chatIdFrom, String chatIdTo, boolean outGoing, String conversationId) {
        this.lable = lable;
        this.activity = activity;
        this.chatIdFrom = chatIdFrom;
        this.chatIdTo = chatIdTo;
        this.outGoing = outGoing;
        this.currentTime = getCurrentTimeData();
        this.currentDate = getCurrentDateData();
        this.conversationId = conversationId;
    }


    public void SetPreviousChatText(String previousChat) {
        this.previousChat = previousChat;
    }

    public ActivityLog() {
    }

    @Generated(hash = 499374932)
    public ActivityLog(Long id, String lable, int activity, String chatIdFrom, String chatIdTo, boolean outGoing, String currentTime,
            String currentDate, long duration, String previousChat, String conversationId) {
        this.id = id;
        this.lable = lable;
        this.activity = activity;
        this.chatIdFrom = chatIdFrom;
        this.chatIdTo = chatIdTo;
        this.outGoing = outGoing;
        this.currentTime = currentTime;
        this.currentDate = currentDate;
        this.duration = duration;
        this.previousChat = previousChat;
        this.conversationId = conversationId;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLable() {
        return this.lable;
    }

    public void setLable(String lable) {
        this.lable = lable;
    }

    public int getActivity() {
        return this.activity;
    }

    public void setActivity(int activity) {
        this.activity = activity;
    }

    public String getChatIdFrom() {
        return this.chatIdFrom;
    }

    public void setChatIdFrom(String chatIdFrom) {
        this.chatIdFrom = chatIdFrom;
    }

    public String getChatIdTo() {
        return this.chatIdTo;
    }

    public void setChatIdTo(String chatIdTo) {
        this.chatIdTo = chatIdTo;
    }


    public boolean getOutGoing() {
        return this.outGoing;
    }


    public void setOutGoing(boolean outGoing) {
        this.outGoing = outGoing;
    }


    private String getCurrentTimeData() {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat mdformat = new SimpleDateFormat("HH:mm");
        return mdformat.format(calendar.getTime());

    }

    private String getCurrentDateData() {
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        String formattedDate = df.format(c);
        return formattedDate;
    }

    public void setCurrentTime(String currentTime) {
        this.currentTime = currentTime;
    }

    public void setCurrentDate(String currentDate) {
        this.currentDate = currentDate;
    }

    public long getDuration() {
        return this.duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public void setPreviousChat(String previousChat) {
        this.previousChat = previousChat;
    }


    public String getConversationId() {
        return this.conversationId;
    }


    public void setConversationId(String conversationId) {
        this.conversationId = conversationId;
    }

    public String getCurrentDate() {
        return this.currentDate;
    }


    public enum enum_activity_log {
        chat(0),
        audio(1),
        video(2);

        private int val;

        enum_activity_log(int val) {
            this.val = val;
        }

        public int getVal() {
            return this.val;
        }

//        public String getStringVal() {
//            return messageTransferStatuses.get(this.val);
//        }
    }


}
