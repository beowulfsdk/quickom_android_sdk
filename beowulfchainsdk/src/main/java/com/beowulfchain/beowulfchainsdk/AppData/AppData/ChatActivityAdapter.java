package com.beowulfchain.beowulfchainsdk.AppData.AppData;


import android.content.Context;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.beowulfchain.beowulfchainsdk.MainApplication;
import com.beowulfchain.beowulfchainsdk.R;

import java.util.ArrayList;

public class ChatActivityAdapter extends RecyclerView.Adapter<ChatActivityAdapter.RecyclerViewHolder> {
    public LayoutInflater inflater;
    Context context;

    ArrayList<ActivityLog> listOfActivitiesLog = new ArrayList<>();
    ItemClickListener itemClickListener;

    public ChatActivityAdapter() {
        inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public RecyclerViewHolder onCreateViewHolder(@NonNull final ViewGroup viewGroup, final int i) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        final View view = inflater.inflate(R.layout.custom_activity_chat_log_row, viewGroup, false);
        return new RecyclerViewHolder(view);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(@NonNull RecyclerViewHolder recyclerViewHolder, final int i) {
        ActivityLog data = this.listOfActivitiesLog.get(i);

        recyclerViewHolder.tv_chat_name.setText(data.lable);
        recyclerViewHolder.tv_chat_date.setText(data.getCurrentDateWithYear());
        recyclerViewHolder.tv_chat_time.setText(data.getCurrentTime());
        recyclerViewHolder.tv_chat_text.setText(data.getPreviousChat());
        String msgIdFrom = data.getChatIdFrom();
        String msgIdTo = data.getChatIdTo();
        String chatConversationId = data.getConversationId();

        int numberOfUnseen = MainApplication.GetUnSeenMess(msgIdFrom, msgIdTo, chatConversationId);

        recyclerViewHolder.tv_notify_new_message.setVisibility(View.GONE);
        if (0< numberOfUnseen) {
            recyclerViewHolder.tv_notify_new_message.setVisibility(View.VISIBLE);
            recyclerViewHolder.tv_notify_new_message.setText(""+numberOfUnseen);
        }

        //String a = data.getPreviousChat();
//        if (true == data.outGoing)
//            recyclerViewHolder.tv_chat_name.setText(data.lable + " in");

        recyclerViewHolder.setItemClickListener(new CallLogClickListner() {
            @Override
            public void Click(View view) {
                if (null != itemClickListener) {
                    itemClickListener.onItemClickListener(view, i);
                }

            }
        });

    }


    public void SetNewList(ArrayList<ActivityLog> arrayList) {
        this.listOfActivitiesLog.clear();
        listOfActivitiesLog = arrayList;
    }


    public ActivityLog getActivityLog(int index) {
        return listOfActivitiesLog.get(index);
    }


    @Override
    public int getItemCount() {
        return this.listOfActivitiesLog.size();
    }

    public ChatActivityAdapter(Context context, ArrayList<ActivityLog> listOfActivitiesLog) {
        this.context = context;

        inflater = LayoutInflater.from(context);
        this.listOfActivitiesLog = listOfActivitiesLog;
    }


    public class RecyclerViewHolder extends RecyclerView.ViewHolder {

        private CallLogClickListner mOnClickListener = null;

        ImageView iv_chat_icon;
        TextView tv_chat_name;
        TextView tv_chat_text;
        TextView tv_chat_date;
        TextView tv_chat_time;
        TextView tv_notify_new_message;


        public RecyclerViewHolder(View itemView) {
            super(itemView);
            iv_chat_icon = itemView.findViewById(R.id.iv_chat_icon);
            tv_chat_name = itemView.findViewById(R.id.tv_chat_name);
            tv_chat_text = itemView.findViewById(R.id.tv_chat_text);
            tv_chat_date = itemView.findViewById(R.id.tv_chat_date);
            tv_chat_time = itemView.findViewById(R.id.tv_chat_time);
            tv_notify_new_message = itemView.findViewById(R.id.tv_notify_new_message);


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (null != mOnClickListener)
                        mOnClickListener.Click(v);
                }
            });

        }

        public void setItemClickListener(CallLogClickListner itemClickListener) {
            this.mOnClickListener = itemClickListener;
        }

    }

    public interface CallLogClickListner {
        public void Click(View view);
    }

    public void SetOnItemClickListner(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        public void onItemClickListener(View view, int position);
    }
}