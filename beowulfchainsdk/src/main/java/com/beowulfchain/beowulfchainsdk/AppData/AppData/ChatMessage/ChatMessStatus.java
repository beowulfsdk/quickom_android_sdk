package com.beowulfchain.beowulfchainsdk.AppData.AppData.ChatMessage;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class ChatMessStatus extends ChatMess {
    JSONObject chatMessReceiveObject;


    public JSONObject getChatMessReceiveObject() {
        return chatMessReceiveObject;
    }

    public ChatMessStatus(JSONObject messageObject, String messageFrom, String messageTo) {
        try {
            chatMessReceiveObject = messageObject;
            if (!chatMessReceiveObject.isNull("msgId"))

                this.msgId = chatMessReceiveObject.getString("msgId");


            if (!chatMessReceiveObject.isNull("data")) {
                JSONObject data = new JSONObject(chatMessReceiveObject.getString("data"));
                if (!data.isNull("status")) {
                    status = data.getString("status");
                }
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }

        time_stamp = getCurrentTimeInString();
        this.messageFrom = messageFrom;
        this.messageTo = messageTo;

    }

    public String getCurrentTimeInString() {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat mdformat = new SimpleDateFormat("HH:mm:ss");
        return mdformat.format(calendar.getTime());
    }

    public long getCurrentTimeInLong() {
        return System.currentTimeMillis() / 1000;
    }

    public ChatMessStatus(String msgId, String messageFrom, String messageTo, String status)
    {
        this.msgId=msgId;
        chatMessReceiveObject=new JSONObject();
        this.status=status;
        JSONObject objData=new JSONObject();
        try {


            this.messageTo=messageTo;
            this.messageFrom=messageFrom;


            objData.put("status",status);
            chatMessReceiveObject.put("data",objData);
            chatMessReceiveObject.put("msgId",msgId);
            chatMessReceiveObject.put("type","notification");
            chatMessReceiveObject.put("name","messageStatus");

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String GetString()
    {
        return chatMessReceiveObject.toString();
    }

}
