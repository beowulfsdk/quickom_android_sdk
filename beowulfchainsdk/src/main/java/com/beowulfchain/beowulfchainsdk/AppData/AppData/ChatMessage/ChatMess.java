package com.beowulfchain.beowulfchainsdk.AppData.AppData.ChatMessage;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;

@Entity(nameInDb = "message")
public class ChatMess {


    @Property(nameInDb = "qrname")
    String qrname="";


    @Id(autoincrement = true)
    private Long id;


//    @Property(nameInDb = "qrname")
//    int msg_status= MsgDefinition.enum_message_send_status.send.getVal();
//
    @Property(nameInDb = "msgId")
String msgId = "";


    @Property(nameInDb = "messageFrom")
    String messageFrom = "";

    @Property(nameInDb = "messageTo")
    String messageTo = "";

    @Property(nameInDb = "data")
    String data="";

    @Property(nameInDb = "conversationId")
    String conversationId="";

    @Property(nameInDb = "type")
    String type = "";

    @Property(nameInDb = "pushContent")
    String pushContent = "";

    @Property(nameInDb = "status")
    String status="";

    @Property(nameInDb = "time_stamp")
    String time_stamp = "";

    @Property(nameInDb = "alias")
    String alias = "";




//    public ChatMess(String messageFrom, String messageTo, String data,
//                        String msgId, String type, String pushContent, String status,String alias,
//                        String time_stamp,String conversationId,String conversationIdxxx) {
//
//        this.messageFrom = messageFrom;
//        this.messageTo = messageTo;
//        this.data = data;
//        this.msgId = msgId;
//        this.type = type;
//        this.pushContent = pushContent;
//        this.status = status;
//        this.alias=alias;
//        this.time_stamp = time_stamp;
//        this.conversationId=conversationId;
//        this.conversationIdxxx="aaaaa";
//    }


    public boolean isSeen()
    {
        return MsgDefinition.enum_message_send_status.seen.getStringVal().matches(status);
    }
    public ChatMess(String messageFrom, String messageTo, String data,
                    String msgId, String type, String pushContent, String status, String alias,
                    String time_stamp, String conversationId) {

        this.messageFrom = messageFrom;
        this.messageTo = messageTo;
        this.data = data;
        this.conversationId = conversationId;
        this.msgId = msgId;
        this.type = type;
        this.pushContent = pushContent;
        this.status = status;
        this.alias=alias;
        this.time_stamp = time_stamp;
        this.conversationId=conversationId;
    }
    public ChatMess() {
    }
    @Generated(hash = 1558340952)
    public ChatMess(String qrname, Long id, String msgId, String messageFrom, String messageTo,
            String data, String conversationId, String type, String pushContent, String status,
            String time_stamp, String alias) {
        this.qrname = qrname;
        this.id = id;
        this.msgId = msgId;
        this.messageFrom = messageFrom;
        this.messageTo = messageTo;
        this.data = data;
        this.conversationId = conversationId;
        this.type = type;
        this.pushContent = pushContent;
        this.status = status;
        this.time_stamp = time_stamp;
        this.alias = alias;
    }
    
    public String getMessageTo() {
        return messageTo;
    }
    public String getMessageFrom() {
        return messageFrom;
    }
    public String getType() {
        return type;
    }
    public String getData() {
        return data;
    }
    public String getConversationId()
    {
        return conversationId;
    }
    public String getMsgId() {
        return msgId;
    }
    public String getPushContent() {
        return pushContent;
    }
    public String getStatus() {
        return status;
    }
    public String getTime_stamp() {
        return time_stamp;
    }






    public String GetString()
    {
        return null;
    }


    public void setMessageFrom(String messageFrom) {
        this.messageFrom = messageFrom;
    }
    public void setMessageTo(String messageTo) {
        this.messageTo = messageTo;
    }
    public void setData(String data) {
        this.data = data;
    }
    public void setConversationId(String conversationId){this.conversationId = conversationId;}
    public void setMsgId(String msgId) {
        this.msgId = msgId;
    }
    public void setType(String type) {
        this.type = type;
    }
    public void setPushContent(String pushContent) {
        this.pushContent = pushContent;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public void setTime_stamp(String time_stamp) {
        this.time_stamp = time_stamp;
    }
    public Long getId() {
        return this.id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getQrname() {
        return this.qrname;
    }
    public void setQrname(String qrname) {
        this.qrname = qrname;
    }
    public String getAlias() {
        return this.alias;
    }
    public void setAlias(String alias) {
        this.alias = alias;
    }

}
