package com.beowulfchain.beowulfchainsdk.AppData.AppData.ChatMessage;

import android.graphics.Bitmap;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class ChatMessReceive extends ChatMess {
    JSONObject chatMessReceiveObject;


    public Bitmap getBitmap() {
        return bitmap;
    }

    Bitmap bitmap;


    public JSONObject getChatMessReceiveObject() {
        return chatMessReceiveObject;
    }

    public ChatMessReceive(JSONObject messageObject, String messageFrom, String messageTo) {
        try {
            chatMessReceiveObject = messageObject;
            if (!messageObject.isNull("msgId"))
                msgId = messageObject.getString("msgId");

            if (!messageObject.isNull("type"))
                type = messageObject.getString("type");

            if (!messageObject.isNull("conversationId"))
                conversationId = messageObject.getString("conversationId");

            if (!messageObject.isNull("pushContent"))
                pushContent = messageObject.getString("pushContent");

            if (!messageObject.isNull("data"))
                data = messageObject.getString("data");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        time_stamp = getCurrentTimeInString();
        this.messageFrom = messageFrom;
        this.messageTo = messageTo;

    }

    public String getCurrentTimeInString() {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat mdformat = new SimpleDateFormat("HH:mm:ss");
        return mdformat.format(calendar.getTime());
    }


    public ChatMessReceive(Bitmap bitmap, String messageFrom, String messageTo) {
        this.bitmap = bitmap;
        this.messageFrom = messageFrom;
        this.messageTo = messageTo;
        this.type = "image";
    }


    public long getCurrentTimeInLong() {
        return System.currentTimeMillis() / 1000;
    }


}
