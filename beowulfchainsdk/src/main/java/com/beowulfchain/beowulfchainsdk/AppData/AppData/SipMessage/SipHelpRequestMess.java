package com.beowulfchain.beowulfchainsdk.AppData.AppData.SipMessage;


import com.beowulfchain.beowulfchainsdk.Service.ChatConnection;
import com.beowulfchain.beowulfchainsdk.account.ChatAccountData;
import com.beowulfchain.beowulfchainsdk.account.SipAccountData;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.util.Date;
import java.util.Random;
import java.util.TimeZone;

public class SipHelpRequestMess extends SipHelpMess {

    JSONObject sipMessobject = null;

    JSONObject extraInfo = new JSONObject();
    JSONObject qrcode = new JSONObject();
    JSONObject data = new JSONObject();


    String pushContent = "";
    String au = "";
    String group_name = "";
    String alias = "";
    String label = "";
    String apiURL = "";
    String request_type = "";
    String sip_proxy = "";


    private String user_name = "";


    public String getUser_sip_id() {
        return user_sip_id;
    }

    private String user_sip_id = "";

    public String getUser_chat_id() {
        return user_chat_id;
    }


    private String user_reference_id = "";


    public void MapingHelpRequestToUser(String _user_name, String _user_sip_id, String _user_chat_id, String _user_reference_id) {
        this.user_name = _user_name;
        this.user_sip_id = _user_sip_id;
        this.user_chat_id = _user_chat_id;
        this.user_reference_id = _user_reference_id;
    }

    public String GetTimeStamp()
    {
        String gmtTime="";
        DateFormat df = DateFormat.getTimeInstance();
        df.setTimeZone(TimeZone.getTimeZone("gmt"));
         gmtTime = df.format(new Date());
        return gmtTime;
    }

    public SipHelpRequestMess(String label, String alias, String group_name, String type, boolean _isToEnterpise, String _request_type, String _apiURL, String _au, String ___pushContent) {

        this.requestTime = System.currentTimeMillis() / 1000 + "";
        this.requestId = System.currentTimeMillis() / 1000 + 1000 + "" + (Math.random() % 1000);
        this.msgId = (SipAccountData.getInstance().getUsername().split("@"))[0] + "_" + requestId;
        this.user_chat_id = ChatAccountData.getInstance().getChat_username().split("@")[0];
        this.request_type = _request_type;
        qrcode = FormQrcode(label, alias, group_name, type);
        extraInfo = FormExtraInfo(_isToEnterpise, _request_type, _apiURL, qrcode, _au);
        data = FormData(extraInfo, ___pushContent, this.requestId, this.requestTime, this.user_chat_id);

        sipMessobject = new JSONObject();
        try {
            sipMessobject.put("data", this.data);
            sipMessobject.put("msgId", this.msgId);
            sipMessobject.put("type", "notification");
            sipMessobject.put("name", "helpRequest");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public void set__qrme__(boolean __qrme__) {
        this.__qrme__ = __qrme__;
    }

    public boolean is__qrme__() {
        return __qrme__;
    }

    boolean __qrme__ = false;


    public String getType() {
        return type;
    }


    public void setAlias(String alias) {
        this.alias = alias;
    }

    public void setType(String type) {
        this.type = type;
    }

    // public void setUserChatId(String userChatId) {
    //    this.userChatId = userChatId;
    //  }

    public void setSip_proxy(String sip_proxy) {
        this.sip_proxy = sip_proxy;
    }

    public void setRequest_type(String request_type) {
        this.request_type = request_type;
    }

    public void setGroup_name(String group_name) {
        this.group_name = group_name;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public void setApiURL(String apiURL) {
        this.apiURL = apiURL;
    }

    public void setAu(String au) {
        this.au = au;
    }

    public String getRequest_type() {
        return request_type;
    }


    public String getLabel() {
        return label;
    }


    public String getGroup_name() {
        return group_name;
    }


    public String getApiURL() {
        return apiURL;
    }


    public String getAlias() {
        return alias;
    }


    public String getAu() {
        return au;
    }


    public String getSip_proxy() {
        return sip_proxy;
    }


//    public String getUserChatId() {
//        return userChatId;
//    }


    public String getRequestTime() {
        return requestTime;
    }

    public void setRequestTime(String requestTime) {
        this.requestTime = requestTime;
    }


    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }


    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }


    public SipHelpRequestMess(JSONObject object) {
        sipMessobject = object;
        try {

            if (!object.isNull("data")) {
                this.data = object.getJSONObject("data");

                if (!this.data.isNull("extraInfo")) {
                    this.extraInfo = this.data.getJSONObject("extraInfo");

                    if (!this.extraInfo.isNull("__qrme__"))
                        __qrme__ = this.extraInfo.getBoolean("__qrme__");

                    if (!this.extraInfo.isNull("sip_proxy"))
                        sip_proxy = this.extraInfo.getString("sip_proxy");

                    if (!this.extraInfo.isNull("request_type"))
                        request_type = this.extraInfo.getString("request_type");

                    if (!this.extraInfo.isNull("apiURL"))
                        apiURL = this.extraInfo.getString("apiURL");
                    if (!this.extraInfo.isNull("qrcode")) {
                        this.qrcode = this.extraInfo.getJSONObject("qrcode");

                        if (!this.qrcode.isNull("label"))
                            label = this.qrcode.getString("label");

                        if (!this.qrcode.isNull("alias"))
                            alias = this.qrcode.getString("alias");

                        if (!this.qrcode.isNull("group_name"))
                            group_name = this.qrcode.getString("group_name");

                        if (!this.qrcode.isNull("type"))
                            type = this.qrcode.getString("type");
                    }
                }

                if (!this.data.isNull("userChatId"))
                    this.user_chat_id = this.data.getString("userChatId");

                if (!this.data.isNull("requestTime"))
                    this.requestTime = this.data.getString("requestTime");

                if (!this.data.isNull("requestId"))
                    this.requestId = this.data.getString("requestId");

                if (!this.data.isNull("pushContent"))
                    this.pushContent = this.data.getString("pushContent");


            }

            if (!object.isNull("msgId"))
                this.msgId = object.getString("msgId");
            if (!object.isNull("type"))
                this.type = object.getString("type");
            if (!object.isNull("name"))
            {
                this.name = object.getString("name");
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public JSONObject FormQrcode(String label, String alias, String group_name, String type) {
        qrcode = new JSONObject();
        try {

            this.label = label;
            this.alias = alias;
            this.group_name = group_name;
            this.type = type;

            qrcode.put("label", label);
            qrcode.put("alias", alias);
            qrcode.put("group_name", group_name);
            qrcode.put("type", type);
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
        return qrcode;
    }


    public JSONObject FormExtraInfoSDK(String displayName, String request_type) {
        extraInfo = new JSONObject();
        this.request_type = request_type;
        try {
            extraInfo.put("name", displayName);// true -> enterprise
            extraInfo.put("request_type", request_type);
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
        return extraInfo;
    }

    public JSONObject FormExtraInfo(boolean isToEnterpise, String request_type, String _qrcodeapiURL, JSONObject _qrcode, String au) {

        //String optionalName = IntentCode.optionalName;

        extraInfo = new JSONObject();
        try {

            this.__qrme__ = isToEnterpise;
            this.sip_proxy = SipAccountData.getInstance().getSip_proxy();
            this.request_type = request_type;
            this.apiURL = apiURL;
            this.qrcode = _qrcode;
            this.au = au;

            extraInfo.put("__qrme__", isToEnterpise);// true -> enterprise

          //  extraInfo.put("name", optionalName);// true -> enterprise

            extraInfo.put("sip_proxy", SipAccountData.getInstance().getSip_proxy());
            extraInfo.put("request_type", request_type);
            extraInfo.put("apiURL", apiURL);
            extraInfo.put("qrcode", _qrcode);
            extraInfo.put("au", au);
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
        return extraInfo;
    }

    public JSONObject FormData(JSONObject _extraInfo, String pushContent, String requestId, String requestTime, String userChatId) {
        data = new JSONObject();
        try {

            extraInfo = _extraInfo;
            userChatId = userChatId;
            requestTime = requestTime;
            requestId = requestId;
            pushContent = pushContent;

            data.put("extraInfo", _extraInfo);
            data.put("userChatId", userChatId);
            data.put("requestTime", requestTime);
            data.put("requestId", requestId);
            data.put("pushContent", pushContent);
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
        return data;
    }


    public JSONObject FormDataSDK(JSONObject _extraInfo, String pushContent, String requestId, String requestTime, String userChatId) {
        data = new JSONObject();
        try {

            extraInfo = _extraInfo;
            userChatId = userChatId;
            requestTime = requestTime;
            requestId = requestId;
            pushContent = pushContent;

            data.put("extraInfo", _extraInfo);
            data.put("userChatId", userChatId);
            data.put("requestTime", requestTime);
            data.put("requestId", requestId);
            data.put("pushContent", pushContent);
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
        return data;
    }


    public String SendHelpRequest(String sendToChatId) {
        String domainName = ChatAccountData.getInstance().getChat_domain();
        //ChatService.instance().getChat_username().split("@").length >= 2 ? (ChatAccountData.getInstance().GetUserName().split("@"))[1] : "";
      //  ChatConnection.getInstance().SendHelpMessage(sipMessobject.toString(), sendToChatId, domainName);
        return requestId;
    }


    public String GetJsonString()
    {
        return sipMessobject.toString();
    }

    public String FormHelpDrop() {
        SipHelpDropMess sipDrop = new SipHelpDropMess(this.user_chat_id, this.requestTime, this.requestId, this.msgId);
        return sipDrop.GetJsonString();
    }


    public String FormHelpRespone() {
        SipHelpResponeMess sipHelpRespone = null;
        JSONObject responeData = new JSONObject();
        try {
            responeData.put("requestId", this.requestId);
            responeData.put("responseTime", this.requestTime);
            responeData.put("helperChatId", ChatAccountData.getInstance().getUserChatIdWithOutDomain());
            sipHelpRespone = new SipHelpResponeMess(this.msgId, responeData);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return sipHelpRespone.GetJsonString();
    }

    public String FormEndCall(String callId) {
        SipEndCallDropMess endCallDropMess = new SipEndCallDropMess(callId,  this.msgId);
        return endCallDropMess.GetJsonString();
    }

    public SipHelpRequestMess(String displayName, String ___pushContent, String request_type) {
        this.requestTime = System.currentTimeMillis() / 1000 + "";
        this.requestId = System.currentTimeMillis() / 1000 + 1000 + "" + (Math.random() % 1000);
        this.msgId = (SipAccountData.getInstance().getUsername().split("@"))[0] + "_" + requestId;
        this.user_chat_id = ChatAccountData.getInstance().getChat_username().split("@")[0];
        qrcode = FormQrcode(label, alias, group_name, type);
        extraInfo = FormExtraInfoSDK(displayName, request_type);
        data = FormDataSDK(extraInfo, ___pushContent, this.requestId, this.requestTime, this.user_chat_id);

        sipMessobject = new JSONObject();
        try {
            sipMessobject.put("data", this.data);
            sipMessobject.put("msgId", this.msgId);
            sipMessobject.put("type", "notification");
            sipMessobject.put("name", "helpRequest");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public SipHelpRequestMess(String displayName, String ___pushContent, String request_type,String msgIdFrom,String msgIdTo,String requestId) {

        final int random = new Random().nextInt(61) + 20;

        this.messageFrom=msgIdFrom;
        this.messageTo=msgIdTo;
        this.requestTime = System.currentTimeMillis() / 1000 + "";
        this.requestId = requestId;
        this.msgId = (SipAccountData.getInstance().getUsername().split("@"))[0] + "_" + requestId;
        this.user_chat_id = ChatAccountData.getInstance().getChat_username().split("@")[0];
        qrcode = FormQrcode(label, alias, group_name, type);
        extraInfo = FormExtraInfoSDK(displayName, request_type);
        data = FormDataSDK(extraInfo, ___pushContent, this.requestId, this.requestTime, this.user_chat_id);

        sipMessobject = new JSONObject();
        try {
            sipMessobject.put("data", this.data);
            sipMessobject.put("msgId", this.msgId);
            sipMessobject.put("type", "notification");
            sipMessobject.put("name", "helpRequest");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }




}
