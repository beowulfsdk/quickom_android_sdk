package com.beowulfchain.beowulfchainsdk.AppData.AppData.SipMessage;

import org.json.JSONException;
import org.json.JSONObject;

public class SipEndCallDropMess extends SipHelpMess {

    JSONObject sipMessobject = null;

    JSONObject data = new JSONObject();

    String userChatId = "";
    String requestTime = "";
    //String requestId = "";

    String msgId = "";
    String type = "";
    String name = "";

    String helperChatId = "";
    String responseTime = "";

    public String GetJsonString() {
        return sipMessobject.toString();
    }

    public SipEndCallDropMess(String _callId, String _msgId) {
        try {

         //   {"data":{"callId":"KsT2jQliwf"},"msgId":"30751565084337944_1565458914.033","type":"notification","name":"endCall"};
            data = new JSONObject();

            data.put("callId",_callId);

            sipMessobject = new JSONObject();
            sipMessobject.put("data", data);
            sipMessobject.put("msgId", _msgId);
            sipMessobject.put("type", "notification");
            sipMessobject.put("name", "endCall");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }



}
