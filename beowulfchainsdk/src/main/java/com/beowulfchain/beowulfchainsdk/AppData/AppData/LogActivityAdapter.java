package com.beowulfchain.beowulfchainsdk.AppData.AppData;


import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.beowulfchain.beowulfchainsdk.R;

import java.util.ArrayList;

public class LogActivityAdapter extends RecyclerView.Adapter<LogActivityAdapter.RecyclerViewHolder> {
    public LayoutInflater inflater;
    Context context;

    public ArrayList<ActivityLog> getListOfActivitiesLog() {
        return listOfActivitiesLog;
    }

    ArrayList<ActivityLog> listOfActivitiesLog = new ArrayList<>();
    ItemClickListener itemClickListener;

    public LogActivityAdapter() {
        inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public RecyclerViewHolder onCreateViewHolder(@NonNull final ViewGroup viewGroup, final int i) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        final View view = inflater.inflate(R.layout.custom_activity_call_log_row, viewGroup, false);
        return new RecyclerViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(@NonNull RecyclerViewHolder recyclerViewHolder, final int i) {
        ActivityLog data = this.listOfActivitiesLog.get(i);

        recyclerViewHolder.tv_call_name.setText(data.lable );

     //   if (true == data.outGoing)
     //       recyclerViewHolder.tv_call_name.setText(data.lable + " out");

        if (2 == data.activity)
            recyclerViewHolder.iv_call_icon.setImageResource(R.drawable.ic_activity_log_video);

        if (1 == data.activity)
            recyclerViewHolder.iv_call_icon.setImageResource(R.drawable.ic_activity_log_audio);


        long seconds = data.getDuration();
        long p1 = seconds % 60;
        long p2 = seconds / 60;
        long p3 = p2 % 60;
        p2 = p2 / 60;


        int phut = Math.round(p2*60);
        int phut2 = Math.round(p3);



        recyclerViewHolder.tv_call_duration.setText(p2 + ":" + p3 + ":" + p1);
        if (p1 == 0) {

            recyclerViewHolder.tv_call_duration.setText(phut + phut2 + " minutes");
        }
        if (p1 > 0) {
            recyclerViewHolder.tv_call_duration.setText(phut + phut2 + 1 + " minutes");
        }





        recyclerViewHolder.tv_call_date.setText(data.getCurrentDateWithYear());
        recyclerViewHolder.tv_call_time.setText(data.getCurrentTime());
        recyclerViewHolder.setItemClickListener(new CallLogClickListner() {
            @Override
            public void Click(View view) {
                if (null != itemClickListener) {
                    itemClickListener.onItemClickListener(view, i);
                }

            }
        });

    }



    public void SetNewList(ArrayList<ActivityLog> arrayList) {
        this.listOfActivitiesLog.clear();
        listOfActivitiesLog = arrayList;
    }


    @Override
    public int getItemCount() {
        return this.listOfActivitiesLog.size();
    }

    public LogActivityAdapter(Context context, ArrayList<ActivityLog> listOfActivitiesLog) {
        this.context = context;

        inflater = LayoutInflater.from(context);
        this.listOfActivitiesLog = listOfActivitiesLog;
    }


    public class RecyclerViewHolder extends RecyclerView.ViewHolder {

        private CallLogClickListner mOnClickListener = null;

        ImageView iv_call_icon;
        TextView tv_call_name;
        TextView tv_call_duration;
        TextView tv_call_date;
        TextView tv_call_time;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            iv_call_icon = itemView.findViewById(R.id.iv_chat_icon);
            tv_call_name = itemView.findViewById(R.id.tv_call_name);
            tv_call_duration = itemView.findViewById(R.id.tv_call_duration);
            tv_call_date = itemView.findViewById(R.id.tv_call_date);
            tv_call_time = itemView.findViewById(R.id.tv_call_time);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (null != mOnClickListener)
                        mOnClickListener.Click(v);
                }
            });

        }
        public void setItemClickListener(CallLogClickListner itemClickListener) {
            this.mOnClickListener = itemClickListener;
        }

    }

    public interface CallLogClickListner {
        public void Click(View view);
    }

    public void SetOnItemClickListner(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        public void onItemClickListener(View view, int position);
    }
}