package com.beowulfchain.beowulfchainsdk.linhphone;


import org.linphone.core.CallParams;

public class BandwidthManager {

    private static final int HIGH_RESOLUTION = 0;
    private static final int LOW_RESOLUTION = 1;
    private static final int LOW_BANDWIDTH = 2;

    private static BandwidthManager sInstance;

    private final int currentProfile = HIGH_RESOLUTION;

    private BandwidthManager() {
        // FIXME register a listener on NetworkManager to get notified of network state
        // FIXME register a listener on Preference to get notified of change in video enable value

        // FIXME initially get those values
    }

    public static synchronized BandwidthManager getInstance() {
        if (sInstance == null) sInstance = new BandwidthManager();
        return sInstance;
    }

    public void updateWithProfileSettings(CallParams callParams) {
        if (callParams != null) { // in call
            // Update video parm if
            if (!isVideoPossible()) { // NO VIDEO
                callParams.enableVideo(false);
                callParams.setAudioBandwidthLimit(40);
            } else {
                callParams.enableVideo(true);
                callParams.setAudioBandwidthLimit(0); // disable limitation
            }
        }
    }

    private boolean isVideoPossible() {
        return currentProfile != LOW_BANDWIDTH;
    }
}