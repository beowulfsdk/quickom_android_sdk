package com.beowulfchain.beowulfchainsdk.linhphone;

/*
LinphoneManager.java
Copyright (C) 2018  Belledonne Communications, Grenoble, France

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.Looper;
import android.os.PowerManager.WakeLock;
import android.os.Vibrator;
import android.view.KeyEvent;


import com.beowulfchain.beowulfchainsdk.MainApplication;
import com.beowulfchain.beowulfchainsdk.account.BeowulInfo;

import org.linphone.core.AccountCreator;
import org.linphone.core.Address;
import org.linphone.core.AuthInfo;
import org.linphone.core.AuthMethod;
import org.linphone.core.Call;
import org.linphone.core.Call.State;
import org.linphone.core.CallLog;
import org.linphone.core.CallParams;
import org.linphone.core.CallStats;
import org.linphone.core.ChatMessage;
import org.linphone.core.ChatRoom;
import org.linphone.core.ConfiguringState;
import org.linphone.core.Content;
import org.linphone.core.Core;
import org.linphone.core.Core.LogCollectionUploadState;
import org.linphone.core.CoreListener;
import org.linphone.core.EcCalibratorStatus;
import org.linphone.core.Event;
import org.linphone.core.Factory;
import org.linphone.core.Friend;
import org.linphone.core.FriendList;
import org.linphone.core.GlobalState;
import org.linphone.core.InfoMessage;
import org.linphone.core.PayloadType;
import org.linphone.core.PresenceModel;
import org.linphone.core.ProxyConfig;
import org.linphone.core.PublishState;
import org.linphone.core.RegistrationState;
import org.linphone.core.SubscriptionState;
import org.linphone.core.TransportType;
import org.linphone.core.VersionUpdateCheckResult;
import org.linphone.core.tools.Log;
import org.linphone.core.tools.OpenH264DownloadHelper;
import org.linphone.core.tools.OpenH264DownloadHelperListener;
import org.linphone.mediastream.video.capture.hwconf.AndroidCameraConfiguration;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import static android.media.AudioManager.STREAM_VOICE_CALL;

/**
 * Manager of the low level LibLinphone stuff.<br>
 * Including:
 *
 * <ul>
 * <li>Starting C liblinphone
 * <li>Reacting to C liblinphone state changes
 * <li>Calling Linphone android service listener methods
 * <li>Interacting from Android GUI/service with low level SIP stuff/
 * </ul>
 *
 * <p>Add Service Listener to react to Linphone state changes.
 */
public class LinphoneManagerClone implements CoreListener {
    public ProxyConfig mProxyConfig;
    public AuthInfo mAuthInfo;

    private static final int LINPHONE_VOLUME_STREAM = STREAM_VOICE_CALL;

    private static LinphoneManagerClone sInstance;
    private static boolean sExited;


    public final String configFile;
    //
    //    /** Called when the activity is first created. */
    //    private final String mLPConfigXsd;
    //
    //    private final String mLinphoneFactoryConfigFile;
    //    private final String mLinphoneDynamicConfigFile, mDefaultDynamicConfigFile;
    //    private final String mChatDatabaseFile;
    private final String mRingSoundFile;
    //    private final String mCallLogDatabaseFile;
    //    private final String mFriendsDatabaseFile;
    //    private final String mUserCertsPath;
    private final Context mServiceContext;
    private final AudioManager mAudioManager;
    //    private final PowerManager mPowerManager;
    private final Resources mRessources;
    private final LinphonePreferences mPrefs;
    public Core mCore;
    private OpenH264DownloadHelper mCodecDownloader;
    private OpenH264DownloadHelperListener mCodecListener;
    private final String mBasePath;
    private boolean mAudioFocused;
    private boolean mEchoTesterIsRunning;
    private boolean mCallGsmON;
    //   private final ConnectivityManager mConnectivityManager;
    private BroadcastReceiver mHookReceiver;
    private BroadcastReceiver mCallReceiver;
    private IntentFilter mHookIntentFilter;
    private IntentFilter mCallIntentFilter;
    private final Handler mHandler = new Handler();
    private WakeLock mProximityWakelock;
    private AccountCreator mAccountCreator;
    //  private final SensorManager mSensorManager;
    // private final Sensor mProximity;
    private boolean mProximitySensingEnabled;
    private boolean mHandsetON = false;
    private Address mCurrentChatRoomAddress;
    private Timer mTimer;
    // private final Map<String, Integer> mUnreadChatsPerRoom;
    // private final MediaScanner mMediaScanner;
    private Call mRingingCall;
    private MediaPlayer mRingerPlayer;
    private final Vibrator mVibrator;
    private boolean mIsRinging;
    private boolean mHasLastCallSasBeenRejected;

    private LinphoneManagerClone(Context c) {
        //        mUnreadChatsPerRoom = new HashMap();
        //        sExited = false;
        //        mEchoTesterIsRunning = false;
        mServiceContext = c;
        mBasePath = c.getFilesDir().getAbsolutePath();
        //        mLPConfigXsd = mBasePath + "/lpconfig.xsd";
        //        mLinphoneFactoryConfigFile = mBasePath + "/linphonerc";
        configFile = mBasePath + "/.linphonerc";
        //        mLinphoneDynamicConfigFile = mBasePath + "/linphone_assistant_create.rc";
        //        mDefaultDynamicConfigFile = mBasePath + "/default_assistant_create.rc";
        //        mChatDatabaseFile = mBasePath + "/linphone-history.db";
        //        mCallLogDatabaseFile = mBasePath + "/linphone-log-history.db";
        //        mFriendsDatabaseFile = mBasePath + "/linphone-friends.db";
        mRingSoundFile = mBasePath + "/share/sounds/linphone/rings/notes_of_the_optimistic.mkv";
        //        mUserCertsPath = mBasePath + "/user-certs";
        //
        //
        mAudioManager = ((AudioManager) c.getSystemService(Context.AUDIO_SERVICE));
        mVibrator = (Vibrator) c.getSystemService(Context.VIBRATOR_SERVICE);
        //        mPowerManager = (PowerManager) c.getSystemService(Context.POWER_SERVICE);
        //        mConnectivityManager =
        //                (ConnectivityManager) c.getSystemService(Context.CONNECTIVITY_SERVICE);
        //        mSensorManager = (SensorManager) c.getSystemService(Context.SENSOR_SERVICE);
        //        mProximity = mSensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);
        mRessources = c.getResources();
        //        mHasLastCallSasBeenRejected = false;
        //
        //        File f = new File(mUserCertsPath);
        //        if (!f.exists()) {
        //            if (!f.mkdir()) {
        //                Log.e("[Manager] " + mUserCertsPath + " can't be created.");
        //            }
        //        }

        mPrefs = LinphonePreferences.instance();
        //   mMediaScanner = new MediaScanner(c);
    }

    public void startEcCalibration() {

        routeAudioToSpeaker();
        setAudioManagerInCallMode();
        Log.i("[Audio Manager] Set audio mode on 'Voice Communication'");
        requestAudioFocus(STREAM_VOICE_CALL);
        int oldVolume = mAudioManager.getStreamVolume(STREAM_VOICE_CALL);
        int maxVolume = mAudioManager.getStreamMaxVolume(STREAM_VOICE_CALL);
        mAudioManager.setStreamVolume(STREAM_VOICE_CALL, maxVolume, 0);
        mCore.startEchoCancellerCalibration();
        mAudioManager.setStreamVolume(STREAM_VOICE_CALL, oldVolume, 0);
    }


    public boolean onKeyVolumeAdjust(int keyCode) {
        if (keyCode == KeyEvent.KEYCODE_VOLUME_UP) {
            adjustVolume(1);
            return true;
        } else if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) {
            adjustVolume(-1);
            return true;
        }
        return false;
    }


    public void requestAudioFocus(int stream) {
        if (!mAudioFocused) {
            int res =
                    mAudioManager.requestAudioFocus(
                            null, stream, AudioManager.AUDIOFOCUS_GAIN_TRANSIENT_EXCLUSIVE);
            Log.d(
                    "[Audio Manager] Audio focus requested: "
                            + (res == AudioManager.AUDIOFOCUS_REQUEST_GRANTED
                            ? "Granted"
                            : "Denied"));
            if (res == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) mAudioFocused = true;
        }
    }


    public static synchronized void createAndStart(Context c, boolean isPush) {
        sInstance = new LinphoneManagerClone(c);
        sInstance.startLibLinphone(c, isPush);
        sInstance.InitBeoWulf();
    }

    public static synchronized LinphoneManagerClone getInstance() {
        if (sInstance != null) return sInstance;
        else
            sInstance = new LinphoneManagerClone(MainApplication.getSDKContext());

        if (sExited) {
            throw new RuntimeException(
                    "[Manager] Linphone Manager was already destroyed. "
                            + "Better use getLcIfManagerNotDestroyedOrNull and check returned value");
        }

        throw new RuntimeException("[Manager] Linphone Manager should be created before accessed");
    }

    public static synchronized Core getLc() {
        return getInstance().mCore;
    }

    //    private static void ContactsManagerDestroy() {
    //        if (LinphoneManager.sInstance != null && LinphoneManager.sInstance.mServiceContext
    // != null)
    //            LinphoneManager.sInstance
    //                    .mServiceContext
    //                    .getContentResolver()
    //                    .unregisterContentObserver(ContactsManager.getInstance());
    //        ContactsManager.getInstance().destroy();
    //    }

    //    private static void BluetoothManagerDestroy() {
    //        BluetoothManager.getInstance().destroy();
    //    }

    public static synchronized void destroy() {
        if (sInstance == null) return;
        sInstance.changeStatusToOffline();
        //     sInstance.mMediaScanner.destroy();
        sExited = true;
        sInstance.destroyCore();
        sInstance = null;
    }

    //    private static boolean reinviteWithVideo() {
    //        return CallManager.getInstance().reinviteWithVideo();
    //    }

    public static synchronized Core getLcIfManagerNotDestroyedOrNull() {
        if (sExited || sInstance == null) {
            // Can occur if the UI thread play a posted event but in the meantime the
            // LinphoneManager was destroyed
            // Ex: stop call and quickly terminate application.
            return null;
        }
        return getLc();
    }

    public static boolean isInstanciated() {
        return sInstance != null;
    }

    private void routeAudioToSpeakerHelper(boolean speakerOn) {
        Log.w(
                "[Manager] Routing audio to "
                        + (speakerOn ? "speaker" : "earpiece")
                        + ", disabling bluetooth audio route");
        //   BluetoothManager.getInstance().disableBluetoothSCO();

        enableSpeaker(speakerOn);
    }

    public boolean isSpeakerEnabled() {
        return mAudioManager != null && mAudioManager.isSpeakerphoneOn();
    }

    public void enableSpeaker(boolean enable) {
        mAudioManager.setSpeakerphoneOn(enable);

    }

    public void routeAudioToSpeaker() {
        routeAudioToSpeakerHelper(true);
    }

    public void routeAudioToReceiver() {
        routeAudioToSpeakerHelper(false);
    }

    private void changeStatusToOffline() {

        //        Core lc = getLcIfManagerNotDestroyedOrNull();
        //        if (isInstanciated() && lc != null) {
        //            PresenceModel model = lc.getPresenceModel();
        //            model.setBasicStatus(PresenceBasicStatus.Closed);
        //            lc.setPresenceModel(model);
        //        }
    }

    //    private void resetCameraFromPreferences() {
    //        boolean useFrontCam = mPrefs.useFrontCam();
    //        int camId = 0;
    //        AndroidCamera[] cameras = AndroidCameraConfiguration.retrieveCameras();
    //        for (AndroidCamera androidCamera : cameras) {
    //            if (androidCamera.frontFacing == useFrontCam) {
    //                camId = androidCamera.id;
    //                break;
    //            }
    //        }
    //        String[] devices = getLc().getVideoDevicesList();
    //        if (camId >= devices.length) {
    //            Log.e(
    //                    "[Manager] Trying to use a camera id that's higher than the linphone's
    // devices list, using 0 to prevent crash...");
    //            camId = 0;
    //        }
    //        String newDevice = devices[camId];
    //        LinphoneManagerClone.getLc().setVideoDevice(newDevice);
    //    }

//    private void enableCamera(Call call, boolean enable) {
//        if (call != null) {
//            call.enableCamera(enable);
//            if (mServiceContext.getResources().getBoolean(R.bool.enable_call_notification))
//                LinphoneService.instance()
//                        .getNotificationManager()
//                        .displayCallNotification(mCore.getCurrentCall());
//        }
//    }
    //
    //    public void playDtmf(ContentResolver r, char dtmf) {
    //        try {
    //            if (Settings.System.getInt(r, Settings.System.DTMF_TONE_WHEN_DIALING) == 0) {
    //                // audible touch disabled: don't play on speaker, only send in outgoing stream
    //                return;
    //            }
    //        } catch (Settings.SettingNotFoundException e) {
    //            Log.e("[Manager] playDtmf exception: " + e);
    //        }
    //
    //        getLc().playDtmf(dtmf, -1);
    //    }

    //    private void terminateCall() {
    //        if (mCore.inCall()) {
    //            mCore.terminateCall(mCore.getCurrentCall());
    //        }
    //    }

    private synchronized void destroyCore() {
        Log.w("[Manager] Destroying Core");
        sExited = true;
        // ContactsManagerDestroy();
        //     BluetoothManagerDestroy();
        try {
            mTimer.cancel();
            destroyLinphoneCore();
        } catch (RuntimeException e) {
            Log.e("[Manager] Destroy Core Runtime Exception: " + e);
        } finally {
            try {
                mServiceContext.unregisterReceiver(mHookReceiver);
            } catch (Exception e) {
                Log.e("[Manager] unregister receiver exception: " + e);
            }
            try {
                mServiceContext.unregisterReceiver(mCallReceiver);
            } catch (Exception e) {
                Log.e("[Manager] unregister receiver exception: " + e);
            }
            mCore = null;
        }
    }

    private synchronized void startLibLinphone(Context c, boolean isPush) {
        try {
            // copyAssetsFromPackage();
            // traces alway startInitChat with traces enable to not missed first initialization


            final Handler sHandler = new Handler(Looper.getMainLooper());

            Factory.instance().setDebugMode(true, "Linphone");

            mCore = Factory.instance().createCore(null, null, c);
            //    mCore = Factory.instance().createCore(configFile, mLinphoneFactoryConfigFile, c);


            mCore.verifyServerCertificates(false);
            mCore.verifyServerCn(false);

            mCore.addListener(this);
            if (isPush) {
                Log.w(
                        "[Manager] We are here because of a received push notification, enter background mode before starting the Core");
                mCore.enterBackground();
            }
            mCore.start();
            TimerTask lTask =
                    new TimerTask() {
                        @Override
                        public void run() {
                            sHandler.post(new Runnable() {
                                @Override
                                public void run() {
                                    if (mCore != null) {
                                        mCore.iterate();
                                    }
                                }
                            });

                        }
                    };
            /*use schedule instead of scheduleAtFixedRate to avoid iterate from being call in burst after cpu wake up*/
            mTimer = new Timer("Linphone scheduler");
            mTimer.schedule(lTask, 0, 20);
        } catch (Exception e) {
            Log.e(e, "[Manager] Cannot startInitChat linphone");
        }

        //  LinphonePreferences.instance().setVideoPreset("default");
        LinphonePreferences.instance().setPreferredVideoFps(16);

        // Enable codec lists
        ArrayList<String> enableCodecList = new ArrayList<>();
        enableCodecList.add("g729");
        enableCodecList.add("g729a");
        enableCodecList.add("opus");
//        enableCodecList.add("vp8");

        PayloadType[] payloadArr = LinphoneManagerClone.getLc().getAudioPayloadTypes();

        for (PayloadType pt : payloadArr) {
            try {
                // Get codec name.
                String codecName = pt.getMimeType().toLowerCase();
                boolean isFound = enableCodecList.contains(codecName);
                if (isFound == true) {
                    pt.enable(true);
                    android.util.Log.d("nhuongnv", "=== codec enable: " + codecName);
                } else {
                    pt.enable(false);
                    android.util.Log.d("nhuongnv", "=== codec disable: " + codecName);
                }
            } catch (Exception ex) {
                Log.w(ex, "Unable to modify status for codec " + ex.getMessage());
            }
        }

        // LinphoneManagerClone.getLc().setAudioPayloadTypes(payloadArr);

        ArrayList<String> enableCodecList2 = new ArrayList<>();
        enableCodecList2.add("vp8");

        PayloadType[] payloadArr2 = LinphoneManagerClone.getLc().getVideoPayloadTypes();

        for (PayloadType pt : payloadArr2) {
            try {
                // Get codec name.
                String codecName = pt.getMimeType().toLowerCase();
                boolean isFound = enableCodecList2.contains(codecName);
                if (isFound == true) {
                    pt.enable(true);
                    android.util.Log.d("nhuongnv", "=== codec2 enable: " + codecName);
                } else {
                    pt.enable(false);
                    android.util.Log.d("nhuongnv", "=== codec2 disable: " + codecName);
                }
            } catch (Exception ex) {
                Log.w(ex, "Unable to modify status for codec " + ex.getMessage());
            }
        }

        //   LinphoneManagerClone.getLc().setVideoPayloadTypes(payloadArr2);

        // Set port range
        LinphoneManagerClone.getLc().setVideoPortRange(20000, 25000);
        LinphoneManagerClone.getLc().setAudioPortRange(10000, 15000);
        LinphoneManagerClone.getLc().reloadVideoDevices();

        //

        PayloadType[] payloadArrx = LinphoneManagerClone.getLc().getAudioPayloadTypes();

        for (PayloadType pt : payloadArrx) {
            try {
                // Get codec name.
                String codecName = pt.getMimeType().toLowerCase();
                android.util.Log.d("nhuongnv", "=== codec" + codecName + " : " + pt.enabled());
            } catch (Exception ex) {
                Log.w(ex, "Unable to modify status for codec " + ex.getMessage());
            }
        }

    }


    public static void reloadVideoDevices() {
        Core core = LinphoneManagerClone.getLc();
        if (core == null) return;

        Log.i("[Utils] Reloading camera");
        core.reloadVideoDevices();

        boolean useFrontCam = LinphonePreferences.instance().useFrontCam();
        int camId = 0;
        AndroidCameraConfiguration.AndroidCamera[] cameras =
                AndroidCameraConfiguration.retrieveCameras();
        for (AndroidCameraConfiguration.AndroidCamera androidCamera : cameras) {
            if (androidCamera.frontFacing == useFrontCam) {
                camId = androidCamera.id;
                break;
            }
        }
        String[] devices = core.getVideoDevicesList();
        if (camId >= devices.length) {
            camId = 0;
        }
        String newDevice = devices[camId];
        core.setVideoDevice(newDevice);
    }

    //

    //    private void initPushNotificationsService() {
    //        PushNotificationUtils.init(mServiceContext);
    //    }

    //    private synchronized void initLiblinphone(Core lc) {
    //        mCore = lc;
    //
    //        mCore.setZrtpSecretsFile(mBasePath + "/zrtp_secrets");
    //        String deviceName = mPrefs.getDeviceName(mServiceContext);
    //        String appName = mServiceContext.getResources().getString(R.string.user_agent);
    //        String androidVersion = BuildConfig.VERSION_NAME;
    //        String userAgent = appName + "/" + androidVersion + " (" + deviceName + ")
    // LinphoneSDK";
    //        mCore.setUserAgent(
    //                userAgent,
    //                getString(R.string.linphone_sdk_version)
    //                        + " ("
    //                        + getString(R.string.linphone_sdk_branch)
    //                        + ")");
    //        // mCore.setChatDatabasePath(mChatDatabaseFile);
    //        mCore.setCallLogsDatabasePath(mCallLogDatabaseFile);
    //        mCore.setFriendsDatabasePath(mFriendsDatabaseFile);
    //        mCore.setUserCertificatesPath(mUserCertsPath);
    //        // mCore.setCallErrorTone(Reason.NotFound, mErrorToneFile);
    //        enableDeviceRingtone(mPrefs.isDeviceRingtoneEnabled());
    //        int availableCores = Runtime.getRuntime().availableProcessors();
    //        Log.w("[Manager] MediaStreamer : " + availableCores + " cores detected and
    // configured");
    //        mCore.migrateLogsFromRcToDb();
    //
    //        // Migrate existing linphone accounts to have conference factory uri and LIME X3Dh url
    // set
    //        String uri = getString(R.string.default_conference_factory_uri);
    //        for (ProxyConfig lpc : mCore.getProxyConfigList()) {
    //            if
    // (lpc.getIdentityAddress().getDomain().equals(getString(R.string.default_domain))) {
    //                if (lpc.getConferenceFactoryUri() == null) {
    //                    lpc.edit();
    //                    Log.i(
    //                            "[Manager] Setting conference factory on proxy config "
    //                                    + lpc.getIdentityAddress().asString()
    //                                    + " to default value: "
    //                                    + uri);
    //                    lpc.setConferenceFactoryUri(uri);
    //                    lpc.done();
    //                }
    //
    //                if (mCore.limeX3DhAvailable()) {
    //                    String url = mCore.getLimeX3DhServerUrl();
    //                    if (url == null || url.length() == 0) {
    //                        url = getString(R.string.default_lime_x3dh_server_url);
    //                        Log.i("[Manager] Setting LIME X3Dh server url to default value: " +
    // url);
    //                        mCore.setLimeX3DhServerUrl(url);
    //                    }
    //                }
    //            }
    //        }
    //
    //        if (mServiceContext.getResources().getBoolean(R.bool.enable_push_id)) {
    //            initPushNotificationsService();
    //        }
    //
    //        mCallIntentFilter = new
    // IntentFilter("android.intent.action.ACTION_NEW_OUTGOING_CALL");
    //        mCallIntentFilter.setPriority(99999999);
    //        mCallReceiver = new OutgoingCallReceiver();
    //        try {
    //            mServiceContext.registerReceiver(mCallReceiver, mCallIntentFilter);
    //        } catch (IllegalArgumentException e) {
    //            e.printStackTrace();
    //        }
    //        mProximityWakelock =
    //                mPowerManager.newWakeLock(
    //                        PowerManager.PROXIMITY_SCREEN_OFF_WAKE_LOCK,
    //                        mServiceContext.getPackageName() + ";manager_proximity_sensor");
    //
    //        mHookIntentFilter = new IntentFilter("com.base.module.phone.HOOKEVENT");
    //        mHookIntentFilter.setPriority(999);
    //        mHookReceiver = new HookReceiver();
    //        mServiceContext.registerReceiver(mHookReceiver, mHookIntentFilter);
    //
    //        resetCameraFromPreferences();
    //
    //        mAccountCreator =
    //                LinphoneManagerClone.getLc()
    //                        .createAccountCreator(LinphonePreferences.instance().getXmlrpcUrl());
    //        mAccountCreator.setListener(this);
    //        mCallGsmON = false;
    //
    //        updateMissedChatCount();
    //    }
    //
    //
    //
    //

    private void destroyLinphoneCore() {
        if (LinphonePreferences.instance() != null) {
            // We set network reachable at false before destroy LC to not send register with expires
            // at 0
            if (LinphonePreferences.instance().isPushNotificationEnabled()) {
                Log.w(
                        "[Manager] Setting network reachability to False to prevent unregister and allow incoming push notifications");
                mCore.setNetworkReachable(false);
            }
        }
        mCore.stop();
    }

    //    public MediaScanner getMediaScanner() {
    //        return mMediaScanner;
    //    }

    private String getString(int key) {
        return mRessources.getString(key);
    }

    public void onNewSubscriptionRequested(Core lc, Friend lf, String url) {
    }

    public void onNotifyPresenceReceived(Core lc, Friend lf) {
    }

    @Override
    public void onEcCalibrationAudioInit(Core lc) {
    }

    @Override
    public void onDtmfReceived(Core lc, Call call, int dtmf) {
        Log.d("[Manager] DTMF received: " + dtmf);
    }

    @Override
    public void onMessageSent(Core core, ChatRoom chatRoom, ChatMessage chatMessage) {

    }


    @Override
    public void onMessageReceived(Core lc, final ChatRoom cr, final ChatMessage message) {
        //        if (mServiceContext.getResources().getBoolean(R.bool.disable_chat)) {
        //            return;
        //        }
        //
        //        if (mCurrentChatRoomAddress != null
        //                && cr.getPeerAddress()
        //                        .asStringUriOnly()
        //                        .equals(mCurrentChatRoomAddress.asStringUriOnly())) {
        //            Log.i(
        //                    "[Manager] Message received for currently displayed chat room, do not
        // make a notification");
        //            return;
        //        }
        //
        //        if (message.getErrorInfo() != null
        //                && message.getErrorInfo().getReason() == Reason.UnsupportedContent) {
        //            Log.w("[Manager] Message received but content is unsupported, do not notify
        // it");
        //            return;
        //        }
        //
        //        if (!message.hasTextContent() && message.getFileTransferInformation() == null) {
        //            Log.w(
        //                    "[Manager] Message has no text or file transfer information to
        // display, ignoring it...");
        //            return;
        //        }
        //
        //        // increaseUnreadCountForChatRoom(cr);
        //
        //        if
        // (mServiceContext.getResources().getBoolean(R.bool.disable_chat_message_notification)
        //                || message.isOutgoing()) {
        //            return;
        //        }
        //
        //        final Address from = message.getFromAddress();
        //        final LinphoneContact contact =
        // ContactsManager.getInstance().findContactFromAddress(from);
        //        final String textMessage =
        //                (message.hasTextContent())
        //                        ? message.getTextContent()
        //                        : getString(R.string.content_description_incoming_file);
        //
        //        String file = null;
        //        for (Content c : message.getContents()) {
        //            if (c.isFile()) {
        //                file = c.getFilePath();
        //                getMediaScanner()
        //                        .scanFile(
        //                                new File(file),
        //                                new MediaScannerListener() {
        //                                    @Override
        //                                    public void onMediaScanned(String path, Uri uri) {
        //                                        createNotification(
        //                                                cr,
        //                                                contact,
        //                                                from,
        //                                                textMessage,
        //                                                message.getTime(),
        //                                                uri,
        //                                                FileUtils.getMimeFromFile(path));
        //                                    }
        //                                });
        //                break;
        //            }
        //        }
        //
        //        if (file == null) {
        //            createNotification(cr, contact, from, textMessage, message.getTime(), null,
        // null);
        //        }
    }

    //    private void createNotification(
    //            ChatRoom cr,
    //            LinphoneContact contact,
    //            Address from,
    //            String textMessage,
    //            long time,
    //            Uri file,
    //            String mime) {
    //        if (cr.hasCapability(ChatRoomCapabilities.OneToOne.toInt())) {
    //            if (contact != null) {
    //                LinphoneService.instance()
    //                        .getNotificationManager()
    //                        .displayMessageNotification(
    //                                cr.getPeerAddress().asStringUriOnly(),
    //                                contact.getFullName(),
    //                                contact.getThumbnailUri(),
    //                                textMessage,
    //                                cr.getLocalAddress(),
    //                                time,
    //                                file,
    //                                mime);
    //            } else {
    //                LinphoneService.instance()
    //                        .getNotificationManager()
    //                        .displayMessageNotification(
    //                                cr.getPeerAddress().asStringUriOnly(),
    //                                from.getUsername(),
    //                                null,
    //                                textMessage,
    //                                cr.getLocalAddress(),
    //                                time,
    //                                file,
    //                                mime);
    //            }
    //        } else {
    //            String subject = cr.getSubject();
    //            if (contact != null) {
    //                LinphoneService.instance()
    //                        .getNotificationManager()
    //                        .displayGroupChatMessageNotification(
    //                                subject,
    //                                cr.getPeerAddress().asStringUriOnly(),
    //                                contact.getFullName(),
    //                                contact.getThumbnailUri(),
    //                                textMessage,
    //                                cr.getLocalAddress(),
    //                                time,
    //                                file,
    //                                mime);
    //            } else {
    //                LinphoneService.instance()
    //                        .getNotificationManager()
    //                        .displayGroupChatMessageNotification(
    //                                subject,
    //                                cr.getPeerAddress().asStringUriOnly(),
    //                                from.getUsername(),
    //                                null,
    //                                textMessage,
    //                                cr.getLocalAddress(),
    //                                time,
    //                                file,
    //                                mime);
    //            }
    //        }
    //    }

    @Override
    public void onEcCalibrationResult(Core lc, EcCalibratorStatus status, int delay_ms) {
        ((AudioManager) MainApplication.getSDKContext().getSystemService(Context.AUDIO_SERVICE))
                .setMode(AudioManager.MODE_NORMAL);
        mAudioManager.abandonAudioFocus(null);
        Log.i("[Manager] Set audio mode on 'Normal'");
    }

    public void onGlobalStateChanged(final Core lc, final GlobalState state, final String message) {
        Log.i("New global state [", state, "]");
        if (state == GlobalState.On) {
            try {
                //    initLiblinphone(lc);
            } catch (IllegalArgumentException iae) {
                Log.e("[Manager] Global State Changed Illegal Argument Exception: " + iae);
            }
        }
    }

    @Override
    public void onRegistrationStateChanged(final Core lc, final ProxyConfig proxy, final RegistrationState state, final String message) {
        android.util.Log.d("MANAGER______", "xxx [Manager] New registration state [" + state + "]");
        if (state == RegistrationState.Ok) {

        }

        if (state == RegistrationState.Failed) {
            ConnectivityManager connectivityManager = (ConnectivityManager) mServiceContext.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            Log.i("[Manager] Active network type: " + activeNetworkInfo.getTypeName());
            if (activeNetworkInfo.isAvailable() && activeNetworkInfo.isConnected()) {
                Log.i("[Manager] Active network is available");
            }
            Log.i(
                    "[Manager] Active network reason and extra info: "
                            + activeNetworkInfo.getReason()
                            + " / "
                            + activeNetworkInfo.getExtraInfo());
            Log.i(
                    "[Manager] Active network state "
                            + activeNetworkInfo.getState()
                            + " / "
                            + activeNetworkInfo.getDetailedState());
        }
    }

//    public Context getContext() {
//        try {
//            if (QuickMainActivity.isInstance()) return QuickMainActivity.instance();
//                // else if (CallingProgress.isInstanciated()) return CallingProgress.instance();
//            else if (IncommingCall.isInstanciated()) return IncommingCall.instance();
//            else if (mServiceContext != null) return mServiceContext;
//            else if (SipService.isReady())
//                return SipService.instance().getApplicationContext();
//        } catch (Exception e) {
//            Log.e(e);
//        }
//        return null;
//    }

    private void setAudioManagerInCallMode() {
        if (mAudioManager.getMode() == AudioManager.MODE_IN_COMMUNICATION) {

            Log.w("[Manager][AudioManager] already in MODE_IN_COMMUNICATION, skipping...");
            return;
        }
        Log.d("[Manager][AudioManager] Mode: MODE_IN_COMMUNICATION");

        mAudioManager.setMode(AudioManager.MODE_IN_COMMUNICATION);
    }

    @SuppressLint("Wakelock")
    public void onCallStateChanged(final Core lc, final Call call, final State state, final String message) {
//        Log.i("[Manager] ___________ New call state [", state, "]");
//        if (state == State.IncomingReceived && !call.equals(lc.getCurrentCall())) {
//            if (call.getReplacedCall() != null) {
//                // attended transfer
//                // it will be accepted automatically.
//                return;
//            }
//        }
//
//        if ((state == State.IncomingReceived || state == State.IncomingEarlyMedia)
//                && getCallGsmON()) {
//            if (mCore != null) {
//                mCore.declineCall(call, Reason.Busy);
//            }
//        } else if (state == State.IncomingReceived
//                && (LinphonePreferences.instance().isAutoAnswerEnabled())
//                && !getCallGsmON()) {
//            TimerTask lTask =
//                    new TimerTask() {
//                        @Override
//                        public void run() {
//                            if (mCore != null) {
//                                if (mCore.getCallsNb() > 0) {
//                                    acceptCall(call);
//                                    if (LinphoneManagerClone.getInstance() != null) {
//                                        LinphoneManagerClone.getInstance().routeAudioToReceiver();
//                                        if (QuickMainActivity.instance() != null)
//                                            QuickMainActivity.instance().startIncallActivity();
//                                    }
//                                }
//                            }
//                        }
//                    };
//            mTimer = new Timer("Auto answer");
//            mTimer.schedule(lTask, mPrefs.getAutoAnswerTime());
//        } else if (state == State.IncomingReceived
//                || (state == State.IncomingEarlyMedia
//                && mRessources.getBoolean(R.bool.allow_ringing_while_early_media))) {
//            // Brighten screen for at least 10 seconds
//            if (mCore.getCallsNb() == 1) {
//                requestAudioFocus(STREAM_RING);
//
//                mRingingCall = call;
//                //      startRinging();
//                // otherwise there is the beep
//            }
//        } else if (call == mRingingCall && mIsRinging) {
//            // previous state was ringing, so stop ringing
//            //  stopRinging();
//        }
//
//        if (state == State.Connected) {
//            if (mCore.getCallsNb() == 1) {
//                // It is for incoming calls, because outgoing calls enter MODE_IN_COMMUNICATION
//                // immediately when they startInitChat.
//                // However, incoming call first use the MODE_RINGING to play the local ring.
//                if (call.getDir() == Call.Dir.Incoming) {
//                    setAudioManagerInCallMode();
//                    // mAudioManager.abandonAudioFocus(null);
//                    requestAudioFocus(STREAM_VOICE_CALL);
//                }
//            }
//
//            if (Hacks.needSoftvolume()) {
//                Log.w("[Manager] Using soft volume audio hack");
//                adjustVolume(0); // Synchronize
//            }
//        }
//
//        if (state == State.End || state == State.Error) {
//            if (mCore.getCallsNb() == 0) {
//                // Disabling proximity sensor
//                Context activity = getContext();
//                if (mAudioFocused) {
//                    int res = mAudioManager.abandonAudioFocus(null);
//                    Log.d(
//                            "[Manager] Audio focus released a bit later: "
//                                    + (res == AudioManager.AUDIOFOCUS_REQUEST_GRANTED
//                                    ? "Granted"
//                                    : "Denied"));
//                    mAudioFocused = false;
//                }
//                if (activity != null) {
//                    TelephonyManager tm =
//                            (TelephonyManager) activity.getSystemService(Context.TELEPHONY_SERVICE);
//                    if (tm.getCallState() == TelephonyManager.CALL_STATE_IDLE) {
//                        Log.d("[Manager] ---AudioManager: back to MODE_NORMAL");
//                        mAudioManager.setMode(AudioManager.MODE_NORMAL);
//                        Log.d("[Manager] All call terminated, routing back to earpiece");
//                        routeAudioToReceiver();
//                    }
//                }
//            }
//        }
//        if (state == State.UpdatedByRemote) {
//            // If the correspondent proposes video while audio call
//            boolean remoteVideo = call.getRemoteParams().videoEnabled();
//            boolean localVideo = call.getCurrentParams().videoEnabled();
//            boolean autoAcceptCameraPolicy =
//                    LinphonePreferences.instance().shouldAutomaticallyAcceptVideoRequests();
//            if (remoteVideo
//                    && !localVideo
//                    && !autoAcceptCameraPolicy
//                    && LinphoneManagerClone.getLc().getConference() == null) {
//                LinphoneManagerClone.getLc().deferCallUpdate(call);
//            }
//        }
//        if (state == State.OutgoingInit) {
//            // Enter the MODE_IN_COMMUNICATION mode as soon as possible, so that ringback
//            // is heard normally in earpiece or bluetooth receiver.
//            setAudioManagerInCallMode();
//            requestAudioFocus(STREAM_VOICE_CALL);
//            // startBluetooth();
//        }
//
//        if (state == State.StreamsRunning) {
//            // startBluetooth();
//            // setAudioManagerInCallMode();
//        }
    }

    //    private void startBluetooth() {
    //        if (BluetoothManager.getInstance().isBluetoothHeadsetAvailable()) {
    //            BluetoothManager.getInstance().routeAudioToBluetooth();
    //        }
    //    }

    public void onCallStatsUpdated(final Core lc, final Call call, final CallStats stats) {
    }

    @Override
    public void onChatRoomStateChanged(Core lc, ChatRoom cr, ChatRoom.State state) {
    }

    @Override
    public void onQrcodeFound(Core lc, String result) {
    }

    public void onCallEncryptionChanged(
            Core lc, Call call, boolean encrypted, String authenticationToken) {
    }


    public void enableDeviceRingtone(boolean use) {
        if (use) {
            mCore.setRing(null);
        } else {
            mCore.setRing(mRingSoundFile);
        }
    }

    //    private synchronized void startRinging() {
    //        if (!LinphonePreferences.instance().isDeviceRingtoneEnabled()) {
    //            // Enable speaker audio route, linphone library will do the ringing itself
    // automatically
    //            routeAudioToSpeaker();
    //            return;
    //        }
    //
    //        if (mRessources.getBoolean(R.bool.allow_ringing_while_early_media)) {
    //            routeAudioToSpeaker(); // Need to be able to ear the ringtone during the early
    // media
    //        }
    //
    //        // if (Hacks.needGalaxySAudioHack())
    //        mAudioManager.setMode(MODE_RINGTONE);
    //
    //        try {
    //            if ((mAudioManager.getRingerMode() == AudioManager.RINGER_MODE_VIBRATE
    //                            || mAudioManager.getRingerMode() ==
    // AudioManager.RINGER_MODE_NORMAL)
    //                    && mVibrator != null
    //                    && LinphonePreferences.instance().isIncomingCallVibrationEnabled()) {
    //                long[] patern = {0, 1000, 1000};
    //                mVibrator.vibrate(patern, 1);
    //            }
    //            if (mRingerPlayer == null) {
    //                requestAudioFocus(STREAM_RING);
    //                mRingerPlayer = new MediaPlayer();
    //                mRingerPlayer.setAudioStreamType(STREAM_RING);
    //
    //                String ringtone =
    //                        LinphonePreferences.instance()
    //                                .getRingtone(Settings.System.DEFAULT_RINGTONE_URI.toString());
    //                try {
    //                    if (ringtone.startsWith("content://")) {
    //                        mRingerPlayer.setDataSource(mServiceContext, Uri.parse(ringtone));
    //                    } else {
    //                        FileInputStream fis = new FileInputStream(ringtone);
    //                        mRingerPlayer.setDataSource(fis.getFD());
    //                        fis.close();
    //                    }
    //                } catch (IOException e) {
    //                    Log.e(e, "[Manager] Cannot set ringtone");
    //                }
    //
    //                mRingerPlayer.prepare();
    //                mRingerPlayer.setLooping(true);
    //                mRingerPlayer.startInitChat();
    //            } else {
    //                Log.w("[Manager] Already ringing");
    //            }
    //        } catch (Exception e) {
    //            Log.e(e, "[Manager] Cannot handle incoming call");
    //        }
    //        mIsRinging = true;
    //    }

    //    private synchronized void stopRinging() {
    //        if (mRingerPlayer != null) {
    //            mRingerPlayer.stop();
    //            mRingerPlayer.release();
    //            mRingerPlayer = null;
    //        }
    //        if (mVibrator != null) {
    //            mVibrator.cancel();
    //        }
    //
    //        if (Hacks.needGalaxySAudioHack()) mAudioManager.setMode(AudioManager.MODE_NORMAL);
    //
    //        mIsRinging = false;
    //        // You may need to call galaxys audio hack after this method
    //        if (!BluetoothManager.getInstance().isBluetoothHeadsetAvailable()) {
    //            if (mServiceContext.getResources().getBoolean(R.bool.isTablet)) {
    //                Log.d("[Manager] Stopped ringing, routing back to speaker");
    //                routeAudioToSpeaker();
    //            } else {
    //                Log.d("[Manager] Stopped ringing, routing back to earpiece");
    //                routeAudioToReceiver();
    //            }
    //        }
    //    }

    /**
     * @return false if already in video call.
     */
    //    public boolean addVideo() {
    //        Call call = mCore.getCurrentCall();
    //        enableCamera(call, true);
    //        return reinviteWithVideo();
    //    }
    public boolean acceptCall(Call call) {
        if (call == null) return false;

        CallParams params = LinphoneManagerClone.getLc().createCallParams(call);

        // boolean isLowBandwidthConnection =
        // !LinphoneUtils.isHighBandwidthConnection(LinphoneService.instance().getApplicationContext());

//        if (params != null) {
//            params.enableLowBandwidth(false);
//            params.setRecordFile(
//                    FileUtils.getCallRecordingFilename(getContext(), call.getRemoteAddress()));
//        } else {
//            Log.e("[Manager] Could not create call params for call");
//            return false;
//        }

        mCore.acceptCallWithParams(call, params);
        return true;
    }

    public void adjustVolume(int i) {
        // starting from ICS, volume must be adjusted by the application, at least for
        // STREAM_VOICE_CALL volume stream
        mAudioManager.adjustStreamVolume(
                LINPHONE_VOLUME_STREAM,
                i < 0 ? AudioManager.ADJUST_LOWER : AudioManager.ADJUST_RAISE,
                AudioManager.FLAG_SHOW_UI);
    }

    //    public void isAccountWithAlias() {
    //        if (!LinphonePreferences.instance().isLinkPopupEnabled()) return;
    //
    //        if (LinphoneManagerClone.getLc().getDefaultProxyConfig() != null) {
    //
    //            long now = new Timestamp(new Date().getTime()).getTime();
    //            if (LinphonePreferences.instance().getLinkPopupTime() != null
    //                    && Long.parseLong(LinphonePreferences.instance().getLinkPopupTime()) >=
    // now)
    //                return;
    //
    //            long future = 60; //  new Timestamp(
    //            //
    // LinphoneActivity.instance().getResources().getInteger(R.integer.popup_time_interval))
    //            //                   .getTime();
    //            long newDate = now + future;
    //
    //            if (mAccountCreator != null) {
    //                mAccountCreator.setUsername(
    //                        LinphonePreferences.instance()
    //                                .getAccountUsername(
    //
    // LinphonePreferences.instance().getDefaultAccountIndex()));
    //                mAccountCreator.isAccountExist();
    //                LinphonePreferences.instance().setLinkPopupTime(String.valueOf(newDate));
    //            }
    //        } else {
    //            LinphonePreferences.instance().setLinkPopupTime(null);
    //        }
    //    }

    //    private void askLinkWithPhoneNumber() {
    //        if (!LinphonePreferences.instance().isLinkPopupEnabled()) return;
    //
    //        long now = new Timestamp(new Date().getTime()).getTime();
    //        if (LinphonePreferences.instance().getLinkPopupTime() != null
    //                && Long.parseLong(LinphonePreferences.instance().getLinkPopupTime()) >= now)
    // return;
    //
    //        long future =
    //                new Timestamp(
    //                        LinphoneActivity.instance()
    //                                .getResources()
    //                                .getInteger(
    //                                        R.integer.phone_number_linking_popup_time_interval))
    //                        .getTime();
    //        long newDate = now + future;
    //
    //        LinphonePreferences.instance().setLinkPopupTime(String.valueOf(newDate));
    //
    //        final Dialog dialog =
    //                LinphoneActivity.instance()
    //                        .displayDialog(
    //                                String.format(
    //                                        getString(R.string.link_account_popup),
    //                                        LinphoneManagerClone.getLc()
    //                                                .getDefaultProxyConfig()
    //                                                .getIdentityAddress()
    //                                                .asStringUriOnly()));
    //        Button delete = dialog.findViewById(R.id.dialog_delete_button);
    //        delete.setVisibility(View.GONE);
    //        Button ok = dialog.findViewById(R.id.dialog_ok_button);
    //        ok.setText(getString(R.string.link));
    //        ok.setVisibility(View.VISIBLE);
    //        Button cancel = dialog.findViewById(R.id.dialog_cancel_button);
    //        cancel.setText(getString(R.string.maybe_later));
    //
    //        dialog.findViewById(R.id.dialog_do_not_ask_again_layout).setVisibility(View.VISIBLE);
    //        final CheckBox doNotAskAgain = dialog.findViewById(R.id.doNotAskAgain);
    //        dialog.findViewById(R.id.doNotAskAgainLabel)
    //                .setOnClickListener(
    //                        new View.OnClickListener() {
    //                            @Override
    //                            public void onClick(View v) {
    //                                doNotAskAgain.setChecked(!doNotAskAgain.isChecked());
    //                            }
    //                        });
    //
    //        ok.setOnClickListener(
    //                new View.OnClickListener() {
    //                    @Override
    //                    public void onClick(View view) {
    //                        Intent assistant = new Intent();
    //                        assistant.setClass(
    //                                LinphoneActivity.instance(),
    //                                PhoneAccountLinkingAssistantActivity.class);
    //                        mServiceContext.startActivity(assistant);
    //                        dialog.dismiss();
    //                    }
    //                });
    //
    //        cancel.setOnClickListener(
    //                new View.OnClickListener() {
    //                    @Override
    //                    public void onClick(View view) {
    //                        if (doNotAskAgain.isChecked()) {
    //                            LinphonePreferences.instance().enableLinkPopup(false);
    //                        }
    //                        dialog.dismiss();
    //                    }
    //                });
    //        dialog.show();
    //    }

    public boolean getCallGsmON() {
        return mCallGsmON;
    }

    @Override
    public void onTransferStateChanged(Core lc, Call call, State new_call_state) {
    }

    @Override
    public void onInfoReceived(Core lc, Call call, InfoMessage info) {
        //        Log.d("[Manager] Info message received from " +
        // call.getRemoteAddress().asString());
        //        Content ct = info.getContent();
        //        if (ct != null) {
        //            Log.d(
        //                    "[Manager] Info received with body with mime type "
        //                            + ct.getType()
        //                            + "/"
        //                            + ct.getSubtype()
        //                            + " and data ["
        //                            + ct.getStringBuffer()
        //                            + "]");
        //        }
    }

    @Override
    public void onChatRoomRead(Core core, ChatRoom chatRoom) {

    }


    @Override
    public void onSubscriptionStateChanged(Core lc, Event ev, SubscriptionState state) {
        //        Log.d(
        //                "[Manager] Subscription state changed to "
        //                        + state
        //                        + " event name is "
        //                        + ev.getName());
    }

    @Override
    public void onCallLogUpdated(Core lc, CallLog newcl) {
    }

    @Override
    public void onNotifyReceived(Core lc, Event ev, String eventName, Content content) {
        //        Log.d("[Manager] Notify received for event " + eventName);
        //        if (content != null)
        //            Log.d(
        //                    "[Manager] With content "
        //                            + content.getType()
        //                            + "/"
        //                            + content.getSubtype()
        //                            + " data:"
        //                            + content.getStringBuffer());
    }

    @Override
    public void onSubscribeReceived(Core lc, Event lev, String subscribeEvent, Content body) {
    }

    @Override
    public void onPublishStateChanged(Core lc, Event ev, PublishState state) {
        Log.d("[Manager] Publish state changed to " + state + " for event name " + ev.getName());
    }

    @Override
    public void onIsComposingReceived(Core lc, ChatRoom cr) {
        Log.d("[Manager] Composing received for chatroom " + cr.getPeerAddress().asStringUriOnly());
    }

    @Override
    public void onMessageReceivedUnableDecrypt(Core lc, ChatRoom room, ChatMessage message) {
    }

    @Override
    public void onConfiguringStatus(Core lc, ConfiguringState state, String message) {
        Log.d("[Manager] Remote provisioning status = " + state.toString() + " (" + message + ")");

//        LinphonePreferences prefs = LinphonePreferences.instance();
//        if (state == ConfiguringState.Successful) {
//            prefs.setPushNotificationEnabled(prefs.isPushNotificationEnabled());
//        }
    }

    @Override
    public void onCallCreated(Core lc, Call call) {
    }

    @Override
    public void onLogCollectionUploadProgressIndication(Core linphoneCore, int offset, int total) {
        //        if (total > 0)
        //            Log.d(
        //                    "[Manager] Log upload progress: currently uploaded = "
        //                            + offset
        //                            + " , total = "
        //                            + total
        //                            + ", % = "
        //                            + String.valueOf((offset * 100) / total));
    }

    @Override
    public void onVersionUpdateCheckResultReceived(
            Core lc, VersionUpdateCheckResult result, String version, String url) {
        //        if (result == VersionUpdateCheckResult.NewVersionAvailable) {
        //            final String urlToUse = url;
        //            final String versionAv = version;
        //            mHandler.postDelayed(
        //                    new Runnable() {
        //                        @Override
        //                        public void run() {
        //                            AlertDialog.Builder builder = new
        // AlertDialog.Builder(getContext());
        //                            builder.setMessage(
        //                                    getString(R.string.update_available) + ": " +
        // versionAv);
        //                            builder.setCancelable(false);
        //                            builder.setNeutralButton(
        //                                    getString(R.string.ok),
        //                                    new DialogInterface.OnClickListener() {
        //                                        @Override
        //                                        public void onClick(
        //                                                DialogInterface dialogInterface, int i) {
        //                                            if (urlToUse != null) {
        //                                                Intent urlIntent = new
        // Intent(Intent.ACTION_VIEW);
        //                                                urlIntent.setData(Uri.parse(urlToUse));
        //                                                getContext().startActivity(urlIntent);
        //                                            }
        //                                        }
        //                                    });
        //                            builder.show();
        //                        }
        //                    },
        //                    1000);
        //        }
    }

    @Override
    public void onEcCalibrationAudioUninit(Core lc) {
    }

    //    private void sendLogs(String info) {
    //        Context context = LinphoneActivity.instance();
    //        final String appName = context.getString(R.string.app_name);
    //
    //        Intent i = new Intent(Intent.ACTION_SEND);
    //        i.putExtra(
    //                Intent.EXTRA_EMAIL,
    //                new String[]{context.getString(R.string.about_bugreport_email)});
    //        i.putExtra(Intent.EXTRA_SUBJECT, appName + " Logs");
    //        i.putExtra(Intent.EXTRA_TEXT, info);
    //        i.setType("application/zip");
    //
    //        try {
    //            context.startActivity(Intent.createChooser(i, "Send mail..."));
    //        } catch (android.content.ActivityNotFoundException ex) {
    //            Log.e(ex);
    //        }
    //    }

    public void AudioMode() {
        if (mAudioManager.getMode() == AudioManager.MODE_IN_COMMUNICATION) {
            Log.w("[Audio Manager] already in MODE_IN_COMMUNICATION, skipping...");
            return;
        }
        Log.d("[Audio Manager] Mode: MODE_IN_COMMUNICATION");

        mAudioManager.setMode(AudioManager.MODE_IN_COMMUNICATION);
    }

    @Override
    public void onLogCollectionUploadStateChanged(
            Core linphoneCore, LogCollectionUploadState state, String info) {
        //        Log.d("[Manager] Log upload state: " + state.toString() + ", info = " + info);
        //        if (state == LogCollectionUploadState.Delivered) {
        //            ClipboardManager clipboard =
        //                    (ClipboardManager)
        // mServiceContext.getSystemService(Context.CLIPBOARD_SERVICE);
        //            ClipData clip = ClipData.newPlainText("Logs url", info);
        //            clipboard.setPrimaryClip(clip);
        //            Toast.makeText(
        //                    LinphoneActivity.instance(),
        //                    getString(R.string.logs_url_copied_to_clipboard),
        //                    Toast.LENGTH_SHORT)
        //                    .show();
        //            sendLogs(info);
        //        }
    }

    @Override
    public void onFriendListCreated(Core lc, FriendList list) {
        //        if (LinphoneService.isReady()) {
        //            list.addListener(ContactsManager.getInstance());
        //        }
    }

    @Override
    public void onFriendListRemoved(Core lc, FriendList list) {
        //      list.removeListener(ContactsManager.getInstance());
    }

    @Override
    public void onReferReceived(Core lc, String refer_to) {
    }

    @Override
    public void onNetworkReachable(Core lc, boolean enable) {
    }

    @Override
    public void onAuthenticationRequested(Core lc, AuthInfo authInfo, AuthMethod method) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onNotifyPresenceReceivedForUriOrTel(
            Core lc, Friend lf, String uri_or_tel, PresenceModel presence_model) {
    }

    @Override
    public void onBuddyInfoUpdated(Core lc, Friend lf) {
    }

    //    @Override
    //    public void onIsAccountExist(
    //            AccountCreator accountCreator, AccountCreator.Status status, String resp) {
    //        if (status.equals(AccountCreator.Status.AccountExist)) {
    //            accountCreator.isAccountLinked();
    //        }
    //    }
    //
    //    @Override
    //    public void onCreateAccount(
    //            AccountCreator accountCreator, AccountCreator.Status status, String resp) {}
    //
    //    @Override
    //    public void onActivateAccount(
    //            AccountCreator accountCreator, AccountCreator.Status status, String resp) {}
    //
    //    @Override
    //    public void onLinkAccount(
    //            AccountCreator accountCreator, AccountCreator.Status status, String resp) {
    //        if (status.equals(AccountCreator.Status.AccountNotLinked)) {
    //            askLinkWithPhoneNumber();
    //        }
    //    }
    //
    //    @Override
    //    public void onActivateAlias(
    //            AccountCreator accountCreator, AccountCreator.Status status, String resp) {}
    //
    //    @Override
    //    public void onIsAccountActivated(
    //            AccountCreator accountCreator, AccountCreator.Status status, String resp) {}
    //
    //    @Override
    //    public void onRecoverAccount(
    //            AccountCreator accountCreator, AccountCreator.Status status, String resp) {}
    //
    //    @Override
    //    public void onIsAccountLinked(
    //            AccountCreator accountCreator, AccountCreator.Status status, String resp) {
    //        if (status.equals(AccountCreator.Status.AccountNotLinked)) {
    //            askLinkWithPhoneNumber();
    //        }
    //    }
    //
    //    @Override
    //    public void onIsAliasUsed(
    //            AccountCreator accountCreator, AccountCreator.Status status, String resp) {}
    //
    //    @Override
    //    public void onUpdateAccount(
    //            AccountCreator accountCreator, AccountCreator.Status status, String resp) {}

    public void InitBeoWulf() {
        mAuthInfo = org.linphone.core.Factory.instance().createAuthInfo(null, null, null, null, null, null);
        mAccountCreator = mCore.createAccountCreator(null);
    }

    public void ConfigAccount(String username, String password) {
        String usernameWithoutDomain = username;
        if (username.split("@").length > 1) {
            usernameWithoutDomain = username.split("@")[0];
        }

        android.util.Log.d("xxxCALL_______________", username);

        mAccountCreator = mCore.createAccountCreator(null);
        mAccountCreator.setDomain(BeowulInfo.getInstance().getDomain());
        mAccountCreator.setUsername(usernameWithoutDomain);
        mAccountCreator.setPassword(password);
        mAccountCreator.configure();

        ProxyConfig[] proxyConfigs = mCore.getProxyConfigList();
        if (proxyConfigs.length > 0) {
            org.linphone.core.tools.Log.i("CALLLLLLLLLLLLLLL t " + proxyConfigs.length);
            mProxyConfig = proxyConfigs[0];
            RegisterAccount(usernameWithoutDomain, password);
        }

        // nhuongnv
        mCore.setMaxCalls(1);
        // ~nhuongnv
    }

    public void RegisterAccount(String username, String password) {
        if (mProxyConfig != null) {

            if (mAuthInfo != null) {
                mAuthInfo.setHa1(null);
                mAuthInfo.setPassword(password);
                if (mCore != null) {
                    mCore.addAuthInfo(mAuthInfo);
                    mCore.refreshRegisters();
                }
            } else {
                android.util.Log.d("xxxx", " mAuthInfo is null");
            }

            mProxyConfig.edit();
            String server = mProxyConfig.getServerAddr();
            Address serverAddr = org.linphone.core.Factory.instance().createAddress(server);
            serverAddr.setTransport(TransportType.Tls);
            server = serverAddr.asString();
            mProxyConfig.setServerAddr(server);
            mProxyConfig.setRoute(BeowulInfo.getInstance().getProxyconfig());//"<sip:sip.beowulfchain.com:443;transport=tls>"

            Address proxy = org.linphone.core.Factory.instance().createAddress(BeowulInfo.getInstance().getProxyconfig());//"<sip:sip.beowulfchain.com:443;transport=tls>"
            mProxyConfig.setServerAddr(proxy.asString());
            mProxyConfig.setRoute(BeowulInfo.getInstance().getProxyconfig());
            mAuthInfo.setUsername(username);

            Address identity = mProxyConfig.getIdentityAddress();
            if (identity != null) {
                identity.setUsername(username);
            }
            mProxyConfig.setIdentityAddress(identity);
            mAuthInfo.setDomain(BeowulInfo.getInstance().getDomain());
            Address identity1 = mProxyConfig.getIdentityAddress();
            if (identity1 != null) identity1.setDomain(BeowulInfo.getInstance().getDomain());

            mProxyConfig.setIdentityAddress(identity1);
            mProxyConfig.setExpires(30);
            mPrefs.enableVideo(true);
            mPrefs.setAutomaticallyAcceptVideoRequests(true);

            mProxyConfig.done();
        }
    }


    public void UnregisterAcc() {

        String username = "1231";
        String password = "456";

        if (mProxyConfig != null) {

            if (mAuthInfo != null) {
                mAuthInfo.setHa1(null);
                mAuthInfo.setPassword(password);
                try {
                    if (mCore != null) {
                        mCore.addAuthInfo(mAuthInfo);
                        mCore.refreshRegisters();
                    }
                } catch (Exception e) {
                    String exx = e.toString();
                }

            } else {
                android.util.Log.d("xxxx", " mAuthInfo is null");
            }

            mProxyConfig.edit();
            String server = mProxyConfig.getServerAddr();
            Address serverAddr = org.linphone.core.Factory.instance().createAddress(server);
            serverAddr.setTransport(TransportType.Tls);
            server = serverAddr.asString();
            mProxyConfig.setServerAddr(server);
            mProxyConfig.setRoute(BeowulInfo.getInstance().getProxyconfig());//"<sip:sip.beowulfchain.com:443;transport=tls>"

            Address proxy = org.linphone.core.Factory.instance().createAddress(BeowulInfo.getInstance().getProxyconfig());//"<sip:sip.beowulfchain.com:443;transport=tls>"
            mProxyConfig.setServerAddr(proxy.asString());
            mProxyConfig.setRoute(BeowulInfo.getInstance().getProxyconfig());
            mAuthInfo.setUsername(username);

            Address identity = mProxyConfig.getIdentityAddress();
            if (identity != null) {
                identity.setUsername(username);
            }
            mProxyConfig.setIdentityAddress(identity);
            mAuthInfo.setDomain(BeowulInfo.getInstance().getDomain());
            Address identity1 = mProxyConfig.getIdentityAddress();
            if (identity1 != null) identity1.setDomain(BeowulInfo.getInstance().getDomain());

            mProxyConfig.setIdentityAddress(identity1);
            mProxyConfig.setExpires(30);
            mPrefs.enableVideo(true);
            mPrefs.setAutomaticallyAcceptVideoRequests(true);

            mProxyConfig.done();

            mProxyConfig.edit();
            mProxyConfig.setExpires(0);
            mProxyConfig.done();

        }
    }

    public void CleanupProxyAndAuthoinfo() {

        try {
            if (null != mProxyConfig) {
                mProxyConfig.edit();
                mProxyConfig.setExpires(0);
                mProxyConfig.done();
            }


            int regis_count = 0;
            if (null != LinphoneManagerClone.getInstance().mCore.getDefaultProxyConfig() && null != LinphoneManagerClone.getInstance().mCore) {
                RegistrationState state = LinphoneManagerClone.getInstance().mCore.getDefaultProxyConfig().getState();
                if (null != state) {
                    while (state != RegistrationState.Cleared && state != RegistrationState.Failed) {
                        LinphoneManagerClone.getInstance().mCore.iterate();
                        regis_count++;
                        if (regis_count > 5) {
                            break;
                        }
                    }
                }
            }

            mCore.clearAllAuthInfo();
            mCore.clearProxyConfig();
            mCore.clearCallLogs();
        } catch (Exception e) {

        }


    }

}



