package com.beowulfchain.beowulfchainsdk.linhphone;

/*
CallVideoFragment.java
Copyright (C) 2017  Belledonne Communications, Grenoble, France

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

import android.app.Fragment;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.constraint.ConstraintLayout;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Chronometer;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.beowulfchain.beowulfchainsdk.R;

import org.linphone.core.Call;
import org.linphone.core.tools.Log;


public class CallVideoFragment extends Fragment implements View.OnTouchListener {
    private TextureView mVideoView;
    private TextureView mCaptureView;
    Chronometer timer;
    ViewGroup _root;
    private int _xDelta;
    private int _yDelta;
    float mRatio = 1.0f;
    int mBaseDist;
    float mBaseRatio;
    final static float STEP = 10000;
    int calling = 0;

    static CallVideoFragment instance = null;

    public static CallVideoFragment getInstance() {
        return instance;
    }

    public static boolean isInstance() {
        return null != instance;
    }

    final DisplayMetrics metrics = new DisplayMetrics();

    int camtureOrgSizeH = 0;
    int camtureOrgSizeW = 0;

    boolean frontCamera = true;

    View capture_screen_cover;

    LinearLayout pre_view_cam;

    ConstraintLayout waiting_cover;
    TextView tv_lable;
    TextView tv_call_type;


    String lable = "";
    String callType = "audio";
    ImageView iv_switch_cam;
    View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

            if (LinphoneManagerClone.getLc().hasCrappyOpengl()) {
                view = inflater.inflate(R.layout.video_no_opengl, container, false);
                getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            } else {
                view = inflater.inflate(R.layout.video, container, false);
                getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            }


            instance = this;
            iv_switch_cam = view.findViewById(R.id.iv_switch_cam);

            LinphoneManagerClone.getInstance().enableSpeaker(true);
            LinphoneManagerClone.getInstance().routeAudioToSpeaker();
            LinphonePreferences.instance().setEchoCancellation(true);
            LinphonePreferences.instance().setPlaybackGainDb((float) 4.0);

            timer = view.findViewById(R.id.current_call_timer);
            waiting_cover = view.findViewById(R.id.waiting_cover);
            tv_lable = view.findViewById(R.id.tv_lable);
            tv_call_type = view.findViewById(R.id.tv_call_type);
            mVideoView = view.findViewById(R.id.videoSurface);
            mCaptureView = view.findViewById(R.id.videoCaptureSurface);
            LinphoneManagerClone.getLc().setNativeVideoWindowId(mVideoView);
            LinphoneManagerClone.getLc().setNativePreviewWindowId(mCaptureView);
            capture_screen_cover = view.findViewById(R.id.capture_screen_cover);
            _root = (ViewGroup) view.findViewById(R.id.video_frame);

            getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
            android.util.Log.d("MARGIN__size", "" + metrics.widthPixels + "-----" + metrics.heightPixels);


            ReSizeRcvScreen(480, 640);

            iv_switch_cam.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    switchCamera();
                }
            });

            pre_view_cam = view.findViewById(R.id.pre_view_cam);
            pre_view_cam.setOnTouchListener(this);

            if (callType.equals("video"))
                tv_call_type.setText(getResources().getText(R.string.text_video_from));

            if (callType.equals("audio"))
                tv_call_type.setText(getResources().getText(R.string.text_audio_from));


            tv_lable.setText(lable);

//            if (getActivity().equals(OutGoingCall.getInstance())){
//                if (OutGoingCall.getInstance().isVideo) HideCover();
//            }

            return view;

    }


    public void HideLable()
    {
        tv_lable.setVisibility(View.GONE);
    }

    public static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    public static int pxToDp(int px) {
        return (int) (px / Resources.getSystem().getDisplayMetrics().density);
    }


    public void ReSizeRcvScreen(int wVideoSize, int hVideoSize) {

        DisplayMetrics metrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int screenWidth = metrics.widthPixels;
        int screenHeight = metrics.heightPixels;
        float screenRatio = (float) screenWidth / screenHeight;
        float videoRatio = (float) wVideoSize / hVideoSize;

        int newWVideoSize = screenWidth;
        int newHVideoSize = (int) (newWVideoSize / videoRatio);

        if (newHVideoSize < screenHeight) {
            newHVideoSize = screenHeight;
            newWVideoSize = (int) (screenHeight * videoRatio);
        }
        final ConstraintLayout.LayoutParams param = (ConstraintLayout.LayoutParams) mVideoView.getLayoutParams();

        param.height = newHVideoSize;
        param.width = newWVideoSize;
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mVideoView.setLayoutParams(param);
            }
        });

    }

    public void HideCover() {
        waiting_cover.setVisibility(View.GONE);
        tv_lable.setVisibility(View.GONE);
        tv_call_type.setVisibility(View.GONE);
    }


    public void registerCallDurationTimer(Call call) {

        timer.setVisibility(View.VISIBLE);

        int callDuration = call.getDuration();
        if (callDuration == 0 && call.getState() != Call.State.StreamsRunning) {
            return;
        }
        timer.setBase(SystemClock.elapsedRealtime() - 1000 * callDuration);
        timer.start();
    }


    public void SetLable(String lable) {
        //    tv_lable.setText(lable);
        this.lable = lable;
    }

    public void SetCallType(String callType) {
        this.callType = callType;
    }

    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onStop() {
        super.onStop();

//        NotificationHelper notificationHelper = new NotificationHelper(getActivity());
//        notificationHelper.createNotification("Quickom", "Calling");
    }

    public void DisableCaptureScreen(final boolean isDisable) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (true == isDisable)
                    capture_screen_cover.setVisibility(View.VISIBLE);
                else
                    capture_screen_cover.setVisibility(View.GONE);
            }
        });

    }

    public void SetFrontCamera() {
        String[] devices = LinphoneManagerClone.getLc().getVideoDevicesList();
        String defaultDevice = "";
        if (devices.length >= 2) {
            defaultDevice = devices[1];
        } else {
            if (devices.length >= 1)
                defaultDevice = devices[0];
        }

        if (!"".matches(defaultDevice)) {
            LinphoneManagerClone.getLc().setVideoDevice(defaultDevice);
            CallManager.getInstance().updateCall();
        }
    }

    public void switchCamera() {
        try {
            String currentDevice = LinphoneManagerClone.getLc().getVideoDevice();
            String[] devices = LinphoneManagerClone.getLc().getVideoDevicesList();
            int index = 0;
            for (String d : devices) {
                if (d.equals(currentDevice)) {
                    break;
                }
                index++;
            }
            String newDevice;
            if (index == 1) newDevice = devices[0];
            else if (devices.length > 1) newDevice = devices[1];
            else newDevice = devices[index];
            LinphoneManagerClone.getLc().setVideoDevice(newDevice);

            CallManager.getInstance().updateCall();
        } catch (ArithmeticException ae) {
            Log.e("[Video Fragment] Cannot swtich camera : no camera");
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (LinphonePreferences.instance().isOverlayEnabled()) {
            //  LinphoneService.instance().destroyOverlay();
        }

        // resizePreview();
    }
    //
    //    @Override
    //    public void onPause() {
    //        Core lc = LinphoneManager.getLcIfManagerNotDestroyedOrNull();
    //        if (LinphonePreferences.instance().isOverlayEnabled()
    //                && lc != null
    //                && lc.getCurrentCall() != null) {
    //            Call call = lc.getCurrentCall();
    //            if (call.getState() == Call.State.StreamsRunning) {
    //                // Prevent overlay creation if video call is paused by remote
    //                LinphoneService.instance().createOverlay();
    //            }
    //        }
    //
    //        super.onPause();
    //    }

    @Override
    public void onDestroy() {
        //  mInCallActivity = null;
        mCaptureView = null;
        if (mVideoView != null) {
            mVideoView.setOnTouchListener(null);
            mVideoView = null;
        }
        super.onDestroy();
        instance=null;
    }


    int getDistance(MotionEvent event) {


//        getActivity().runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//                try {
//                    Thread.sleep(5000);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//                android.support.constraint.ConstraintLayout.LayoutParams layoutParams = (android.support.constraint.ConstraintLayout.LayoutParams) mCaptureView.getLayoutParams();
//                float ratioImage = mCaptureView.getWidth() / mCaptureView.getHeight();
//                layoutParams.height = metrics.heightPixels / 3;
//                layoutParams.width = (int) (layoutParams.height * ratioImage);
//                mCaptureView.setLayoutParams(layoutParams);
//            }
//        });


        int dx = (int) (event.getX(0) - event.getX(1));
        int dy = (int) (event.getY(0) - event.getY(1));
        return (int) (Math.sqrt(dx * dx + dy * dy));
    }

    @Override
    public boolean onTouch(View view, MotionEvent event) {
//        try {
//            Thread.sleep(10);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//        if(event.getPointerCount()==2)
//        {
//            camtureOrgSizeH= mCaptureView.getHeight();
//            camtureOrgSizeW=mCaptureView.getWidth();
//
//            int action = event.getAction();
//            int pureaction = action & MotionEvent.ACTION_MASK;
//            if (pureaction == MotionEvent.ACTION_POINTER_DOWN) {
//                mBaseDist = getDistance(event);
//                mBaseRatio = mRatio;
//            } else {
//                float delta = (getDistance(event) - mBaseDist) / STEP;
//                float multi = (float) Math.pow(2, delta);
//                mRatio = Math.min(1024.0f, Math.max(0.1f, mBaseRatio * multi));
//                android.support.constraint.ConstraintLayout.LayoutParams layoutParams = (android.support.constraint.ConstraintLayout.LayoutParams) view.getLayoutParams();
//                layoutParams.height=(int)(camtureOrgSizeH*mRatio);
//                layoutParams.width=(int)(camtureOrgSizeW*mRatio);
//                view.setLayoutParams(layoutParams);
////                layoutParams.leftMargin = X - _xDelta;
////                layoutParams.topMargin = Y - _yDelta;
////                layoutParams.rightMargin = -250;
////                layoutParams.bottomMargin = -250;
////                view.setLayoutParams(layoutParams);
//
//            }
//
//        }
        ;

        pre_view_cam.getHeight();


        camtureOrgSizeW = mCaptureView.getWidth();
        camtureOrgSizeH = mCaptureView.getHeight();


        final int X = (int) event.getRawX();
        final int Y = (int) event.getRawY();
        switch (event.getAction() & MotionEvent.ACTION_MASK) {


            case MotionEvent.ACTION_DOWN:
                android.support.constraint.ConstraintLayout.LayoutParams lParams = (android.support.constraint.ConstraintLayout.LayoutParams) view.getLayoutParams();
                _xDelta = X - lParams.leftMargin;
                _yDelta = Y - lParams.topMargin;
                break;
            case MotionEvent.ACTION_UP:
                break;
            case MotionEvent.ACTION_POINTER_DOWN:
                break;
            case MotionEvent.ACTION_POINTER_UP:
                break;
            case MotionEvent.ACTION_MOVE:
                android.util.Log.d("MARGIN__bview ", view.getX() + "" + view.getY());


                android.support.constraint.ConstraintLayout.LayoutParams layoutParams = (android.support.constraint.ConstraintLayout.LayoutParams) view.getLayoutParams();

                layoutParams.leftMargin = X - _xDelta;
                layoutParams.topMargin = Y - _yDelta;
                layoutParams.rightMargin = -250;
                layoutParams.bottomMargin = -250;


                android.util.Log.d("MARGIN__l", "" + layoutParams.leftMargin);
                android.util.Log.d("MARGIN__t", "" + layoutParams.topMargin);
                android.util.Log.d("MARGIN__r", "" + layoutParams.rightMargin);
                android.util.Log.d("MARGIN__b", "" + layoutParams.bottomMargin);


                android.util.Log.d("MARGIN__b", "" + (X + pre_view_cam.getWidth()));

                if (view.getX() < 0)
                    view.setX(0);
                if (view.getY() < 0)
                    view.setY(0);

                if ((view.getX() + pre_view_cam.getWidth()) > metrics.widthPixels)
                    view.setX(metrics.widthPixels - pre_view_cam.getWidth());

                if ((view.getY() + pre_view_cam.getHeight() > metrics.heightPixels))
                    view.setY(metrics.heightPixels - pre_view_cam.getHeight());


                view.setLayoutParams(layoutParams);
                break;
        }
        _root.invalidate();

        return true;
    }


    public void FinishThisCall() {
        getActivity().finish();
    }




}

