package com.beowulfchain.beowulfchainsdk.linhphone;

/*
CallManager.java
Copyright (C) 2017  Belledonne Communications, Grenoble, France

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/


import android.util.Base64;
import com.beowulfchain.beowulfchainsdk.account.SipAccountData;
import org.json.JSONException;
import org.json.JSONObject;
import org.linphone.core.Address;
import org.linphone.core.Call;
import org.linphone.core.CallParams;
import org.linphone.core.Core;
import org.linphone.core.MediaEncryption;
import org.linphone.core.tools.Log;

import java.io.UnsupportedEncodingException;


/**
 * Handle call updating, reinvites.
 */
public class CallManager {

    private static CallManager sInstance;

    public static synchronized CallManager getInstance() {
        if (sInstance == null) sInstance = new CallManager();
        return sInstance;
    }

    private CallManager() {
    }

    private BandwidthManager getBandwidthManager() {
        return BandwidthManager.getInstance();
    }


    public void updateCall() {
        Core lc = LinphoneManagerClone.getLc();
        Call lCall = lc.getCurrentCall();
        if (lCall == null) {
            return;
        }
        CallParams params = lc.createCallParams(lCall);
        getBandwidthManager().updateWithProfileSettings(params);
        lc.updateCall(lCall, null);
    }


    public void newOutgoingCall(AddressType address) {
        String to = address.getText().toString();
        newOutgoingCall(to, address.getDisplayedName());
    }


    public void newOutgoingCallAudioOnly(String to, String displayName,String qrCodeAlias,String rid) {
      //  if (null != displayName) Log.i("newOutgoingCall " + displayName);

        Address lAddress;
        lAddress = LinphoneManagerClone.getInstance().mCore.interpretUrl(to); // InterpretUrl does normalizePhoneNumber

        android.util.Log.d(
                "LINPHONE_MAG", "[Manager] Couldn't convert to String to Address : " + to);
        if (lAddress == null) {
            Log.e("[Manager] Couldn't convert to String to Address : " + to);
            return;
        }

        if (null != displayName) Log.i("newOutgoingCall " + displayName);

        if (displayName != null) lAddress.setDisplayName(displayName);

        if (null != lAddress) Log.i("newOutgoingCall lAddress " + lAddress.asString());

        newOutgoingCallAudio(lAddress ,qrCodeAlias,rid);
    }


    public void newOutgoingCallAudio(Address to,String qrCodeAlias,String rid) {
        if (to == null) return;
        //{"isVideoCall":"1"}/quickcom
        String appAgent = "quickom_android";

        String strBase64="";
        JSONObject objCall = new JSONObject();
        try {
            objCall.put("isVideoCall", "0");
            objCall.put("qrcode", qrCodeAlias);
            objCall.put("rid", rid);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            byte[] bytes = objCall.toString().getBytes("UTF-8");
            byte[] bytesBase64Agent=Base64.encode(bytes, Base64.DEFAULT);
            strBase64=new String(bytesBase64Agent);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }


        if (LinphoneManagerClone.getInstance().mCore.isNetworkReachable()) {


            Core lc = LinphoneManagerClone.getLc();
            lc.setUserAgent(strBase64,"");
            CallParams params = lc.createCallParams(null);

            params.enableVideo(false);
            params.enableLowBandwidth(false);

            params.setMediaEncryption(MediaEncryption.None);


            lc.inviteAddressWithParams(to, params);


        }

    }

    public void TerminateAllCall()
    {
        LinphoneManagerClone.getInstance().mCore.terminateAllCalls();
    }

    public void newOutgoingCall(String to, String displayName) {
        if (null != displayName) Log.i("newOutgoingCall " + displayName);

        Address lAddress;
        lAddress = LinphoneManagerClone.getInstance().mCore.interpretUrl(to); // InterpretUrl does normalizePhoneNumber

        if (lAddress == null) {
            Log.e("[Manager] Couldn't convert to String to Address : " + to);
            return;
        }

        if (null != displayName) Log.i("newOutgoingCall " + displayName);

        if (displayName != null) lAddress.setDisplayName(displayName);

        if (null != lAddress) Log.i("newOutgoingCall lAddress " + lAddress.asString());

        newOutgoingCall(lAddress);
    }


    public void newOutgoingCallVideo(String to, String displayName,String qrCodeAlias,String rid) {
        if (null != displayName) Log.i("newOutgoingCall " + displayName);


        if (to.split("@").length < 2) {
            to = "sip:" + to + "@" + SipAccountData.getInstance().getSip_domain();//"@beowulfchain.com";
        }
        Address lAddress;
        lAddress = LinphoneManagerClone.getInstance().mCore.interpretUrl(to); // InterpretUrl does normalizePhoneNumber


        if (lAddress == null) {
            Log.e("[Manager] Couldn't convert to String to Address (VIDEO): " + to);
            return;
        }

        if (null != displayName) Log.i("newOutgoingCall " + displayName);

        if (displayName != null) lAddress.setDisplayName(displayName);

        if (null != lAddress) Log.i("newOutgoingCall lAddress " + lAddress.asString());

        newOutgoingCallVideo(lAddress,qrCodeAlias,rid);
    }

    public void newOutgoingCallVideo(Address to,String qrCodeAlias,String rid) {
        if (to == null) return;

        JSONObject objCall = new JSONObject();
        try {
            objCall.put("isVideoCall", 0);
            objCall.put("qrcode", qrCodeAlias);
            objCall.put("rid", rid);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (LinphoneManagerClone.getInstance().mCore.isNetworkReachable()) {

            Core lc = LinphoneManagerClone.getLc();
            lc.setUserAgent(objCall.toString(),"1.5/version");
            CallParams params = lc.createCallParams(null);

            params.enableVideo(true);
            params.enableLowBandwidth(false);

            params.setMediaEncryption(MediaEncryption.ZRTP);

//

            lc.inviteAddressWithParams(to, params);

//
        }

    }


    public void newOutgoingCall(Address to) {
        if (to == null) return;

        //{"isVideoCall":"1"}/quickcom
        String appAgent = "quickom_android";
        JSONObject objCall = new JSONObject();
        try {
            objCall.put("isVideoCall", 1);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        byte[] base64byte = appAgent.getBytes();
        String base64Str = Base64.encodeToString(base64byte, Base64.DEFAULT);
        android.util.Log.d("BASE64", "_____" + base64Str);
        LinphoneManagerClone.getLc().setUserAgent(base64Str, appAgent);


        //        ProxyConfig lpc = mCore.getDefaultProxyConfig();
        //        if (mRessources.getBoolean(R.bool.forbid_self_call)
        //                && lpc != null
        //                && to.weakEqual(lpc.getIdentityAddress())) {
        //            return;
        //        }

//        boolean isLowBandwidthConnection =
//                !LinphoneUtils.isHighBandwidthConnection(
//                        LinphoneService.instance().getApplicationContext());
//        isLowBandwidthConnection = false;
        if (LinphoneManagerClone.getInstance().mCore.isNetworkReachable()) {
            //            if (Version.isVideoCapable()) {
            //                boolean prefVideoEnable = mPrefs.isVideoEnabled();
            //                boolean prefInitiateWithVideo = mPrefs.shouldInitiateVideoCall();
            //
            //                CallManager.getInstance()
            //                        .inviteAddress(
            //                                to,
            //                                false /*prefVideoEnable && prefInitiateWithVideo*/,
            //                                isLowBandwidthConnection);
            //            } else {
            //
            //                CallManager.getInstance().inviteAddress(to, false,
            // isLowBandwidthConnection);
            //            }

            Core lc = LinphoneManagerClone.getLc();

            CallParams params = lc.createCallParams(null);

            params.enableVideo(false);
            params.enableLowBandwidth(false);

            params.setMediaEncryption(MediaEncryption.ZRTP);

//            String recordFile =
//                    FileUtils.getCallRecordingFilename(
//                            LinphoneManager.getInstance().getContext(), to);
            //  params.setRecordFile(recordFile);

            lc.inviteAddressWithParams(to, params);

//        } else if (LinphoneActivity.isInstanciated()) {
//            LinphoneActivity.instance()
//                    .displayCustomToast(
//                            getString(R.string.error_network_unreachable), Toast.LENGTH_LONG);
//        } else {
//            Log.e("[Manager] Error: " + getString(R.string.error_network_unreachable));
//        }
        }

    }



    public void newOutgoingVideoCall(String to, String displayName,String domain) {
        if (null != displayName) Log.i("newOutgoingCall " + displayName);


        if (to.split("@").length < 2) {
            to = "sip:" + to + "@" + SipAccountData.getInstance().getSip_domain();//"@beowulfchain.com";
        }
        Address lAddress;
        lAddress = LinphoneManagerClone.getInstance().mCore.interpretUrl(to); // InterpretUrl does normalizePhoneNumber


        if (lAddress == null) {
            Log.e("[Manager] Couldn't convert to String to Address (VIDEO): " + to);
            return;
        }

        if (null != displayName) Log.i("newOutgoingCall " + displayName);

        if (displayName != null) lAddress.setDisplayName(displayName);

        if (null != lAddress) Log.i("newOutgoingCall lAddress " + lAddress.asString());

        if (LinphoneManagerClone.getInstance().mCore.isNetworkReachable()) {
            Core lc = LinphoneManagerClone.getLc();
            CallParams params = lc.createCallParams(null);
            params.enableVideo(true);
            params.enableLowBandwidth(false);
            params.setMediaEncryption(MediaEncryption.ZRTP);

            lc.inviteAddressWithParams(lAddress, params);
        }
    }

    public void newOutgoingVoiceCall(String to, String displayName,String groupnameOrQRCode,String requestId) {
        if (null != displayName) Log.i("newOutgoingCall " + displayName);


        if (to.split("@").length < 2) {
            to = "sip:" + to + "@" + SipAccountData.getInstance().getSip_domain();//"@beowulfchain.com";
        }
        Address lAddress;
        lAddress = LinphoneManagerClone.getInstance().mCore.interpretUrl(to); // InterpretUrl does normalizePhoneNumber


        if (lAddress == null) {
            Log.e("[Manager] Couldn't convert to String to Address (VIDEO): " + to);
            return;
        }

        if (null != displayName) Log.i("newOutgoingCall " + displayName);

        if (displayName != null) lAddress.setDisplayName(displayName);

        if (null != lAddress) Log.i("newOutgoingCall lAddress " + lAddress.asString());



        String appAgent = "quickom_android";

        String strBase64="";
        JSONObject objCall = new JSONObject();
        try {
            objCall.put("isVideoCall", "0");
            objCall.put("qrcode", groupnameOrQRCode);
            objCall.put("rid", requestId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String userAgentBeforeBase64=objCall.toString();
        String userAgentAfterBase64="";

        try {
            byte[] bytes = objCall.toString().getBytes("UTF-8");
            byte[] bytesBase64Agent=Base64.encode(bytes, Base64.DEFAULT);
            strBase64=new String(bytesBase64Agent);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        userAgentAfterBase64=strBase64;

        if (LinphoneManagerClone.getInstance().mCore.isNetworkReachable()) {
            Core lc = LinphoneManagerClone.getLc();
           // lc.setUserAgent(strBase64,strBase64);
            CallParams params = lc.createCallParams(null);
            params.enableVideo(false);
            params.enableLowBandwidth(false);
            params.setMediaEncryption(MediaEncryption.ZRTP);

            lc.inviteAddressWithParams(lAddress, params);
        }
    }



    public interface AddressType {
        CharSequence getText();

        void setText(CharSequence s);

        String getDisplayedName();

        void setDisplayedName(String s);
    }
}
