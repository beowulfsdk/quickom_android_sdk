package com.beowulfchain.beowulfchainsdk;

import android.os.SystemClock;

import com.beowulfchain.beowulfchainsdk.AppData.AppData.ChatMessage.ChatMess;
import com.beowulfchain.beowulfchainsdk.AppData.AppData.ChatMessage.ChatMessSend;
import com.beowulfchain.beowulfchainsdk.AppData.AppData.ChatMessage.ChatMessStatus;
import com.beowulfchain.beowulfchainsdk.Service.ChatConnection;
import com.beowulfchain.beowulfchainsdk.Service.SipConnection;
import com.beowulfchain.beowulfchainsdk.account.ChatAccountData;
import com.beowulfchain.beowulfchainsdk.linhphone.LinphoneManagerClone;
import com.beowulfchain.beowulfchainsdk.linhphone.LinphonePreferences;
import com.beowulfchain.beowulfchainsdk.util.ChatObservation;

import org.linphone.core.Call;
import org.linphone.core.Core;

import java.io.File;
import java.util.ArrayList;

public class BWFCallCenter {

    private static SdkCallBack resultCallback = null;
    private static ArrayList<ErrorCallBack> errorCallback = new ArrayList<>();
    private static ArrayList<VoiceVideoCallCallBack> requestError = new ArrayList<>();
    static ArrayList<ChatObservation> observationArrayList = new ArrayList<>();
    private static BWFCallCenter instance = null;
    boolean sipResult = false;
    boolean chatResult = false;

    public static BWFCallCenter getInstance() {
        if (null == instance)
            instance = new BWFCallCenter();
        return instance;
    }

    public void startAsAnonymousRequester() {
        BWFCallCenter.getInstance().StartSip();
        MainApplication.GetIdleAccount(MainApplication.api_key);
    }

    public void StartSip() {
        LinphoneManagerClone.createAndStart(MainApplication.getSDKContext(), true);
        LinphoneManagerClone.getLc().addListener(SipConnection.getInstance());
        LinphonePreferences.instance().setMicGainDb((float) 2.5f);
    }

    public void configureWithApiKey(String apiKey) {
        MainApplication.api_key = apiKey;
    }

    public void SetSDKInitCallback(SdkCallBack resultCallback) {
        if (null != this.resultCallback)
            this.resultCallback = null;
        this.resultCallback = resultCallback;
    }


    public void RemoveCallBack() {
        this.resultCallback = null;
    }


    public void onSipResult(boolean result) {
        sipResult = result;
        if (null != this.resultCallback) {
            this.resultCallback.onSipResult(result);
            this.resultCallback.onSuccess(chatResult & sipResult);
        }
    }

    public void onChatResult(boolean result) {
        chatResult = result;
        if (null != this.resultCallback) {
            this.resultCallback.onChatResult(result);
            this.resultCallback.onSuccess(chatResult & sipResult);

        }

    }

    public void EndCall() {
        SipConnection.getInstance().EndCall();
        ChatConnection.getInstance().DropTheOthersCallExceptAvailableId();
    }


    public static void RegisterErrorCallBack(ErrorCallBack callBack) {
        if (!errorCallback.contains(callBack))
            errorCallback.add(callBack);
    }

    public static void RemoveErrorCallBack(ErrorCallBack callBack) {
        if (errorCallback.contains(callBack))
            errorCallback.remove(callBack);
    }

    public static void RegisterVoiceVideoCallCallback(VoiceVideoCallCallBack callBack) {
        if (!requestError.contains(callBack))
            requestError.add(callBack);
    }

    public static void RemoveVoiceVideoCallCallback(VoiceVideoCallCallBack callBack) {
        if (requestError.contains(callBack))
            requestError.remove(callBack);
    }


    public static void NotifyNewChat(ChatMess textChatMess) {
        for (ChatObservation observation : observationArrayList) {
            observation.onUserDataChanged(textChatMess);
        }
    }


    public static void VoiceVideoCallResult(int error, String description) {
        for (VoiceVideoCallCallBack callback : requestError) {
            callback.onResult(error, description);
        }
    }


    public void onNumberOfSupporterRemainded(int number) {
        for (ErrorCallBack callback : this.errorCallback) {
            callback.onNumberOfSupporterRemainded(number);
        }
    }

    public String requestVoiceCallWithDisplayName(String optionnal_name, String group_name) {
        return MainApplication.GetUserViaGroupNameForCall(group_name, optionnal_name);
        //    MainApplication.GetQrDetailInfoAndCall(supporter_name);

    }


    public static boolean sendText(String expectedConversationId, ChatMess message) {
        boolean ret = false;
        ret = ChatConnection.PushChatToQueue(expectedConversationId, message);
        MainApplication.InsertTextChatMsg(message);
        return ret;
    }


    public static boolean sendText(String messContent, String msgIdTo) {
        boolean ret = false;
        String expectedConversationId = ChatConnection.getInstance().getCurrentConversation();
        ChatMessSend message = new ChatMessSend(messContent, ChatAccountData.getInstance().getUserChatIdWithOutDomain(), msgIdTo, expectedConversationId);
        message.setConversationId(expectedConversationId);
        ret = ChatConnection.PushChatToQueue(expectedConversationId, message);
        MainApplication.InsertTextChatMsg(message);
        return ret;
    }


    public void stop() {

        ChatConnection.getInstance().Disconnect();
    }

    public void RemoveAllChat() {
        MainApplication.getInstance().DeleteAllChatRecord();
    }

    public static String sendImage(File compressedImgFile) {
        String expectedConversationId = ChatConnection.getInstance().getCurrentConversation();
        return ChatConnection.UploadImageMessage(expectedConversationId, compressedImgFile);

    }

    public String requestChatWithDisplayName(String optionnal_name, String group_name) {
        return MainApplication.GetUserViaGroupNameForChat(optionnal_name, group_name);

    }


    public void EnableVideo(boolean enable) {
        if (true == enable)
            LinphoneManagerClone.getInstance().routeAudioToSpeaker();
        else
            LinphoneManagerClone.getInstance().routeAudioToReceiver();

    }

    public boolean SendSeen(ChatMessStatus message) {
        return ChatConnection.getInstance().SendSeen(message);
    }


    static ArrayList<CallStateChange> stateChange = new ArrayList<>();

    public interface CallStateChange {
        public void onCallStateChage(int state);
    }

    public static void RegisterState(CallStateChange callStateChange) {
        if (!stateChange.contains(callStateChange))
            stateChange.add(callStateChange);
    }

    public static void RemoveCallState(CallStateChange callStateChange) {
        if (stateChange.contains(callStateChange))
            stateChange.remove(callStateChange);

    }


    public static void RegisterChatObserver(ChatObservation observation) {
        if (!observationArrayList.contains(observation))
            observationArrayList.add(observation);
    }


    public static void RemoveChatObserver(ChatObservation observation) {
        if (observationArrayList.contains(observation))
            observationArrayList.remove(observation);
    }


    public static void SetCallStateChange(int state) {
        for (CallStateChange newstate : stateChange) {
            newstate.onCallStateChage(state);
        }
    }

    public interface ErrorCallBack {
        public void onNumberOfSupporterRemainded(int numberOfRemainded);
    }


    public interface VoiceVideoCallCallBack {
        public void onResult(int errorCode, String description);
    }

    public interface SdkCallBack {

        public void onChatResult(boolean chatResult);

        public void onSipResult(boolean sipResult);

        public void onSuccess(boolean result);

    }


    public long getCallDuration() {
        long ret = 0;
        Core core = LinphoneManagerClone.getLc();
        if (null != core) {
            Call call = core.getCurrentCall();
            if (null != call) {
                int callDuration = call.getDuration();
                if (callDuration == 0 && call.getState() != Call.State.StreamsRunning) {
                    ret = 0;
                } else
                    ret = SystemClock.elapsedRealtime() - 1000 * callDuration;
            }

        }

        return ret;
    }

    public static enum State {
        Idle(0),
        IncomingReceived(1),
        OutgoingInit(2),
        OutgoingProgress(3),
        OutgoingRinging(4),
        OutgoingEarlyMedia(5),
        Connected(6),
        StreamsRunning(7),
        Pausing(8),
        Paused(9),
        Resuming(10),
        Referred(11),
        Error(12),
        End(13),
        PausedByRemote(14),
        UpdatedByRemote(15),
        IncomingEarlyMedia(16),
        Updating(17),
        Released(18),
        EarlyUpdatedByRemote(19),
        EarlyUpdating(20);

        protected final int mValue;

        private State(int value) {
            this.mValue = value;
        }

        public int toInt() {
            return mValue;
        }

    }

}
