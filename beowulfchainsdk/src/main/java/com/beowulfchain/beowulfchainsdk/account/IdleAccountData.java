package com.beowulfchain.beowulfchainsdk.account;

import org.json.JSONException;
import org.json.JSONObject;

public class IdleAccountData {
    static IdleAccountData instance = null;
    public static IdleAccountData getInstance() {
        if (null == instance)
            instance = new IdleAccountData();
        return instance;
    }
    public static boolean isInstance() {
        return null != instance;
    }

    String super_account_id = "";
    String account_id = "";
    String reference_id = "";
    String sip_id = "";
    String created_at = "";
    String account_type = "";
    String status = "";
    String name = "";
    String group = "";
    String chat_id = "";


    public void ParseData(String jsonData) {
        super_account_id = "";
        account_id = "";
        reference_id = "";
        sip_id = "";
        created_at = "";
        account_type = "";
        status = "";
        name = "";
        group = "";
        chat_id = "";

        try {
            JSONObject object = new JSONObject(jsonData);

            if (!object.isNull("super_account_id"))
                super_account_id = object.getString("super_account_id");

            if (!object.isNull("account_id"))
                account_id = object.getString("account_id");

            if (!object.isNull("reference_id"))
                reference_id = object.getString("reference_id");

            if (!object.isNull("sip_id"))
                sip_id = object.getString("sip_id");

            if (!object.isNull("created_at"))
                created_at = object.getString("created_at");

            if (!object.isNull("account_type"))
                account_type = object.getString("account_type");

            if (!object.isNull("status"))
                status = object.getString("status");

            if (!object.isNull("name"))
                name = object.getString("name");

            if (!object.isNull("group"))
                group = object.getString("group");

            if (!object.isNull("chat_id")) {
                chat_id = object.getString("chat_id");
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    public void ClearAlldata() {
        super_account_id = "";
        account_id = "";
        reference_id = "";
        sip_id = "";
        created_at = "";
        account_type = "";
        status = "";
        name = "";
        group = "";
        chat_id = "";
        instance = null;
    }
}
