package com.beowulfchain.beowulfchainsdk.account;

import org.json.JSONException;
import org.json.JSONObject;

public class SipAccountData {

    private static SipAccountData instance = null;


    String username = "";
    String password = "";
    String chat_host = "";
    String chat_port = "";
    String chat_domain = "";

    String sip_domain = "";
    String sip_proxy = "";
    String sip_stun = "";

    public String getSip_domain() {
        return sip_domain;
    }

    public String getSip_proxy() {
        return sip_proxy;
    }


    public static SipAccountData getInstance() {
        if (null == instance) {
            instance = new SipAccountData();
        }
        return instance;
    }

    public boolean isInstance() {
        return instance != null;
    }

    public void ParseData(String jsonData) {
        username = "";
        password = "";
        chat_host = "";
        chat_port = "";
        chat_domain = "";
        sip_domain = "";
        sip_proxy = "";
        sip_stun = "";

        try {
            JSONObject object = new JSONObject(jsonData);
            if (!object.isNull("username"))
                username = object.getString("username");

            if (!object.isNull("password"))
                password = object.getString("password");

            if (!object.isNull("chat_host"))
                chat_host = object.getString("chat_host");

            if (!object.isNull("chat_port"))
                chat_port = object.getString("chat_port");

            if (!object.isNull("chat_domain"))
                chat_domain = object.getString("chat_domain");

            if (!object.isNull("sip_domain"))
                sip_domain = object.getString("sip_domain");

            if (!object.isNull("sip_proxy"))
                sip_proxy = object.getString("sip_proxy");

            if (!object.isNull("sip_stun"))
                sip_stun = object.getString("sip_stun");


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public String getUsername()
    {
        return username;
    }
    public String getPassword()
    {
        return password;
    }

}
