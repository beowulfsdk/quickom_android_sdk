package com.beowulfchain.beowulfchainsdk.account;

import org.json.JSONException;
import org.json.JSONObject;

public class ChatAccountData {


    static ChatAccountData instance = null;

    String chat_username = "";
    String chat_password = "";
    String chat_port = "";
    String chat_domain = "";
    String chat_host = "";

    public void setChat_username(String chat_username) {
        this.chat_username = chat_username;
    }

    public String getChat_username() {
        return chat_username;
    }
    public String getUserChatIdWithOutDomain()
    {
        String ret="";
        String userChat[]=chat_username.split("@");
        if(userChat.length>=2)
            ret= userChat[0];

        return ret;
    }

    public void setChat_password(String chat_password) {
        this.chat_password = chat_password;
    }

    public String getChat_password() {
        return chat_password;
    }

    public void setChat_host(String chat_host) {
        this.chat_host = chat_host;
    }

    public String getChat_host() {
        return chat_host;
    }

    public void setChat_port(String chat_port) {
        this.chat_port = chat_port;
    }

    public String getChat_port() {
        return chat_port;
    }

    public String getChat_domain() {
        return chat_domain;
    }

    public void setChat_domain(String chat_domain) {
        this.chat_domain = chat_domain;
    }

    public static void setInstance(ChatAccountData instance) {
        ChatAccountData.instance = instance;
    }

    public static ChatAccountData getInstance() {
        if (null == instance) {
            instance = new ChatAccountData();
        }
        return instance;
    }

    public boolean isInstance() {
        return instance != null;
    }

    public void ParseData(String jsonData) {
        chat_username = "";
        chat_password = "";
        chat_host = "";
        chat_port = "";
        chat_domain = "";
        try {
            JSONObject object = new JSONObject(jsonData);

            if (!object.isNull("chat_username"))
                chat_username = object.getString("chat_username");

            if (!object.isNull("chat_password"))
                chat_password = object.getString("chat_password");

            if (!object.isNull("chat_host"))
                chat_host = object.getString("chat_host");


            if (!object.isNull("chat_port"))
                chat_port = object.getString("chat_port");


            if (!object.isNull("chat_domain"))
                chat_domain = object.getString("chat_domain");


        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    public String GetUserName() {
        return chat_username;
    }

    public boolean isAvailable() {
        return (
                !"".matches(chat_username)
                        && !"".matches(chat_password)
                        && !"".matches(chat_host)
                        && !"".matches(chat_port)
                        && !"".matches(chat_domain)
        );
    }


    public String GetChatId() {
        String ret = "";

        String[] chatUserNameWithDomain = chat_username.split("@");
        if (chatUserNameWithDomain.length > 1) {
            ret = chatUserNameWithDomain[0];
        }

        return ret;
    }


    public String GetChatIdWithoutDomain() {
        String ret = "";
        if (chat_username.split("@").length >1)
            ret = chat_username.split("@")[0];
        return ret;
    }

}
