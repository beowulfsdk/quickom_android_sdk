package com.beowulfchain.beowulfchainsdk.account;

public class IntentData {
    public static final String AUTHEN_TYPE_KEY = "AUTHEN_TYPE";

    public static final String MSGIN = "MSGIN";
    public static final String MSGIDFROM = "MSGIDFROM";
    public static final String MSGIDTO = "MSGIDTO";
    public static final String MSGLABLE = "MSGLABLE";
    public static final String MSGCONVERSATIONID_ = "MSGCONVERSATIONID";

}
