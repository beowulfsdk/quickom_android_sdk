package com.beowulfchain.beowulfchainsdk;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;

import com.beowulfchain.beowulfchainsdk.AppData.AppData.ActivityLog;
import com.beowulfchain.beowulfchainsdk.AppData.AppData.ActivityLogDao;
import com.beowulfchain.beowulfchainsdk.AppData.AppData.ChatMessage.ChatMess;
import com.beowulfchain.beowulfchainsdk.AppData.AppData.ChatMessage.ChatMessDao;
import com.beowulfchain.beowulfchainsdk.AppData.AppData.ChatMessage.ChatMessSend;
import com.beowulfchain.beowulfchainsdk.AppData.AppData.ChatMessage.ChatMessStatus;

import com.beowulfchain.beowulfchainsdk.AppData.AppData.ChatMessage.ChatMessReceive;
import com.beowulfchain.beowulfchainsdk.AppData.AppData.DaoMaster;
import com.beowulfchain.beowulfchainsdk.AppData.AppData.DaoSession;
import com.beowulfchain.beowulfchainsdk.AppData.AppData.QrData;
import com.beowulfchain.beowulfchainsdk.AppData.AppData.SipMessage.SipHelpRequestMess;
import com.beowulfchain.beowulfchainsdk.AppData.AppUri;
import com.beowulfchain.beowulfchainsdk.Service.ChatConnection;
import com.beowulfchain.beowulfchainsdk.Service.SipConnection;
import com.beowulfchain.beowulfchainsdk.account.BeowulInfo;
import com.beowulfchain.beowulfchainsdk.account.ChatAccountData;
import com.beowulfchain.beowulfchainsdk.account.IdleAccountData;
import com.beowulfchain.beowulfchainsdk.account.SipAccountData;
import com.beowulfchain.beowulfchainsdk.linhphone.LinphoneManagerClone;
import com.beowulfchain.beowulfchainsdk.network.JsonAPI;
import com.beowulfchain.beowulfchainsdk.util.Util;

import org.greenrobot.greendao.query.Query;
import org.greenrobot.greendao.query.QueryBuilder;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import javax.net.ssl.HttpsURLConnection;

public class MainApplication extends ContentProvider {
    private static Context context;
    private static DaoSession mDaoSession;
    public static String api_key = "";

    private static MainApplication instance;

    public static MainApplication getInstance() {
        return instance;
    }



    public static Context getSDKContext() {
        return context;
    }

    @Override
    public boolean onCreate() {
        context = getContext();
        instance = this;
        mDaoSession = new DaoMaster(new DaoMaster.DevOpenHelper(context, "chat_text_msg.db").getWritableDb()).newSession();
        //   api_key=context.getResources().getString(R.string.apikey);
        return false;
    }


    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        return null;
    }


    @Override
    public String getType(Uri uri) {
        return null;
    }


    @Override
    public Uri insert(Uri uri, ContentValues values) {
        return null;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        return 0;
    }


    public int GetAllSeenMess() {
        int ret = 0;
        ArrayList<ChatMess> messesages = new ArrayList<>();
        try {
            ChatMessDao usersDao = mDaoSession.getChatMessDao();
            QueryBuilder<ChatMess> qb = usersDao.queryBuilder();
            messesages = (ArrayList<ChatMess>) qb.list();
        } catch (Exception e) {

        }


        for (ChatMess mess : messesages) {
            if (!mess.isSeen())
                ret += 1;
        }


        return ret;
    }

    public int numberOfUnseenRecord() {
        ArrayList<String> listOfConversationIds = new ArrayList<>();
        int ret = 0;
        ChatMessDao usersDao = mDaoSession.getChatMessDao();
        Query<ChatMess> query = usersDao.queryBuilder().where(ChatMessDao.Properties.Status.notEq("seen")).build();
        ArrayList<ChatMess> listxxx = (ArrayList<ChatMess>) query.list();
        for (ChatMess mess : listxxx) {

            if (!listOfConversationIds.contains(mess.getConversationId()) && mess instanceof ChatMessReceive) {
                listOfConversationIds.add(mess.getConversationId());
            }
        }
        ret = listOfConversationIds.size();

        return ret;
    }

    public static int GetUnSeenMess(String idFrom, String idTo, String conversationId) {
        int ret = 0;
        ArrayList<ChatMess> messesages = new ArrayList<>();
        try {
            ChatMessDao usersDao = mDaoSession.getChatMessDao();
            QueryBuilder<ChatMess> qb = usersDao.queryBuilder();
            messesages = (ArrayList<ChatMess>) qb.where(
                    qb.or(
                            qb.and(ChatMessDao.Properties.MessageFrom.eq(idFrom),
                                    ChatMessDao.Properties.MessageTo.eq(idTo),
                                    ChatMessDao.Properties.ConversationId.eq(conversationId)),
                            qb.and(ChatMessDao.Properties.MessageFrom.eq(idTo),
                                    ChatMessDao.Properties.MessageTo.eq(idFrom),
                                    ChatMessDao.Properties.ConversationId.eq(conversationId))
                    )
            ).list();

        } catch (Exception e) {

        }


        for (ChatMess mess : messesages) {
            if (!mess.isSeen() && mess instanceof ChatMessReceive)
                ret += 1;
        }

        return ret;
    }


    public static ArrayList<ChatMess> GetListChatWithConversationId() {

        String conversationId = ChatConnection.getInstance().getCurrentConversation();
        ArrayList<ChatMess> messesages = new ArrayList<>();
        try {
            ChatMessDao usersDao = mDaoSession.getChatMessDao();
            QueryBuilder<ChatMess> qb = usersDao.queryBuilder();
            messesages = (ArrayList<ChatMess>) qb.where(ChatMessDao.Properties.ConversationId.eq(conversationId)).list();

        } catch (Exception e) {

        }

        ArrayList<ChatMess> pppp = GetListChatAll();

        return messesages;
    }

    public static ArrayList<ChatMess> GetListChat(String idFrom, String idTo, String conversationId) {
        ArrayList<ChatMess> messesages = new ArrayList<>();
        try {
            ChatMessDao usersDao = mDaoSession.getChatMessDao();
            QueryBuilder<ChatMess> qb = usersDao.queryBuilder();
            messesages = (ArrayList<ChatMess>) qb.where(
                    qb.or(
                            qb.and(ChatMessDao.Properties.MessageFrom.eq(idFrom),
                                    ChatMessDao.Properties.MessageTo.eq(idTo),
                                    ChatMessDao.Properties.ConversationId.eq(conversationId)),
                            qb.and(ChatMessDao.Properties.MessageFrom.eq(idTo),
                                    ChatMessDao.Properties.MessageTo.eq(idFrom),
                                    ChatMessDao.Properties.ConversationId.eq(conversationId))
                    )
            ).list();

        } catch (Exception e) {

        }

        ArrayList<ChatMess> pppp = GetListChat(idFrom, idTo);

        return messesages;
    }

    public static ArrayList<ChatMess> GetListChatAll() {
        ArrayList<ChatMess> messesages = new ArrayList<>();
        ChatMessDao usersDao = mDaoSession.getChatMessDao();
        QueryBuilder<ChatMess> qb = usersDao.queryBuilder();
        messesages = (ArrayList<ChatMess>) qb.list();
        return messesages;
    }

    public static ArrayList<ChatMess> GetListChat(String idFrom, String idTo) {
        ArrayList<ChatMess> messesages = new ArrayList<>();
        ChatMessDao usersDao = mDaoSession.getChatMessDao();
        QueryBuilder<ChatMess> qb = usersDao.queryBuilder();
        messesages = (ArrayList<ChatMess>) qb.where(
                qb.or(
                        qb.and(ChatMessDao.Properties.MessageFrom.eq(idFrom),
                                ChatMessDao.Properties.MessageTo.eq(idTo)
                        ),
                        qb.and(ChatMessDao.Properties.MessageFrom.eq(idTo),
                                ChatMessDao.Properties.MessageTo.eq(idFrom)
                        )
                )
        ).list();

        return messesages;
    }

    public static void DeleteAllChatRecord() {
        mDaoSession.deleteAll(ChatMess.class);
    }

    public static void DeleteAllActivityLogRecord() {
        mDaoSession.deleteAll(ActivityLog.class);
    }

    public static void InsertTextChatMsg(ChatMess chatMess) {
        String fromIdWithOutDomain = chatMess.getMessageFrom();
        String toIdWithOutDomain = chatMess.getMessageTo();

        if (fromIdWithOutDomain.split("@").length >= 2)
            fromIdWithOutDomain = fromIdWithOutDomain.split("@")[0];

        if (toIdWithOutDomain.split("@").length >= 2)
            toIdWithOutDomain = toIdWithOutDomain.split("@")[0];

        chatMess.setMessageFrom(fromIdWithOutDomain);
        chatMess.setMessageTo(toIdWithOutDomain);

        android.util.Log.d("LOGXXX", "insert chat " + " FROM:" + chatMess.getMessageFrom() + " to:" + chatMess.getMessageTo() + " conversationid:" + chatMess.getConversationId() + " lable:");

        try {
            mDaoSession.getChatMessDao().insert(chatMess);
            if (chatMess instanceof ChatMessStatus) {
                ChatMessDao usersDao = mDaoSession.getChatMessDao();
                String msgId = chatMess.getMsgId();
                ChatMess newMsg = usersDao.queryBuilder().where(ChatMessDao.Properties.MsgId.eq(msgId)).build().uniqueOrThrow();
                String status = chatMess.getStatus();
                newMsg.setStatus(status);
                usersDao.update(newMsg);
            }
        } catch (Exception e) {

        }


    }

    public static void UpdateActivity(ActivityLog activityLog) {
        mDaoSession.getActivityLogDao().update(activityLog);
    }

    public static void InsertActivity(ActivityLog activityLog) {
        try {
            String fromIdWithOutDomain = activityLog.getChatIdFrom();
            String toIdWithOutDomain = activityLog.getChatIdTo();

            if (fromIdWithOutDomain.split("@").length >= 2)
                fromIdWithOutDomain = fromIdWithOutDomain.split("@")[0];


            if (toIdWithOutDomain.split("@").length >= 2)
                toIdWithOutDomain = toIdWithOutDomain.split("@")[0];


            activityLog.setChatIdFrom(fromIdWithOutDomain);
            ;
            activityLog.setChatIdTo(toIdWithOutDomain);
            ;

            mDaoSession.getActivityLogDao().insert(activityLog);
            android.util.Log.d("LOGXXX", "insert " + activityLog.getActivity() + " FROM:" + activityLog.getChatIdFrom() + " to:" + activityLog.getChatIdTo() + " conversationid:" + activityLog.getConversationId() + " lable:" + activityLog.getLable());


            ArrayList<ActivityLog> ppp = GetListActivityCallLog();
            String aaaa = "ssssss";

        } catch (Exception e) {
            String err = e.toString();
        }

    }

    public static ArrayList<ActivityLog> GetListActivityCallLog() {

        ArrayList<ActivityLog> reverse = new ArrayList<>();
        try {
            ActivityLogDao activityLogDao = mDaoSession.getActivityLogDao();
            ArrayList<ActivityLog> activityLogDaos = (ArrayList<ActivityLog>) activityLogDao.queryBuilder().where(ActivityLogDao.Properties.Activity.ge(1)).build().list();
            for (ActivityLog activity : activityLogDaos) {
                reverse.add(0, activity);
            }
        } catch (Exception e) {

        }


        return reverse;
    }

    public static ArrayList<ActivityLog> GetListActivityChatLog() {
        ArrayList<ActivityLog> reverse = new ArrayList<>();
        try {
            ActivityLogDao activityLogDao = mDaoSession.getActivityLogDao();
            ArrayList<ActivityLog> activityLogDaos = (ArrayList<ActivityLog>) activityLogDao.queryBuilder().where(ActivityLogDao.Properties.Activity.eq(0)).build().list();


            for (ActivityLog activity : activityLogDaos) {
                reverse.add(0, activity);
            }
        } catch (Exception e) {

        }

        return reverse;
    }


    public static void CallRating(boolean callee, String transaction_id, String comment, float star, String type) {
        //Data
        JsonAPI.JsonCallback jsonCallback = new JsonAPI.JsonCallback() {
            @Override
            public void onResponse(int statusCode, String str) {
                Log.d("testrating", str);
                if (statusCode == HttpsURLConnection.HTTP_OK) {
                    try {
                        JSONObject json = new JSONObject(str);
                        if (!json.isNull("success")) {
                            boolean sucess = json.getBoolean("success");
                            if (true == sucess) {
                                //Welcome.instance().FinishCreateNewMember();
                            }
                        }

                    } catch (JSONException e) {
                        String errorStr = " Exception: " + e.getMessage();
                    }
                } else {

                }
            }
        };

        //show hide progress
        JsonAPI.JsonRequestListener requestListener = new JsonAPI.JsonRequestListener() {
            @Override
            public void onBeginRequest() {
                //  showDialogProgress();
            }

            @Override
            public void onEndRequest() {
                //   hideDialogProgress();
            }
        };

        JSONObject json = new JSONObject();
        try {
            json.put("callee", callee);
            json.put("transaction_id", transaction_id);
            json.put("comment", comment);
            json.put("star", star);
            json.put("type", type);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String jsonStr = json.toString();
        Log.d("jsonratin", jsonStr);


        HashMap<String, String> requestHeader = new HashMap<String, String>();
        requestHeader.put("Content-Type", "application/json");
        //requestHeader.put("Authorization", BeowulInfo.getInstance().token);
        JsonAPI.getInstance().post(requestHeader, AppUri.ENTERPRISE_CALL_RATING_POST, jsonStr, jsonCallback, requestListener);
    }

    public static void CallLogoutAPI() {

        String divice_id = Util.getDeviceId(context);
        JsonAPI.JsonCallback jsonCallback = new JsonAPI.JsonCallback() {
            @Override
            public void onResponse(int statusCode, String str) {
                if (statusCode == HttpsURLConnection.HTTP_OK) {
                    try {
                        JSONObject json = new JSONObject(str);


                    } catch (JSONException e) {
                        String errorStr = " Exception: " + e.getMessage();
                        //    showErrorDlg(context, errorStr);
                    }

                } else if (statusCode == HttpsURLConnection.HTTP_BAD_REQUEST) {
                    //   if (Welcome.isInstance())
                    //    showErrorDlg(context, "Fail");
                } else {
                    //   showErrorDlg(context, str);
                }
            }
        };

        //show hide progress
        JsonAPI.JsonRequestListener requestListener = new JsonAPI.JsonRequestListener() {
            @Override
            public void onBeginRequest() {
                //    showDialogProgress();
            }

            @Override
            public void onEndRequest() {
                //   hideDialogProgress();
            }
        };


        JSONObject json = new JSONObject();
        try {
            json.put("device_id", divice_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String jsonStr = json.toString();
        HashMap<String, String> requestHeader = new HashMap<String, String>();
        requestHeader.put("Content-Type", "application/json");
        requestHeader.put("Authorization", BeowulInfo.getInstance().getToken());


        JsonAPI.getInstance().post(requestHeader, AppUri.ENTERPRISE_LOGOUT_POST, jsonStr, jsonCallback, requestListener);

    }

    public static void GetIdelSipAcc() {
        JsonAPI.JsonCallback jsonCallback = new JsonAPI.JsonCallback() {
            @Override
            public void onResponse(int statusCode, String str) {

                String super_account_id;
                String account_id;
                String reference_id;
                String sip_id;
                String created_at;
                String account_type;
                String status;
                String name;
                String group;
                String chat_id;

                if (statusCode == HttpsURLConnection.HTTP_OK) {
                    try {
                        JSONObject obj = new JSONObject(str);
                        if (!obj.isNull("super_account_id"))
                            super_account_id = obj.getString("super_account_id");

                        if (!obj.isNull("account_id"))
                            account_id = obj.getString("account_id");


                        if (!obj.isNull("reference_id"))
                            reference_id = obj.getString("reference_id");


                        if (!obj.isNull("sip_id"))
                            sip_id = obj.getString("sip_id");

                        if (!obj.isNull("created_at"))
                            created_at = obj.getString("created_at");

                        if (!obj.isNull("account_type"))
                            account_type = obj.getString("account_type");

                        if (!obj.isNull("status"))
                            status = obj.getString("status");

                        if (!obj.isNull("name"))
                            name = obj.getString("name");

                        if (!obj.isNull("group"))
                            group = obj.getString("group");

                        if (!obj.isNull("chat_id"))
                            chat_id = obj.getString("chat_id");

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }
        };

        JsonAPI.JsonRequestListener jsonRequestListener = new JsonAPI.JsonRequestListener() {
            @Override
            public void onBeginRequest() {


            }

            @Override
            public void onEndRequest() {

            }
        };

        HashMap<String, String> requestHeader = new HashMap<String, String>();
        requestHeader.put("Authorization", context.getResources().getString(R.string.apikey));
        JsonAPI.getInstance().get(requestHeader, AppUri.ENTERPRISE_ACCOUNT_IDLE_GET, jsonCallback, jsonRequestListener);
    }

    public static void SwitchToOnLine(boolean online, String accountId) {
        JsonAPI.JsonCallback jsonCallback = new JsonAPI.JsonCallback() {
            @Override
            public void onResponse(int statusCode, String str) {

                if (statusCode == HttpsURLConnection.HTTP_OK) {
                    try {
                        JSONObject object = new JSONObject(str);
                        android.util.Log.d("ONLINESTATUS", str);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        };

        JsonAPI.JsonRequestListener jsonRequestListener = new JsonAPI.JsonRequestListener() {
            @Override
            public void onBeginRequest() {

            }

            @Override
            public void onEndRequest() {

            }
        };

        JSONObject json = new JSONObject();
        try {
            json.put("account_id", accountId);
            json.put("status", online == true ? "online" : "offline");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        String jsonStr = json.toString();
        HashMap<String, String> requestHeader = new HashMap<String, String>();

        requestHeader.put("Content-Type", "application/json");
        requestHeader.put("Authorization", BeowulInfo.getInstance().getToken());
        JsonAPI.getInstance().post(requestHeader, AppUri.SUBACC_STATUS, jsonStr, jsonCallback, jsonRequestListener);
    }

    public static void GetIdleAccount(final String apikey) {

        JsonAPI.JsonCallback jsonCallback = new JsonAPI.JsonCallback() {
            @Override
            public void onResponse(int statusCode, String str) {
                if (statusCode == HttpsURLConnection.HTTP_OK) {
                    IdleAccountData.getInstance().ParseData(str);
                    GetSipAccoutInfo(apikey);
                    GetChatInfo(apikey);
                }
            }
        };

        JsonAPI.JsonRequestListener jsonRequestListener = new JsonAPI.JsonRequestListener() {
            @Override
            public void onBeginRequest() {
            }

            @Override
            public void onEndRequest() {

            }
        };
        HashMap<String, String> requestHeader = new HashMap<String, String>();
        requestHeader.put("Authorization", apikey);
        JsonAPI.getInstance().get(requestHeader, AppUri.ENTERPRISE_ACCOUNT_IDLE_GET, jsonCallback, jsonRequestListener);
    }

    public static void GetSipAccoutInfo(final String apikey) {
        JsonAPI.JsonCallback jsonCallback = new JsonAPI.JsonCallback() {
            @Override
            public void onResponse(int statusCode, String str) {
                if (statusCode == HttpsURLConnection.HTTP_OK) {
                    SipAccountData.getInstance().ParseData(str);
                    LinphoneManagerClone.getInstance().ConfigAccount(SipAccountData.getInstance().getUsername(), SipAccountData.getInstance().getPassword());
                }
            }
        };

        JsonAPI.JsonRequestListener jsonRequestListener = new JsonAPI.JsonRequestListener() {
            @Override
            public void onBeginRequest() {
            }

            @Override
            public void onEndRequest() {
            }
        };

        JSONObject json = new JSONObject();
        try {
            json.put("account_id", BeowulInfo.getInstance().getAccountId());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        String jsonStr = json.toString();
        HashMap<String, String> requestHeader = new HashMap<String, String>();
        requestHeader.put("Content-Type", "application/json");
        requestHeader.put("Authorization", apikey);
        JsonAPI.getInstance().post(requestHeader, AppUri.SIP_ACCOUNT_INFO, jsonStr, jsonCallback, jsonRequestListener);
    }

    public static void GetChatInfo(final String apikey) {
        JsonAPI.JsonCallback jsonCallback = new JsonAPI.JsonCallback() {
            @Override
            public void onResponse(int statusCode, String str) {
                if (statusCode == HttpsURLConnection.HTTP_OK) {
                    ChatAccountData.getInstance().ParseData(str);
                    SipConnection.getInstance().InitChatConenction();
                }
            }
        };

        JsonAPI.JsonRequestListener jsonRequestListener = new JsonAPI.JsonRequestListener() {
            @Override
            public void onBeginRequest() {

            }

            @Override
            public void onEndRequest() {

            }
        };

        JSONObject json = new JSONObject();
        try {
            json.put("reference_id", BeowulInfo.getInstance().getAccountId());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        String jsonStr = json.toString();
        HashMap<String, String> requestHeader = new HashMap<String, String>();
        requestHeader.put("Content-Type", "application/json");
        requestHeader.put("Authorization", apikey);
        JsonAPI.getInstance().post(requestHeader, AppUri.CHAT_ACCOUNT_INFO, jsonStr, jsonCallback, jsonRequestListener);
    }

//        public void getCallHistory(String from_hex_id) {
//            JsonAPI.JsonCallback jsonCallback = new JsonAPI.JsonCallback() {
//                @Override
//                public void onResponse(int statusCode, String str) {
//
//                    if (statusCode == HttpsURLConnection.HTTP_OK) {
//                        try {
//                            JSONObject object = new JSONObject(str);
//                            android.util.Log.d("PUSHTOKEN", str);
//
//
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                }
//            };
//
//            JsonAPI.JsonRequestListener jsonRequestListener = new JsonAPI.JsonRequestListener() {
//                @Override
//                public void onBeginRequest() {
//
//                }
//
//                @Override
//                public void onEndRequest() {
//
//                }
//            };
//
//            JSONObject json = new JSONObject();
//            try {
//                json.put("limit", 20);
//                json.put("from_hex_id", from_hex_id);
//                json.put("action", "next");
//
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//
//            String jsonStr = json.toString();
//            HashMap<String, String> requestHeader = new HashMap<String, String>();
//
//            requestHeader.put("Content-Type", "application/json");
//            requestHeader.put("Authorization", BeowulInfo.getInstance().getToken());
//            JsonAPI.getInstance().post(requestHeader, AppUri.ENTERPRISE_CALLHISTORY, jsonStr, jsonCallback, jsonRequestListener);
//        }

    public static void pushToken(String device_id, String push_token) {
        JsonAPI.JsonCallback jsonCallback = new JsonAPI.JsonCallback() {
            @Override
            public void onResponse(int statusCode, String str) {

                if (statusCode == HttpsURLConnection.HTTP_OK) {
                    try {
                        JSONObject object = new JSONObject(str);
                        android.util.Log.d("PUSHTOKEN", str);


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        };

        JsonAPI.JsonRequestListener jsonRequestListener = new JsonAPI.JsonRequestListener() {
            @Override
            public void onBeginRequest() {

            }

            @Override
            public void onEndRequest() {

            }
        };

        JSONObject json = new JSONObject();
        try {
            json.put("device_id", device_id);
            json.put("push_token", push_token);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        String jsonStr = json.toString();
        HashMap<String, String> requestHeader = new HashMap<String, String>();

        requestHeader.put("Content-Type", "application/json");
        requestHeader.put("Authorization", BeowulInfo.getInstance().getToken());
        JsonAPI.getInstance().post(requestHeader, AppUri.PUSH_TOKEN_ACCOUNT, jsonStr, jsonCallback, jsonRequestListener);
    }


    public static void GetQrDetailInfo(String aliasString) {
        JsonAPI.JsonCallback jsonCallback = new JsonAPI.JsonCallback() {
            @Override
            public void onResponse(int statusCode, String str) {

                if (statusCode == HttpsURLConnection.HTTP_OK) {


                    QrData qrData = new QrData(str);
                    String _calltype = qrData.getCall_type();/*video*/

                    String label = qrData.getLabel();/*Định - 2 sup*/
                    String alias = qrData.getAlias();/*60151560760046179*/
                    String group_name = qrData.getGroup_name();/*2 sup*/
                    String type = qrData.getType();/*group_call*/
                    boolean _isEnterprise = qrData.get_me();/*false*/
                    _calltype = qrData.getCall_type();/*video*/
                    String _url = qrData.getUrl();
                    String _au_ = MainApplication.getSDKContext().getResources().getString(R.string.apikey);
                    String ___pushContent = "Incomming call";


                    for (int i = 0; i < qrData.getUsers().size(); i++) {
                        SipHelpRequestMess requestMess = new SipHelpRequestMess(label, alias, group_name, type, _isEnterprise, _calltype, _url, _au_, ___pushContent);
                        requestMess.MapingHelpRequestToUser(qrData.getUsers().get(i).getName(), qrData.getUsers().get(i).getSip_id(), qrData.getUsers().get(i).getChat_id(), qrData.getUsers().get(i).getReference_id());
                        String requestId = requestMess.SendHelpRequest(qrData.getUsers().get(i).getChat_id());


                    }
//                    if (!_calltype.matches("chat"))
//                        Welcome.instance().LauchDummyOutgoingActivity(label, _calltype);
                }
            }
        };

        JsonAPI.JsonRequestListener jsonRequestListener = new JsonAPI.JsonRequestListener() {
            @Override
            public void onBeginRequest() {

            }

            @Override
            public void onEndRequest() {

            }
        };

        HashMap<String, String> requestHeader = new HashMap<String, String>();
        requestHeader.put("Authorization", MainApplication.getSDKContext().getResources().getString(R.string.apikey));
        JsonAPI.getInstance().get(requestHeader, AppUri.ENTERPRISE_QR_DETAIL_GET + aliasString, jsonCallback, jsonRequestListener);

    }
//
//    public static void GetQrDetailInfoAndCall(String aliasString) {
//        JsonAPI.JsonCallback jsonCallback = new JsonAPI.JsonCallback() {
//            @Override
//            public void onResponse(int statusCode, String str) {
//
//                if (statusCode == HttpsURLConnection.HTTP_OK) {
//
//                    //{"alias":"60151560760046179","label":"Định - 2 sup","group_name":"2 sup","type":"group_call","call_type":"video","allow_chat":true,"allow_call":true,"me":false,"users":[{"name":"sup1","sip_id":"91032005533723305","chat_id":"30101560759992728","reference_id":"sup1@gmail.com"},{"name":"sup2","sip_id":"61921067828396879","chat_id":"32501560760019419","reference_id":"sup2@gmail.com"}],"url":"https://quickom.beowulfchain.com/qr-code/60151560760046179"}
//
//                    QrData qrData = new QrData(str);
//                    String _calltype = qrData.getCall_type();/*video*/
//
//
//                    String label = qrData.getLabel();/*Định - 2 sup*/
//                    String alias = qrData.getAlias();/*60151560760046179*/
//                    String group_name = qrData.getGroup_name();/*2 sup*/
//                    String type = qrData.getType();/*group_call*/
//                    boolean _isEnterprise = qrData.get_me();/*false*/
//                    _calltype = qrData.getCall_type();/*video*/
//                    String _url = qrData.getUrl();
//                    String _au_ = api_key;
//                    String ___pushContent = "Incomming call";
//
//
//                    for (int i = 0; i < qrData.getUsers().size(); i++) {
//                        SipHelpRequestMess requestMess = new SipHelpRequestMess(label, alias, group_name, type, _isEnterprise, _calltype, _url, _au_, ___pushContent);
//                        requestMess.MapingHelpRequestToUser(qrData.getUsers().get(i).getName(), qrData.getUsers().get(i).getSip_id(), qrData.getUsers().get(i).getChat_id(), qrData.getUsers().get(i).getReference_id());
//
//                        String domainName = ChatAccountData.getInstance().getChat_domain();
//                        ChatConnection.getInstance().SendHelpMessage(requestMess.GetJsonString(), qrData.getUsers().get(i).getChat_id(), domainName);
//                        String requestId = requestMess.getRequestId();
//                        AddSipHelpRequestMess(requestId, requestMess);
//
//                    }
//
//                }
//            }
//        };
//
//        JsonAPI.JsonRequestListener jsonRequestListener = new JsonAPI.JsonRequestListener() {
//            @Override
//            public void onBeginRequest() {
//
//            }
//
//            @Override
//            public void onEndRequest() {
//
//            }
//        };
//
//        HashMap<String, String> requestHeader = new HashMap<String, String>();
//        requestHeader.put("Authorization", api_key);
//        JsonAPI.getInstance().get(requestHeader,
//                AppUri.ENTERPRISE_QR_DETAIL_GET + aliasString, jsonCallback, jsonRequestListener);
//
//    }


    public static String GetUserViaGroupNameForChat(final String optionalname, String groupName) {
        final String myRequestId = System.currentTimeMillis() / 1000 + 1000 + "" + (Math.random() % 1000);
        JsonAPI.JsonCallback jsonCallback = new JsonAPI.JsonCallback() {
            @Override
            public void onResponse(int statusCode, String str) {
                android.util.Log.d("PAYMENT_CALLING", "endcall " + str);

                if (statusCode == HttpsURLConnection.HTTP_OK) {

                    QrData qrData = new QrData(str);
                    String _calltype = qrData.getCall_type();/*video*/
                    String label = qrData.getLabel();/*Định - 2 sup*/
                    String alias = qrData.getAlias();/*60151560760046179*/
                    String group_name = qrData.getGroup_name();/*2 sup*/
                    String type = qrData.getType();/*group_call*/
                    boolean _isEnterprise = qrData.get_me();/*false*/
                    _calltype = qrData.getCall_type();/*video*/
                    String _url = qrData.getUrl();
                    String _au_ = api_key;
                    String ___pushContent = "Incomming call";

                    for (int i = 0; i < qrData.getUsers().size(); i++) {
                        SipHelpRequestMess requestMess = new SipHelpRequestMess(optionalname, "Incomming call", "chat", ChatAccountData.getInstance().getUserChatIdWithOutDomain(), qrData.getUsers().get(i).getChat_id(), myRequestId);
                        requestMess.MapingHelpRequestToUser(qrData.getUsers().get(i).getName(), qrData.getUsers().get(i).getSip_id(), qrData.getUsers().get(i).getChat_id(), qrData.getUsers().get(i).getReference_id());
                        String domainName = ChatAccountData.getInstance().getChat_domain();
                        ChatConnection.getInstance().SendHelpMessage(requestMess.GetJsonString(), qrData.getUsers().get(i).getChat_id(), domainName);
                        String requestId = requestMess.getRequestId();
                        ChatConnection.getInstance().AddSipHelpRequestMess(requestId, requestMess);
                    }
                } else {
                    try {

                        String error_description = "";
                        int errorCode = 0;
                        JSONObject errorObject = new JSONObject(str);
                        if (!errorObject.isNull("error_description")) {
                            error_description = errorObject.getString("error_description");
                        }

                        if (!errorObject.isNull("error")) {
                            final String error_code = errorObject.getString("error");
                            errorCode = Integer.parseInt(error_code);
                        }

                        BWFCallCenter.VoiceVideoCallResult(errorCode, error_description);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }
        };

        JsonAPI.JsonRequestListener jsonRequestListener = new JsonAPI.JsonRequestListener() {
            @Override
            public void onBeginRequest() {
            }

            @Override
            public void onEndRequest() {
            }
        };

        JSONObject json = new JSONObject();
        try {
            json.put("group_name", groupName);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String jsonStr = json.toString();
        HashMap<String, String> requestHeader = new HashMap<String, String>();
        requestHeader.put("Content-Type", "application/json");
        requestHeader.put("Authorization", api_key);
        JsonAPI.getInstance().post(requestHeader, AppUri.ENTERPRISE_USER_LIST_GET, jsonStr, jsonCallback, jsonRequestListener);
        return myRequestId;
    }


    public static String GetUserViaGroupNameForCall(String groupName, final String optionalname) {
        final String myRequestId = System.currentTimeMillis() / 1000 + 1000 + "" + (Math.random() % 1000);
        JsonAPI.JsonCallback jsonCallback = new JsonAPI.JsonCallback() {
            @Override
            public void onResponse(int statusCode, String str) {
                android.util.Log.d("PAYMENT_CALLING", "endcall " + str);

                if (statusCode == HttpsURLConnection.HTTP_OK) {
                    QrData qrData = new QrData(str);
                    String _calltype = qrData.getCall_type();/*video*/
                    String label = qrData.getLabel();/*Định - 2 sup*/
                    String alias = qrData.getAlias();/*60151560760046179*/
                    String group_name = qrData.getGroup_name();/*2 sup*/
                    String type = qrData.getType();/*group_call*/
                    boolean _isEnterprise = qrData.get_me();/*false*/
                    _calltype = qrData.getCall_type();/*video*/
                    String _url = qrData.getUrl();
                    String _au_ = api_key;
                    String ___pushContent = "Incomming call";

                    for (int i = 0; i < qrData.getUsers().size(); i++) {
                        SipHelpRequestMess requestMess = new SipHelpRequestMess(optionalname, "Incomming call", "voice", ChatAccountData.getInstance().getUserChatIdWithOutDomain(), qrData.getUsers().get(i).getChat_id(), myRequestId);
                        requestMess.MapingHelpRequestToUser(qrData.getUsers().get(i).getName(), qrData.getUsers().get(i).getSip_id(), qrData.getUsers().get(i).getChat_id(), qrData.getUsers().get(i).getReference_id());
                        String domainName = ChatAccountData.getInstance().getChat_domain();
                        ChatConnection.getInstance().SendHelpMessage(requestMess.GetJsonString(), qrData.getUsers().get(i).getChat_id(), domainName);
                        String requestId = requestMess.getRequestId();
                        ChatConnection.getInstance().AddSipHelpRequestMess(requestId, requestMess);
                    }
                    if (qrData.getUsers().size() > 0)
                        BWFCallCenter.VoiceVideoCallResult(0, "");
                    else {
                        BWFCallCenter.VoiceVideoCallResult(1, "no supporter found");
                    }
                } else {

                    try {

                        String error_description = "";
                        int errorCode = 0;
                        JSONObject errorObject = new JSONObject(str);
                        if (!errorObject.isNull("error_description")) {
                            error_description = errorObject.getString("error_description");
                        }

                        if (!errorObject.isNull("error")) {
                            final String error_code = errorObject.getString("error");
                            errorCode = Integer.parseInt(error_code);
                        }

                        BWFCallCenter.VoiceVideoCallResult(errorCode, error_description);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }
        };

        JsonAPI.JsonRequestListener jsonRequestListener = new JsonAPI.JsonRequestListener() {
            @Override
            public void onBeginRequest() {
            }

            @Override
            public void onEndRequest() {
            }
        };

        JSONObject json = new JSONObject();
        try {
            json.put("group_name", groupName);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String jsonStr = json.toString();
        HashMap<String, String> requestHeader = new HashMap<String, String>();
        requestHeader.put("Content-Type", "application/json");
        requestHeader.put("Authorization", api_key);
        JsonAPI.getInstance().post(requestHeader, AppUri.ENTERPRISE_USER_LIST_GET, jsonStr, jsonCallback, jsonRequestListener);
        return myRequestId;
    }


    public static boolean SendTextChat(ChatMessSend textChatMessSend) {
        String payload = textChatMessSend.getChatMessSendObject().toString();//, String sendToChatId, String mDomainName
        String domainName = ChatAccountData.getInstance().getChat_domain();
        String lable = "", msgIdFrom = "", msgIdTo = "", chatConversationId;
//        msgIdFrom = mess.getMessageFrom();
//        msgIdTo = mess.getMessageTo();
//        chatConversationId = mess.getConversationId();

        return ChatConnection.SendTextChat(payload, textChatMessSend.getMessageTo(), domainName);
    }






    public String CreateNewConversation(final String conversationId, final String chatIDFrom, final String chatIDTo, final String QRCode) {

        JsonAPI.JsonCallback jsonCallback = new JsonAPI.JsonCallback() {
            @Override
            public void onResponse(int statusCode, String str) {
                android.util.Log.d("PAYMENT_CALLING", "endcall " + str);

                if (statusCode == HttpsURLConnection.HTTP_OK) {

                }


            }
        };

        JsonAPI.JsonRequestListener jsonRequestListener = new JsonAPI.JsonRequestListener() {
            @Override
            public void onBeginRequest() {
            }

            @Override
            public void onEndRequest() {
            }
        };

        JSONObject json = new JSONObject();
        try {
            json.put("conversation_id", conversationId);
            json.put("from_chat_id", chatIDFrom);
            json.put("to_chat_id", chatIDTo);
            json.put("qr_code", QRCode);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String jsonStr = json.toString();
        HashMap<String, String> requestHeader = new HashMap<String, String>();
        requestHeader.put("Content-Type", "application/json");

        JsonAPI.getInstance().post(requestHeader, AppUri.CREATE_CHAT_CONVERSATION, jsonStr, jsonCallback, jsonRequestListener);
        return conversationId;
    }



}
