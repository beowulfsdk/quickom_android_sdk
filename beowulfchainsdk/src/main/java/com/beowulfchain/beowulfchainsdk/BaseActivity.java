package com.beowulfchain.beowulfchainsdk;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

public class BaseActivity extends AppCompatActivity {

    public static Context context = null;
    protected Toolbar toolbar;

    public BaseActivity() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
//        toolbar = findViewById(R.id.toolbar);
//        if (toolbar != null) {
//            setSupportActionBar(toolbar);
//            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    onBackPressed();
//                }
//            });
//        }
    }

    protected void showTitle(int resId) {
        if (toolbar != null) {
            getSupportActionBar().setTitle(getString(resId));
        }
    }

    protected void showTitle(String title) {
        if (toolbar != null) {
            getSupportActionBar().setTitle(title);
        }
    }

    protected void showHomeButton(boolean bShow) {
        if (toolbar != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(bShow);
            getSupportActionBar().setDisplayShowHomeEnabled(bShow);
        }
    }

    public void changeIcon() {
//        if (toolbar != null)
//            toolbar.setNavigationIcon(R.drawable.ic_left_arrow);
    }

}
