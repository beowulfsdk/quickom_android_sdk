package com.beowulfchain.beowulfchainsdk.Service;

import android.os.Looper;

import com.beowulfchain.beowulfchainsdk.BWFCallCenter;
import com.beowulfchain.beowulfchainsdk.linhphone.LinphoneManagerClone;

import org.linphone.core.Call;
import org.linphone.core.Core;
import org.linphone.core.CoreListenerStub;
import org.linphone.core.GlobalState;
import org.linphone.core.ProxyConfig;
import org.linphone.core.RegistrationState;

public class SipConnection extends CoreListenerStub {

    private static SipConnection instance = null;

    public static SipConnection getInstance() {
        if (null == instance) {
            instance = new SipConnection();
        }
        return instance;
    }

    @Override
    public void onCallStateChanged(Core lc, Call call, Call.State state, String message) {
        android.util.Log.d("LIN_XXX", message);

        BWFCallCenter.SetCallStateChange(state.toInt());

        if (state == Call.State.Connected) {
        }

        if (state == Call.State.StreamsRunning) {
        }

        if (state == Call.State.IncomingReceived || state == Call.State.IncomingEarlyMedia) {
        }

        if (state == Call.State.End
                || state == Call.State.Released
                || state == Call.State.Error) {
        }

        if (state == Call.State.OutgoingInit
                || state == Call.State.OutgoingProgress
                || state == Call.State.OutgoingRinging) {
        }
    }

    @Override
    public void onGlobalStateChanged(Core lc, GlobalState state, String message) {
        android.util.Log.d("xxxonGlobalState", message);
    }

    @Override
    public void onRegistrationStateChanged(Core lc, ProxyConfig cfg, RegistrationState state, String smessage) {


        if (state == RegistrationState.Ok) {
            BWFCallCenter.getInstance().onSipResult(true);
        }

        if (state == RegistrationState.Failed) {
            BWFCallCenter.getInstance().onSipResult(false);
        }

    }

    public void EndCall()
    {
        LinphoneManagerClone.getLc().terminateAllCalls();
    }

    public void InitChatConenction()
    {

        new Thread(new Runnable() {
            @Override
            public void run() {
                Looper.prepare();
                ChatConnection.getInstance().ConnectServer();
                Looper.loop();
            }
        }).start();
        //THE CODE HERE RUNS IN A BACKGROUND THREAD.


    }
}
