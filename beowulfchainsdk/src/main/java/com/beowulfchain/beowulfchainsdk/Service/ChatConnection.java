package com.beowulfchain.beowulfchainsdk.Service;

import android.util.Log;

import com.beowulfchain.beowulfchainsdk.AppData.AppData.ChatMessage.ChatMess;
import com.beowulfchain.beowulfchainsdk.AppData.AppData.ChatMessage.ChatMessReceive;
import com.beowulfchain.beowulfchainsdk.AppData.AppData.ChatMessage.ChatMessSend;
import com.beowulfchain.beowulfchainsdk.AppData.AppData.ChatMessage.MsgDefinition;
import com.beowulfchain.beowulfchainsdk.AppData.AppData.ChatMessage.ChatMessStatus;
import com.beowulfchain.beowulfchainsdk.AppData.AppData.SipMessage.SipHelpDropMess;
import com.beowulfchain.beowulfchainsdk.AppData.AppData.SipMessage.SipHelpMess;
import com.beowulfchain.beowulfchainsdk.AppData.AppData.SipMessage.SipHelpRequestMess;
import com.beowulfchain.beowulfchainsdk.AppData.AppData.SipMessage.SipHelpResponeMess;
import com.beowulfchain.beowulfchainsdk.BWFCallCenter;
import com.beowulfchain.beowulfchainsdk.MainApplication;
import com.beowulfchain.beowulfchainsdk.account.ChatAccountData;
import com.beowulfchain.beowulfchainsdk.linhphone.CallManager;
import com.beowulfchain.beowulfchainsdk.util.ApiClient;
import com.beowulfchain.beowulfchainsdk.util.UploadAPIs;

import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.ConnectionListener;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.chat2.Chat;
import org.jivesoftware.smack.chat2.ChatManager;
import org.jivesoftware.smack.chat2.IncomingChatMessageListener;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration;
import org.json.JSONException;
import org.json.JSONObject;
import org.jxmpp.jid.EntityBareJid;
import org.jxmpp.jid.impl.JidCreate;
import org.jxmpp.stringprep.XmppStringprepException;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChatConnection {

    private static ChatConnection instance = null;
    private static XMPPTCPConnection mConnection = null;
    private static ConnectionListener mConnectionListener = null;
    private static IncomingChatMessageListener mIncommingMessageListener = null;
    private static HashMap<String, SipHelpRequestMess> helpRequestMapping = new HashMap<>();
    private static HashMap<String, SipHelpRequestMess> responeRequestMapping = new HashMap<>();
    private static HashMap<String, ArrayList<ChatMess>> queueOfTextChatMsg = new HashMap<String, ArrayList<ChatMess>>();


    public void Disconnect()
    {
        if(null!=mConnection)
            mConnection.disconnect();
    }

    public String getCurrentConversation() {
        return currentConversation;
    }

    private String currentConversation = "";

    public static ChatConnection getInstance() {
        if (null == instance)
            instance = new ChatConnection();
        return instance;
    }

    private ChatConnection() {

        mConnectionListener = new ConnectionListener() {
            @Override
            public void connected(XMPPConnection connection) {
                String awdawda = "";
            }

            @Override
            public void authenticated(XMPPConnection connection, boolean resumed) {
                String awdawda = "";
                if (true != resumed) {
                    BWFCallCenter.getInstance().onChatResult(true);
                }
            }

            @Override
            public void connectionClosed() {
                String awdawda = "";
            }

            @Override
            public void connectionClosedOnError(Exception e) {
                String awdawda = "";
            }
        };

        mIncommingMessageListener = new IncomingChatMessageListener() {
            @Override
            public void newIncomingMessage(EntityBareJid from, Message message, Chat chat) {

                String messFrom = message.getFrom().toString();
                String messFromWithDomain = message.getFrom().toString();
                String messTo = message.getTo().toString();
                String messToWithDomain = message.getTo().toString();

                if (messFrom.split("@").length >= 2)
                    messFrom = messFrom.split("@")[0];

                if (messTo.split("@").length >= 2)
                    messTo = messTo.split("@")[0];

                try {
                    JSONObject commonMessObj = new JSONObject(message.getBody());
                    ProcessIncommingMessage(commonMessObj, messFrom, messTo, messFromWithDomain, messToWithDomain);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        };
    }

    public void ProcessIncommingMessage(JSONObject jsonMessage, String messFrom, String messTo, String msgFromWithDomain, String msgToWithDomain) throws JSONException {
        if (!jsonMessage.isNull("type")) {
            String type = jsonMessage.getString("type");
//                        if (type.matches("admin_message")) {
//                            if (!commonMessObj.isNull("name") && !commonMessObj.isNull("data")) {
//                                JSONObject tokenObject = commonMessObj.getJSONObject("data");
//                                String access_token = tokenObject.getString("access_token");
//                                String isLogout = commonMessObj.getString("name");
//
//                                if (isLogout.matches("account_deleted")) {
//                                    if (Welcome.isInstance())
//                                        Welcome.instance().DoLogout();
//                                    return;
//                                }
//
//                                if ("logout".matches(isLogout) && access_token.matches(BeowulInfo.getInstance().getToken())) {
//                                    if (Welcome.isInstance()){
//                                        login = true;
//                                        Welcome.instance().DoLogout();
//
//                                    }
//
//                                    return;
//                                }
//                            }
//                        }

            do {
                if (type.matches("notification")) {
                    if (!jsonMessage.isNull("name")) {
                        String strMessageStatus = jsonMessage.getString("name");
                        if ("messageStatus".matches(strMessageStatus)) {
                            ChatMess messStatus = new ChatMessStatus(jsonMessage, messFrom, messTo);
                            BWFCallCenter.NotifyNewChat(messStatus);
                        }

                        String strHelpType = jsonMessage.getString("name");
                        do {
                            if (strHelpType.matches("helpResponse")) {
                                // check here to make sure incalling or not
                                SipHelpMess sipHelpRespone = new SipHelpResponeMess(jsonMessage, messFrom, messTo);
                                String reqestId = ((SipHelpResponeMess) sipHelpRespone).getRequestId();

                                currentConversation=MainApplication.getInstance().CreateNewConversation(reqestId, messFrom, messTo, "");



                                SipHelpRequestMess myHelpRequest = PopExistedHelpRequestFromHelpList(reqestId+"@"+messFrom);
                                responeRequestMapping.put(reqestId, myHelpRequest);

                                if (null != myHelpRequest) {
                                    String request_type = myHelpRequest.getRequest_type();
                                    do {
                                        if ("video".matches(request_type)) {
                                            CallManager.getInstance().newOutgoingVideoCall(myHelpRequest.getUser_sip_id(), "", reqestId);
                                            DropTheOthersCallExceptAvailableId();
                                            break;
                                        }
                                        if ("audio".matches(request_type) || "voice".matches(request_type)) {
                                            CallManager.getInstance().newOutgoingVoiceCall(myHelpRequest.getUser_sip_id(), "", "xxxx", reqestId);
                                            DropTheOthersCallExceptAvailableId();
                                            break;
                                        }
                                        if ("chat".matches(request_type)) {
                                            String msgIdFrom = "";
                                            String msgIdTo = "";
                                            String lable = "";

                                            msgIdFrom = myHelpRequest.getMessageFrom();
                                            msgIdTo = myHelpRequest.getMessageTo();
                                            lable = myHelpRequest.getLabel();

//                                            ActivityLog chatActivity = new ActivityLog(lable, ActivityLog.enum_activity_log.chat.getVal(), msgIdFrom, msgIdTo, true, reqestId);
//                                            MainApplication.InsertActivity(chatActivity);

                                            if (queueOfTextChatMsg.containsKey(reqestId)) {
                                                ArrayList<ChatMess> listOfMsg = queueOfTextChatMsg.get(reqestId);
                                                for (ChatMess textChatMessSend : listOfMsg) {
                                                    String payload = ((ChatMessSend) textChatMessSend).getChatMessSendObject().toString();
                                                    String domainName = ChatAccountData.getInstance().getChat_domain();
                                                    boolean sendResult = SendTextChat(payload, textChatMessSend.getMessageTo(), domainName);
                                                    if (true == sendResult) {
                                                        listOfMsg.remove(listOfMsg);
                                                    }
                                                }
                                                queueOfTextChatMsg.remove(reqestId);
                                                queueOfTextChatMsg.put(reqestId, listOfMsg);
                                            }

                                            break;
                                        }
                                    }
                                    while (false);

                                }

                                break;
                            }

                            if (strHelpType.matches("helpDrop")) {
                                SipHelpDropMess sipHelpMess = new SipHelpDropMess(jsonMessage);
                                sipHelpMess.setMessageFrom(messFrom);
                                sipHelpMess.setMessageTo(messTo);
                                String requestIdDeCline = sipHelpMess.getRequestId();
                                int numberOfWaiter=0;
                                numberOfWaiter=RemoveSipHelpRequestMess(requestIdDeCline+"@"+messFrom);
                                BWFCallCenter.getInstance().onNumberOfSupporterRemainded(numberOfWaiter);
                                break;
                            }
                            if (strHelpType.matches("endCall")) {
                                String aaaaa = "";

                            }

                        } while (false);
                        break;
                    }
                } else {
                    if (type.matches("text")) {
                        ChatMess mess = new ChatMessReceive(jsonMessage, messFrom, messTo);
                        mess.setStatus(MsgDefinition.enum_message_send_status.delivered.getStringVal());
                        BWFCallCenter.NotifyNewChat(mess);

                        MainApplication.InsertTextChatMsg(mess);

                        //confirm delivered
                        ChatMessStatus responStatus = new ChatMessStatus(mess.getMsgId(), mess.getMessageTo(), mess.getMessageFrom(), MsgDefinition.enum_message_send_status.delivered.getStringVal());
                        SendDelivered(responStatus);
                    }
                    if (type.matches("image")) {

                        if (!jsonMessage.isNull("pushContent")) {
                            String pushContent = jsonMessage.getString("pushContent");
                        }
                        if (!jsonMessage.isNull("data")) {
                            ChatMess withBitmap = new ChatMessReceive(jsonMessage, messFrom, messTo);
                            withBitmap.setStatus(MsgDefinition.enum_message_send_status.delivered.getStringVal());
                            BWFCallCenter.NotifyNewChat(withBitmap);
                            MainApplication.InsertTextChatMsg(withBitmap);
                            ChatMessStatus responStatus = new ChatMessStatus(withBitmap.getMsgId(), withBitmap.getMessageTo(), withBitmap.getMessageFrom(), MsgDefinition.enum_message_send_status.delivered.getStringVal());
                            SendDelivered(responStatus);
                        }

                    }

                }

            }
            while (false);

        }
    }

    public void DropTheOthersCallExceptAvailableId() {
        for (SipHelpRequestMess requestMess : helpRequestMapping.values()) {
            String payload = requestMess.FormHelpDrop();
            String domainName = ChatAccountData.getInstance().getChat_domain();
            SendDropMessage(payload, requestMess.getUser_chat_id(), domainName);
        }
        RemoveAllHelpRequest();

    }


    private static void Login(String username, String password) {

        String arrUsername[] = (ChatAccountData.getInstance().getChat_username().split("@"));
        if (arrUsername.length >= 2) {
            username = arrUsername[0];
        }

        password = ChatAccountData.getInstance().getChat_password();
        try {
            mConnection.login(username, password);
        } catch (XMPPException e) {
            e.printStackTrace();
        } catch (SmackException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public boolean SendDropMessage(String payload, String sendToChatId, String mDomainName) {
        boolean success = true;
        EntityBareJid jid = null;
        ChatManager chatManager = ChatManager.getInstanceFor(mConnection);
        try {
            jid = JidCreate.entityBareFrom(sendToChatId + "@" + mDomainName);
            Chat chat = chatManager.chatWith(jid);
            Message message = new Message(jid, Message.Type.chat);
            message.setBody(payload);
            chat.send(message);
        } catch (XmppStringprepException | SmackException.NotConnectedException | InterruptedException e) {
            success = false;
            e.printStackTrace();
        }
        return success;

    }

    public boolean SendHelpMessage(String payload, String sendToChatId, String mDomainName) {
        boolean success = true;
        EntityBareJid jid = null;
        ChatManager chatManager = ChatManager.getInstanceFor(mConnection);
        try {
            jid = JidCreate.entityBareFrom(sendToChatId + "@" + mDomainName);
            Chat chat = chatManager.chatWith(jid);
            Message message = new Message(jid, Message.Type.chat);

            message.setBody(payload);
            chat.send(message);
        } catch (XmppStringprepException | SmackException.NotConnectedException | InterruptedException e) {
            success = false;
            e.printStackTrace();
        }
        return success;

    }


    public void ConnectServer() {
        String domain = ChatAccountData.getInstance().getChat_domain();
        String chatHost = ChatAccountData.getInstance().getChat_host();
        String chatPort = ChatAccountData.getInstance().getChat_port();
        EntityBareJid jid = null;
        try {
            jid = JidCreate.entityBareFrom(ChatAccountData.getInstance().getChat_username() + "/android");
            XMPPTCPConnectionConfiguration conf = XMPPTCPConnectionConfiguration.builder()
                    .setXmppDomain(domain)
                    .setHost(chatHost)
                    .setPort(Integer.parseInt(chatPort))
                    .setAuthzid(jid)

                    //Was facing this issue
                    //https://discourse.igniterealtime.org/t/connection-with-ssl-fails-with-java-security-keystoreexception-jks-not-found/62566
                    .setKeystoreType(null) //This line seems to get rid of the problem
                    .setSecurityMode(ConnectionConfiguration.SecurityMode.required)
                    .setCompressionEnabled(true).build();

            mConnection = new XMPPTCPConnection(conf);
            mConnection.removeConnectionListener(mConnectionListener);
            mConnection.addConnectionListener(mConnectionListener);
            ChatManager.getInstanceFor(mConnection).setXhmtlImEnabled(true);
            ChatManager.getInstanceFor(mConnection).addIncomingListener(mIncommingMessageListener);
            mConnection.connect();

            String password = ChatAccountData.getInstance().getChat_password();
            String username = ChatAccountData.getInstance().getChat_username();

            Login(username, password);

        } catch (XmppStringprepException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (SmackException e) {
            e.printStackTrace();
        } catch (XMPPException e) {
            e.printStackTrace();
        }

    }

    public static void AddSipHelpRequestMess(String requestId, SipHelpRequestMess requestMess) {
        String newrequestId=requestId+"@"+requestMess.getMessageTo();
        if (!helpRequestMapping.containsKey(newrequestId)) {
            helpRequestMapping.put(newrequestId, requestMess);
        }
    }

    public static SipHelpRequestMess PopExistedHelpRequestFromHelpList(String requestId) {
        SipHelpRequestMess ret = null;
        if (helpRequestMapping.containsKey(requestId)) {
            ret = helpRequestMapping.get(requestId);
            helpRequestMapping.remove(requestId);
        }
        return ret;
    }

    public void RemoveAllHelpRequest() {
        helpRequestMapping.clear();
    }

    public int RemoveSipHelpRequestMess(String requestId) {
        if (helpRequestMapping.containsKey(requestId)) {
            helpRequestMapping.remove(requestId);
        }
        return helpRequestMapping.size();
    }


    public static boolean SendTextChat(String payload, String sendToChatId, String mDomainName) {
        boolean ret = false;
        EntityBareJid jid = null;
        ChatManager chatManager = ChatManager.getInstanceFor(mConnection);
        try {
            jid = JidCreate.entityBareFrom(sendToChatId + "@" + mDomainName);
            Chat chat = chatManager.chatWith(jid);
            Message message = new Message(jid, Message.Type.chat);
            message.setBody(payload);
            chat.send(message);
            ret = true;

        } catch (XmppStringprepException e) {
            e.printStackTrace();
        } catch (SmackException.NotConnectedException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return ret;
    }


    public static boolean PushChatToQueue(String expectedConversationId, ChatMess message) {
        boolean sendResult = false;
        if (responeRequestMapping.containsKey(expectedConversationId)) {
            SipHelpRequestMess responeMsg = responeRequestMapping.get(expectedConversationId);
            String responeFrom = responeMsg.getMessageTo();
            String currentAccountChatId = ChatAccountData.getInstance().GetChatIdWithoutDomain();
            message.setMessageFrom(currentAccountChatId);
            message.setMessageTo(responeFrom);

            String payload = ((ChatMessSend) message).getChatMessSendObject().toString();
            String domainName = ChatAccountData.getInstance().getChat_domain();
            sendResult = SendTextChat(payload, responeFrom, domainName);
        } else {
            if (!queueOfTextChatMsg.containsKey(expectedConversationId)) {
                ArrayList<ChatMess> msgList = new ArrayList<>();
                msgList.add(message);
                queueOfTextChatMsg.put(expectedConversationId, msgList);
            } else {
                queueOfTextChatMsg.get(expectedConversationId).add(message);
            }
        }
        return sendResult;
    }

    public static String UploadImageMessage(String expectedConversationId, File compressedImgFile) {


        String ret = "";
        if (responeRequestMapping.containsKey(expectedConversationId)) {
            SipHelpRequestMess responeMsg = responeRequestMapping.get(expectedConversationId);
            ChatMess message = new ChatMessSend("", ChatAccountData.getInstance().getUserChatIdWithOutDomain(), responeMsg.getMessageTo(), expectedConversationId, true);
            ret = message.getMsgId();
            ChatConnection.getInstance().uploadToServer(String.valueOf(compressedImgFile), expectedConversationId, responeMsg.getMessageTo(), message);
//
//
//            try {
//              Bitmap bitmap = MediaStore.Images.Media.getBitmap(MainApplication.getSDKContext().getContentResolver(),path);
//
//                DoDisplayRcvBitmap(bitmap, "", "");
//                File file = new File(imgDecodableString);
//                long fileSizeInBytes = file.length();
//                long fileSizeInKB = fileSizeInBytes / 1024;
//                long fileSizeInMB = fileSizeInKB / 1024;
//
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
        }
        return ret;
    }

    public boolean SendDelivered(ChatMessStatus message) {
        message.setStatus(MsgDefinition.enum_message_send_status.delivered.toString());
        boolean ret = false;
        String payload = message.GetString();
        String domainName = ChatAccountData.getInstance().getChat_domain();
        ret = SendTextChat(payload, message.getMessageTo(), domainName);
        return ret;
    }

    public boolean SendSeen(ChatMessStatus message) {
        message.setStatus(MsgDefinition.enum_message_send_status.seen.toString());
        boolean ret = false;
        String payload = message.GetString();
        String domainName = ChatAccountData.getInstance().getChat_domain();
        ret = SendTextChat(payload, message.getMessageTo(), domainName);
        return ret;
    }

    public void uploadToServer(String filePath, final String expectedConversationId, final String msgIdTo, final ChatMess message) {
        UploadAPIs uploadAPIs = ApiClient.getApiClient().create(UploadAPIs.class);
        File file = new File(filePath);
        RequestBody fileReqBody = RequestBody.create(MediaType.parse("image/*"), file);
        MultipartBody.Part part = MultipartBody.Part.createFormData("image", file.getName(), fileReqBody);
        final RequestBody conversationId = RequestBody.create(MediaType.parse("text/plain"), expectedConversationId);
        RequestBody chatId = RequestBody.create(MediaType.parse("text/plain"), msgIdTo);
        //
        Call<ResponseBody> call = uploadAPIs.uploadImage(part, conversationId, chatId);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                try {
                    String url = response.body().string();
                    message.setData(url);
                    boolean ret = BWFCallCenter.sendText(expectedConversationId, message);

                    if (true == ret) {
//                        String myChatid = ChatAccountData.getInstance().getUserChatIdWithOutDomain();
//                        message.setMessageFrom(msgIdTo);
//                        message.setMessageTo(msgIdFrom);
//                        message.setQrname(alias);
                        //   MainApplication.getInstance().InsertTextChatMsg(message);
                        // DoDisplaySendMesg(url, timeStamp);


                        //  DoDisplaySendMesg(message.getPushContent(), message.getTime_stamp(), message.getMsgId());
                    }


                } catch (IOException e) {
                    e.printStackTrace();

                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("a", "a");
            }
        });
    }
}
