package com.beowulfchain.beowulfchainsdk;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.widget.Toast;

public class HttpRequestActivity extends BaseActivity {
    static ProgressDialog dlgProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
    }

    public void showDialogProgress() {
        try {
            if (dlgProgress == null) {
                dlgProgress = new ProgressDialog(this);
                dlgProgress.setMessage("Loading.....");
                dlgProgress.setCancelable(false);
            }
            dlgProgress.show();
        } catch (Exception e) {

        }

    }

    public static void hideDialogProgress() {
        if (dlgProgress != null && dlgProgress.isShowing()) {
            dlgProgress.dismiss();
            dlgProgress=null;
        }

    }


    public static void ToastMessage(String mess) {
        Toast.makeText(context, mess, Toast.LENGTH_LONG).show();
    }

    public static void showErrorDlg(Context context, String msg) {

        AlertDialog infoDlg = new AlertDialog.Builder(context, R.style.AlertDialogErrorStyle)
              //  .setIcon(R.drawable.ic_alert_error)
                .setMessage(msg)
                .setPositiveButton("OK", null)
                .setCancelable(false)
                .create();

        infoDlg.show();
    }


}
