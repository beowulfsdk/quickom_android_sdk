package com.beowulfchain.beowulfchainsdk.util;

public interface ChatSubject {
    public void RegisterChatObserver(ChatObservation observation);
    public void RemoveChatObserver(ChatObservation observation);


}
