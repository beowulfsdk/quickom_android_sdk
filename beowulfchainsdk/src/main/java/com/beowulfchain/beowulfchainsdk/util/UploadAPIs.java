package com.beowulfchain.beowulfchainsdk.util;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface UploadAPIs {
    @Multipart
    @POST("/apiv1/chat/upload-photo")
    Call<ResponseBody> uploadImage(@Part MultipartBody.Part file, @Part("conversation_id") RequestBody conversation_id, @Part("user_chat_id") RequestBody user_chat_id);
}
