package com.beowulfchain.beowulfchainsdk.util;


import com.beowulfchain.beowulfchainsdk.AppData.AppData.ChatMessage.ChatMess;

public interface ChatObservation {

    void onUserDataChanged(ChatMess msg);

}
