package com.beowulfchain.beowulfchainsdk.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.net.Uri;
import android.os.SystemClock;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.view.Display;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;


import com.beowulfchain.beowulfchainsdk.R;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Formatter;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;


public class Util
{
	public static String getCurrentTime() {
		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat mdformat = new SimpleDateFormat("HH:mm:ss");
		return mdformat.format(calendar.getTime());
	}

	public static float dp2px(Resources resources, float dp) {
		final float scale = resources.getDisplayMetrics().density;
		return  dp * scale + 0.5f;
	}

	public static float sp2px(Resources resources, float sp){
		final float scale = resources.getDisplayMetrics().scaledDensity;
		return sp * scale;
	}

	public static float round(float d, int decimalPlace) {
		BigDecimal bd = new BigDecimal(Float.toString(d));
		bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
		return bd.floatValue();
	}


	private static String toHexString(final byte[] bytes) {
		final Formatter formatter = new Formatter();
		for (final byte b : bytes) {
			formatter.format("%02x", b);
		}
		return formatter.toString();
	}

	public static String hmacSha256(final String key, final String s){
		try
		{
			final Mac mac = Mac.getInstance("HmacSHA256");
			mac.init(new SecretKeySpec(key.getBytes(), "HmacSHA256"));
			return toHexString(mac.doFinal(s.getBytes()));
		}
		catch (final Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	public static String getAppVersion(Context context)
	{
		String version = "";
		try
		{
			PackageInfo pinfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
			version = pinfo.versionName;
		}
		catch (Exception e)
		{

		}

		return version;
	}


	// 2-byte number
	public static char ShortC2Java(int i)
	{
	    return (char)(((i>>8)&0xff)|((i << 8)&0xff00));
	}

	// 4-byte number
	public static int IntC2Java(int i)
	{
	    return((i&0xff)<<24)|((i&0xff00)<<8)|((i&0xff0000)>>8)|((i>>24)&0xff);
	}

	public static String removeTrailingZero(String s)
	{
		return s.indexOf(".") < 0 ? s : s.replaceAll("0*$", "").replaceAll("\\.$", "");
	}



	public static Bitmap TextToImageEncode(String value) {
		Bitmap bitmap = null;

		MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
		try {
			BitMatrix bitMatrix = multiFormatWriter.encode(value, BarcodeFormat.QR_CODE, 1000, 1000);
			BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
			bitmap = barcodeEncoder.createBitmap(bitMatrix);

		} catch (WriterException e) {
			e.printStackTrace();
		}
		return bitmap;
	}

	public static void hideEmulatorKeyboard(Activity activity)
	{
		if (activity != null && activity.getCurrentFocus() != null) {
			InputMethodManager im = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
			im.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
		}
	}

	public static Point getDisplaySize(Context context)
	{
		Display display = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		return size;
	}

	public static void waitInMilisecond(int miliseconds)
	{
		SystemClock.sleep(miliseconds);
	}

	public static void showErrMessage(Context context, String msg)
	{
		Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
	}

	public static void gotoAppSetting(Context context)
	{
		Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
		Uri uri = Uri.fromParts("package", context.getPackageName(), null);
		intent.setData(uri);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
		intent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
		context.startActivity(intent);
	}



	public static void showWarningDlg(Context context, String msg)
	{
		//AlertDialog infoDlg = new AlertDialog.Builder(context, R.style.AlertDialogWarningStyle)
				//.setIcon(R.drawable.ic_alert_warning)
		AlertDialog infoDlg = new AlertDialog.Builder(context)
				.setTitle(R.string.app_name)
				.setMessage(msg)
				.setPositiveButton("OK", null)
				.setCancelable(false)
				.create();

		infoDlg.show();
	}



	public static int getStatusBarHeight(Context context) {
		int result = 0;
		int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
		if (resourceId > 0) {
			result = context.getResources().getDimensionPixelSize(resourceId);
		}
		return result;
	}


	/* create device id by generate UUID
	public synchronized static String getDeviceId(Context context) {
		String uniqueID = null;
		final String PREF_UNIQUE_ID = "PREF_UNIQUE_ID";
		if (uniqueID == null) {
			SharedPreferences sharedPrefs = context.getSharedPreferences(
					PREF_UNIQUE_ID, Context.MODE_PRIVATE);
			uniqueID = sharedPrefs.getString(PREF_UNIQUE_ID, null);
			if (uniqueID == null) {
				uniqueID = UUID.randomUUID().toString();
				SharedPreferences.Editor editor = sharedPrefs.edit();
				editor.putString(PREF_UNIQUE_ID, uniqueID);
				editor.commit();
			}
		}
		return uniqueID;
	}
	*/

	public static String getDeviceId(Context context)
	{
		String androidId = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
		return androidId;
	}

}
