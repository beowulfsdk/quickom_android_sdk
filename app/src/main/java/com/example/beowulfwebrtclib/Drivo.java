package com.example.beowulfwebrtclib;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.beowulfwebrtc.API.UpdateLocation;
import com.example.beowulfwebrtc.AppData.WebRTCCallInfo;
import com.example.beowulfwebrtc.CallCenterManager.CallCenterInstance;
import com.example.beowulfwebrtc.SDKProtocol.BWF_CMM_CallManager;
import com.example.beowulfwebrtc.Signal.CallOutSignal;
import com.example.beowulfwebrtc.Signal.WaitCallSignal;

public class Drivo extends AppCompatActivity implements CallCenterInstance.CallCenter {

    EditText et_drivo_id;
    Button bt_call_drivo;
    CallOutSignal outSignal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drivo);
        CallCenterInstance.addListener(this);

      //
  //      new UpdateLocation( 106.705395,10.772004).Execute("Basic YTBkOTI1YTYtMGY1Yy00NGNlLTljM2ItZDhmMzNhMTEyZGJhOjAyY2MyZWExLTU1NmQtNGNiOC04ZDY4LWFlYmQwODgzZTgyMg==");

    //    BWF_CMM_CallManager.SetMyCallId("6a680530-7bc6-4741-96bf-0f58d6de77fb");

        WaitCallSignal.getInstance().connect("6b37dfa0-1bb6-4f0f-ace4-d2c73a4a2273");

        et_drivo_id = findViewById(R.id.et_drivo_id);
        bt_call_drivo = findViewById(R.id.bt_call_drivo);
        bt_call_drivo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CallOutSignal outSignal = new CallOutSignal();
                outSignal.connectToWsAndCallDrivo(WebRTCCallInfo.getMyCallId(), "dinh", "video", "11521580809962809", 106.705474, 10.771959, "");
              //  BWF_CMM_CallManager.LauchOutgoingCall(true);
            }
        });


    }

    @Override
    public void onCallCenterConnected() {
        String onConnected = "connected";
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
              //  Toast.makeText(Drivo.this, "connected", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onCallCenterDisconnected() {

    }

    @Override
    public void onCallCancel() {

    }

    @Override
    public void onCallIncomming() {

    }

    @Override
    public void onCallError(String error) {

    }

    @Override
    public void onChannelMessage(String message) {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        CallCenterInstance.removeListener(this);
    }
}
