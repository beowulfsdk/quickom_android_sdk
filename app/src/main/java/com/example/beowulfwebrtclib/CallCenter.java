package com.example.beowulfwebrtclib;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.beowulfwebrtc.API.GetIdleAccount;
import com.example.beowulfwebrtc.API.startWithIdentifier;
import com.example.beowulfwebrtc.AppData.IdleAccountInfo;
import com.example.beowulfwebrtc.AppData.WebRTCCallInfo;
import com.example.beowulfwebrtc.CallCenterManager.CallCenterInstance;
import com.example.beowulfwebrtc.SDKProtocol.BWF_CMM_CallManager;
import com.example.beowulfwebrtc.SDKProtocol.BWF_CMM_Error;
import com.example.beowulfwebrtc.SDKProtocol.BWF_CMM_MessageDef;
import com.example.beowulfwebrtc.SDKProtocol.BWF_CMM_Protocol;
import com.example.beowulfwebrtc.SDKProtocol.BWF_CMM_Protocol_deligate;
import com.example.beowulfwebrtc.Signal.CallOutSignal;

public class CallCenter extends AppCompatActivity {

    CallOutSignal outSignal = null;

    EditText et_call_alias;
    EditText et_displayname;
    Button bt_call;
    String alias = "";
    String displayName = "";
    IdleAccountInfo idleAccountInfo = null;

    boolean isVideoCall = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call_center);

        outSignal = new CallOutSignal();


        et_call_alias = findViewById(R.id.et_call_alias);
        et_displayname = findViewById(R.id.et_displayname);
        bt_call = findViewById(R.id.bt_call);


        bt_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                alias = et_call_alias.getText().toString();
                displayName = et_displayname.getText().toString();


                AlertDialog.Builder builder = new AlertDialog.Builder(CallCenter.this);
                builder.setTitle("Do you want to make video call or voice only");
                builder.setMessage("Please select");

                builder.setCancelable(true);

                builder.setPositiveButton("Video", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        isVideoCall = true;
                        outSignal.connectToWsAndCall(WebRTCCallInfo.getMyCallId(), alias, displayName, "video", idleAccountInfo.getChat_id());
                        BWF_CMM_CallManager.LauchOutgoingCall(isVideoCall);
                        dialog.dismiss();
                    }
                });


                builder.setNegativeButton("Audio/Voice", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        isVideoCall = false;
                        outSignal.connectToWsAndCall(WebRTCCallInfo.getMyCallId(), alias, displayName, "audio", idleAccountInfo.getChat_id());
                        BWF_CMM_CallManager.LauchOutgoingCall(isVideoCall);
                        dialog.dismiss();

                    }
                });
                builder.show();


            }
        });


        GetIdleAccount.addListener(new GetIdleAccount.OnResult() {
            @Override
            public void onResultListener(IdleAccountInfo _idleAccountInfo) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                     //   Toast.makeText(CallCenter.this, "Ready to make a Call", Toast.LENGTH_SHORT).show();
                        idleAccountInfo = _idleAccountInfo;
                    }
                });
            }
        });
        GetIdleAccount.Execute();

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

        GetIdleAccount.removeListener();
    }


}
