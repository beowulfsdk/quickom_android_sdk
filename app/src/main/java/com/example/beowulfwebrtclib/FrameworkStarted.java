package com.example.beowulfwebrtclib;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.HandlerThread;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.beowulfwebrtc.BWBroadcastReciever.ConnectivityChangeReceiver;
import com.example.beowulfwebrtc.Interface.ChannelMessage;
import com.example.beowulfwebrtc.MessageTriggling.XMPP.BeowulfXMPPConfig;
import com.example.beowulfwebrtc.PeerSettings.PeerConnectionConfig;
import com.example.beowulfwebrtc.SDKApplication;
import com.example.beowulfwebrtc.SDKProtocol.BWF_CMM_AccountData;
import com.example.beowulfwebrtc.SDKProtocol.BWF_CMM_CallManager;
import com.example.beowulfwebrtc.SDKProtocol.BWF_CMM_Error;
import com.example.beowulfwebrtc.SDKProtocol.BWF_CMM_MessageDef;
import com.example.beowulfwebrtc.SDKProtocol.BWF_CMM_Protocol;
import com.example.beowulfwebrtc.SDKProtocol.BWF_CMM_Protocol_deligate;
import com.example.beowulfwebrtc.Signal.CallingSignal;
import com.example.beowulfwebrtc.WebSocket.WebSocketChannel;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Semaphore;

public class FrameworkStarted extends AppCompatActivity implements BWF_CMM_Protocol, ChannelMessage.ChannelEvents {

    Button bt_your_id;
    Button bt_make_a_call;
    String TAG = "TEST_FRAMEWORK";


    AlertDialog infoDlg = null;
    EditText et_calling_to;

    static boolean isCallProcessed = false;
    boolean isCallingWithVideo = false;

    CountDownTimer countDownTimer = null;


    private String generateLoginURI(String userId) {
        String ret = "";
        try {
            ret = new URI(PeerConnectionConfig.callingEndPoint + "/?id=" + userId).toString();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return ret;
    }

    String uri = "";
    WebSocketChannel socket = null;
    Handler handler = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_framework_started);
        et_calling_to = findViewById(R.id.et_calling_to);


        HandlerThread handlerThread = new HandlerThread(TAG);
        handlerThread.start();
        handler = new Handler(handlerThread.getLooper());
        socket = new WebSocketChannel(handler, this);

        uri = generateLoginURI("123321465").toString();
        //socket.connect(uri, false);


        infoDlg = new AlertDialog.Builder(FrameworkStarted.this)
                .setCancelable(true)
                .setPositiveButton("ACCEPT", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        BWF_CMM_CallManager.acceptIncomingCall();
                        if (true == isCallingWithVideo) {

                            Intent t = new Intent(FrameworkStarted.this, MainActivity.class);
                            t.putExtra("OUTGOING", false);
                            startActivity(t);
                        } else {
                            Intent t = new Intent(FrameworkStarted.this, MainActivityAudio.class);
                            t.putExtra("OUTGOING", false);
                            startActivity(t);
                        }

                    }
                })
                .setNegativeButton("DENY", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        BWF_CMM_CallManager.denyIncomingCall();
                    }
                })
                .create();


        BWF_CMM_Protocol_deligate.AddListener(this);

        bt_your_id = findViewById(R.id.bt_your_id);
        bt_make_a_call = findViewById(R.id.bt_make_a_call);

        bt_your_id.setText("your  id: " + BWF_CMM_AccountData.referenceId + " - chat:" + BeowulfXMPPConfig.getUsername());


        bt_make_a_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (false == isCallProcessed) {
                    isCallProcessed = true;
                    String identifier = et_calling_to.getText().toString();
                    if (!"".matches(identifier))
                        BWF_CMM_CallManager.getInstance().voiceCallTo(identifier);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            bt_make_a_call.setText("Cancel Call");
                        }
                    });

                    countDownTimer = new CountDownTimer(BWF_CMM_CallManager.timeoutInSecond, 1000) {
                        public void onTick(long millisUntilFinished) {
                            bt_make_a_call.setText("Cancel Call (" + millisUntilFinished / 1000 + ")");
                        }

                        public void onFinish() {
                            bt_make_a_call.setText("Make a Call");
                            isCallProcessed = false;
                        }

                    };
                    countDownTimer.start();
                } else {
                    isCallProcessed = false;
                    BWF_CMM_CallManager.cancelCurrentCall();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            bt_make_a_call.setText("Make a Call");
                        }
                    });
                    countDownTimer.cancel();
                }

            }
        });

    }

    @Override
    public void onStop() {
        super.onStop();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        BWF_CMM_Protocol_deligate.RemoveListener(this);
    }

    @Override
    public void bwf_cmm_didStartWithIdentifier(String identifier) {
    }

    @Override
    public void bwf_cmm_failedWithError(BWF_CMM_Error error) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //Toast.makeText(FrameworkStarted.this, error.getDescription(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void bwf_cmm_Calling_CallDidChangeToStateReceivedIncomingCallFrom(String fromIdentifier, boolean isVideo) {
        BWF_CMM_CallManager.LauchIncommingCall();
    }


    @Override
    public void bwf_cmm_Calling_CallDidChangeToStateConnected(String fromIdentifier, boolean isVideo) {

    }

    @Override
    public void bwf_cmm_Calling_CallDidChangeToStateEnd(long totalDuration, BWF_CMM_Error error) {

        long duration = totalDuration;
    }

    @Override
    public void bwf_cmm_receivePushContent(String base64DecodedStr) {

    }

    @Override
    public void bwf_cmm_generatePushContent(String base64DecodedStr, String request_id) {

    }

    @Override
    public void bwf_cmm_Calling_CallDidChangeToStateOutgoing(String fromIdentifier, boolean isVideo) {

        BWF_CMM_CallManager.LauchOutgoingCall(isVideo);
        if(null!=countDownTimer)
        {
            countDownTimer.cancel();
            countDownTimer.onFinish();

        }


//        runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//                Toast.makeText(FrameworkStarted.this, "calling is processing", Toast.LENGTH_SHORT).show();
//            }
//        });
    }

    @Override
    public void bwf_cmm_Calling_CallDidChangeToStateOutgoing(boolean isVideo) {



    }

    @Override
    public void bwf_cmm_Call_Denied(String fromIdentifier, String reason) {
        //infoDlg.dismiss();
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
               // Toast.makeText(FrameworkStarted.this, "Call canceled: " + reason, Toast.LENGTH_SHORT).show();
                if(null!=countDownTimer)
                {
                    countDownTimer.cancel();
                    countDownTimer.onFinish();

                }

            }
        });
    }

    @Override
    public void bwf_cmm_generatePushContent(String data, String identifier, BWF_CMM_MessageDef.bwf_cmm_push_content_type type) {
        Log.d("MY_PUSHING_MESSAGE", data + " _to_ " + identifier + "  _type_ " + type.getVal());

    }

    @Override
    public void bwf_cmm_receivePushContent(String data, String identifier, BWF_CMM_MessageDef.bwf_cmm_push_content_type type) {

        Log.d("RECEIVE_MY_PUSHING", data + " _to_ " + identifier + "  _type_ " + type.getVal());


    }

    @Override
    public void onChannelOpen() {

        Log.d(TAG, "onChannelOpen");
    }

    @Override
    public void onChannelError(String error) {
        Log.d(TAG, "onChannelError: " + error);
    }

    @Override
    public void onChannelClosed(String reseason) {


    }

    @Override
    public void onChannelMessage(String message) {

    }
}
