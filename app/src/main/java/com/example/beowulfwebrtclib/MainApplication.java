package com.example.beowulfwebrtclib;

import android.app.Application;
import android.util.Log;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

public class MainApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
                FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                String fireBaseToken = instanceIdResult.getToken();
                Log.d("FIREBASETOKEN", fireBaseToken);
            }
        });
    }
}
