package com.example.beowulfwebrtclib;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.example.beowulfwebrtc.AppData.WebRTCCallInfo;
import com.example.beowulfwebrtc.CallCenterManager.CallCenterInstance;
import com.example.beowulfwebrtc.SDKProtocol.BWF_CMM_CallManager;
import com.example.beowulfwebrtc.Signal.CallingSignal;

import org.webrtc.SurfaceViewRenderer;

import java.util.ArrayList;

public class MainActivityAudio extends AppCompatActivity implements CallCenterInstance.CallCenter {
    CallingSignal callingSignal;

    private static final int CAPTURE_PERMISSION_REQUEST_CODE = 1;
    Chronometer current_call_timer;
    Button end_call;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_audio);

        end_call=findViewById(R.id.end_call);

        CallCenterInstance.addListener(this);

        checkAndRequestCallPermissions();

        current_call_timer = findViewById(R.id.current_call_timer);

        callingSignal = new CallingSignal(this, null, null, false);

        BWF_CMM_CallManager.getInstance().setCallingSignal(callingSignal);

        if (getIntent().hasExtra("OUTGOING")) {
            boolean outGoing = false;
            outGoing=getIntent().getBooleanExtra("OUTGOING", false);

            String partnerId = WebRTCCallInfo.getMyPartnerCallId();
            String myCallId = WebRTCCallInfo.getMyCallId();


            if (true == outGoing) {
                partnerId= WebRTCCallInfo.getMyPartnerCallId();
                myCallId= WebRTCCallInfo.getMyCallId();

            }
            callingSignal.StartToCallWithPartnerId(partnerId, outGoing);

            connectWebRTC(myCallId);
        }
        current_call_timer.setBase(SystemClock.elapsedRealtime());
        current_call_timer.start();
        end_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BWF_CMM_CallManager.endCurrentCall();
                finish();
            }
        });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        CallCenterInstance.removeListener(this);
        CallCenterInstance.releaseOutGoingCall();
    }

    private void checkAndRequestCallPermissions() {
        ArrayList<String> permissionsList = new ArrayList<>();

        int recordAudio = getPackageManager().checkPermission(Manifest.permission.RECORD_AUDIO, getPackageName());
        permissionsList.add(Manifest.permission.RECORD_AUDIO);
        int camera = getPackageManager().checkPermission(Manifest.permission.CAMERA, getPackageName());
        permissionsList.add(Manifest.permission.CAMERA);
        int phone = getPackageManager().checkPermission(Manifest.permission.CALL_PHONE, getPackageName());
        permissionsList.add(Manifest.permission.CALL_PHONE);
        int data_read = getPackageManager().checkPermission(Manifest.permission.READ_EXTERNAL_STORAGE, getPackageName());
        permissionsList.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        int data_write = getPackageManager().checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, getPackageName());
        permissionsList.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);


        if (phone != PackageManager.PERMISSION_GRANTED) {
//            Log.i("[Permission] Asking for record audio");
            permissionsList.add(Manifest.permission.CALL_PHONE);
        }

        if (data_read != PackageManager.PERMISSION_GRANTED) {
            permissionsList.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }

        if (data_write != PackageManager.PERMISSION_GRANTED) {
            permissionsList.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }


        if (permissionsList.size() > 0) {
            String[] permissions = new String[permissionsList.size()];
            permissions = permissionsList.toArray(permissions);
            ActivityCompat.requestPermissions(this, permissions, 0);
        }
    }


    public void connectWebRTC(String id) {
        callingSignal.connect(id);
    }

    @Override
    public void onCallCenterConnected() {

    }

    @Override
    public void onCallCenterDisconnected() {

    }

    @Override
    public void onCallCancel() {

    }

    @Override
    public void onCallIncomming() {

    }

    @Override
    public void onCallError(String error) {

    }

    @Override
    public void onChannelMessage(String message) {

    }
}
