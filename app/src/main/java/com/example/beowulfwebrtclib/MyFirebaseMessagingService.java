package com.example.beowulfwebrtclib;

import android.provider.Settings;
import android.util.Log;


import com.example.beowulfwebrtc.SDKProtocol.BWF_CMM_Protocol_deligate;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    public MyFirebaseMessagingService() {
//        BWF_CMM_Protocol_deligate.AddListener(this);
    }

    @Override
    public void onMessageReceived(RemoteMessage message) {
        super.onMessageReceived(message);


        String body = message.getData().get("body");

        Log.d("FIREBASE_BODY", body);

        BWF_CMM_Protocol_deligate.nofityOnReceivePushContent(body);


    }

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        String android_id = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
        Log.d("androidID", s);

    }

    public static String recurseKeys(JSONObject jObj, String findKey) throws JSONException {
        String finalValue = "";
        if (jObj == null) {
            return "";
        }

        Iterator<String> keyItr = jObj.keys();
        Map<String, String> map = new HashMap<>();

        while (keyItr.hasNext()) {
            String key = keyItr.next();
            map.put(key, jObj.getString(key));
        }

        for (Map.Entry<String, String> e : (map).entrySet()) {
            String key = e.getKey();
            if (key.equalsIgnoreCase(findKey)) {
                return jObj.getString(key);
            }

            // read value
            Object value = jObj.get(key);

            if (value instanceof JSONObject) {
                finalValue = recurseKeys((JSONObject) value, findKey);
            }
        }

        // key is not found
        return finalValue;
    }


//    @Override
//    public void bwf_cmm_didStartWithIdentifier(String identifier) {
//
//    }
//
//    @Override
//    public void bwf_cmm_failedWithError(BWF_CMM_Error error) {
//
//    }
//
//    @Override
//    public void bwf_cmm_Calling_CallDidChangeToStateOutgoing(boolean isVideo) {
//
//    }
//
//    @Override
//    public void bwf_cmm_Call_Denied(String fromIdentifier, String reason) {
//
//    }
//
//    @Override
//    public void bwf_cmm_generatePushContent(String data, String identifier, BWF_CMM_MessageDef.bwf_cmm_push_content_type type) {
//
//    }
//
//    @Override
//    public void bwf_cmm_receivePushContent(String data, String identifier, BWF_CMM_MessageDef.bwf_cmm_push_content_type type) {
//
//    }
//
//    @Override
//    public void bwf_cmm_Calling_CallDidChangeToStateReceivedIncomingCallFrom(String fromIdentifier, boolean isVideo) {
//        BWF_CMM_CallManager.LauchIncommingCall();
//    }
//
//    @Override
//    public void bwf_cmm_Calling_CallDidChangeToStateOutgoing(String fromIdentifier, boolean isVideo) {
//
//    }
//
//    @Override
//    public void bwf_cmm_Calling_CallDidChangeToStateConnected(String fromIdentifier, boolean isVideo) {
//
//    }
//
//    @Override
//    public void bwf_cmm_Calling_CallDidChangeToStateEnd(long totalDuration, BWF_CMM_Error error) {
//
//    }
//
//    @Override
//    public void bwf_cmm_receivePushContent(String base64DecodedStr) {
//
//    }
//
//    @Override
//    public void bwf_cmm_generatePushContent(String base64DecodedStr, String request_id) {
//
//    }
}