package com.example.beowulfwebrtclib;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.beowulfwebrtc.API.startWithIdentifier;
import com.example.beowulfwebrtc.API.stop;
import com.example.beowulfwebrtc.LogUtil;
import com.example.beowulfwebrtc.SDKProtocol.BWF_CMM_CallManager;
import com.example.beowulfwebrtc.SDKProtocol.BWF_CMM_Error;
import com.example.beowulfwebrtc.SDKProtocol.BWF_CMM_MessageDef;
import com.example.beowulfwebrtc.SDKProtocol.BWF_CMM_Protocol;
import com.example.beowulfwebrtc.SDKProtocol.BWF_CMM_Protocol_deligate;

public class LoginSDK extends AppCompatActivity implements BWF_CMM_Protocol, LogUtil.LoggingDisplay {

    LinearLayout ll_processing;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        BWF_CMM_Protocol_deligate.AddListener(this);
        setContentView(R.layout.activity_login_sdk);
        LogUtil.addListener(this);

        BWF_CMM_CallManager.configureWithApiKey("UVIxU2dMMnhBczNxNE5ndDZVbjd5VUovTkhDSG9VckZiWFZScldGd0JGLzVsU2JldFE1WlVYS25aRnA3bFRuMA");

        BWF_CMM_CallManager.HandleLauchIntent(this);

        BWF_CMM_CallManager.setCustomCallingSoundPathForOutgoingCall(R.raw.ring_fore_2);
        BWF_CMM_CallManager.setCustomCallingSoundPathForIncomingCall(R.raw.ring_fore_2);
        BWF_CMM_CallManager.shouldPlaySoundForOutgoingCall(true);
        BWF_CMM_CallManager.shouldPlaySoundForIncomingCall(true);


        ll_processing = findViewById(R.id.ll_processing);
        progressBar = findViewById(R.id.progressBar);

        Button bt_start_framework = findViewById(R.id.bt_start_framework);
        bt_start_framework.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(LoginSDK.this);
                builder.setTitle("Your Identifier");
                builder.setMessage("Please enter your identifier");

                final EditText input = new EditText(LoginSDK.this);
                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT);
                input.setLayoutParams(lp);
                builder.setView(input); // uncomment this line
                input.setHint("your Id");
                input.setText("d100");
                builder.setCancelable(false);

                builder.setPositiveButton("Start", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        String identifier = input.getText().toString();
                        if (!"".matches(identifier))

                            new startWithIdentifier(identifier).Execute();
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                progressBar.setVisibility(View.VISIBLE);
                            }
                        });

                        dialog.dismiss();
                    }
                });


                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();

                    }
                });
                builder.show();


            }
        });
    }

    @Override
    public void bwf_cmm_didStartWithIdentifier(String identifier) {

        startActivity(new Intent(LoginSDK.this, FrameworkStarted.class));
        finish();
        Handler mainHandler = new Handler(LoginSDK.this.getMainLooper());

        Runnable myRunnable = new Runnable() {
            @Override
            public void run() {

                AlertDialog.Builder builder = new AlertDialog.Builder(LoginSDK.this);
                builder.setTitle("Your reference id is: " + identifier);
                builder.setMessage("START to start to call or receive call");
                builder.setCancelable(false);
                builder.setPositiveButton("Start", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        startActivity(new Intent(LoginSDK.this, FrameworkStarted.class));
                        finish();
                        dialog.dismiss();
                    }
                });


                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        new stop().Execute();

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                progressBar.setVisibility(View.GONE);
                                ll_processing.removeAllViews();

                            }
                        });

                        dialog.dismiss();

                    }
                });
              //  builder.show();


            } // This is your code
        };
     //   mainHandler.post(myRunnable);
    }

    @Override
    public void bwf_cmm_failedWithError(BWF_CMM_Error error) {

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progressBar.setVisibility(View.GONE);
                ll_processing.removeAllViews();
            }
        });

        BWF_CMM_Protocol_deligate.RemoveListener(this);
        LogUtil.removeListener(this);
    }


    @Override
    public void bwf_cmm_Calling_CallDidChangeToStateReceivedIncomingCallFrom(String fromIdentifier, boolean isVideo) {

    }

    @Override
    public void bwf_cmm_Calling_CallDidChangeToStateOutgoing(String fromIdentifier, boolean isVideo) {

    }

    @Override
    public void bwf_cmm_Calling_CallDidChangeToStateConnected(String fromIdentifier, boolean isVideo) {

    }

    @Override
    public void bwf_cmm_Calling_CallDidChangeToStateEnd(long totalDuration, BWF_CMM_Error error) {

    }

    @Override
    public void bwf_cmm_receivePushContent(String base64DecodedStr) {

    }

    @Override
    public void bwf_cmm_generatePushContent(String base64EncodedStr, String _request_id) {

    }

    @Override
    public void bwf_cmm_Calling_CallDidChangeToStateOutgoing(boolean isVideo) {


    }

    @Override
    public void bwf_cmm_Call_Denied(String fromIdentifier, String reason) {

    }

    @Override
    public void bwf_cmm_generatePushContent(String data, String identifier, BWF_CMM_MessageDef.bwf_cmm_push_content_type type) {

    }

    @Override
    public void bwf_cmm_receivePushContent(String data, String identifier, BWF_CMM_MessageDef.bwf_cmm_push_content_type type) {

    }

    @Override
    public void DisplayString(String log) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                TextView textView = new TextView(LoginSDK.this);
                textView.setSingleLine(true);
                textView.setText(log);

                ll_processing.addView(textView);
            }
        });
    }
}
