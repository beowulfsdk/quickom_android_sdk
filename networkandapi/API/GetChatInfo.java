package com.example.beowulfwebrtclib.API;



import com.example.beowulfwebrtc.AppData.AppUri;
import com.example.beowulfwebrtc.Signal.WaitCallSignal;
import com.example.beowulfwebrtc.network.JsonAPI;
import com.example.beowulfwebrtclib.BeowulInfo;
import com.example.beowulfwebrtclib.ChatAccountData;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import javax.net.ssl.HttpsURLConnection;

public class GetChatInfo {


    private static GetChatInfo instance = null;

    public static boolean isInstance() {
        return null != instance;
    }

    public static GetChatInfo getInstance() {
        if (null == instance) {
            instance = new GetChatInfo();
        }
        return instance;
    }

    GetChatInfo() {

    }

    JsonAPI.JsonCallback jsonCallback = (statusCode, str) -> {

        if (statusCode == HttpsURLConnection.HTTP_OK) {

            ChatAccountData.getInstance().ParseData(str);
            String chatId = ChatAccountData.getInstance().GetChatIdWithoutDomain();
            WaitCallSignal.getInstance().connect(chatId);
            //NewChatConnection.getInstance().InitChatConenction();

        }
    };

    JsonAPI.JsonRequestListener jsonRequestListener = new JsonAPI.JsonRequestListener() {
        @Override
        public void onBeginRequest() {
        }

        @Override
        public void onEndRequest() {
        }
    };


    public void Execute(String authorization) {
        JSONObject json = new JSONObject();
        try {
            json.put("reference_id", BeowulInfo.getInstance().getAccountId());
        } catch (
                JSONException e) {
            e.printStackTrace();
        }
        String jsonStr = json.toString();
        HashMap<String, String> requestHeader = new HashMap<String, String>();
        requestHeader.put("Content-Type", "application/json");
        requestHeader.put("Authorization", authorization);
        JsonAPI.getInstance().post(requestHeader, AppUri.CHAT_ACCOUNT_INFO, jsonStr, jsonCallback, jsonRequestListener);

    }
}
