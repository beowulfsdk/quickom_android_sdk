package com.example.beowulfwebrtclib.API;



import com.example.beowulfwebrtc.AppData.AppUri;
import com.example.beowulfwebrtc.network.JsonAPI;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import javax.net.ssl.HttpsURLConnection;

public class CreateNewConversation {


    private static CreateNewConversation instance = null;

    public static boolean isInstance() {
        return null != instance;
    }

    public static CreateNewConversation getInstance() {
        if (null == instance) {
            instance = new CreateNewConversation();
        }
        return instance;
    }

    CreateNewConversation() {

    }


    JsonAPI.JsonCallback jsonCallback = (statusCode, str) -> {
        android.util.Log.d("PAYMENT_CALLING", "endcall " + str);

        if (statusCode == HttpsURLConnection.HTTP_OK) {

        }


    };

    JsonAPI.JsonRequestListener jsonRequestListener = new JsonAPI.JsonRequestListener() {
        @Override
        public void onBeginRequest() {
        }

        @Override
        public void onEndRequest() {
        }
    };


    public String Execute(final String conversationId, final String chatIDFrom, final String chatIDTo, final String QRCode) {


        JSONObject json = new JSONObject();
        try {
            json.put("conversation_id", conversationId);
            json.put("from_chat_id", chatIDFrom);
            json.put("to_chat_id", chatIDTo);
            json.put("qr_code", QRCode);
        } catch (
                JSONException e) {
            e.printStackTrace();
        }

        String jsonStr = json.toString();
        HashMap<String, String> requestHeader = new HashMap<String, String>();
        requestHeader.put("Content-Type", "application/json");
        JsonAPI.getInstance().post(requestHeader, AppUri.CREATE_CHAT_CONVERSATION, jsonStr, jsonCallback, jsonRequestListener);


        return conversationId;

    }
}
