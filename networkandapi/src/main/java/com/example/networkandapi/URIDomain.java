package com.example.networkandapi;

public class URIApi
{
    public static boolean bAutoAccept = true;
    public static boolean bProduction = true;
    //    static String URL_TEST_SERVER = "https://testnet-tel.beowulfchain.com";
    public static String URL_REAL_SERVER = "https://quickom-prod-account.beowulfchain.com";
    static String URL_SERVER = URL_REAL_SERVER;//bProduction ? URL_REAL_SERVER : URL_TEST_SERVER;

    public static String CALL_CENTER_DOMAIN = bProduction ? "dispatcher-v2.beowulfchain.com" : "call-center.beowulfchain.com";
    public static String CALL_SIGNAL_DOMAIN = bProduction ? "signaler-v2.beowulfchain.com" : "call-signal.beowulfchain.com";

    public static String ENTERPRISE_LOGIN_POST = URL_SERVER + "/apiv1/enterprise/login";
    public static String ENTERPRISE_LOGIN_GG_POST = URL_SERVER + "/apiv1/enterprise/login-gg";
    public static String ENTERPRISE_LOGIN_FB_POST = URL_SERVER + "/apiv1/enterprise/login-fb";
    public static String ENTERPRISE_PUSH_MESSAGE = URL_SERVER + "/apiv1/account/push/send-to-many";

    public static String ENTERPRISE_ACCDETAIL_GET = URL_SERVER + "/apiv1/enterprise/details";
    public static String ENTERPRISE_LOGOUT_POST = URL_SERVER + "/apiv1/enterprise/logout";
    public static String ENTERPRISE_QRLIST_GET = URL_SERVER + "/apiv1/enterprise/qrcode/list";
    public static String ENTERPRISE_2FA_VERIFY_POST = URL_SERVER + "/apiv1/enterprise/2fa/verify";
    public static String ENTERPRISE_2FA_ENABLE_POST = URL_SERVER + "/apiv1/enterprise/2fa/enable";
    public static String ENTERPRISE_2FA_DISABLE_POST = URL_SERVER + "/apiv1/enterprise/2fa/disable";
    public static String ENTERPRISE_CREATE_QR_POST = URL_SERVER + "/apiv1/enterprise/qrcode/create";
    public static String ENTERPRISE_DELETE_QR_POST = URL_SERVER + "/apiv1/enterprise/qrcode/delete";
    public static String ENTERPRISE_UPDATE_QR_POST = URL_SERVER + "/apiv1/enterprise/qrcode/update";
    public static String ENTERPRISE_DELETE_GR_POST = URL_SERVER + "/apiv1/enterprise/group/delete";
    public static String ENTERPRISE_CHANGE_PASS = URL_SERVER + "/apiv1/enterprise/users/reset-pass";
    public static String ENTERPRISE_UPDATE_USER = URL_SERVER + "/apiv1/enterprise/users/update";
    public static String ENTERPRISE_DELETE_USERGROUP = URL_SERVER + "/apiv1/enterprise/group/remove-users";
    public static String ENTERPRISE_CHANGE_PASS_ACC = URL_SERVER + "/apiv1/enterprise/change-pwd";
    public static String ENTERPRISE_SET_PASS_ACC = URL_SERVER + "/apiv1/enterprise/set-pwd";
    public static String ENTERPRISE_CALLHISTORY = URL_SERVER + "/apiv1/call/history";

    public static String ENTERPRISE_LIST_GROUP_GET = URL_SERVER + "/apiv1/enterprise/group/list";
    public static String ENTERPRISE_LIST_COMPANY_GET = URL_SERVER + "/apiv1/company/list";
    public static String ENTERPRISE_LIST_MEMBER_GET = URL_SERVER + "/apiv1/member/list";
    public static String ENTERPRISE_UPDATE_COMPANY_NAME = URL_SERVER + "/apiv1/company/update";

    public static String ENTERPRISE_ADD_NEW_MEMBER_POST = URL_SERVER + "/apiv1/member/add";
    public static String ENTERPRISE_REMOVE_MEMBER_POST = URL_SERVER + "/apiv1/member/remove";
    public static String ENTERPRISE_CREATE_NEW_GROUP_POST = URL_SERVER + "/apiv1/enterprise/group/create";
    public static String ENTERPRISE_ADD_MEMBER_TO_GROUP_POST = URL_SERVER + "/apiv1/enterprise/group/add-users";
    public static String ENTERPRISE_GET_LIST_WALLET_GET = URL_SERVER + "/apiv1/enterprise/wallet/list";
    public static String ENTERPRISE_FORGOT_PASS_GET = URL_SERVER + "/apiv1/enterprise/forgot-pwd";
    public static String ENTERPRISE_CALL_RATING_POST = URL_SERVER + "/apiv1/call/rate";
    public static String ENTERPRISE_LIST_MEMBERGROUP_GET = URL_SERVER + "/apiv1/enterprise/users/list";
    public static String ENTERPRISE_CREATE_CHAT = URL_SERVER + "/apiv1/chat/create";

    public static String ENTERPRISE_GET_WALLETNAME = URL_SERVER + "/web/account/wallet/list";
    public static String ENTERPRISE_HELP_REQUEST = URL_SERVER + "/apiv1/help-request/event/update";

    // public static String ENTERPRISE_LIST_SUB_USER_POST =URL_SERVER+"/apiv1/enterprise/users/list";

    public static String ENTERPRISE_ACCOUNT_IDLE_GET = URL_SERVER + "/apiv1/account/users/available";
    public static String ENTERPRISE_QR_DETAIL_GET = URL_SERVER + "/apiv1/quickom/qrcode/detail?alias=";
    public static String ENTERPRISE_USER_LIST_GET = URL_SERVER + "/apiv1/enterprise/group/list-users";


    public static String ENTERPRISE_REGISTER_POST = URL_SERVER + "/apiv1/enterprise/register";
    public static String ENTERPRISE_ACTIVE_EMAIL_POST = URL_SERVER + "/apiv1/enterprise/activate";
    public static String ENTERPRISE_RESEND_EMAIL_POST = URL_SERVER + "/apiv1/enterprise/register/resend";

    public static String SIP_ACCOUNT_INFO = URL_SERVER + "/apiv1/account/sip/info";
    public static String CHAT_ACCOUNT_INFO = URL_SERVER + "/apiv1/account/chat/info";
    public static String SUP_ACCOUNT_DETAIL = URL_SERVER + "/apiv1/account/users/detail";
    public static String CALLING_PAYMENT = URL_SERVER + "/apiv1/call/event/update";
    public static String SUBACC_STATUS = URL_SERVER + "/apiv1/account/users/update-status";

    public static String PUSH_TOKEN_ACCOUNT = URL_SERVER + "/apiv1/account/push/update";

    public static String CREATE_CHAT_CONVERSATION = URL_SERVER + "/apiv1/chat/create";

    public static String QR_DETAIL_CONTACT = URL_SERVER + "/apiv1/quickom/qrcode/short-detail?alias=";
    public static String CREATE_CONTACT = URL_SERVER + "/apiv1/enterprise/contact/create";
    public static String LIST_CONTACT = URL_SERVER + "/apiv1/enterprise/contact/list";
    public static String CONTACT_DETAIL = URL_SERVER + "/apiv1/enterprise/contact/detail";
    public static String DELETE_CONTACT = URL_SERVER + "/apiv1/enterprise/contact/delete";
    public static String UPDATE_CONTACT = URL_SERVER + "/apiv1/enterprise/contact/update";

    public static String WEBRTC_ACCEPT_CALL = "https://" + CALL_CENTER_DOMAIN + "/callee/take-call";
    public static String WEBRTC_DENY_CALL = "https://" + CALL_CENTER_DOMAIN + "/callee/cancel-call";
    public static String WEBRTC_CALL_WALLET = "https://bw.beowulfchain.com/rpc";
}
