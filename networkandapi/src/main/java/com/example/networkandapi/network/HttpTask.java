package com.example.beowulfwebrtc.network;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;


import com.example.beowulfwebrtc.SDKApplication;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;

import javax.net.ssl.HttpsURLConnection;


/**
 * Created by amitaymolko on 2/16/16.
 */

public class HttpTask extends AsyncTask<HttpRequest, String, HttpResponse> {

    final static int REQUEST_TIMEOUT = 30000;

    private JsonAPI.JsonRequestListener jsonRequestListener;

    public HttpTask(JsonAPI.JsonRequestListener jsonRequestListener)
    {
        this.jsonRequestListener = jsonRequestListener;
    }

    @Override
    protected HttpResponse doInBackground(HttpRequest... params) {

        URL url;
        HttpURLConnection urlConnection = null;
        HttpResponse response = new HttpResponse();

        try {
            HttpRequest request = params[0];
            if (request == null || request.getURL() == null || request.getMethod() == null) {
                Log.e("HttpTask", "BAD HttpRequest");
                throw new Exception();
            }
            url = new URL(request.getURL());
            urlConnection = (HttpURLConnection) url.openConnection();
            Log.v("HttpTask", request.getMethodString());

            urlConnection.setRequestMethod(request.getMethodString());

            if (request.getHeaders() != null) {
                for (HashMap.Entry<String, String> pair : request.getHeaders().entrySet()) {
                    if (pair == null) {
                        Log.v("HttpTask", "Request Header Size: "+ request.getHeaders().size());
                        Log.v("HttpTask", "URL: "+ request.getURL());
                        Log.v("HttpTask", "why null is hereeeeeeeeeeeee");
                    }
                    urlConnection.setRequestProperty(pair.getKey(), pair.getValue());
                }
            }
            urlConnection.setConnectTimeout(REQUEST_TIMEOUT);
            urlConnection.setReadTimeout(REQUEST_TIMEOUT);

            //pbtdanh
            urlConnection.setInstanceFollowRedirects(false);

            if (request.getPostData() != null) {
                urlConnection.setDoOutput(true);
                    DataOutputStream wr = new DataOutputStream( urlConnection.getOutputStream());
                    byte[] postData       = request.getPostData().getBytes();
                    wr.write( postData );
            }

            urlConnection.connect();

            int responseCode = urlConnection.getResponseCode();
            String responseString;

            if ((responseCode == HttpsURLConnection.HTTP_OK) ||
                (responseCode == HttpsURLConnection.HTTP_MOVED_TEMP)){
                responseString = readStream(urlConnection.getInputStream());
            }
            else
                responseString = readStream(urlConnection.getErrorStream());

            /*pbtdanh remove Nov 8, 2018
            if (responseCode == HttpsURLConnection.HTTP_OK){
                responseString = readStream(urlConnection.getInputStream());
            }else if(responseCode == HttpsURLConnection.HTTP_MOVED_TEMP)
            {
                responseString = readHeader(urlConnection);
            }else{
                responseString = readStream(urlConnection.getErrorStream());
            }
            */
            Log.v("HttpTask", "URL: "+ request.getURL());
            Log.v("HttpTask", "Response code: "+ responseCode);
            Log.v("HttpTask", "GenericData: " + responseString);
            response = new HttpResponse(responseCode, responseString, request.getCallback());

        } catch (final Exception e) {

            if (true) {
                HttpRequest request = params[0];
                Log.v("HttpTask", "XXX URL: " + request.getURL());
                Log.v("HttpTask", "Network XXX error: " + e.getMessage());
            }
            e.printStackTrace();

            Handler mHandler = new Handler(Looper.getMainLooper());
            mHandler.post(() -> {
                Context context = SDKApplication.getAppContext();
              //  if (ApiUrl.bProduction)
//                    Util.showErrMessage(context, context.getString(R.string.str_error_connection));
             //   else
              //      Util.showErrMessage(context, "Network error: " + e.getMessage());
            });

        } finally {
            if(urlConnection != null)
                urlConnection.disconnect();
        }

        return response;
    }

    private String readHeader(HttpURLConnection urlConnection)
    {
        String cookie = urlConnection.getHeaderField("Set-Cookie");
        String location = urlConnection.getHeaderField("Location");

        JSONObject jsonObject = new JSONObject();
        try
        {
            jsonObject.put("Set-Cookie", cookie);
            jsonObject.put("Location", location);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject.toString();
    }

    private String readStream(InputStream in) {
        BufferedReader reader = null;
        StringBuffer response = new StringBuffer();
        try {
            reader = new BufferedReader(new InputStreamReader(in));
            String line = "";
            while ((line = reader.readLine()) != null) {
                response.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return response.toString();
    }

    @Override
    protected void onPostExecute(HttpResponse response) {
        super.onPostExecute(response);
        //pbtdanh response.getCallback().onResponse(response);

        //pbtdanh re modify
        HttpRequest.RequestCallback callBack = response.getCallback();
        if (callBack != null)
            callBack.onResponse(response);

        if (jsonRequestListener != null)
            jsonRequestListener.onEndRequest();
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        if (jsonRequestListener != null)
            jsonRequestListener.onBeginRequest();
    }
}