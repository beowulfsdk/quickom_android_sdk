package com.example.beowulfwebrtc.network;

import android.util.Log;


import org.json.JSONObject;

import java.util.HashMap;

import static android.os.AsyncTask.SERIAL_EXECUTOR;

/**
 * Created by amitaymolko on 2/19/16.
 */
public class JsonAPI {

    public interface JsonCallback {
        //pbtdanh remove void onResponse(int statusCode, JSONObject json);
        void onResponse(int statusCode, String str);
    }

    public interface JsonRequestListener
    {
        void onBeginRequest();
        void onEndRequest();
    }

    private static JsonAPI ourInstance = new JsonAPI();
    public static JsonAPI getInstance() {
        return ourInstance;
    }

    private JsonAPI() {

    }

//    private HashMap<String, String> headers = new HashMap<>();
//
//    private void userJsonHeader()
//    {
//        headers.clear();
//        headers.put("Content-Type","application/json");
//        //headers.put("Accept","application/json");
//    }
//
//
//    private void setupHeader(HashMap<String, String> requestHeader)
//    {
//        userJsonHeader();
//        if ((requestHeader != null) && !requestHeader.isEmpty())
//            headers.putAll(requestHeader);
//    }


    public void get(HashMap<String, String> requestHeader, String url, final JsonCallback callback, JsonRequestListener requestListener) {
        Log.d("GET Method: ", url);

        requestHeader.put("Content-Type","application/json");

        HttpRequest r = new HttpRequest(HttpRequest.Method.GET, url);
        r.setHeaders(requestHeader);
        r.setCallback(callbackForJsonCallback(callback));
        new HttpTask(requestListener).executeOnExecutor(SERIAL_EXECUTOR, r);
    }

    public void post(HashMap<String, String> requestHeader, String url, JSONObject postData, final JsonCallback callback, JsonRequestListener requestListener) {
        Log.d("POST Json Method: ", url);

        requestHeader.put("Content-Type","application/json");

        HttpRequest r = new HttpRequest(HttpRequest.Method.POST, url);
        r.setHeaders(requestHeader);
        r.setCallback(callbackForJsonCallback(callback));
        if (postData != null)
            r.setPostData(postData.toString());
        new HttpTask(requestListener).executeOnExecutor(SERIAL_EXECUTOR, r);
    }

    public void post(HashMap<String, String> requestHeader, String url, String postData, final JsonCallback callback, JsonRequestListener requestListener) {
        Log.d("POST String Method: ", url);

        requestHeader.put("Content-Type","application/json");

        HttpRequest r = new HttpRequest(HttpRequest.Method.POST, url);
        r.setHeaders(requestHeader);
        r.setCallback(callbackForJsonCallback(callback));
        r.setPostData(postData);
        new HttpTask(requestListener).executeOnExecutor(SERIAL_EXECUTOR, r);
    }

    public void delete(HashMap<String, String> requestHeader, String url, String postData, final JsonCallback callback, JsonRequestListener requestListener) {
        Log.d("POST String Method: ", url);

        requestHeader.put("Content-Type","application/json");

        HttpRequest r = new HttpRequest(HttpRequest.Method.DELETE, url);
        r.setHeaders(requestHeader);
        r.setCallback(callbackForJsonCallback(callback));
        r.setPostData(postData);
        new HttpTask(requestListener).executeOnExecutor(SERIAL_EXECUTOR, r);
    }


    /*
    public void post(String url, String checksum, String postData, final JsonCallback callback, JsonRequestListener requestListener) {
        userHtmlHeader();
        headers.put("Checksum", checksum);

        HttpRequest r = new HttpRequest(HttpRequest.Method.POST, url);
        r.setHeaders(headers);
        r.setCallback(callbackForJsonCallback(callback));
        r.setPostData(postData);
        new HttpTask(requestListener).execute(r);
    }

    public void post(String url, String cookie, JSONObject postData, final JsonCallback callback, JsonRequestListener requestListener) {
        userJsonHeader();
        headers.put("Cookie", cookie);

        HttpRequest r = new HttpRequest(HttpRequest.Method.POST, url);
        r.setHeaders(headers);
        r.setCallback(callbackForJsonCallback(callback));
        if (postData != null)
            r.setPostData(postData.toString());
        new HttpTask(requestListener).execute(r);
    }
*/


    /*
    public void get(String url, String cookie, final JsonCallback callback, JsonRequestListener requestListener) {
        userJsonHeader();
        headers.put("Cookie", cookie);

        HttpRequest r = new HttpRequest(HttpRequest.Method.GET, url);
        r.setHeaders(headers);
        r.setCallback(callbackForJsonCallback(callback));
        new HttpTask(requestListener).execute(r);
    }

    public void post(String url, String cookie, JSONObject postData, final JsonCallback callback, JsonRequestListener requestListener) {
        userJsonHeader();
        headers.put("Cookie", cookie);

        HttpRequest r = new HttpRequest(HttpRequest.Method.POST, url);
        r.setHeaders(headers);
        r.setCallback(callbackForJsonCallback(callback));
        if (postData != null)
            r.setPostData(postData.toString());
        new HttpTask(requestListener).execute(r);
    }

    public void post(String url, String cookie, String checksum, JSONObject postData, final JsonCallback callback, JsonRequestListener requestListener) {
        userJsonHeader();
        headers.put("Cookie", cookie);
        headers.put("Checksum", checksum);

        HttpRequest r = new HttpRequest(HttpRequest.Method.POST, url);
        r.setHeaders(headers);
        r.setCallback(callbackForJsonCallback(callback));
        if (postData != null)
            r.setPostData(postData.toString());
        new HttpTask(requestListener).execute(r);
    }

    public void post(String url, String cookie, String postData, final JsonCallback callback, JsonRequestListener requestListener) {
        userHtmlHeader();
        headers.put("Cookie", cookie);

        HttpRequest r = new HttpRequest(HttpRequest.Method.POST, url);
        r.setHeaders(headers);
        r.setCallback(callbackForJsonCallback(callback));
        r.setPostData(postData);
        new HttpTask(requestListener).execute(r);
    }
    */

    private HttpRequest.RequestCallback callbackForJsonCallback(final JsonCallback jsonCallback) {
        return new HttpRequest.RequestCallback() {
            @Override
            public void onResponse(HttpResponse r) {
                String strRespone = r.getResponse();
                jsonCallback.onResponse(r.getResponseCode(), strRespone);

                /*pbtdanh remove
                JSONObject json = null;
                try {
                    String strRespone = r.getResponse();
                    if (!strRespone.isEmpty())
                        json =  new JSONObject(strRespone);

                }
                catch (JSONException e) {
                    e.printStackTrace();
                }
                jsonCallback.onResponse(r.getResponseCode(), json);
                */
            }
        };
    }
}