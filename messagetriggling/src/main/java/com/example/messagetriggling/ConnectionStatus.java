package com.example.messagetriggling;

public interface ConnectionStatus {
    void onConnected();

    void onDisconnected();

    void onAuthenticated();

    void onConnectionClosedOnError();
}
