package com.example.messagetriggling.XMPP;

import android.os.Looper;

import com.example.messagetriggling.ConnectionStatus;

import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.ConnectionListener;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.chat2.ChatManager;
import org.jivesoftware.smack.chat2.IncomingChatMessageListener;
import org.jivesoftware.smack.chat2.OutgoingChatMessageListener;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration;
import org.jxmpp.stringprep.XmppStringprepException;

import java.io.IOException;

public class BeowulfXmpp {

    static XMPPConnectionState state = XMPPConnectionState.NEW;

    static ConnectionStatus status = null;


    final String TAG = "XMPP_CHAT";
    private static BeowulfXmpp instance = null;
    private static Object mutex = new Object();
    private static XMPPTCPConnection mConnection = null;
    private static XMPPTCPConnectionConfiguration configuration = null;

    private static IncomingChatMessageListener mIncommingMessageListener = null;
    private static OutgoingChatMessageListener mOutgoingMessageListener = null;

    enum XMPPConnectionState {
        NEW,
        CONNECTED,
        CLOSED,
        ERROR,
        AUTHENTICATED
    }


    private static ConnectionListener mConnectionListener = new ConnectionListener() {
        @Override
        public void connected(XMPPConnection connection) {

            if (null != status)
                status.onConnected();
        }

        @Override
        public void authenticated(XMPPConnection connection, boolean resumed) {
            state = XMPPConnectionState.AUTHENTICATED;
            if (true != resumed) {


                if (null != status)
                    status.onAuthenticated();
            }
        }

        @Override
        public void connectionClosed() {
            state = XMPPConnectionState.CLOSED;
            if (null != status)
                status.onDisconnected();
        }

        @Override
        public void connectionClosedOnError(Exception e) {
            state = XMPPConnectionState.ERROR;
            if (null != status)
                status.onConnectionClosedOnError();
        }
    };

    public static BeowulfXmpp getInstance() {
        BeowulfXmpp result = instance;
        if (null == result) {
            synchronized (mutex) {
                if (null == result)
                    instance = result = new BeowulfXmpp();
            }
        }
        return result;
    }

    BeowulfXmpp() {
        try {
            configuration = XMPPTCPConnectionConfiguration.builder()
                    .setXmppDomain(BeowulfXMPPConfig.getDomain())
                    .setHost(BeowulfXMPPConfig.getHost())
                    .setPort(BeowulfXMPPConfig.getPort())
                    .allowEmptyOrNullUsernames()
                    //Was facing this issue
                    //https://discourse.igniterealtime.org/t/connection-with-ssl-fails-with-java-security-keystoreexception-jks-not-found/62566
                    .setKeystoreType(null) //This line seems to get rid of the problem
                    .setSecurityMode(ConnectionConfiguration.SecurityMode.required)
                    .setCompressionEnabled(true).build();


            mConnection = new XMPPTCPConnection(configuration);
            mConnection.removeConnectionListener(mConnectionListener);
            mConnection.addConnectionListener(mConnectionListener);
            ChatManager.getInstanceFor(mConnection).setXhmtlImEnabled(true);

            ChatManager.getInstanceFor(mConnection).addIncomingListener(mIncommingMessageListener);

            // ChatManager.getInstanceFor(mConnection).removeOutgoingListener(mOutgoingMessageListener);
            ChatManager.getInstanceFor(mConnection).addOutgoingListener(mOutgoingMessageListener);


        } catch (XmppStringprepException e) {
            e.printStackTrace();
        }
    }

    public void Login() {

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Looper.prepare();
                    mConnection.connect();
                    state = XMPPConnectionState.CONNECTED;
                    String username = BeowulfXMPPConfig.getUsername();
                    String password = BeowulfXMPPConfig.getPassword();

                    mConnection.login(username, password);


                } catch (XMPPException e) {
                    e.printStackTrace();
                } catch (SmackException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Looper.loop();
            }
        }, "xmppthread").start();


    }

    private static class ReconnectionChat {
        static ReconnectionChat instance = null;
        static org.jivesoftware.smack.ReconnectionManager reconnectionManager = null;

        public static ReconnectionChat getInstance() {
            if (null == instance) {
                instance = new ReconnectionChat();
            }
            return instance;
        }

        ReconnectionChat() {
            reconnectionManager = org.jivesoftware.smack.ReconnectionManager.getInstanceFor(mConnection);

        }

        public static void Init() {
            reconnectionManager.setEnabledPerDefault(true);
            reconnectionManager.enableAutomaticReconnection();
            reconnectionManager.setFixedDelay(5);
        }
    }


}
